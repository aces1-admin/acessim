# README  {#README}

Welcome to the code repository for the Attitude Control for prEcise Sciences
(ACES) project! This is a fork of the Basilisk simulation environment
repository. All of the custom ACES code is contained in the `aces/` folder.


### Linux Setup Steps ###
Make sure the following packages are installed and `apt install` them if
necessary:
```
$ sudo apt update
$ sudo apt install git build-essential python3 python3-setuptools python3-dev python3-tk python3-pip python3.10-venv swig
```

Set the working directory to the ACES repository folder:
```
$ cd path/to/acessim
```

Set up Python virtual environment:
```
$ python3 -m venv .venv
$ source .venv/bin/activate
```

Install required Python packages:
```
$ pip install -r requirements.txt
```


### Running Sims ###

The virtual environment must be active while building and running sims. To
activate it, use the command:
```
$ source .venv/bin/activate
```

If later you would like to deactivate the Python virtual environment, use the
command:
```
$ deactivate
```

Build Basilisk:
```
(.venv) $ python conanfile.py
```

Build Basilisk with ACES code:
```
(.venv) $ python conanfile.py --pathToExternalModules ./aces
```

Run a Basilisk sim:
```
(.venv) $ python path/to/scenario.py
```

If you would like to manually clean Basilisk, you can run:
```
(.venv) $ rm -rf dist3 docs/build docs/source/_images/Scnarios
```


### Editing ACES Custom Code ###
To include a specific custom module within a python simulation script include
the following line:
```
from Basilisk.ExternalModules import <moduleName>
```


To create a custom module, edit `makeACESModule.py` within the current folder
to contain the desired information and run the script from the command line.
This will generate the folder and file structures of the new module within the
sub-folder `ExternalModules/`.


### Basilisk ###

* [Summary of Basilisk](http://hanspeterschaub.info/basilisk/index.html)
* [Versions](http://hanspeterschaub.info/basilisk/Support/bskReleaseNotes.html)

### How do I get set up? ###
The following links contain installation instructions for the supported platforms:

- [Setup a macOS Development Environment](http://hanspeterschaub.info/basilisk/Install/installOnMacOS.html)

- [Setup a Linux Development Environment](http://hanspeterschaub.info/basilisk/Install/installOnLinux.html)

- [Setup a Windows Development Environment](http://hanspeterschaub.info/basilisk/Install/installOnWindows.html)


### Basilisk Development guidelines ###

* [Coding Guidelines](http://hanspeterschaub.info/basilisk/Support/Developer/CodingGuidlines.html)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Getting Started
To get started with Basilisk (BSK), several tutorial python files are provided in the installed package.  Within this web page documentation site, they are listed and discussed in the <a href="modules.html">Manual</a> tab.  The documentation lists the scenarios in an order that facilitates learning basic BSK features. In the source code they are stored under `src\examples\`. A good start would be to run `scenarioBasicOrbit.py`.

To play with the tutorials, it is suggested the user makes a copy of these tutorial files, and use the copies in order to learn, test and experiment. To copy them, first find the location of the Basilisk installation. After installing, you can find the installed location of Basilisk by opening a python interpreter and running:

```
import Basilisk
basiliskPath = Basilisk.__path__[0]
print(basiliskPath)
```

Now copy the folder `{basiliskPath}/src/examples` into a new folder, and change to that directory.

To run the default scenario 1 of scenarioBasicOrbit, in the directory of the copied tutorials, call the python script: `python3 scenarioBasicOrbit.py`


Now, when you want to use a tutorial, navigate inside that folder, and edit and execute the *copied* integrated tests.

<!--Any new BSK module development should not occur within the BSK folder as this will be updated rapidly.  Rather, new FSW algorithm or simulation code modules should be created in a custom folder outside of the BSK directory.  A sample folder is provided named `BasiliskCustom` which contains sample FSW and Simulation modules.-->

We are working on the ability to develop custom BSK modules outside of the Basilisk folder.  This will enable developers to safely write their own BSK modules and still be able to do a drop in replacement of future BSK releases.

To use the standalone 3D Visualization, download the [Vizard](http://hanspeterschaub.info/basilisk/Vizard/Vizard.html).  This is in development, but does provide a 3D view of many of the simulation states.


### Who do I talk to? ###

There is a [Basilisk Google Forum](https://groups.google.com/forum/embed/?place=forum%2Fbasilisk-forum) where you can post questions and answers.
