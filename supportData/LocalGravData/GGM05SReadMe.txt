Grace Gravity Model v5 (GGM05S)

Description:

GGM05S.txt is a reformatted version of the original GGM05S.GEO file downloadable from 
http://www2.csr.utexas.edu/grace/gravity/. This file contains spherical harmonic 
coefficients for Earth's gravity field. Note: The documentation for GGM05 recommends 
not using a spherical harmonic degree over 150 without data smoothing.

Format:

-First Row: Earth's equitorial radius [m], Earth's gravitational parameter [m^3/s^2],
            Earth's rotation rate [rad/s], max degree.
-Remaining Rows: degree, order, C, S, C-sigma, S-sigma

Citation:

J. Ries, S. Bettadpur, R. Eanes, Z. Kang, U. Ko, C. McCullough, P. Nagel, N. Pie, 
S. Poole, T. Richter, H. Save, and B. Tapley (2016) Development and Evaluation of 
the Global Gravity Model GGM05, CSR-16-02, Center for Space Research, The University 
of Texas at Austin.
