/*
 ACES, University of Colorado Boulder, 2022

 ModeController output message

 Author: Brennan Gray
 Created: 19 Sep 2022
 Last Edit: 19 Sep 2022

*/

#ifndef modeControllerFswMsg_H
#define modeControllerFswMsg_H

// Mode and settled output (commands are output separately using the existing output messages)
typedef struct {
    int modeId;       //!< Mode ID for the current mode
    int isSettled;    //!< Flag for whether the spacecraft is settled on the new attitude, high when settled

    int pointingControllerEnabled;  //!< Whether pointing control is enabled currently. High when enabled.
    int mtbFeedforwardEnabled;      //!< Whether MTB feedforward is enabled currently. High when enabled.

    int shouldResetPointing;        //!< Whether the pointing controller should reset this cycle. High to reset.
}ModeControllerMsgPayload;

#endif
