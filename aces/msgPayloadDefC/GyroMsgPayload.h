#ifndef GYRO_MESSAGE_H
#define GYRO_MESSAGE_H
#include <stdint.h>


/*! @brief Structure used to define the output of the GyroSensor module.*/
typedef struct {
    uint64_t timeTag;               //!< [ns] Time tag for measurement
    double omega_GN_G[3];           //!< [rad/s] Angular velcoity of Gyro with respect to Inertial
    int valid;                      //!< [-] Flag that is high when measurement is valid
}GyroMsgPayload;


#endif

