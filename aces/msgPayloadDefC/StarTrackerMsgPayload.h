#ifndef STAR_TRACKER_MESSAGE_H
#define STAR_TRACKER_MESSAGE_H
#include <stdint.h>


/*! @brief Structure used to define the output of the StarTracker module.*/
typedef struct {
    uint64_t timeTag;               //!< [ns] Time tag for measurement
    double q_CN[4];                 //!< [-] Quaternion orientation of Case relative to Inertial
    int valid;                      //!< [-] Flag that is high when measurement is valid
}StarTrackerMsgPayload;


#endif

