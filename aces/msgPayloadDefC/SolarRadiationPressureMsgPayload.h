/*
 ACES, University of Colorado Boulder, 2022

 solarRadiationPressure Module

 Author: Chase Heinen
 Created: 30 Jan, 2022
 Last Edit: 18 Feb, 2022

*/

#ifndef solarRadiationPressureSimMsg_H
#define solarRadiationPressureSimMsg_H

// solar radiation pressure force and torque definitions
typedef struct {
    double solarRadiationPressureForce_B[3];     //!< [N] force vector from srp in body frame
    double solarRadiationPressureTorque_B[3];    //!< [N-m] torque vector from srp in body frame
}SolarRadiationPressureMsgPayload;

#endif
