/*
 ACES, University of Colorado Boulder, 2022

 modeController Module

 Author: Brennan Gray
 Created: 19 Sep 2022
 Last Edit: 27 Oct 2022

 */


#ifndef MODECONTROLLER_H
#define MODECONTROLLER_H

#include <stdint.h>
#include "cMsgCInterface/AttGuidMsg_C.h"
#include "cMsgCInterface/AttRefMsg_C.h"
#include "cMsgCInterface/CmdTorqueBodyMsg_C.h"
#include "cMsgCInterface/DipoleRequestBodyMsg_C.h"
#include "cMsgCInterface/ModeControllerMsg_C.h"
#include "architecture/utilities/bskLogging.h"

// The max number of commands which can be stored
#define MAX_NUM_MODE_COMMANDS 10

/*! @brief This module selects between multiple attitude references and dipole command sets (modes).
 */
typedef struct {

    /*
     * Mode IDs are defined as follows:
     *  0 = Off mode (no command outputs)
     *  1 = Detumble mode (only dipole outputs)
     *  2 = Safe mode
     *  3 = Sun-pointing mode
     *  4 = Commanded mode 1 (nominally comms mode)
     *  5 = Commanded mode 2 (nominally science mode)
     */

    /*
     * Declare inputs
     */

    // Off mode
    AttGuidMsg_C offAttGuidInMsg;                               //!< Input message of off attitude errors
    AttRefMsg_C offAttRefInMsg;                                 //!< Input message of off attitude reference

    // Detumble mode
    AttGuidMsg_C detumbleAttGuidInMsg;                          //!< Input message of detumble attitude errors
    AttRefMsg_C detumbleAttRefInMsg;                            //!< Input message of detumble attitude reference
    DipoleRequestBodyMsg_C detumbleDipoleRequestBodyInMsg;      //!< Input message of requested detumble dipole command

    // Safe mode
    AttGuidMsg_C safeAttGuidInMsg;                               //!< Input message of requested safe mode attitude errors
    AttRefMsg_C safeAttRefInMsg;                                 //!< Input message of safe mode attitude reference
    DipoleRequestBodyMsg_C safeDipoleRequestBodyInMsg;           //!< Input message of requested safe mode dipole command

    // Sun-pointing mode
    AttGuidMsg_C sunPointingAttGuidInMsg;                        //!< Input message of requested sun-pointing mode attitude errors
    AttRefMsg_C sunPointingAttRefInMsg;                          //!< Input message of sun-pointing mode attitude reference
    DipoleRequestBodyMsg_C sunPointingDipoleRequestBodyInMsg;    //!< Input message of requested sun-pointing mode dipole command

    // Commanded mode 1
    AttGuidMsg_C firstCommandAttGuidInMsg;                       //!< Input message of requested first commanded mode attitude errors
    AttRefMsg_C firstCommandAttRefInMsg;                       //!< Input message of first commanded mode attitude reference
    DipoleRequestBodyMsg_C firstCommandDipoleRequestBodyInMsg;   //!< Input message of requested first commanded mode dipole command

    // Commanded mode 2
    AttGuidMsg_C secondCommandAttGuidInMsg;                      //!< Input message of requested second commanded mode attitude errors
    AttRefMsg_C secondCommandAttRefInMsg;                      //!< Input message of second commanded mode attitude reference
    DipoleRequestBodyMsg_C secondCommandDipoleRequestBodyInMsg;  //!< Input message of requested second commanded mode dipole command

    /*
     * Declare outputs
     */
    AttGuidMsg_C attErrorOutMsg;                        //!< Attitude guidance of the current mode
    AttRefMsg_C attRefOutMsg;                           //!< Attitude reference of the current mode
    DipoleRequestBodyMsg_C dipoleRequestBodyOutMsg;     //!< Dipole request of the current mode
    ModeControllerMsg_C modeOutMsg;                     //!< Mode controller output with current mode ID and settled flag

    BSKLogger* bskLogger;                               //!< BSK Logging

    /* 
     * Declare configuration data
     */
    int detumbleTimeout;                                //!< [s] Timeout at which to consider detumble failed and transition to safe mode
    double detumbleExitRateError;                       //!< [rad/s] Rate error at which to consider detumble successful and transition to sun-pointing

    // Settling configuration parameters. Think of the settled zone as a box in a scalar MRP pointing error - scalar
    // rate error phase space. These variables define the dimensions of the settled box, and the hysteresis check as
    // remaining within that box for a set number of control cycles.
    double settledPointingError;                        //!< [] Maximum MRP pointing error which is considered settled
    double settledRateError;                            //!< [rad/s] Maximum rate error which is considered settled
    int settledCycles;                                  //!< [] Number of control cycles required to be considered settled

    // Command storage
    int commandedModes[MAX_NUM_MODE_COMMANDS];          //!< Array of commanded modes for timed commands
    int commandedTimes[MAX_NUM_MODE_COMMANDS];          //!< [s] Array of times for commanded modes (either absolute or relative to detumble time)
    int commandedTimeTypes[MAX_NUM_MODE_COMMANDS];      //!< Array of time type flags (0 = absolute time from controller start, 1 = relative to detumble time)
    int totalNumCommands;                               //!< The total number of stored commands

    /*
     * Declare internal variables
     */
    uint64_t startTime;                                 //!< [ns] Mode controller start time
    uint64_t detumbleStartTime;                         //!< [ns] Detumble mode start time
    uint64_t detumbledTime;                             //!< [ns] Time at which the spacecraft was considered detumbled
    int hasDetumbled;                                   //!< Flag of whether the spacecraft has detumbled (high = true)

    int nextCommand;                                    //!< Index of the next command to execute. Note that commands are only checked in order.

    int currentSettledCycles;                           //!< The current number of cycles spent in the settled box

    int currentMode;                                    //!< Current mode ID
    int settled;                                        //!< Settled flag (high = settled)

}modeControllerConfig;


/* 
 * Ensure module will build if compiled as C++
 */
#ifdef __cplusplus
extern "C" {
#endif
    void SelfInit_modeController(modeControllerConfig *configData, int64_t moduleID);
    void Update_modeController(modeControllerConfig *configData, uint64_t callTime, int64_t moduleID);
    void Reset_modeController(modeControllerConfig *configData, uint64_t callTime, int64_t moduleID);

#ifdef __cplusplus
}
#endif

#endif
