/*

 ACES, University of Colorado Boulder, 2022

*/

%module modeController
%{
    #include "modeController.h"
%}

%pythoncode %{
    from Basilisk.architecture.swig_common_model import *
%}
%include "swig_conly_data.i"
%constant void Update_modeController(void*, uint64_t, uint64_t);
%ignore Update_modeController;
%constant void SelfInit_modeController(void*, uint64_t);
%ignore SelfInit_modeController;
%constant void Reset_modeController(void*, uint64_t, uint64_t);
%ignore Reset_modeController;

%include "stdint.i"
%include "modeController.h"

%include "architecture/msgPayloadDefC/AttGuidMsgPayload.h"
struct AttGuidMsg_C;
%include "architecture/msgPayloadDefC/AttRefMsgPayload.h"
struct AttRefMsg_C;
%include "architecture/msgPayloadDefC/CmdTorqueBodyMsgPayload.h"
struct CmdTorqueBodyMsg_C;
%include "architecture/msgPayloadDefC/DipoleRequestBodyMsgPayload.h"
struct DipoleRequestBodyMsg_C;
%include "msgPayloadDefC/ModeControllerMsgPayload.h"
struct ModeControllerMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

