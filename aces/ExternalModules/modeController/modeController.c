/* 
 ACES, University of Colorado Boulder, 2022
 
 modeController Module
  
 Author: Brennan Gray
 Created: 19 Sep 2022
 Last Edit: 27 Oct 2022
 
 */


#include "ExternalModules/modeController/modeController.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/macroDefinitions.h"
#include "string.h"
#include <math.h>

/*! This method sets up the module output messages
 @return    void
 @param     configData: The configuration data associated with this module
 @param     moduleID: The module identifier
 */
void SelfInit_modeController(modeControllerConfig *configData, int64_t moduleID)
{
    AttGuidMsg_C_init(&configData->attErrorOutMsg);
    AttRefMsg_C_init(&configData->attRefOutMsg);
    DipoleRequestBodyMsg_C_init(&configData->dipoleRequestBodyOutMsg);
    ModeControllerMsg_C_init(&configData->modeOutMsg);
}


/*! This method performs a complete reset of the module. Local module variables that retain
    time varying states between function calls are reset to their default values.
 @return    void
 @param     configData: The configuration data associated with the module
 @param     callTime: The clock time at which the function was called [ns]
 @param     moduleID: The module identifier
*/
void Reset_modeController(modeControllerConfig *configData, uint64_t callTime, int64_t moduleID)
{

    /*
     * Check input message connections
     */

    // Off att guid msg
    if(!AttGuidMsg_C_isLinked(&configData->offAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.offAttGuidInMsg was not connected.");
    }

    // Off att ref msg
    if(!AttRefMsg_C_isLinked(&configData->offAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.offAttRefInMsg was not connected.");
    }

    // Detumble att guid msg
    if(!AttGuidMsg_C_isLinked(&configData->detumbleAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.detumbleAttGuidInMsg was not connected.");
    }

    // Detumble att ref msg
    if(!AttRefMsg_C_isLinked(&configData->detumbleAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.detumbleAttRefInMsg was not connected.");
    }

    // Detumble dipole msg
    if(!DipoleRequestBodyMsg_C_isLinked(&configData->detumbleDipoleRequestBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.detumbleDipoleRequestBodyInMsg was not connected.");
    }

    // Safe guidance msg
    if(!AttGuidMsg_C_isLinked(&configData->safeAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.safeAttGuidInMsg was not connected.");
    }

    // Safe att ref msg
    if(!AttRefMsg_C_isLinked(&configData->safeAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.safeAttRefInMsg was not connected.");
    }

    // Safe dipole msg
    if(!DipoleRequestBodyMsg_C_isLinked(&configData->safeDipoleRequestBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.safeDipoleRequestBodyInMsg was not connected.");
    }

    // Sun-pointing guidance msg
    if(!AttGuidMsg_C_isLinked(&configData->sunPointingAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.sunPointingAttGuidInMsg was not connected.");
    }

    // Sun-pointing att ref msg
    if(!AttRefMsg_C_isLinked(&configData->sunPointingAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.sunPointingAttRefInMsg was not connected.");
    }

    // Sun-pointing dipole msg
    if(!DipoleRequestBodyMsg_C_isLinked(&configData->sunPointingDipoleRequestBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.sunPointingDipoleRequestBodyInMsg was not connected.");
    }

    // Commanded mode 1 guidance msg
    if(!AttGuidMsg_C_isLinked(&configData->firstCommandAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_WARNING, "Warning: modeController.firstCommandAttGuidInMsg was not connected. First commanded mode will not function.");
    }

    // Commanded mode 1 att ref msg
    if(!AttRefMsg_C_isLinked(&configData->firstCommandAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.firstCommandAttRefInMsg was not connected.");
    }

    // Commanded mode 1 dipole msg
    if(!DipoleRequestBodyMsg_C_isLinked(&configData->firstCommandDipoleRequestBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_WARNING, "Warning: modeController.firstCommandDipoleRequestBodyInMsg was not connected. First commanded mode will not function.");
    }

    // Commanded mode 2 guidance msg
    if(!AttGuidMsg_C_isLinked(&configData->secondCommandAttGuidInMsg)) {
        _bskLog(configData->bskLogger, BSK_WARNING, "Warning: modeController.secondCommandAttGuidInMsg was not connected. Second commanded mode will not function.");
    }

    // Commanded mode 2 att ref msg
    if(!AttRefMsg_C_isLinked(&configData->secondCommandAttRefInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.secondCommandAttRefInMsg was not connected.");
    }

    // Commanded mode 2 dipole msg
    if(!DipoleRequestBodyMsg_C_isLinked(&configData->secondCommandDipoleRequestBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_WARNING, "Warning: modeController.secondCommandDipoleRequestBodyInMsg was not connected. Second commanded mode will not function.");
    }

    /*
     * Check configuration data
     */

    // Require that the total number of commands is less than or equal to the max
    if (configData->totalNumCommands > MAX_NUM_MODE_COMMANDS) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: Total number of mode commands is greater than maximum allowed.");
    }

    // Require a positive detumble exit rate error
    if (configData->detumbleExitRateError <= 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.detumbleExitRateError is negative or zero.");
    }

    // Require a positive max settled pointing error
    if (configData->settledPointingError <= 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.settledPointingError is negative or zero.");
    }

    // Require a positive max settled rate error
    if (configData->settledRateError <= 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: modeController.settledRateError is negative or zero.");
    }

    /*
     * Initialize internal variables
     */
    configData->startTime = callTime;
    configData->detumbleStartTime = 0;
    configData->detumbledTime = 0;
    configData->hasDetumbled = 0;
    configData->nextCommand = 0;
    configData->currentMode = 0;
    configData->settled = 0;
}


/*! This method determines the current mode, settling status, and when to transition to the next commanded mode.
 @return    void
 @param     configData: The configuration data associated with the module
 @param     callTime: The clock time at which the function was called [ns]
 @param     moduleID: The module identifier
*/
void Update_modeController(modeControllerConfig *configData, uint64_t callTime, int64_t moduleID)
{

    /*
     * Prepare module I/O
     */
    AttGuidMsgPayload attErrorInMsgBuffer;                                                  //!< local copy of message buffer
    AttRefMsgPayload attRefInMsgBuffer;                                                     //!< local copy of message buffer
    DipoleRequestBodyMsgPayload dipoleRequestBodyOutMsgBuffer;                              //!< local copy of message buffer
    ModeControllerMsgPayload modeOutMsgBuffer;                                              //!< local copy of message buffer
    dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_zeroMsgPayload();                //!< zero output message buffers
    modeOutMsgBuffer = ModeControllerMsg_C_zeroMsgPayload();                                //!< zero output message buffers

    uint64_t currentTime = callTime - configData->startTime; //!< [ns] Current time local variable

    /*
     * Read the current attitude guidance message
     */
    switch(configData->currentMode){

        case 0: // Off
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->offAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->offAttRefInMsg);
            break;

        case 1: // Detumble
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->detumbleAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->detumbleAttRefInMsg);
            break;

        case 2: // Safe
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->safeAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->safeAttRefInMsg);
            break;

        case 3: // Sun pointing
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->sunPointingAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->sunPointingAttRefInMsg);
            break;

        case 4: // Commanded mode 1
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->firstCommandAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->firstCommandAttRefInMsg);
            break;

        case 5: // Commanded mode 2
            attErrorInMsgBuffer = AttGuidMsg_C_read(&configData->secondCommandAttGuidInMsg);
            attRefInMsgBuffer = AttRefMsg_C_read(&configData->secondCommandAttRefInMsg);
            break;

    }

    /*
     *  Check settling status
     */

    // Don't consider settling for off or detumble modes
    if(configData->settled == 0 && configData->currentMode > 1 &&
       v3Norm(attErrorInMsgBuffer.sigma_BR) <= configData->settledPointingError &&
       v3Norm(attErrorInMsgBuffer.omega_BR_B) <= configData->settledRateError) {

        // Increment the settled counter
        configData->currentSettledCycles++;

    }else{

        // Reset the settled counter
        configData->currentSettledCycles = 0;

    }

    // Transition settled flag high if we've reached the requisite number of cycles. There's no way for the settled flag
    // to return to low except by mode transition.
    if(configData->settled == 0 && configData->currentSettledCycles >= configData->settledCycles) {

        // Set settled flag high
        configData->settled = 1;

    }

    /*
     * Check detumble status
     */

    int pointingReset; // Whether the pointing controller should reset this cycle

    if(configData->currentMode == 1) {
        uint64_t timeoutNs = (uint64_t) configData->detumbleTimeout;
        timeoutNs *= SEC2NANO;

        // Check if detumble successful
        if (v3Norm(attErrorInMsgBuffer.omega_BR_B) <= configData->detumbleExitRateError) {

            // Set detumble time, flag detumble as successful
            configData->detumbledTime = currentTime;
            configData->hasDetumbled = 1;

            // Transition into sun-pointing mode automatically
            configData->currentMode = 3;

            // Set settled flag low
            configData->settled = 0;

            // Should reset this cycle
            pointingReset = 1;

        // Check if detumble has timed out
        } else if (currentTime > timeoutNs + configData->detumbleStartTime) {

            // Transition into safe mode automatically
            configData->currentMode = 2;

            // Set settled flag low
            configData->settled = 0;

            // Should reset this cycle
            pointingReset = 1;

        }
    }

    /*
     *  Check if next command has happened
     */

    // Not running past max commands, not overrunning total number of commands given
    if(configData->nextCommand < MAX_NUM_MODE_COMMANDS && configData->nextCommand < configData->totalNumCommands) {

        // Pull out next transition time and convert to nanoseconds
        uint64_t nextTransition = (uint64_t) configData->commandedTimes[configData->nextCommand];
        nextTransition *= SEC2NANO;

        // Next command is detumble time relative
        if (configData->commandedTimeTypes[configData->nextCommand] == 1 &&
            configData->hasDetumbled == 1 &&
            currentTime - configData->detumbledTime >= nextTransition) {

            // Transition to next mode
            configData->currentMode = configData->commandedModes[configData->nextCommand];

            // Increment next command
            configData->nextCommand++;

            // Set settled flag low
            configData->settled = 0;

            // Should reset this cycle
            pointingReset = 1;

        // Next command is absolute time relative
        } else if (configData->commandedTimeTypes[configData->nextCommand] == 0 &&
                   currentTime >= nextTransition) {

            // Transition to next mode
            configData->currentMode = configData->commandedModes[configData->nextCommand];

            // Increment next command
            configData->nextCommand++;

            // Set settled flag low
            configData->settled = 0;

            // Set detumble start time if we transitioned to detumble mode
            if(configData->currentMode == 1) {
                configData->detumbleStartTime = currentTime;
            }

            // Should reset this cycle
            pointingReset = 1;
        }
    }

    /*
     * Write to output messages
     */

    // Buffer integers for pointing and MTB feedforward enables.
    int pointingEnabled;
    int feedforwardEnabled;

    // Set dipole command
    switch(configData->currentMode) {

        case 0: // Off mode, no commands (leave dipole zeroed)
            pointingEnabled = 0;
            feedforwardEnabled = 0;
            break;

        case 1: // Detumble mode
            dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_read(&configData->detumbleDipoleRequestBodyInMsg);
            pointingEnabled = 0;
            feedforwardEnabled = 0;
            break;

        case 2: // Safe mode
            dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_read(&configData->safeDipoleRequestBodyInMsg);
            pointingEnabled = 1;
            feedforwardEnabled = 1;
            break;

        case 3: // Sun-pointing mode
            dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_read(&configData->sunPointingDipoleRequestBodyInMsg);
            pointingEnabled = 1;
            feedforwardEnabled = 1;
            break;

        case 4: // Commanded mode 1
            dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_read(&configData->firstCommandDipoleRequestBodyInMsg);
            pointingEnabled = 1;
            feedforwardEnabled = 1;
            break;

        case 5: // Commanded mode 2
            dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_read(&configData->secondCommandDipoleRequestBodyInMsg);
            pointingEnabled = 1;
            feedforwardEnabled = 1;
            break;

    }

    // Create mode controller out message
    modeOutMsgBuffer.modeId = configData->currentMode;
    modeOutMsgBuffer.isSettled = configData->settled;
    modeOutMsgBuffer.pointingControllerEnabled = pointingEnabled;
    modeOutMsgBuffer.mtbFeedforwardEnabled = feedforwardEnabled;
    modeOutMsgBuffer.shouldResetPointing = pointingReset;

    // Write all messages
    AttGuidMsg_C_write(&attErrorInMsgBuffer, &configData->attErrorOutMsg, moduleID, callTime);
    AttRefMsg_C_write(&attRefInMsgBuffer, &configData->attRefOutMsg, moduleID, callTime);
    DipoleRequestBodyMsg_C_write(&dipoleRequestBodyOutMsgBuffer, &configData->dipoleRequestBodyOutMsg, moduleID, callTime);
    ModeControllerMsg_C_write(&modeOutMsgBuffer, &configData->modeOutMsg, moduleID, callTime);

}
