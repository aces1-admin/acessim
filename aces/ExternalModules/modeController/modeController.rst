Executive Summary
-----------------
This module selects between multiple attitude guidance, attitude reference, and dipole command sets (modes).

Message Connection Descriptions
-------------------------------
The following table lists all the module input and output messages.  
The module msg connection is set by the user from python.  
The msg type contains a link to the message structure definition, while the description 
provides information on what this message is used for.

.. list-table:: Module I/O Messages
    :widths: 25 25 50
    :header-rows: 1

    * - Msg Variable Name
      - Msg Type
      - Description
    * - offAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the off mode.
    * - offAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the off mode.
    * - detumbleAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the detumble mode.
    * - detumbleAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the detumble mode.
    * - detumbleDipoleRequestBodyInMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Input message of the dipole command for the detumble mode.
    * - safeAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the safe mode.
    * - safeAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the safe mode.
    * - safeDipoleRequestBodyInMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Input message of the dipole command for the safe mode.
    * - sunPointingAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the sun-pointing mode.
    * - sunPointingAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the sun-pointing mode.
    * - sunPointingDipoleRequestBodyInMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Input message of the dipole command for the sun-pointing mode.
    * - firstCommandAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the first command (nominally comms) mode.
    * - firstCommandAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the first command (nominally comms) mode.
    * - firstCommandDipoleRequestBodyInMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Input message of the dipole command for the first command (nominally comms) mode.
    * - secondCommandAttGuidInMsg
      - :ref:`AttGuidMsgPayload`
      - Input message of attitude guidance for the second command (nominally science) mode.
    * - secondCommandAttRefInMsg
      - :ref:`AttRefMsgPayload`
      - Input message of attitude reference for the second command (nominally science) mode.
    * - secondCommandDipoleRequestBodyInMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Input message of the dipole command for the second command (nominally science) mode.
    * - attErrorOutMsg
      - :ref:`AttGuidMsgPayload`
      - Output message of attitude guidance for the current mode.
    * - attRefOutMsg
      - :ref:`AttRefMsgPayload`
      - Output message of attitude reference for the current mode.
    * - dipoleRequestBodyOutMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - Output message of the dipole command for the current mode.
    * - modeOutMsg
      - :ref:`ModeControllerMsgPayload`
      - Output message of the current mode (mode ID, settled flag, enable feedback and feedforward flags).