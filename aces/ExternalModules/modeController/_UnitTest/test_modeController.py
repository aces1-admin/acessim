#
#   Mode Controller Unit Test Script
#   Module Name:        test_modeController
#   Author:             Brennan Gray
#   Creation Date:      03 November 2022
#
# import packages as needed e.g. 'numpy', 'ctypes, 'math' etc.
import pytest
import numpy as np

# Import all the modules that are going to be called in this simulation
from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport                  # General support file with common unit test functions
from Basilisk.ExternalModules import modeController             # Import the module that is to be tested
from Basilisk.utilities import macros
from Basilisk.architecture import messaging                     # Import the message definitions
from Basilisk.architecture import bskLogging

# Comparison accuracy
accuracy = 1E-12

# Comparison default vector
defaultVector = [0.1, 0.2, 0.3]

# Comparison zero vector
zeroVector = [0., 0., 0.]

# Total number of modes
totalNumModes = 6

def subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig):
    moduleConfig.offAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.offAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.detumbleAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.detumbleAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.detumbleDipoleRequestBodyInMsg.subscribeTo(dipoleReqZeroMsg)
    moduleConfig.safeAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.safeAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.safeDipoleRequestBodyInMsg.subscribeTo(dipoleReqZeroMsg)
    moduleConfig.sunPointingAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.sunPointingAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.sunPointingDipoleRequestBodyInMsg.subscribeTo(dipoleReqZeroMsg)
    moduleConfig.firstCommandAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.firstCommandAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.firstCommandDipoleRequestBodyInMsg.subscribeTo(dipoleReqZeroMsg)
    moduleConfig.secondCommandAttGuidInMsg.subscribeTo(attGuidZeroMsg)
    moduleConfig.secondCommandAttRefInMsg.subscribeTo(attRefZeroMsg)
    moduleConfig.secondCommandDipoleRequestBodyInMsg.subscribeTo(dipoleReqZeroMsg)


def resetConfigToDefault(moduleConfig):
    moduleConfig.detumbleTimeout = 10
    moduleConfig.detumbleExitRateError = 0.1
    moduleConfig.settledPointingError = 1
    moduleConfig.settledRateError = 10
    moduleConfig.settledCycles = 5
    moduleConfig.commandedModes = []
    moduleConfig.commandedTimes = []
    moduleConfig.commandedTimeTypes = []
    moduleConfig.totalNumCommands = 0


def subscribeModeToMessages(modeId, attGuidMsg, attRefMsg, dipoleReqMsg, moduleConfig):
    if modeId == 0:
        moduleConfig.offAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.offAttRefInMsg.subscribeTo(attRefMsg)
    elif modeId == 1:
        moduleConfig.detumbleAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.detumbleAttRefInMsg.subscribeTo(attRefMsg)
        moduleConfig.detumbleDipoleRequestBodyInMsg.subscribeTo(dipoleReqMsg)
    elif modeId == 2:
        moduleConfig.safeAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.safeAttRefInMsg.subscribeTo(attRefMsg)
        moduleConfig.safeDipoleRequestBodyInMsg.subscribeTo(dipoleReqMsg)
    elif modeId == 3:
        moduleConfig.sunPointingAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.sunPointingAttRefInMsg.subscribeTo(attRefMsg)
        moduleConfig.sunPointingDipoleRequestBodyInMsg.subscribeTo(dipoleReqMsg)
    elif modeId == 4:
        moduleConfig.firstCommandAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.firstCommandAttRefInMsg.subscribeTo(attRefMsg)
        moduleConfig.firstCommandDipoleRequestBodyInMsg.subscribeTo(dipoleReqMsg)
    elif modeId == 5:
        moduleConfig.secondCommandAttGuidInMsg.subscribeTo(attGuidMsg)
        moduleConfig.secondCommandAttRefInMsg.subscribeTo(attRefMsg)
        moduleConfig.secondCommandDipoleRequestBodyInMsg.subscribeTo(dipoleReqMsg)


def test_modeController():
    r"""
    **Validation Test Description**

    This script tests that the mode controller transitions properly for all automated transitions and commands, and
    settles properly. Also checks that the proper output messages are passed out in each mode.

    **Description of Variables Being Tested**

    In this file we are checking the values of the output message variables:

    - ``modeId``
    - ``isSettled``

    Additionally, the output control messages themselves are checked:

    - ``attErrorOutMsg``
    - ``attRefOutMsg``
    - ``dipoleRequestBodyOutMsg``
    """
    # Call the test function and output failures via assertion
    [testResults, testMessage] = modeControllerModuleTestFunction()
    assert testResults < 1, testMessage
    
def modeControllerModuleTestFunction():
    testFailCount = 0                       # zero unit test result counter
    testMessages = []                       # create empty array to store test log messages
    unitTaskName = "unitTask"               # arbitrary name (don't change)
    unitProcessName = "TestProcess"         # arbitrary name (don't change)
    bskLogging.setDefaultLogLevel(bskLogging.BSK_WARNING)

    # Create a sim module as an empty container
    unitTestSim = SimulationBaseClass.SimBaseClass()

    # Create test thread
    testProcessRate = macros.sec2nano(0.1)     # [s] update process rate update time
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # Initialize module under test's config message and add module to runtime call list
    moduleConfig = modeController.modeControllerConfig()
    moduleWrap = unitTestSim.setModelDataWrap(moduleConfig)
    moduleWrap.ModelTag = "modeController"
    unitTestSim.AddModelToTask(unitTaskName, moduleWrap, moduleConfig)

    # Initialize zero'd AttGuid msg
    attGuidZeroMsgContainer = messaging.AttGuidMsgPayload()
    attGuidZeroMsg = messaging.AttGuidMsg().write(attGuidZeroMsgContainer)

    # Initialize non-zero AttGuid msg
    attGuidMsgContainer = messaging.AttGuidMsgPayload()
    attGuidMsgContainer.sigma_BR = defaultVector
    attGuidMsgContainer.omega_BR_B = defaultVector
    attGuidMsg = messaging.AttGuidMsg().write(attGuidMsgContainer)

    # Initialize zero'd AttRef msg
    attRefZeroMsgContainer = messaging.AttRefMsgPayload()
    attRefZeroMsg = messaging.AttRefMsg().write(attRefZeroMsgContainer)

    # Initialize non-zero AttRef msg
    attRefMsgContainer = messaging.AttRefMsgPayload()
    attRefMsgContainer.sigma_RN = defaultVector
    attRefMsg = messaging.AttRefMsg().write(attRefMsgContainer)

    # Initialize zero'd DipoleRequest msg
    dipoleReqZeroMsgContainer = messaging.DipoleRequestBodyMsgPayload()
    dipoleReqZeroMsg = messaging.DipoleRequestBodyMsg().write(dipoleReqZeroMsgContainer)

    # Initialize non-zero DipoleRequest msg
    dipoleReqMsgContainer = messaging.DipoleRequestBodyMsgPayload()
    dipoleReqMsgContainer.dipole_B = defaultVector
    dipoleReqMsg = messaging.DipoleRequestBodyMsg().write(dipoleReqMsgContainer)

    # Set up logging on the test module output message so that we get all the writes to it
    resultModeOutMsg = moduleConfig.modeOutMsg.recorder()
    resultAttErrorOutMsg = moduleConfig.attErrorOutMsg.recorder()
    resultAttRefOutMsg = moduleConfig.attRefOutMsg.recorder()
    resultDipoleOutMsg = moduleConfig.dipoleRequestBodyOutMsg.recorder()

    # Add modules to task
    unitTestSim.AddModelToTask(unitTaskName, resultModeOutMsg)
    unitTestSim.AddModelToTask(unitTaskName, resultAttErrorOutMsg)
    unitTestSim.AddModelToTask(unitTaskName, resultAttRefOutMsg)
    unitTestSim.AddModelToTask(unitTaskName, resultDipoleOutMsg)

    # Connect the message interfaces
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)

    # Configure mode controller parameters.
    resetConfigToDefault(moduleConfig)

    # Set the simulation time.
    unitTestSim.ConfigureStopTime(macros.sec2nano(2.0))        # seconds to stop simulation
    unitTestSim.InitializeSimulation()

    '''
        TEST 1: 
            Check that off mode stays off without any commands.
    '''
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(0, resultModeOutMsg.modeId[-1], "test 1",
                                                             testFailCount, testMessages)

    '''
        TEST 2: 
            Check that commanded transition to another mode executes properly.
    '''
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
    resetConfigToDefault(moduleConfig)
    moduleConfig.commandedModes = [2]  # Don't transition to detumble to ensure we don't auto-transition out
    moduleConfig.commandedTimes = [0]
    moduleConfig.commandedTimeTypes = [0]
    moduleConfig.totalNumCommands = 1
    unitTestSim.InitializeSimulation()
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(2, resultModeOutMsg.modeId[-1], "test 2",
                                                             testFailCount, testMessages)

    '''
        TEST 3: 
            Check that commanded transition to detumble executes properly and auto-transitions to sun-pointing.
    '''
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
    resetConfigToDefault(moduleConfig)
    moduleConfig.commandedModes = [1]
    moduleConfig.commandedTimes = [0]
    moduleConfig.commandedTimeTypes = [0]
    moduleConfig.totalNumCommands = 1
    moduleConfig.detumbleTimeout = 1
    unitTestSim.InitializeSimulation()
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(1, resultModeOutMsg.modeId[0], "test 3, initial mode ID",
                                                             testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(3, resultModeOutMsg.modeId[-1], "test 3, final mode ID",
                                                             testFailCount, testMessages)

    '''
        TEST 4: 
            Check that commanded transition to detumble timeout transitions to safe.
    '''
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
    resetConfigToDefault(moduleConfig)
    moduleConfig.commandedModes = [1]
    moduleConfig.commandedTimes = [0]
    moduleConfig.commandedTimeTypes = [0]
    moduleConfig.totalNumCommands = 1
    moduleConfig.detumbleTimeout = 1
    moduleConfig.detumbleAttGuidInMsg.subscribeTo(attGuidMsg)
    unitTestSim.InitializeSimulation()
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(1, resultModeOutMsg.modeId[0], "test 4, initial mode ID",
                                                             testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(2, resultModeOutMsg.modeId[-1], "test 4, final mode ID",
                                                             testFailCount, testMessages)

    '''
        TEST 5: 
            Check that settling occurs properly when errors are below threshold.
    '''
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
    resetConfigToDefault(moduleConfig)
    moduleConfig.commandedModes = [3]
    moduleConfig.commandedTimes = [0]
    moduleConfig.commandedTimeTypes = [0]
    moduleConfig.totalNumCommands = 1
    subscribeModeToMessages(3, attGuidMsg, attRefMsg, dipoleReqMsg, moduleConfig)
    unitTestSim.InitializeSimulation()
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(1, resultModeOutMsg.isSettled[-1], "test 5",
                                                             testFailCount, testMessages)

    '''
        TEST 6: 
            Check that settling does not occur when errors are above threshold.
    '''
    subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
    resetConfigToDefault(moduleConfig)
    moduleConfig.commandedModes = [3]
    moduleConfig.commandedTimes = [0]
    moduleConfig.commandedTimeTypes = [0]
    moduleConfig.totalNumCommands = 1
    moduleConfig.settledRateError = 0.1
    moduleConfig.settledPointingError = 0.1
    subscribeModeToMessages(3, attGuidMsg, attRefMsg, dipoleReqMsg, moduleConfig)
    unitTestSim.InitializeSimulation()
    unitTestSim.ExecuteSimulation()
    testFailCount, testMessages = unitTestSupport.compareInt(0, resultModeOutMsg.isSettled[-1], "test 6",
                                                             testFailCount, testMessages)

    '''
        TEST 7: 
            Single transition multi-test: proper settling, proper attitude reference out, proper guidance message out, 
            proper dipole message out
    '''
    for i in range(totalNumModes):
        subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
        resetConfigToDefault(moduleConfig)
        moduleConfig.commandedModes = [i]
        moduleConfig.commandedTimes = [0]
        moduleConfig.commandedTimeTypes = [0]
        moduleConfig.totalNumCommands = 1
        subscribeModeToMessages(i, attGuidMsg, attRefMsg, dipoleReqMsg, moduleConfig)
        settleExpectation = i > 1
        dipoleExpectation = zeroVector if i == 0 else defaultVector
        unitTestSim.InitializeSimulation()
        unitTestSim.ExecuteSimulation()
        failStr = "test 7, mode " + str(i) + " "
        testFailCount, testMessages = unitTestSupport.compareInt(i, resultModeOutMsg.modeId[-1], failStr +
                                                                 "final mode ID", testFailCount, testMessages)
        testFailCount, testMessages = unitTestSupport.compareInt(settleExpectation, resultModeOutMsg.isSettled[-1],
                                                                 failStr + "final settled flag", testFailCount,
                                                                 testMessages)
        testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                    resultAttErrorOutMsg.sigma_BR[-1],
                                                                    accuracy,
                                                                    failStr + "attitude error message sigma BR",
                                                                    testFailCount, testMessages)
        testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                    resultAttErrorOutMsg.omega_BR_B[-1],
                                                                    accuracy,
                                                                    failStr + "attitude error message omega BR B",
                                                                    testFailCount, testMessages)
        testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                    resultAttRefOutMsg.sigma_RN[-1],
                                                                    accuracy,
                                                                    failStr + "attitude reference message",
                                                                    testFailCount, testMessages)
        testFailCount, testMessages = unitTestSupport.compareVector(dipoleExpectation,
                                                                    resultDipoleOutMsg.dipole_B[-1],
                                                                    accuracy,
                                                                    failStr + "dipole request message",
                                                                    testFailCount, testMessages)

    '''
        TEST 8: 
            Multi-transition multi-test: proper settling, proper attitude reference out, proper guidance message out, 
            proper dipole message out
    '''
    for i in range(totalNumModes):
        for j in range(totalNumModes):
            subscribeAllToZeroMsg(attGuidZeroMsg, attRefZeroMsg, dipoleReqZeroMsg, moduleConfig)
            resetConfigToDefault(moduleConfig)
            moduleConfig.commandedModes = [i, j]
            moduleConfig.commandedTimes = [0, 1]
            moduleConfig.commandedTimeTypes = [0, 0]
            moduleConfig.totalNumCommands = 2
            subscribeModeToMessages(j, attGuidMsg, attRefMsg, dipoleReqMsg, moduleConfig)
            settleExpectation = j > 1
            dipoleExpectation = zeroVector if j == 0 else defaultVector
            unitTestSim.InitializeSimulation()
            unitTestSim.ExecuteSimulation()
            failStr = "test 8, mode " + str(i) + " to mode " + str(j) + " "
            testFailCount, testMessages = unitTestSupport.compareInt(j, resultModeOutMsg.modeId[-1], failStr +
                                                                     "final mode ID", testFailCount, testMessages)
            testFailCount, testMessages = unitTestSupport.compareInt(settleExpectation, resultModeOutMsg.isSettled[-1],
                                                                     failStr + "final settled flag", testFailCount,
                                                                     testMessages)
            testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                        resultAttErrorOutMsg.sigma_BR[-1],
                                                                        accuracy,
                                                                        failStr + "attitude error message sigma BR",
                                                                        testFailCount, testMessages)
            testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                        resultAttErrorOutMsg.omega_BR_B[-1],
                                                                        accuracy,
                                                                        failStr + "attitude error message omega BR B",
                                                                        testFailCount, testMessages)
            testFailCount, testMessages = unitTestSupport.compareVector(defaultVector,
                                                                        resultAttRefOutMsg.sigma_RN[-1],
                                                                        accuracy,
                                                                        failStr + "attitude reference message",
                                                                        testFailCount, testMessages)
            testFailCount, testMessages = unitTestSupport.compareVector(dipoleExpectation,
                                                                        resultDipoleOutMsg.dipole_B[-1],
                                                                        accuracy,
                                                                        failStr + "dipole request message",
                                                                        testFailCount, testMessages)

    if testFailCount == 0:
        print("PASSED: modeController unit test")
    else:
        print("Failed: modeController unit test")
    return [testFailCount, ''.join(testMessages)]


#
# This statement below ensures that the unitTestScript can be run as a
# stand-alone python script
#
if __name__ == "__main__":
    test_modeController()
    