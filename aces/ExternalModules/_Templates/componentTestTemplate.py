#
# Component Level Testing Template
#
# Purpose:  Component level testing template for
#           ACES software.
# Author:   Chase Heinen
# Creation Date: October 19, 2022
# Last Edit Date: October 19, 2022
#
# Description:
#
#   This script provides a template to evaluate the performance of
#   ACES dynamics and flight software. For an applied example of this
#   template, see aces/ExternalModules/attitudeEKF/_ComponentTest/
#   test_attitudeEKF.py.
#

#
# Import required modules.
from dataclasses import dataclass
from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.utilities import macros
# *** INSERT OTHER MODULE IMPORTS HERE ***
from Basilisk import __path__
bskPath = __path__[0]


#
# Add other folders to path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), '../../sim/analysis'))

#
# Import plotting methods.
from scenarioBaselinePlotting import *

#
# Configure matplotlib properties.
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=MEDIUM_SIZE) # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE) # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE) # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE) # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE) # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE) # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE) # fontsize of the figure title
plt.rcParams['figure.figsize'] = [16.0, 9.0]
plt.rcParams['lines.linewidth'] = 2

#
# Define simulation constants.
TASK_NAME = "testTask"
DYNAMICS_PROCESS_NAME = 'DynamicsProcess'
FSW_PROCESS_NAME = 'FSWProcess'

#
# Define test constants.
# *** INSERT CUSTOM TEST CONSTANTS HERE ***

#
# Define custom helper methods.
# *** INSERT CUSTOM HELPER METHODS HERE ***

#
# Define custom plotting methods.
# *** INSERT CUSTOM PLOTTING METHODS HERE ***

#
# Define component configuration dataclass.
@dataclass
class configData:
    """
    Component configuration data class.
    """

    #
    # Define class attributes.
    # *** INSERT CONFIGURATION PARAMETERS ***
    # Example: 
    # parameter: datatype

#
# Define component level testing class.
class ComponentTest(SimulationBaseClass.SimBaseClass):
    """
    Component level testing simulation class.
    """

    def __init__(self, dynRate, fswRate, simLength, testNum, configData):
        """
        Initializes the component level simulation
        base class.
        """

        #
        # Set the simulation properties.
        self.dynRate = dynRate
        self.fswRate = fswRate
        self.simTime = macros.min2nano(simLength)

        #
        # Set the test properties.
        self.testNum = testNum

        #
        # Set the configuration data.
        self.configData = configData

        #
        # Create the simulation module.
        SimulationBaseClass.SimBaseClass.__init__(self)

        #
        # Set dynamics and fsw process variables.
        self.dynProc = None
        self.fswProc = None

        #
        # Declare message recording variables.
        self.msgRecList = {}
        # *** INSERT MODULE MESSAGE NAMES HERE ***
        # Example: 
        # self.dynModuleMsgName = "dynModuleNameMsg"
        # self.fswModuleMsgName = "fswModuleNameMsg"

        #
        # Set dynamics and FSW.
        self.set_dyn_model()
        self.set_fsw_model()

        #
        # Link message.
        self.link_messages()

        #
        # Call functions to configure the initial simulation conditions and log outputs.
        self.configure_initial_conditions()
        self.log_outputs()

    def set_dyn_model(self):
        """
        Sets the simulation dynamics model.
        """

        #
        # Create process.
        self.dynProc = self.CreateNewProcess(DYNAMICS_PROCESS_NAME)

        #
        # Create dynamics side task.
        dynTaskRate = macros.sec2nano(self.dynRate)
        self.dynProc.addTask(self.CreateNewTask(TASK_NAME, dynTaskRate))

        #
        # Instantiate dynamics modules.
        # *** INSTANIATE DESIRED DYNAMICS MODULE(S) HERE ***
        # Example: 
        # self.dynModule = moduleName.ModuleName()

        #
        # Configure dynamics modules.
        # *** CALL SETUP FUNCTIONS FOR THE ABOVE MODULE(S) HERE ***
        # Example: 
        # self.set_dynModule()

        #
        # Add modules to task list.
        # *** ADD ABOVE MODULE(S) TO TASK LIST HERE ***
        # Example: 
        # self.AddModelToTask(TASK_NAME, self.dynModule)
        pass

    def set_fsw_model(self):
        """
        Sets the simulation fsw model.
        """

        #
        # Create process.
        self.fswProc = self.CreateNewProcess(FSW_PROCESS_NAME)

        #
        # Create fsw side task.
        fswTaskRate = macros.sec2nano(self.fswRate)
        self.fswProc.addTask(self.CreateNewTask(TASK_NAME, fswTaskRate))

        #
        # Instantiate fsw modules.
        # *** INSTANIATE DESIRED FSW MODULE(S) HERE ***
        # Example: 
        # self.fswModuleConfig = moduleName.ModuleName()
        # self.fswModuleWrap = self.setModelDataWrap(self.fswModuleConfig)

        #
        # Configure fsw modules.
        # *** CALL SETUP FUNCTIONS FOR THE ABOVE MODULE(S) HERE ***
        # Example: 
        # self.set_fswModule()

        #
        # Add modules to task list.
        # *** ADD ABOVE MODULE(S) TO TASK LIST HERE ***
        # Example: 
        # self.AddModelToTask(TASK_NAME, self.fswModuleWrap, self.fswModuleConfig)
        pass

    # *** INSERT DYNAMICS MODULE(S) SETUP METHODS HERE ***
    # Example:
    # def set_dynModule(self):
    #     """
    #     Set the dynamics module.
    #     """

    #     #
    #     # Configure module.
    #     self.dynModule.ModelTag = "dynModuleName"
    #     self.dynModule.config_param = self.configData.config_param

    # *** INSERT FSW MODULE(S) SETUP METHODS HERE ***
    # Example:
    # def set_fswModule(self):
    #     """
    #     Set the fsw module.
    #     """

    #     #
    #     # Configure module.
    #     self.fswModuleConfig.ModelTag = "fswModuleName"
    #     self.fswModuleConfig.config_param = self.configData.config_param

    def link_messages(self):
        """
        Link module messages.
        """

        #
        # Link messages for modules.
        # *** INSERT MESSAGE SUBSCRIPTIONS HERE ***
        # Example: 
        # self.fswModuleConfig.inMsg.subscribeTo(self.dynModule.outMsg)
        pass

    def configure_initial_conditions(self):
        """
        Configure the initial simulation conditions.
        """

        #
        # Set initial simulation conditions.
        # *** CONFIGURE INITIAL CONDITIONS HERE ***
        # Example:
        # self.dynModule.init_value = self.configData.init_value
        pass

    def log_outputs(self):
        """
        Log output variables for plotting.
        """

        #
        # Calculate recording variables.
        sampleRate = min([self.dynRate, self.fswRate])
        numDataPoints = int(self.simTime / sampleRate)
        samplingTime = unitTestSupport.samplingTime(self.simTime, sampleRate, numDataPoints)

        #
        # Record dynModule message.
        # *** INSERT DYNAMICS MODULE(S) MESSAGE RECORDERS HERE ***
        # Example:
        # self.msgRecList[self.dynModuleMsgName] = self.dynModule.outMsg.recorder(samplingTime)
        # self.AddModelToTask(TASK_NAME, self.msgRecList[self.dynModuleMsgName])

        #
        # Record fswModule message.
        # *** INSERT FSW MODULE(S) MESSAGE RECORDERS HERE ***
        # Example:
        # self.msgRecList[self.fswModuleMsgName] = self.fswModuleConfig.outMsg.recorder(samplingTime)
        # self.AddModelToTask(TASK_NAME, self.msgRecList[self.fswModuleMsgName])

        #
        # Log fswModule variable.
        # *** INSERT VARIABLE LOGGING HERE ***
        # Example:
        # self.AddVariableForLogging(self.fswModuleWrap.ModelTag + "." + "variable_name", sampleRate, "start_index", "end_index", "datatype")
        pass

    def pull_outputs(self):
        """
        Pull output variables and plot them.
        """

        #
        # Retrieve variables from recorders.
        # *** PULL RECORDER VARIABLES HERE ***
        # Example:
        # dynModuleVariable = self.msgRecList[self.dynModuleMsgName].variable_name
        # fswModuleVariable = self.msgRecList[self.fswModuleMsgName].variable_name
        # simTimes = self.msgRecList[self.dynModuleMsgName].times() * macros.NANO2MIN

        #
        # Retrieve variables from log.
        # *** PULL LOGGED VARIABLES HERE ***
        # Example:
        # logVariable = self.GetLogVariableData(self.fswModuleWrap.ModelTag + "." + "variable_name")
        # logVariable = logVariable[:, 1:] # Remove time tags.

        #
        # Construct plots.
        # *** CALL PLOTTING METHODS HERE ***
        pass

def run_test(dynRate, fswRate, simLength, testNum, configData):
    """
    Initializes and executes the scenario.
    """

    #
    # Create the test scenario.
    testScenario = ComponentTest(dynRate, fswRate, simLength, testNum, configData)

    #
    # Initialize and execute the simulation.
    testScenario.SetProgressBar(True)
    testScenario.InitializeSimulation()
    testScenario.ConfigureStopTime(testScenario.simTime)
    testScenario.ExecuteSimulation()

    #
    # Pull outputs.
    testScenario.pull_outputs()

def test_cases():
    """
    The main testing script.
    """

    #
    # Test 1:
    #   *** INSERT TEST NAME HERE ***
    #
    # Description:  
    #   *** INSERT TEST DESCRIPTION HERE ***
    #

    #
    # Define test and simulation parameters.
    testNum = 1
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Configure test data.
    # *** DEFINE MODULE CONFIGURATION PARAMETERS HERE ***
    # Example:
    # config = configData(
    #     parameter = value,
    # )

    #
    # Run Test 1.
    run_test(dynRate, fswRate, simLength, testNum, config)

    # *** INSERT MORE TESTS HERE ***

if __name__ == "__main__":
    
    #
    # Run component test script.
    test_cases()