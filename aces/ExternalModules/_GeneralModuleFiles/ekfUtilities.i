%module ekfUtilities
%{
  #include "ekfUtilities.h"
%}

%include "ekfUtilities.h"

%pythoncode %{
from Basilisk.architecture.swig_common_model import *
%}

%include "swig_conly_data.i"

%constant void ekfPropagateCovarianceMatrix(double*, double*, double*, size_t, double*);
%ignore ekfPropagateCovarianceMatrix;
%constant int ekfComputeKalmanGain(double*, double*, double*, size_t, size_t, double*);
%ignore ekfComputeKalmanGain;
%constant void ekfUpdateCovariance(double*, double*, double*, double*, double*, size_t, size_t, double*);
%ignore ekfUpdateCovariance;
%constant void ekfUpdateStateEstimate(double*, double*, double*, size_t, size_t, double*);
%ignore ekfUpdateStateEstimate;
%constant void ekfComputeStateTransitionMatrix(double*, double*, size_t, double, double*);
%ignore ekfComputeStateTransitionMatrix;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

