/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "attitudeEkfMath.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/rigidBodyKinematics.h"
#include "mathUtilities.h"
#include <math.h>


/*! Computes the state dynamics matrix of the attitude extended kalman filter based on the current estimated attitude or angular rate.
 @return void
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter.
 @param wBiasCompensatedBN_B The estimated angular rate of the spacecraft hub that has been bias compensated and expressed in the body frame.
 @param A The state dynamics matrix.
 */
void computeDynamicsMatrix(double *sigmaEst_BN, double *wBiasCompensatedBN_B, double *A)
{
    // Declare local variables.
    double dfdmrp[3 * 3];
    double dfdb[3 * 3];
    double term1[3 * 3];
    double term2[3 * 3];
    double wTilde[3 * 3];
    double term4[3 * 3];
    double *mrp = &(sigmaEst_BN[0]);
    double wDotMrp = v3Dot(wBiasCompensatedBN_B, mrp);
    double dfdbias[3 * 3];
    double I_3X3[3*3];
    mSetIdentity(I_3X3, 3, 3);
    
    /*
     * Compute the partial of the bias compensated mrp kinematics wrt
     * the current mrp estimate.
     */
    vOuterProduct(mrp, 3, wBiasCompensatedBN_B, 3, term1);
    vOuterProduct(wBiasCompensatedBN_B, 3, mrp, 3, term2);
    v3TildeM(wBiasCompensatedBN_B, wTilde);
    mScale(wDotMrp, I_3X3, 3, 3, term4);
    mSubtract(term1, 3, 3, term2, dfdmrp);
    mSubtract(dfdmrp, 3, 3, wTilde, dfdmrp);
    mAdd(dfdmrp, 3, 3, term4, dfdmrp);
    mScale(0.5, dfdmrp, 3, 3, dfdmrp);
    
    /*
     * Compute the partial of the bias compensated mrp kinematics wrt
     * the current bias estimate: -1/4 [B(mrpEst)]
     */
    BMatMrpM(mrp, dfdb);
    mScale(-0.25, dfdb, 3, 3, dfdb);

    // Populate the dynamics matrix.
    A[MXINDEX(6, 0, 0)] = dfdmrp[MXINDEX(3, 0, 0)];
    A[MXINDEX(6, 0, 1)] = dfdmrp[MXINDEX(3, 0, 1)];
    A[MXINDEX(6, 0, 2)] = dfdmrp[MXINDEX(3, 0, 2)];
    A[MXINDEX(6, 1, 0)] = dfdmrp[MXINDEX(3, 1, 0)];
    A[MXINDEX(6, 1, 1)] = dfdmrp[MXINDEX(3, 1, 1)];
    A[MXINDEX(6, 1, 2)] = dfdmrp[MXINDEX(3, 1, 2)];
    A[MXINDEX(6, 2, 0)] = dfdmrp[MXINDEX(3, 2, 0)];
    A[MXINDEX(6, 2, 1)] = dfdmrp[MXINDEX(3, 2, 1)];
    A[MXINDEX(6, 2, 2)] = dfdmrp[MXINDEX(3, 2, 2)];
    A[MXINDEX(6, 0, 3)] = dfdb[MXINDEX(3, 0, 0)];
    A[MXINDEX(6, 0, 4)] = dfdb[MXINDEX(3, 0, 1)];
    A[MXINDEX(6, 0, 5)] = dfdb[MXINDEX(3, 0, 2)];
    A[MXINDEX(6, 1, 3)] = dfdb[MXINDEX(3, 1, 0)];
    A[MXINDEX(6, 1, 4)] = dfdb[MXINDEX(3, 1, 1)];
    A[MXINDEX(6, 1, 5)] = dfdb[MXINDEX(3, 1, 2)];
    A[MXINDEX(6, 2, 3)] = dfdb[MXINDEX(3, 2, 0)];
    A[MXINDEX(6, 2, 4)] = dfdb[MXINDEX(3, 2, 1)];
    A[MXINDEX(6, 2, 5)] = dfdb[MXINDEX(3, 2, 2)];
}


/*! Computes the discrete time process noise contribution of the gyroscope.
 @return void
 @param gyroRateNoiseStd The magnitude of the angle random walk (ARW).
 @param gyroBiasNoiseStd The anticipated magnitude of the rate random walk (RRW).
 @param dt The propagation time step.
 @param Qgyro The matrix that the gyro process noise is stored in.
 */
void computeDiscreteTimeProcessNoiseForGyro(double gyroRateNoiseStd, double gyroBiasNoiseStd, double dt, double *Qgyro)
{
    Qgyro[MXINDEX(6, 0, 0)] = gyroRateNoiseStd * gyroRateNoiseStd;
    Qgyro[MXINDEX(6, 1, 1)] = gyroRateNoiseStd * gyroRateNoiseStd;
    Qgyro[MXINDEX(6, 2, 2)] = gyroRateNoiseStd * gyroRateNoiseStd;
    Qgyro[MXINDEX(6, 3, 3)] = gyroBiasNoiseStd * gyroBiasNoiseStd;
    Qgyro[MXINDEX(6, 4, 4)] = gyroBiasNoiseStd * gyroBiasNoiseStd;
    Qgyro[MXINDEX(6, 5, 5)] = gyroBiasNoiseStd * gyroBiasNoiseStd;
}


/*! Computes the matrix that maps the discrete time gyro process noise into the filter state.
 @return void
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter.
 @param G The matrix the gyro process noise transition matrix is stored in. This is also sometimes referred to as lambda.
 */
void computeProcessNoiseTransitionMatrix(double *sigmaEst_BN, double *G)
{
    double dgdb[3 * 3];
    double *mrp = &(sigmaEst_BN[0]);
    
    /*
     * Compute the partial of the nonlinear function, g, that maps
     * the gyro noise into the filter state.
     */
    BMatMrpM(mrp, dgdb);
    mScale(-0.25, dgdb, 3, 3, dgdb);
    
    // Assign elements of the process noise transition matrix.
    G[MXINDEX(6, 0, 0)] = dgdb[MXINDEX(3, 0, 0)];
    G[MXINDEX(6, 0, 1)] = dgdb[MXINDEX(3, 0, 1)];
    G[MXINDEX(6, 0, 2)] = dgdb[MXINDEX(3, 0, 2)];
    G[MXINDEX(6, 1, 0)] = dgdb[MXINDEX(3, 1, 0)];
    G[MXINDEX(6, 1, 1)] = dgdb[MXINDEX(3, 1, 1)];
    G[MXINDEX(6, 1, 2)] = dgdb[MXINDEX(3, 1, 2)];
    G[MXINDEX(6, 2, 0)] = dgdb[MXINDEX(3, 2, 0)];
    G[MXINDEX(6, 2, 1)] = dgdb[MXINDEX(3, 2, 1)];
    G[MXINDEX(6, 2, 2)] = dgdb[MXINDEX(3, 2, 2)];
    G[MXINDEX(6, 3, 3)] = 1.0;
    G[MXINDEX(6, 4, 4)] = 1.0;
    G[MXINDEX(6, 5, 5)] = 1.0;
}


/*! Computes the total process noise to be added to the covariance matrix in the propagation step of the filter. The total process noise includes
    the noise from process noise from the gyro and the additional process noise Q.
 @return void
 @param gyroRateNoiseStd The magnitude of the angle random walk (ARW).
 @param gyroBiasNoiseStd The anticipated magnitude of the rate random walk (RRW).
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter.
 @param G The matrix the gyro process noise transition matrix is stored in. This is also sometimes referred to as lambda.
 @param Q Matrix that contains additional process noise to be added to the covariance.
 @param Qgyro The matrix that the gyro process noise is stored in.
 @param dt The propagation time step.
 @param Qtotal The matrix that total process noise to be added to the sytem is stored in.
 */
void computeProcessNoiseMatrix(double gyroRateNoiseStd, double gyroBiasNoiseStd, double *sigmaEst_BN, double *G, double *Q, double *Qgyro, double dt, double *Qtotal)
{
    double GTranspose[6 * 6];
    double G_Q[6 * 6];
    double G_Q_Gt[6 * 6];
    
    // Compute discrete time process noise from gyro.
    computeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt, Qgyro);
    
    // Compute process noise transition matrix for gyro
    computeProcessNoiseTransitionMatrix(sigmaEst_BN, G);
    
    // dt * (G * Qgyro * G^T)
    mTranspose(G, 6, 6, GTranspose);
    mMultM(G, 6, 6, Qgyro, 6, 6, G_Q);
    mMultM(G_Q, 6, 6, GTranspose, 6, 6, G_Q_Gt);
    mScale(dt, G_Q_Gt, 6, 6, G_Q_Gt);
    
    // dt * (G * Qgyro * G^T) + Q
    mAdd(G_Q_Gt, 6, 6, Q, Qtotal);
}


/*! Propagates the attitude forward in time by dt seconds using euler propagation.
 @return void
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter. This is also the variable the propagated attitude is stored in.
 @param wBiasCompensatedBN_B The estimated angular rate of the spacecraft hub that has been bias compensated and expressed in the body frame.
 @param dt The propagation time step.
 */
void attitudePropagateSingleStep(double *sigmaEst_BN, double *wBiasCompensatedBN_B, double dt)
{
    double mrpDotBN[3];
    double mrpDelta[3];
    double *mrp_BN = &(sigmaEst_BN[0]);
    
    // Compute the rate of change of the attitude.
    dMRP(mrp_BN, wBiasCompensatedBN_B, mrpDotBN);
    
    // Scale by dt to get a delta rotation.
    v3Scale(dt, mrpDotBN, mrpDelta);
    
    /*
     * Add delta rotation to attitude to yield net rotation. This assumed
     * that the delta rotation is "small".
     */
    v3Add(sigmaEst_BN, mrpDelta, mrp_BN);
}


/*! Switches the current attitude mrp and covariance to the shadow set avoiding the singularity that exists at a 360 degree rotation.
 @return void
 @param P The propagated covariance matrix. This is also the variable the shadow covariance is stored in.
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter. This is also the variable the shadow attitude is stored in.
 */
void switchMrpAndCovarianceToShadowSet(double *P, double *sigmaEst_BN)
{
    double L[6 * 6];
    double Ltranspose[6 * 6];
    double term1[3 * 3];
    double term2[3 * 3];
    double upperLeftBlock[3 * 3];
    double LP[6 * 6];
    mSetZero(L, 6, 6);
    double I_3X3[9];
    mSetIdentity(I_3X3, 3, 3);
    
    // Compute upper left 3x3 block of L.
    if (v3Norm(sigmaEst_BN) > DB0_EPS)
    {
        vOuterProduct(sigmaEst_BN, 3, sigmaEst_BN, 3, term1);
        double sigmaNorm = v3Norm(sigmaEst_BN);
        mScale(2.0 / pow(sigmaNorm, 4.0), term1, 3, 3, term1);
        mScale(-1.0 / pow(sigmaNorm, 2.0), I_3X3, 3, 3, term2);
        mAdd(term1, 3, 3, term2, upperLeftBlock);
        
        // Assign elements of L.
        L[MXINDEX(6, 0, 0)] = upperLeftBlock[MXINDEX(3, 0, 0)];
        L[MXINDEX(6, 0, 1)] = upperLeftBlock[MXINDEX(3, 0, 1)];
        L[MXINDEX(6, 0, 2)] = upperLeftBlock[MXINDEX(3, 0, 2)];
        L[MXINDEX(6, 1, 0)] = upperLeftBlock[MXINDEX(3, 1, 0)];
        L[MXINDEX(6, 1, 1)] = upperLeftBlock[MXINDEX(3, 1, 1)];
        L[MXINDEX(6, 1, 2)] = upperLeftBlock[MXINDEX(3, 1, 2)];
        L[MXINDEX(6, 2, 0)] = upperLeftBlock[MXINDEX(3, 2, 0)];
        L[MXINDEX(6, 2, 1)] = upperLeftBlock[MXINDEX(3, 2, 1)];
        L[MXINDEX(6, 2, 2)] = upperLeftBlock[MXINDEX(3, 2, 2)];
        L[MXINDEX(6, 3, 3)] = 1.0;
        L[MXINDEX(6, 4, 4)] = 1.0;
        L[MXINDEX(6, 5, 5)] = 1.0;
        
        // Ps = L * P * L^T
        mTranspose(L, 6, 6, Ltranspose);
        mMultM(L, 6, 6, P, 6, 6, LP);
        mMultM(LP, 6, 6, Ltranspose, 6, 6, P);
        
        // Switch mrp to shadow set.
        MRPshadow(sigmaEst_BN, sigmaEst_BN);
    }
}


/*! Computes the MRP measurement residual. Switching to the shadow set if desired.
 @return void
 @param sigmaEst_BN The estimated attitude in the form of a Modified Rodriguez Parameter.
 @param sigmaMeasured_BN The measured attitude in the form of a Modified Rodriguez Parameter.
 @param residual The array the MRP measurement residual is stored in.
 */
void computeMeasurementResdiual(double *sigmaEst_BN, double *sigmaMeasured_BN, double *residual)
{
    v3Subtract(sigmaMeasured_BN, sigmaEst_BN, residual);
    
    // Check to see if the shadow set should be considered.
    if (v3Norm(sigmaMeasured_BN) > 1.0 / 3.0)
    {
        // Compute the shadow mrp.
        double sigmaShadow_BN[3];
        MRPshadow(sigmaMeasured_BN, sigmaShadow_BN);
        double residualShadow[3];
        v3Subtract(sigmaShadow_BN, sigmaEst_BN, residualShadow);
        
        // If the shadow set produces a smaller residual than the non shadow mrp, use it.
        if (v3Norm(residualShadow) < v3Norm(residual))
        {
            v3Copy(residualShadow, residual);
        }
    }
}
