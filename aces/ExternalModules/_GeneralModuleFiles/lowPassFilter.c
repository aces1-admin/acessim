/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "lowPassFilter.h"
#include "architecture/utilities/linearAlgebra.h"


/*! Sets the coefficiants of a first order digital low pass filter.
 @return void
 @param filter Structure of type LPF.
 @param num Numerator coeffcients of the z domain transfer function implemented.
 @param den Denominator coefficients of the z domain transfer function implemented.
 */
void filterSetCoeffs(LPF *filter, double num[2], double den[2])
{
    filter->num[0] = num[0];
    filter->num[1] = num[1];
    filter->den[0] = den[0];
    filter->den[1] = den[1];
}


/*! Resets the filter back to its starting state. (This is useful on initialization or if the filter has ingested bad values and needs to be reset.)
 @return void
 @param filter Structure of type LPF.
 */
void filterReset(LPF *filter)
{
    filter->y = 0;
    filter->yPrev = 0;
    filter->u = 0;
    filter->uPrev = 0;
}


/*! Applies the low pass filter defined by the filter numerator and denominator coefficients to a scalar input u.
 @return void
 @param filter Structure of type LPF.
 @param u Input to be filtered.
 */
double filter(LPF *filter, double u)
{
    if (filter->den[0] > DB0_EPS)
    {
        filter->u = u;
        filter->y = (filter->num[0] * filter->u + filter->num[1] * filter->uPrev - filter->den[1] * filter->yPrev) / filter->den[0];
        filter->yPrev = filter->y;
        filter->uPrev = filter->u;
    }
    
    return filter->y;
}
