%module mathUtilities
%{
  #include "mathUtilities.h"
%}

%include "mathUtilities.h"

%pythoncode %{
from Basilisk.architecture.swig_common_model import *
%}
%include "swig_conly_data.i"
%constant void v3TildeM(double*, void*);
%ignore v3TildeM;
%constant BMatMrpM(double*, void*);
%ignore BMatMrpM;
%constant rotateVectorWithMrp(double*, double*, double*)
%ignore rotateVectorWithMrp;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}
