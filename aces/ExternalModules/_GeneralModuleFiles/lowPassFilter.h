#ifndef _LOW_PASS_FILTER_H_
#define _LOW_PASS_FILTER_H_

//! LPF structure definition.
typedef struct{
    double num[2];      //!< Numerator coeffcients of the z domain transfer function implemented.
    double den[2];      //!< Denominator coeffcients of the z domain transfer function implemented.
    double y;           //!< Current filter output.
    double yPrev;       //!< Previous filter output.
    double u;           //!< Current filter input.
    double uPrev;       //!< Previous filter input.
} LPF;

#ifdef __cplusplus
extern "C" {
#endif

void filterSetCoeffs(LPF *filter, double num[2], double den[2]);
void filterReset(LPF *filter);
double filter(LPF *filter, double u);

#ifdef __cplusplus
}
#endif


#endif
