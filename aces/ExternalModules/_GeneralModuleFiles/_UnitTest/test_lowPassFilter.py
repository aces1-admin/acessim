#
# Low Pass Filter Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 20 2022
#

import pytest
import os, inspect
from Basilisk.ExternalModules import lowPassFilter
from Basilisk.utilities import unitTestSupport
import numpy as np
from random import uniform
import matplotlib.pyplot as plt

filename = inspect.getframeinfo(inspect.currentframe()).filename
path = os.path.dirname(os.path.abspath(filename))

def setDoubleArray(lowPassFilter, array, values, length):
    '''
    This function populates a c array that's been wrapped in python with the
    elements contained in values.
    '''
    for k in range(0, length): lowPassFilter.doubleArray_setitem(array, k, values[k])

def fillNpArrayDoubleArray(lowPassFilter, array, length):
    '''
    This functions populates a numpy array with the elements of the c array
    wrapped in python, array.
    '''
    result = np.zeros((length, ))
    for k in range(0, length): result[k] = lowPassFilter.doubleArray_getitem(array, k)
    return result
        
def setupFilter(lowPassFilter, lpf, numCoeffs, denCoeffs):
    '''
    This functions sets up the the low pass filter.
    '''
    # Create arrays for filter numerator and denomenator coefficients.
    filterNumCoeffs = lowPassFilter.new_doubleArray(2)
    filterDenCoeffs = lowPassFilter.new_doubleArray(2)
    
    # Setup filter coefficients.
    setDoubleArray(lowPassFilter, filterNumCoeffs, numCoeffs, 2)
    setDoubleArray(lowPassFilter, filterDenCoeffs, denCoeffs, 2)
    lowPassFilter.filterSetCoeffs(lpf, filterNumCoeffs, filterDenCoeffs)
    
def test_lowPassFilter(showPlots, accuracy):
    r"""
    **Validation Test Description**

    This tests all paths of the functions contained in lowPassFilter.c and
    verifies that filter is setup, reset, and implemented properly.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests

    **Description of Variables Being Tested**

    The tests in this file check the internal variables of the
    LPF structure as well as the output of the filter y. 
    """

    [testResults, testMessage] = lowPassFilterTestFunction(showPlots, accuracy)
    assert testResults < 1, testMessage

def lowPassFilterTestFunction(showPlots, accuracy):

    testFailCount = 0       # zero unit test result counter
    testMessages = []       # create empty array to store test log messages

    # Create LPF structure.
    lpf = lowPassFilter.LPF()
    

    '''
    Test: Test that numerator and denominator coefficients are setup 
            properly using filterSetCoeffs().
    '''
    numCoeffsIn = np.array([1., 2.])
    denCoeffsIn = np.array([3., 4.])
    setupFilter(lowPassFilter, lpf, numCoeffsIn, denCoeffsIn)
    numCoeffsOut = fillNpArrayDoubleArray(lowPassFilter, lpf.num, 2)
    denCoeffsOut = fillNpArrayDoubleArray(lowPassFilter, lpf.den, 2)
    testFailCount, testMessages = unitTestSupport.compareVector(numCoeffsIn,
                                            numCoeffsOut, accuracy, "Test 1",
                                            testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(denCoeffsIn,
                                        denCoeffsOut,
                                        accuracy, "Test 2",
                                        testFailCount, testMessages)
    
    '''
    Test: Test that filter returns expected value when given the input u = 2.
    '''
    numCoeffsIn = np.array([1., 1.])
    denCoeffsIn = np.array([1., 1.])
    setupFilter(lowPassFilter, lpf, numCoeffsIn, denCoeffsIn)
    expectedResult = 2.
    
    # Verify output.
    result = lowPassFilter.filter(lpf, expectedResult)
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        result, accuracy, "Test 3", 
                                        testFailCount, testMessages)
    # Verify LPF.y.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.y, accuracy, "Test 4", 
                                        testFailCount, testMessages)
    # Verify LPF.yPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.yPrev, accuracy, "Test 5", 
                                        testFailCount, testMessages)
    # Verify LPF.u.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.u, accuracy, "Test 6", 
                                        testFailCount, testMessages) 
    # Verify LPF.uPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.uPrev, accuracy, "Test 7", 
                                        testFailCount, testMessages)
    
    '''
    Test: Test filter is reset properly with filterReset().
    '''
    lowPassFilter.filterReset(lpf)
    expectedResult = 0.
    
    # Verify LPF.y.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.y, accuracy, "Test 8", 
                                        testFailCount, testMessages)
    # Verify LPF.yPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.yPrev, accuracy, "Test 9", 
                                        testFailCount, testMessages)
    # Verify LPF.u.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.u, accuracy, "Test 10", 
                                        testFailCount, testMessages) 
    # Verify LPF.uPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf.uPrev, accuracy, "Test 11", 
                                        testFailCount, testMessages)
    
    '''
    Test: Test filter with a zero input using filter().
    '''
    numCoeffsIn = np.array([1., 1.])
    denCoeffsIn = np.array([1., 1.])
    setupFilter(lowPassFilter, lpf, numCoeffsIn, denCoeffsIn)
    expectedResult = 0.
    result = lowPassFilter.filter(lpf, expectedResult)
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        result, accuracy, "Test 12", 
                                        testFailCount, testMessages)
    
    '''
    Test: Test that filter with divide by zero condition is present using
          filter().
    '''
    lpf2 = lowPassFilter.LPF()
    result = lowPassFilter.filter(lpf2, 2.)
    expectedResult = 0.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        result, accuracy, "Test 13", 
                                        testFailCount, testMessages)
    # Verify LPF.y.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf2.y, accuracy, "Test 14", 
                                        testFailCount, testMessages)
    # Verify LPF.yPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf2.yPrev, accuracy, "Test 15", 
                                        testFailCount, testMessages)
    # Verify LPF.u.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf2.u, accuracy, "Test 16", 
                                        testFailCount, testMessages) 
    # Verify LPF.uPrev.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        lpf2.uPrev, accuracy, "Test 17", 
                                        testFailCount, testMessages)
    
    '''
    Test: Set up filter with a 10 rad/s time constant a test that filter
            settles to a values within 1E-4 of 1 using filter(). See technical
            memo on how to generate filter coefficients.
    '''
    lowPassFilter.filterReset(lpf)
    numCoeffsIn = np.array([1./3., 1./3.])
    denCoeffsIn = np.array([1., -1./3.])
    setupFilter(lowPassFilter, lpf, numCoeffsIn, denCoeffsIn)
    expectedResult = 1.
    filteredValues = []
    for k in range(0, 10): 
        result = lowPassFilter.filter(lpf, expectedResult)
        filteredValues.append(result)
        
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedResult, 
                                        result, accuracy * 1E8, "Test 18", 
                                        testFailCount, testMessages)
    
    '''
    Test: Stress test filter with random inputs using filter().
    '''
    numCoeffsIn = np.array([1., 2.])
    denCoeffsIn = np.array([3., 4.])
    setupFilter(lowPassFilter, lpf, numCoeffsIn, denCoeffsIn)
    for k in range(0, 100): lowPassFilter.filter(lpf, uniform(-1000000., 
                                                              1000000.))

    if testFailCount == 0:
        print("PASSED: " + "test_lowPassFilter.py")
    else:
        print(testMessages)
        
    if showPlots:
        plt.figure()
        plt.plot(filteredValues)
        plt.grid(True)
        plt.ylabel("Filtered Value")
        plt.xlabel("Number of Values")
        plt.show()
    return [testFailCount, ''.join(testMessages)]


#
# This statement below ensures that the unitTestScript can be run as a
# stand-alone python script.
#
if __name__ == "__main__":
    test_lowPassFilter(False, 1e-12)   
