#
# Attitude EKF Math Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 24 2022
#

import pytest
import os, inspect
from Basilisk.ExternalModules import attitudeEkfMath
from Basilisk.utilities import unitTestSupport
import numpy as np
from random import uniform
from Basilisk.utilities import RigidBodyKinematics
from Basilisk.utilities import macros

filename = inspect.getframeinfo(inspect.currentframe()).filename
path = os.path.dirname(os.path.abspath(filename))

def setDoubleArray(attitudeEkfMath, array, values, length):
    '''
    This function populates a c array that's been wrapped in pyhton with the
    elements contained in values.
    '''
    for k in range(0, length): attitudeEkfMath.doubleArray_setitem(array, k, values[k])

def fillNpArrayDoubleArray(attitudeEkfMath, array, length):
    '''
    This functions populates a numpy array with the elements of the c array
    wrapped in python, array.
    '''
    result = np.zeros((length, ))
    for k in range(0, length): result[k] = attitudeEkfMath.doubleArray_getitem(array, k)
    return result

def truthComputeDynamicsMatrix(sigmaEst_BN, wBiasCompensatedBN_B):
    s = sigmaEst_BN
    w = wBiasCompensatedBN_B
    B = RigidBodyKinematics.BmatMRP(s)
    wTilde = np.array(RigidBodyKinematics.v3Tilde(w))
    O3x3 = np.zeros((3,3))
    I3x3 = np.eye(3)
    upperLeftBlock = 1./2. * (np.outer(s, w) - np.outer(w, s) - wTilde + np.dot(w, s) * I3x3)
    A = np.block([[upperLeftBlock, -1./4. * B], [O3x3, O3x3]])
    return np.reshape(A, (36, ))

def truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt):
    a = gyroRateNoiseStd
    b = gyroBiasNoiseStd
    I3x3 = np.eye(3)
    upperLeft = (a**2 * dt + 1./3. * b**2 * dt**3) * I3x3
    upperRight = 1./2. * b**2 * dt**2 * I3x3
    lowerLeft = upperRight
    lowerRight = b**2 * dt * I3x3
    Qgyro = np.block([[upperLeft, upperRight], [lowerLeft, lowerRight]])
    return np.reshape(Qgyro, (36, ))

def truthComputeProcessNoiseTransitionMatrix(sigmaEst_BN):
    s = sigmaEst_BN
    B = RigidBodyKinematics.BmatMRP(s)
    O3x3 = np.zeros((3,3))
    I3x3 = np.eye(3)
    G = np.block([[-1./4. * B, O3x3], [O3x3, I3x3]])
    return np.reshape(G, (36, ))

def truthComputeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, sigmaEst_BN, Q, dt):
    Qgyro = np.reshape(truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt), (6,6))
    G = np.reshape(truthComputeProcessNoiseTransitionMatrix(sigmaEst_BN), (6,6))
    Qtotal = G @ Qgyro @ G.T + Q
    return np.reshape(Qtotal, (36, ))

def truthAttitudePropagateSingleStep(sigmaEst_BN, wBiasCompensatedBN_B, dt):
    ds = RigidBodyKinematics.dMRP(sigmaEst_BN, wBiasCompensatedBN_B)
    delta = ds * dt
    return sigmaEst_BN + delta

def truthSwitchMrpAndCovarianceToShadowSet(P, sigmaEst_BN):
    s = sigmaEst_BN
    if np.linalg.norm(s) > 1E-30:
        sMag = np.linalg.norm(s)
        I3x3 = np.eye(3)
        O3x3 = np.zeros((3,3))
        alpha = 2 * sMag**(-4) * np.outer(s, s) - sMag**(-2) * I3x3
        L = np.block([[alpha, O3x3], [O3x3, I3x3]])
        s = - s / np.dot(s, s)
        P = np.reshape(L @ P @ L.T, (36, ))
    else:
        s = s
        P = np.reshape(P, (36, ))
    return s, P
    

def truthComputeMeasurementResdiual(sigmaEst_BN, sigmaMeasured_BN):
    s = sigmaEst_BN
    sMeas = sigmaMeasured_BN
    residual = sMeas - s
    if np.linalg.norm(sMeas) > 1./3.:
        sMeasShadow = - sMeas / np.dot(sMeas, sMeas)
        residualShadow = sMeasShadow - s
        if np.linalg.norm(residualShadow) < np.linalg.norm(residual):
            residual = residualShadow
    return residual

def test_attitudeEkfMath(accuracy):
    r"""
    **Validation Test Description**

    This tests all paths of the functions contained in attitudeEkfMath.c and
    verifies that they are implemented as intended.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests

    **Description of Variables Being Tested**
    A:                          state dynamics matrix
    G:                          process noise transition matrix
    Qgyro                       process noise matrix for gyro
    Qtotal                      total process noise
    measurementResidual:        mrp measurement residual
    sigmaEst_BN                 estimated mrp
    """

    [testResults, testMessage] = attitudeEkfMathTestFunction(accuracy)
    assert testResults < 1, testMessage

def attitudeEkfMathTestFunction(accuracy):

    testFailCount = 0       # zero unit test result counter
    testMessages = []       # create empty array to store test log messages
    
    '''
    Test:   Test computeDynamicsMatrix() with a arbitrary inputs.
    '''
    # setup
    sTruth = np.array([0.1, 0.2, 0.3])
    wTruth = np.array([0.3, 0.2, 0.1])
    numStates = 6
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    w = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, w, wTruth, 3)
    
    # call truth function
    expected = truthComputeDynamicsMatrix(sTruth, wTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeDynamicsMatrix(s, w, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 1",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test computeDynamicsMatrix() with zeros as inputs.
    '''
    # setup
    sTruth = np.zeros((3, ))
    wTruth = np.zeros((3, ))
    numStates = 6
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    w = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, w, wTruth, 3)
    
    # call truth function
    expected = truthComputeDynamicsMatrix(sTruth, wTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeDynamicsMatrix(s, w, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)
 
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 2",
                                        testFailCount, testMessages)

    '''
    Test:   Test computeDiscreteTimeProcessNoiseForGyro() arbitrary inputs.
    '''
    # setup
    gyroRateNoiseStd = 1.
    gyroBiasNoiseStd = 2.
    dt = 0.1
    numStates = 6
    
    # call truth function
    expected = truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 3",
                                        testFailCount, testMessages)
  
    '''
    Test:   Test computeDiscreteTimeProcessNoiseForGyro() with zeros as 
            inputs.
    '''
    # setup
    gyroRateNoiseStd = 0.
    gyroBiasNoiseStd = 0.
    dt = 0.
    numStates = 6
    
    # call truth function
    expected = truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 4",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test computeProcessNoiseTransitionMatrix() with arbitrary inputs.
    '''
    # setup
    sTruth = np.array([0.1, 0.2, 0.3])
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    numStates = 6
    
    # call truth function
    expected = truthComputeProcessNoiseTransitionMatrix(sTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeProcessNoiseTransitionMatrix(s, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 5",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test computeProcessNoiseTransitionMatrix() with zeros as 
            inputs.
    '''
    # setup
    sTruth = np.zeros((3, ))
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    numStates = 6
    
    # call truth function
    expected = truthComputeProcessNoiseTransitionMatrix(sTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeProcessNoiseTransitionMatrix(s, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 6",
                                        testFailCount, testMessages)

    '''
    Test:   Test computeProcessNoiseMatrix() with arbitrary inputs.
    '''
    # setup
    numStates = 6
    gyroRateNoiseStd = 1.
    gyroBiasNoiseStd = 2.
    dt = 0.1
    
    sTruth = np.array([0.1, 0.2, 0.3])
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    
    Qtruth = np.random.rand(numStates, numStates) # create a random positive semi-definite matrix
    Qtruth = np.dot(Qtruth, Qtruth.T)
    Q = attitudeEkfMath.new_doubleArray(numStates**2)
    setDoubleArray(attitudeEkfMath, Q, np.reshape(Qtruth, (numStates**2,)), numStates**2)
    
    Qgyro = attitudeEkfMath.new_doubleArray(numStates**2)
    
    G = attitudeEkfMath.new_doubleArray(numStates**2)
    
    # call truth function
    expected = truthComputeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, sTruth, Qtruth, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, s, G, Q, Qgyro, dt, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 7",
                                        testFailCount, testMessages)

    '''
    Test:   Test computeProcessNoiseMatrix() with zeros as inputs.
    '''
    # setup
    numStates = 6
    gyroRateNoiseStd = 0.
    gyroBiasNoiseStd = 0.
    dt = 0.
    
    sTruth = np.zeros((3, ))
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    
    Qtruth = np.zeros((numStates, numStates))
    Q = attitudeEkfMath.new_doubleArray(numStates**2)
    setDoubleArray(attitudeEkfMath, Q, np.reshape(Qtruth, (numStates**2,)), numStates**2)
    
    Qgyro = attitudeEkfMath.new_doubleArray(numStates**2)
    
    G = attitudeEkfMath.new_doubleArray(numStates**2)
    
    # call truth function
    expected = truthComputeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, sTruth, Qtruth, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(numStates**2)
    attitudeEkfMath.computeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, s, G, Q, Qgyro, dt, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test attitudePropagateSingleStep() with a arbitrary inputs.
    '''
    # setup
    sTruth = np.array([0.1, 0.2, 0.3])
    wTruth = np.array([0.3, 0.2, 0.1])
    dt = 0.1
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    w = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, w, wTruth, 3)
    
    # call truth function
    expected = truthAttitudePropagateSingleStep(sTruth, wTruth, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(3)
    attitudeEkfMath.attitudePropagateSingleStep(s, w, dt)
    result = fillNpArrayDoubleArray(attitudeEkfMath, s, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    '''
    Test:   Test attitudePropagateSingleStep() with zeros as inputs.
    '''
    # setup
    sTruth = np.zeros((3, ))
    wTruth = np.zeros((3, ))
    dt = 0.
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
    w = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, w, wTruth, 3)
    
    # call truth function
    expected = truthAttitudePropagateSingleStep(sTruth, wTruth, dt)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(3)
    attitudeEkfMath.attitudePropagateSingleStep(s, w, dt)
    result = fillNpArrayDoubleArray(attitudeEkfMath, s, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)

    '''
    Test:   Test switchMrpAndCovarianceToShadowSet() with a arbitrary inputs.
    '''
    # setup
    numStates = 6
    sTruth = np.array([0.1, 0.2, 0.3])
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
        
    Ptruth = np.random.rand(numStates, numStates) # create a random positive semi-definite matrix
    Ptruth = np.dot(Ptruth, Ptruth.T)
    P = attitudeEkfMath.new_doubleArray(numStates**2)
    setDoubleArray(attitudeEkfMath, P, np.reshape(Ptruth, (numStates**2,)), numStates**2)
    
    # call truth function
    expectedS, expectedP = truthSwitchMrpAndCovarianceToShadowSet(Ptruth, sTruth)
    
    # call c function
    attitudeEkfMath.switchMrpAndCovarianceToShadowSet(P, s)
    resultS = fillNpArrayDoubleArray(attitudeEkfMath, s, 3)
    resultP = fillNpArrayDoubleArray(attitudeEkfMath, P, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expectedS,
                                        resultS,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(expectedP,
                                        resultP,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)

    '''
    Test:   Test switchMrpAndCovarianceToShadowSet() with a zeros as inputs.
    '''
    # setup
    numStates = 6
    sTruth = np.zeros((3, ))
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
        
    Ptruth = np.zeros((numStates, numStates))
    P = attitudeEkfMath.new_doubleArray(numStates**2)
    setDoubleArray(attitudeEkfMath, P, np.reshape(Ptruth, (numStates**2,)), numStates**2)
    
    # call truth function
    expectedS, expectedP = truthSwitchMrpAndCovarianceToShadowSet(Ptruth, sTruth)
    
    # call c function
    attitudeEkfMath.switchMrpAndCovarianceToShadowSet(P, s)
    resultS = fillNpArrayDoubleArray(attitudeEkfMath, s, 3)
    resultP = fillNpArrayDoubleArray(attitudeEkfMath, P, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expectedS,
                                        resultS,
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(expectedP,
                                        resultP,
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)

    '''
    Test:   Test switchMrpAndCovarianceToShadowSet() with a zero mrp and
            non-zero P.
    '''
    # setup
    numStates = 6
    sTruth = np.array([1E-30, 0., 0.])
    s = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, s, sTruth, 3)
        
    Ptruth = np.random.rand(numStates, numStates) # create a random positive semi-definite matrix
    Ptruth = np.dot(Ptruth, Ptruth.T)
    P = attitudeEkfMath.new_doubleArray(numStates**2)
    setDoubleArray(attitudeEkfMath, P, np.reshape(Ptruth, (numStates**2,)), numStates**2)
    
    # call truth function
    expectedS, expectedP = truthSwitchMrpAndCovarianceToShadowSet(Ptruth, sTruth)
    
    # call c function
    attitudeEkfMath.switchMrpAndCovarianceToShadowSet(P, s)
    resultS = fillNpArrayDoubleArray(attitudeEkfMath, s, 3)
    resultP = fillNpArrayDoubleArray(attitudeEkfMath, P, numStates**2)

    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expectedS,
                                        resultS,
                                        accuracy,
                                        "Test 11",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(expectedP,
                                        resultP,
                                        accuracy,
                                        "Test 11",
                                        testFailCount, testMessages)

    '''
    Test:   Test computeMeasurementResdiual() for case when the measured mrp
            is greater than 1/3 and the shadow set residual is smaller.
    '''
    # setup
    numStates = 6
    angles = np.array([0., 0., 179. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sEstTruth = RigidBodyKinematics.C2MRP(C)
    sEst = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sEst, sEstTruth, 3)
    
    angles = np.array([0., 0., -170. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sMeasTruth = RigidBodyKinematics.C2MRP(C)
    sMeas = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sMeas, sMeasTruth, 3)
    
    # call truth function
    expected = truthComputeMeasurementResdiual(sEstTruth, sMeasTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(3)
    attitudeEkfMath.computeMeasurementResdiual(sEst, sMeas, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 12",
                                        testFailCount, testMessages)
    

    '''
    Test:   Test computeMeasurementResdiual() for case when the measured mrp
            is greater than 1/3 and the non-shadow set residual is smaller.
    '''
    # setup
    numStates = 6
    angles = np.array([0., 0., 90. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sEstTruth = RigidBodyKinematics.C2MRP(C)
    sEst = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sEst, sEstTruth, 3)
    
    angles = np.array([0., 0., 92. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sMeasTruth = RigidBodyKinematics.C2MRP(C)
    sMeas = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sMeas, sMeasTruth, 3)
    
    # call truth function
    expected = truthComputeMeasurementResdiual(sEstTruth, sMeasTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(3)
    attitudeEkfMath.computeMeasurementResdiual(sEst, sMeas, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 13",
                                        testFailCount, testMessages)

    '''
    Test:   Test computeMeasurementResdiual() for case when the measured mrp
            is less than 1/3.
    '''
    # setup
    numStates = 6
    angles = np.array([0., 0., 1. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sEstTruth = RigidBodyKinematics.C2MRP(C)
    sEst = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sEst, sEstTruth, 3)
    
    angles = np.array([0., 0., 2. * macros.D2R])
    C = RigidBodyKinematics.euler3212C(angles)
    sMeasTruth = RigidBodyKinematics.C2MRP(C)
    sMeas = attitudeEkfMath.new_doubleArray(3)
    setDoubleArray(attitudeEkfMath, sMeas, sMeasTruth, 3)
    
    # call truth function
    expected = truthComputeMeasurementResdiual(sEstTruth, sMeasTruth)
    
    # call c function
    result_c_array = attitudeEkfMath.new_doubleArray(3)
    attitudeEkfMath.computeMeasurementResdiual(sEst, sMeas, result_c_array)
    result = fillNpArrayDoubleArray(attitudeEkfMath, result_c_array, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 14",
                                        testFailCount, testMessages)
    
    if testFailCount == 0:
        print("PASSED: " + "test_attitudeEkfMath.py")
    else:
        print(testMessages)
        
    return [testFailCount, ''.join(testMessages)]


#
# This statement below ensures that the unitTestScript can be run as a
# stand-along python script
#
if __name__ == "__main__":
    test_attitudeEkfMath(1e-12)   
