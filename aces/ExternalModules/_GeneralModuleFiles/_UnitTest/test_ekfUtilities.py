#
# EKF Utilities Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 24 2022
#

import pytest
import os, inspect
from Basilisk.ExternalModules import ekfUtilities
from Basilisk.utilities import unitTestSupport
import numpy as np
from random import uniform
from Basilisk.utilities import macros

filename = inspect.getframeinfo(inspect.currentframe()).filename
path = os.path.dirname(os.path.abspath(filename))

def setDoubleArray(ekfUtilities, array, values, length):
    '''
    This function populates a c array that's been wrapped in python with the
    elements contained in values.
    '''
    for k in range(0, length): ekfUtilities.doubleArray_setitem(array, k, values[k])

def fillNpArrayDoubleArray(ekfUtilities, array, length):
    '''
    This functions populates a numpy array with the elements of the c array
    wrapped in python, array.
    '''
    result = np.zeros((length, ))
    for k in range(0, length): result[k] = ekfUtilities.doubleArray_getitem(array, k)
    return result

def truthComputeStateTransitionMatrix(A, I, numStates, dt):
    F = I + A * dt + A @ A * dt**2 / 2.0
    return np.reshape(F, (numStates**2, ))

def truthPropagateCovarianceMatrix(F, Q, P, numStates):
    P = F @ P @ F.T + Q
    return np.reshape(P, (numStates**2,))

def truthEkfComputeKalmanGain(P, H, R, numStates, numMeasurements):
    K = P @ H.T @ np.linalg.inv(H @ P @ H.T + R)
    return np.reshape(K, (numStates * numMeasurements, ))

def truthEkfUpdateCovariance(P, H, K, I, R, numStates):
    IKH = I - K @ H
    P = IKH @ P @ IKH.T + K @ R @ K.T
    return np.reshape(P, (numStates**2,))

def truthEkfUpdateStateEstimate(x, K, measurementResidual, numStates, numMeasurements):
    x +=  K @ (measurementResidual)
    return x
    
def test_ekfUtilities(accuracy):
    r"""
    **Validation Test Description**

    Tests all paths of the functions contained in ekfUtilities.c and
    verifies that the standard ekf equations are implemented correctly.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests

    **Description of Variables Being Tested**

    F:          state transition matrix. 
    P:          covariance matrix.
    K:          kalman gain.
    status:     determines whether or not kalman gain was succesfully computed.
    x:          state.
    """

    [testResults, testMessage] = ekfUtilitiesTestFunction(accuracy)
    assert testResults < 1, testMessage

def ekfUtilitiesTestFunction(accuracy):

    testFailCount = 0       # zero unit test result counter
    testMessages = []       # create empty array to store test log messages
    
    '''
    Test:   Test ekfComputeStateTransitionMatrix() with arbitrary inputs.
    '''
    # setup
    numStates = 3
    Atrue = np.array([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]])
    Itrue = np.eye(numStates)
    dt = 0.1
    
    A = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, A, np.reshape(Atrue, (numStates**2, )), numStates**2)
    
    I = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, I, np.reshape(Itrue, (numStates**2, )), numStates**2)
    
    # call truth function
    expected = truthComputeStateTransitionMatrix(Atrue, Itrue, numStates, dt)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    ekfUtilities.ekfComputeStateTransitionMatrix(A, I, numStates, dt, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 1",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test ekfComputeStateTransitionMatrix() with zeros as inputs. Use
            different sized matrices to get test diversity.        
    '''
    # setup
    numStates = 2
    Atrue = np.array([[0., 0.], [0., 0.]])
    Itrue = np.zeros((numStates, numStates))
    dt = 0.
    
    A = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, A, np.reshape(Atrue, (numStates**2, )), numStates**2)
    
    I = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, I, np.reshape(Itrue, (numStates**2, )), numStates**2)
    
    # call truth function
    expected = truthComputeStateTransitionMatrix(Atrue, Itrue, numStates, dt)

    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    ekfUtilities.ekfComputeStateTransitionMatrix(A, I, numStates, dt, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 2",
                                        testFailCount, testMessages)
  
    '''
    Test:   Test ekfPropagateCovarianceMatrix() with arbitrary inputs.        
    '''
    # setup
    numStates = 3
    Ftrue = np.array([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]])
    Ptrue = np.array([[2., -1., 0.], [-1., 2., -1.], [0., -1., 2.]])
    Qtrue = Ptrue * -0.5
    
    F = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, F, np.reshape(Ftrue, (numStates**2, )), numStates**2)
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    Q = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, Q, np.reshape(Qtrue, (numStates**2, )), numStates**2)
    
    # call truth function
    expected = truthPropagateCovarianceMatrix(Ftrue, Qtrue, Ptrue, numStates)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    ekfUtilities.ekfPropagateCovarianceMatrix(F, Q, P, numStates, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 3",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test ekfPropagateCovarianceMatrix() with zeros as inputs. Use
            different sized matrices to get test diversity.        
    '''
    # setup
    numStates = 2
    Ftrue = np.zeros((numStates, numStates))
    Ptrue = np.zeros((numStates, numStates))
    Qtrue = np.zeros((numStates, numStates))
    
    F = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, F, np.reshape(Ftrue, (numStates**2, )), numStates**2)
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    Q = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, Q, np.reshape(Qtrue, (numStates**2, )), numStates**2)
    
    # call truth function
    expected = truthPropagateCovarianceMatrix(Ftrue, Qtrue, Ptrue, numStates)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    ekfUtilities.ekfPropagateCovarianceMatrix(F, Q, P, numStates, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 4",
                                        testFailCount, testMessages)
   
    '''
    Test:   Test ekfComputeKalmanGain() with arbitrary inputs that don't result
            in a singular matrix attempting to be inverted.
    '''
    # setup
    numStates = 3
    numMeasurements = 2
    Ptrue = np.array([[2., -1., 0.], [-1., 2., -1.], [0., -1., 2.]])
    Htrue = np.array([[1., 0, 0.], [0., 1., 0.]])
    Rtrue = np.array([[2., -1.], [-1., 2.]]) * 1E-3
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    H = ekfUtilities.new_doubleArray(numMeasurements * numStates)
    setDoubleArray(ekfUtilities, H, np.reshape(Htrue, (numMeasurements * numStates, )), numMeasurements * numStates)
    
    R = ekfUtilities.new_doubleArray(numMeasurements**2)
    setDoubleArray(ekfUtilities, R, np.reshape(Rtrue, (numMeasurements**2, )), numMeasurements**2)
    
    # call truth function
    expected = truthEkfComputeKalmanGain(Ptrue, Htrue, Rtrue, numStates, numMeasurements)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    status = ekfUtilities.ekfComputeKalmanGain(P, H, R, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array,  numStates * numMeasurements)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 5",
                                        testFailCount, testMessages)
    
    # Verify status returned is correct.
    testFailCount, testMessages = unitTestSupport.compareDouble(0, 
                                        status, accuracy, "Test 5", 
                                        testFailCount, testMessages)    
    
    '''
    Test:   Test ekfComputeKalmanGain() with zeros as inputs. Use
            different sized matrices to get test diversity. Note that this 
            results in a matrix inverse not being possible.
    '''
    # setup
    numStates = 2
    numMeasurements = 2
    Ptrue = np.zeros((numStates, numStates))
    Htrue = np.zeros((numMeasurements, numStates))
    Rtrue = np.zeros((numMeasurements, numMeasurements))
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    H = ekfUtilities.new_doubleArray(numMeasurements * numStates)
    setDoubleArray(ekfUtilities, H, np.reshape(Htrue, (numMeasurements * numStates, )), numMeasurements * numStates)
    
    R = ekfUtilities.new_doubleArray(numMeasurements**2)
    setDoubleArray(ekfUtilities, R, np.reshape(Rtrue, (numMeasurements**2, )), numMeasurements**2)
    
    # call truth function
    #expected = truthEkfComputeKalmanGain(Ptrue, Htrue, Rtrue, numStates, numMeasurements)
    expected = np.zeros((numMeasurements * numStates, ))
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    status = ekfUtilities.ekfComputeKalmanGain(P, H, R, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    print("Note that a singular matrix is expected for this test\n\n")
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 6 pt 1",
                                        testFailCount, testMessages)
    # Verify status returned is correct.
    testFailCount, testMessages = unitTestSupport.compareDouble(1, 
                                        status, accuracy, "Test 6 pt 2", 
                                        testFailCount, testMessages)
    
    '''
    Test:   Test ekfUpdateCovariance() with arbitrary inputs.
    '''
    # setup
    numStates = 3
    numMeasurements = 2
    Ptrue = np.array([[2., -1., 0.], [-1., 2., -1.], [0., -1., 2.]])
    Htrue = np.array([[1., 0, 0.], [0., 1., 0.]])
    Rtrue = np.array([[2., -1.], [-1., 2.]]) * 1E-3
    Itrue = np.eye(numStates)
    Ktrue = np.array([[1., 2.], [3., 4.], [5., 6.]]) # arbitrary  
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    H = ekfUtilities.new_doubleArray(numMeasurements * numStates)
    setDoubleArray(ekfUtilities, H, np.reshape(Htrue, (numMeasurements * numStates, )), numMeasurements * numStates)
    
    R = ekfUtilities.new_doubleArray(numMeasurements**2)
    setDoubleArray(ekfUtilities, R, np.reshape(Rtrue, (numMeasurements**2, )), numMeasurements**2)
    
    I = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, I, np.reshape(Itrue, (numStates**2, )), numStates**2)
    
    K = ekfUtilities.new_doubleArray(numStates * numMeasurements)
    setDoubleArray(ekfUtilities, K, np.reshape(Ktrue, (numStates * numMeasurements, )), numStates * numMeasurements)
    
    # call truth function
    expected = truthEkfUpdateCovariance(Ptrue, Htrue, Ktrue, Itrue, Rtrue, numStates)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    status = ekfUtilities.ekfUpdateCovariance(P, H, K, I, R, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 7",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test ekfUpdateCovariance() with zeros as inputs. Use different sized 
            matrices to get test diversity.
    '''
    # setup
    numStates = 2
    numMeasurements = 2
    Ptrue = np.zeros((numStates, numStates))
    Htrue = np.zeros((numMeasurements, numStates))
    Rtrue = np.zeros((numMeasurements, numMeasurements))
    Itrue = np.zeros((numStates, numStates))
    Ktrue = np.zeros((numStates, numMeasurements))
    
    P = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, P, np.reshape(Ptrue, (numStates**2, )), numStates**2)
    
    H = ekfUtilities.new_doubleArray(numMeasurements * numStates)
    setDoubleArray(ekfUtilities, H, np.reshape(Htrue, (numMeasurements * numStates, )), numMeasurements * numStates)
    
    R = ekfUtilities.new_doubleArray(numMeasurements**2)
    setDoubleArray(ekfUtilities, R, np.reshape(Rtrue, (numMeasurements**2, )), numMeasurements**2)
    
    I = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, I, np.reshape(Itrue, (numStates**2, )), numStates**2)
    
    K = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, K, np.reshape(Ktrue, (numStates * numMeasurements, )), numStates * numMeasurements)
    
    # call truth function
    expected = truthEkfUpdateCovariance(Ptrue, Htrue, Ktrue, Itrue, Rtrue, numStates)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    status = ekfUtilities.ekfUpdateCovariance(P, H, K, I, R, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates**2)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)

    '''
    Test:   Test ekfUpdateStateEstimate() with arbitrary inputs.
    '''
    # setup
    numStates = 3
    numMeasurements = 2
    Xtrue = np.array([1., 2., 3.])
    Ktrue = np.array([[1., 2.], [3., 4.], [5., 6.]])
    measurementResidualTrue = np.array([0.1, 0.2])
    
    X = ekfUtilities.new_doubleArray(numStates)
    setDoubleArray(ekfUtilities, X, np.reshape(Xtrue, (numStates, )), numStates)
    
    K = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, K, np.reshape(Ktrue, (numStates * numMeasurements, )), numStates * numMeasurements)
    
    measurementResidual = ekfUtilities.new_doubleArray(numMeasurements)
    setDoubleArray(ekfUtilities, measurementResidual, np.reshape(measurementResidualTrue, (numMeasurements, )), numMeasurements)
    
    # call truth function
    expected = truthEkfUpdateStateEstimate(Xtrue, Ktrue, measurementResidualTrue, numStates, numMeasurements)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates)
    status = ekfUtilities.ekfUpdateStateEstimate(X, K, measurementResidual, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)

    
    '''
    Test:   Test ekfUpdateStateEstimate() with zeros as inputs. Use different sized 
            matrices to get test diversity.
    '''
    # setup
    numStates = 2
    numMeasurements = 2
    Xtrue = np.zeros((numStates, ))
    Ktrue = np.zeros((numStates, numMeasurements))
    measurementResidualTrue = np.zeros((numMeasurements, ))
    
    K = ekfUtilities.new_doubleArray(numStates**2)
    setDoubleArray(ekfUtilities, K, np.reshape(Ktrue, (numStates * numMeasurements, )), numStates * numMeasurements)
    
    X = ekfUtilities.new_doubleArray(numStates)
    setDoubleArray(ekfUtilities, X, np.reshape(Xtrue, (numStates, )), numStates)
    
    measurementResidual = ekfUtilities.new_doubleArray(numMeasurements)
    setDoubleArray(ekfUtilities, measurementResidual, np.reshape(measurementResidualTrue, (numMeasurements, )), numMeasurements)
    
    # call truth function
    expected = truthEkfUpdateStateEstimate(Xtrue, Ktrue, measurementResidualTrue, numStates, numMeasurements)
    
    # call c function
    result_c_array = ekfUtilities.new_doubleArray(numStates**2)
    status = ekfUtilities.ekfUpdateStateEstimate(X, K, measurementResidual, numStates, numMeasurements, result_c_array)
    result = fillNpArrayDoubleArray(ekfUtilities, result_c_array, numStates)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)
    

    if testFailCount == 0:
        print("PASSED: " + "test_ekfUtilities.py")
    else:
        print(testMessages)
        
    return [testFailCount, ''.join(testMessages)]


#
# This statement below ensures that the unitTestScript can be run as a
# stand-alone python script.
#
if __name__ == "__main__":
    test_ekfUtilities(1e-12)   
