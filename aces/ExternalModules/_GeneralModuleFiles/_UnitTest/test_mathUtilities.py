#
# Math Utilities Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 26 2022
#

import pytest
import os, inspect
from Basilisk.ExternalModules import mathUtilities
from Basilisk.utilities import unitTestSupport
import numpy as np
from Basilisk.utilities import RigidBodyKinematics

filename = inspect.getframeinfo(inspect.currentframe()).filename
path = os.path.dirname(os.path.abspath(filename))

def setDoubleArray(mathUtilities, array, values, length):
    '''
    This function populates a c array that's been wrapped in pyhton with the
    elements contained in values.
    '''
    for k in range(0, length): mathUtilities.doubleArray_setitem(array, k, values[k])

def fillNpArrayDoubleArray(mathUtilities, array, length):
    '''
    This functions populates a numpy array with the elements of the c array
    wrapped in python, array.
    '''
    result = np.zeros((length, ))
    for k in range(0, length): result[k] = mathUtilities.doubleArray_getitem(array, k)
    return result
    
def test_mathUtilities(accuracy):
    r"""
    **Validation Test Description**

    This tests all paths of the functions contained in mathUtilities.c.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
    """

    [testResults, testMessage] = mathUtilitiesTestFunction(accuracy)
    assert testResults < 1, testMessage

def mathUtilitiesTestFunction(accuracy):

    testFailCount = 0       # zero unit test result counter
    testMessages = []       # create empty array to store test log messages


    '''
    Test:   Test v3TildeM() with an arbitrary input.
    '''
    # setup
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([1., 2., 3.]), 3)
    
    # define expected result
    expected = np.array(RigidBodyKinematics.v3Tilde([1., 2., 3.]))
    expected = np.reshape(expected, (9, ))
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(9)
    mathUtilities.v3TildeM(v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 9)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 1",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test v3TildeM() with zero vector for input.
    '''
    # setup
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([0., 0., 0.]), 3)
    
    # define expected result
    expected = np.array(RigidBodyKinematics.v3Tilde([0., 0., 0.]))
    expected = np.reshape(expected, (9, ))
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(9)
    mathUtilities.v3TildeM(v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 9)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 2",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test BMatMrpM() with an arbitrary input.
    '''
    # setup
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([1., 2., 3.]), 3)
    
    # define expected result
    expected = RigidBodyKinematics.BmatMRP([1., 2., 3.])
    expected = np.reshape(expected, (9, ))
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(9)
    mathUtilities.BMatMrpM(v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 9)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 3",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test BMatMrpM() with zero vector for input.
    '''
    # setup
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([0., 0., 0.]), 3)
    
    # define expected result
    expected = RigidBodyKinematics.BmatMRP([0., 0., 0.])
    expected = np.reshape(expected, (9, ))
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(9)
    mathUtilities.BMatMrpM(v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 9)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 4",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test rotateVectorWithMrp() with an arbitrary input.
    '''
    # setup
    sigma = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, sigma, np.array([1., 2., 3.]), 3)
    
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([1., 2., 3.]), 3)
    
    # define expected result
    C = RigidBodyKinematics.MRP2C(np.array([1., 2., 3.]))
    expected = C @ np.array([1., 2., 3.])
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(3)
    mathUtilities.rotateVectorWithMrp(sigma, v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 5",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test rotateVectorWithMrp() with zeros for the inputs.
    '''
    # setup
    sigma = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, sigma, np.array([0, 0., 0.]), 3)
    
    v = mathUtilities.new_doubleArray(3)
    setDoubleArray(mathUtilities, v, np.array([0., 0., 0.]), 3)
    
    # define expected result
    C = RigidBodyKinematics.MRP2C(np.array([0., 0., 0.]))
    expected = C @ np.array([0., 0., 0.])
    
    # call c function
    result_c_array = mathUtilities.new_doubleArray(3)
    mathUtilities.rotateVectorWithMrp(sigma, v, result_c_array)
    result = fillNpArrayDoubleArray(mathUtilities, result_c_array, 3)
    
    # check expected against result
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        result,
                                        accuracy,
                                        "Test 6",
                                        testFailCount, testMessages)
    
    
    if testFailCount == 0:
        print("PASSED: " + "test_mathUtilities.py")
    else:
        print(testMessages)
        
    return [testFailCount, ''.join(testMessages)]


#
# This statement below ensures that the unitTestScript can be run as a
# stand-along python script
#
if __name__ == "__main__":
    test_mathUtilities(1e-12)   
