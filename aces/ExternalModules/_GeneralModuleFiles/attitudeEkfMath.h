#ifndef _ATTITUDE_EKF_MATH_H_
#define _ATTITUDE_EKF_MATH_H_

#ifdef __cplusplus
extern "C" {
#endif

void computeDynamicsMatrix(double *sigmaEst_BN, double *wBiasCompensatedBN_B, double *A);
void computeDiscreteTimeProcessNoiseForGyro(double gyroRateNoiseStd, double gyroBiasNoiseStd, double dt, double *Qgyro);
void computeProcessNoiseTransitionMatrix(double *sigmaEst_BN, double *G);
void computeProcessNoiseMatrix(double gyroRateNoiseStd, double gyroBiasNoiseStd, double *sigmaEst_BN, double *G, double *Q, double *Qgyro, double dt, double *Qtotal);
void attitudePropagateSingleStep(double *sigmaEst_BN, double *wBiasCompensatedBN_B, double dt);
void switchMrpAndCovarianceToShadowSet(double *P, double *sigmaEst_BN);
void computeMeasurementResdiual(double *sigmaEst_BN, double *sigmaMeasured_BN, double *residual);

#ifdef __cplusplus
}
#endif


#endif
