/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "ekfUtilities.h"
#include "architecture/utilities/linearAlgebra.h"
#include <stddef.h>


/*! Computes the state transition matrix using a first order approximation via a Taylor series expansion.
 @return void
 @param A State dynamics matrix.
 @param I Identity matrix.
 @param numStates Number of states in the filter. This should equal the number of rows and cols of both the A and I matrix.
 @param dt The propagation time step.
 @param result The matrix the state transition matrix is stored in.
 */
void ekfComputeStateTransitionMatrix(double *A, double *I, size_t numStates, double dt, double *result)
{
    // I + A*dt + A*dt^2/2
    double A_dt[EKF_MAX_DIM*EKF_MAX_DIM];
    double A_dt2_2[EKF_MAX_DIM*EKF_MAX_DIM];
    mScale(dt, A, numStates, numStates, A_dt);
    mMultM(A, numStates, numStates, A, numStates, numStates, A_dt2_2);
    mScale(dt*dt / 2.0, A_dt2_2, numStates, numStates, A_dt2_2);
    mAdd(I, numStates, numStates, A_dt, result);
    mAdd(result, numStates, numStates, A_dt2_2, result);
}


/*! Propagated the covariance matrix forward by dt seconds. Accounts for uncertainty in dynamics through the matrix Q.
 @return void
 @param F State transition  matrix.
 @param Q Discrete time process noise matrix.
 @param P Covariance matrix.
 @param numStates Number of states in the filter.
 @param result The matrix the propagated covariance matrix is stored in.
 */
void ekfPropagateCovarianceMatrix(double *F, double *Q, double *P, size_t numStates, double *result)
{
    double Ftranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    double F_P[EKF_MAX_DIM * EKF_MAX_DIM];
    double F_P_Ft[EKF_MAX_DIM * EKF_MAX_DIM];
    
    // F * P * F^T
    mTranspose(F, numStates, numStates, Ftranspose);
    mMultM(F, numStates, numStates, P, numStates, numStates, F_P);
    mMultM(F_P, numStates, numStates, Ftranspose, numStates, numStates, F_P_Ft);
    
    // F * P * F^T + Q
    mAdd(F_P_Ft, numStates, numStates, Q, result);
}


/*! Computes the Kalman gain.
 @return void
 @param P Covariance matrix.
 @param H Measurement sensitivity matrix.
 @param R Measurement noise covariance matrix.
 @param numStates Number of states in the filter.
 @param numMeasurements Number of measurements.
 @param result The matrix the kalman gain is stored in.
 */
int ekfComputeKalmanGain(double *P, double *H, double *R, size_t numStates, size_t numMeasurements, double *result)
{
    double Htranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    double P_Ht[EKF_MAX_DIM * EKF_MAX_DIM];
    double H_P_Ht[EKF_MAX_DIM * EKF_MAX_DIM];
    double P_Ht_P_R[EKF_MAX_DIM * EKF_MAX_DIM];
    double P_Ht_P_R_Inv[EKF_MAX_DIM * EKF_MAX_DIM];
    int status;
    
    // P * H^T
    mTranspose(H, numMeasurements, numStates, Htranspose);
    mMultM(P, numStates, numStates, Htranspose, numStates, numMeasurements, P_Ht);
    
    // (H * P * H^T + R)^-1
    mMultM(H, numMeasurements, numStates, P_Ht, numStates, numMeasurements, H_P_Ht);
    mAdd(H_P_Ht, numMeasurements, numMeasurements, R, P_Ht_P_R);
    status = mInverse(P_Ht_P_R, numMeasurements, P_Ht_P_R_Inv);
    
    // P * H^T * (H * P * H^T + R)^-1
    mMultM(P_Ht, numStates, numMeasurements, P_Ht_P_R_Inv, numMeasurements, numMeasurements, result);
    
    return status;
}


/*! Updates the covariance. Used when the filter ingests a valid measurement.
 @return void
 @param P Covariance matrix.
 @param H Measurement sensitivity matrix.
 @param K Kalman gain matrix.
 @param I Identity matrix.
 @param R Measurement noise covariance matrix.
 @param numStates Number of states in the filter.
 @param numMeasurements Number of measurements.
 @param result The matrix the updated covariance matrix is stored in.
 */
void ekfUpdateCovariance(double *P, double *H, double *K, double *I, double *R, size_t numStates, size_t numMeasurements, double *result)
{
    double K_H[EKF_MAX_DIM * EKF_MAX_DIM];
    double I_KH[EKF_MAX_DIM * EKF_MAX_DIM];
    double I_KHtranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    double K_R[EKF_MAX_DIM * EKF_MAX_DIM];
    double Ktranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    double K_R_Ktranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    double I_KH_P[EKF_MAX_DIM * EKF_MAX_DIM];
    double I_KH_P_I_KHtranspose[EKF_MAX_DIM * EKF_MAX_DIM];
    
    // (I - K * H)
    mMultM(K, numStates, numMeasurements, H, numMeasurements, numStates, K_H);
    mSubtract(I, numStates, numStates, K_H, I_KH);
    
    // (I - K * H)^T
    mTranspose(I_KH, numStates, numStates, I_KHtranspose);
    
    // (I - K * H) * P
    mMultM(I_KH, numStates, numStates, P, numStates, numStates, I_KH_P);
    
    // (I - K * H) * P * (I - K * H)^T
    mMultM(I_KH_P, numStates, numStates, I_KHtranspose, numStates, numStates, I_KH_P_I_KHtranspose);
    
    // K * R
    mMultM(K, numStates, numMeasurements, R, numMeasurements, numMeasurements, K_R);
    
    // K * R * K^T
    mTranspose(K, numStates, numMeasurements, Ktranspose);
    mMultM(K_R, numStates, numMeasurements, Ktranspose, numMeasurements, numStates, K_R_Ktranspose);
    
    // (I - K * H) * P * (I - K * H)^T + K * R * K^T
    mAdd(I_KH_P_I_KHtranspose, numStates, numStates, K_R_Ktranspose, result);
}


/*! Updates the filter state given a measurement residual and the Kalman gain.
 @return void
 @param x Current filter state estimate.
 @param K Kalman gain matrix.
 @param measurementResidual Current residual of the filter.
 @param numStates Number of states in the filter.
 @param numMeasurements Number of measurements.
 @param result The array the updated estimate is stored in.
 */
void ekfUpdateStateEstimate(double *x, double *K, double *measurementResidual, size_t numStates, size_t numMeasurements, double *result)
{
    double dx[EKF_MAX_DIM];
    
    // x + K * measurementResidual
    mMultV(K, numStates, numMeasurements, measurementResidual, dx);
    vAdd(x, numStates, dx, result);
}
