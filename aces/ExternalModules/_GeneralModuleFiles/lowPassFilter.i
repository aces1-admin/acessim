%module lowPassFilter
%{
  #include "lowPassFilter.h"
%}

%include "lowPassFilter.h"

%pythoncode %{
from Basilisk.architecture.swig_common_model import *
%}
%include "swig_conly_data.i"
%constant void filterSetCoeffs(LPF*, double*, double*);
%ignore filterSetCoeffs;
%constant void filterReset(LPF*);
%ignore filterReset;
%constant double filter(LPF*, double);
%ignore filter;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}
