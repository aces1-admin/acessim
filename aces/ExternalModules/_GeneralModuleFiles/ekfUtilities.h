#ifndef _EKF_UTILITIES_H_
#define _EKF_UTILITIES_H_

#include <stdint.h>
#include <string.h>
#include<architecture/utilities/bskLogging.h>

//! Max allowable filter state dimension.
#define EKF_MAX_DIM 20


#ifdef __cplusplus
extern "C" {
#endif
    void ekfComputeStateTransitionMatrix(double *A, double *I, size_t numStates, double dt, double *result);
    void ekfPropagateCovarianceMatrix(double *F, double *Q, double *P, size_t numStates, double *result);
    int  ekfComputeKalmanGain(double *P, double *H, double *R, size_t numStates, size_t numMeasurements, double *result);
    void ekfUpdateCovariance(double *P, double *H, double *K, double *I, double *R, size_t numStates, size_t numMeasurements, double *result);
    void ekfUpdateStateEstimate(double *x, double *K, double *measurementResidual, size_t numStates, size_t numMeasurements, double *result);

#ifdef __cplusplus
}
#endif


#endif
