#ifndef _MATH_UTILITY_H_
#define _MATH_UTILITY_H_


#ifdef __cplusplus
extern "C" {
#endif

void v3TildeM(double v[3], void *result);
void BMatMrpM(double q[3], void *result);
void rotateVectorWithMrp(double q[3], double v[3], double result[3]);

#ifdef __cplusplus
}
#endif


#endif
