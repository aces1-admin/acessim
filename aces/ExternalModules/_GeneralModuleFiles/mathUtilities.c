/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "mathUtilities.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/rigidBodyKinematics.h"


/*! Computes tilde matrix given an array, v. This matrix is also commonly referred to as the cross-product matrix.
 @return void
 @param v Vector to create tilde matrix from.
 @param result The matrix the tilde matrix is stored in.
 */
void v3TildeM(double v[3], void *result)
{
    double *m_result = (double *)result;
    m_result[MXINDEX(3, 0, 0)] = 0.0;
    m_result[MXINDEX(3, 0, 1)] = -v[2];
    m_result[MXINDEX(3, 0, 2)] = v[1];
    m_result[MXINDEX(3, 1, 0)] = v[2];
    m_result[MXINDEX(3, 1, 1)] = 0.0;
    m_result[MXINDEX(3, 1, 2)] = -v[0];
    m_result[MXINDEX(3, 2, 0)] = -v[1];
    m_result[MXINDEX(3, 2, 1)] = v[0];
    m_result[MXINDEX(3, 2, 2)] = 0.0;
}


/*! Computes the 3x3 matrix which relates the body angular velocity to the MRP rate through the B matrix.
    dq/dt = 1/4 [B(q)] w
 @return void
 @param q Current MRP.
 @param result The matrix the B matrix is stored in.
 */
void BMatMrpM(double q[3], void *result)
{
    double s2;
    double *B = (double *)result;

    s2 = v3Dot(q, q);
    B[MXINDEX(3, 0, 0)] = 1 - s2 + 2 * q[0] * q[0];
    B[MXINDEX(3, 0, 1)] = 2 * (q[0] * q[1] - q[2]);
    B[MXINDEX(3, 0, 2)] = 2 * (q[0] * q[2] + q[1]);
    B[MXINDEX(3, 1, 0)] = 2 * (q[1] * q[0] + q[2]);
    B[MXINDEX(3, 1, 1)] = 1 - s2 + 2 * q[1] * q[1];
    B[MXINDEX(3, 1, 2)] = 2 * (q[1] * q[2] - q[0]);
    B[MXINDEX(3, 2, 0)] = 2 * (q[2] * q[0] - q[1]);
    B[MXINDEX(3, 2, 1)] = 2 * (q[2] * q[1] + q[0]);
    B[MXINDEX(3, 2, 2)] = 1 - s2 + 2 * q[2] * q[2];
}

/*! Rotates the vector v given the current MRP, q.
 @return void
 @param q Current MRP.
 @param v Vector to be rotation by q.
 @param result The array the rotated vector is stored in.
 */
void rotateVectorWithMrp(double q[3], double v[3], double result[3])
{
    double C[3][3];
    MRP2C(q, C);
    m33MultV3(C, v, result);
}
