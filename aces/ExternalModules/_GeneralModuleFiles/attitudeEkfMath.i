%module attitudeEkfMath
%{
  #include "attitudeEkfMath.h"
%}

%include "attitudeEkfMath.h"

%pythoncode %{
from Basilisk.architecture.swig_common_model import *
%}

%include "swig_conly_data.i"

%constant void computeMeasurementResdiual(double*, double*, double*);
%ignore computeMeasurementResdiual;
%constant void computeDynamicsMatrix(double*, double*, double*);
%ignore computeDynamicsMatrix;
%constant void computeDiscreteTimeProcessNoiseForGyro(double, double, double, double*);
%ignore computeDiscreteTimeProcessNoiseForGyro;
%constant void computeProcessNoiseTransitionMatrix(double*, double*);
%ignore computeProcessNoiseTransitionMatrix;
%constant void computeProcessNoiseMatrix(double, double, double*, double*, double*, double*, double, double*);
%ignore computeProcessNoiseMatrix;
%constant void attitudePropagateSingleStep(double*, double*, double);
%ignore attitudePropagateSingleStep;
%constant void switchMrpAndCovarianceToShadowSet(double*, double*);
%ignore switchMrpAndCovarianceToShadowSet;


%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

