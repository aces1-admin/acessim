Executive Summary
-----------------
This module implements a discrete time model of a mems gyroscope. At the time this message was wriiten, this includes modeling of errors due to rate and angle random walk.

Message Connection Descriptions
-------------------------------
The following table lists all the module input and output messages.  
The module msg connection is set by the user from python.  
The msg type contains a link to the message structure definition, while the description 
provides information on what this message is used for.

.. list-table:: Module I/O Messages
    :widths: 25 25 50
    :header-rows: 1

    * - Msg Variable Name
      - Msg Type
      - Description
    * - scStateInMsg
      - :ref:`SCStatesMsgPayload`
      - [-] Input message for spacecraft states.
    * - gyroOutMsg
      - :ref:`GyroMsgPayload`
      - [-] Output message for Gyro.

