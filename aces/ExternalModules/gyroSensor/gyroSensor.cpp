/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "ExternalModules/gyroSensor/gyroSensor.h"
#include "architecture/utilities/avsEigenSupport.h"
#include "architecture/utilities/rigidBodyKinematics.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/macroDefinitions.h"
#include "../_GeneralModuleFiles/mathUtilities.h"
#include <cstring>
#include <random> // used for gaussion noise generation
#include <cmath>

/*! This is the constructor for the module class.  It sets default variable
    values and initializes the various parts of the model */
GyroSensor::GyroSensor()
{
    this->rateNoiseStd = 0.0;
    this->biasNoiseStd = 0.0;
    this->rngSeed = 0x1badcad1;
    this->firstCycle = true;
    this->wBias_GN_G.fill(0.0);
    std::normal_distribution<double>::param_type meanPlusStddev(0.0, 1.0);
    this->normalDist.param(meanPlusStddev);
    this->wBiasInit.fill(0.0);
    v3SetZero(this->sigma_GB);
    this->wBias_GN_G.fill(0.0);
    this->wBiasPrevious_GN_G.fill(0.0);
    this->wPlusBiasPlusNoise.fill(0.0);
    this->PreviousSimNanos = 0;
    this->dtModule = 0.0;
    this->noiseForRate.fill(0.0);
    this->noiseForBias.fill(0.0);
    this->randNumHolder1.fill(0.0);
    this->randNumHolder2.fill(0.0);
}


/*! Module Destructor */
GyroSensor::~GyroSensor()
{
}


/*! This method is used to reset the module and checks that required input messages are connected.
    @return void
*/
void GyroSensor::Reset(uint64_t CurrentSimNanos)
{
    // Check that required input message is connected.
    if (!this->scStateInMsg.isLinked()) {
        bskLogger.bskLog(BSK_ERROR, "GyroSensor.scStateInMsg was not linked.");
    }
    
    this->firstCycle = true;
    this->wBias_GN_G = this->wBiasInit;
    this->wBiasPrevious_GN_G = this->wBiasInit;
    this->PreviousSimNanos = 0;
    this->dtModule = 0.0;
}


/*! This is the main method that gets called every time the module is updated. This method outputs the noisy angular rates.
    @return void
*/
void GyroSensor::UpdateState(uint64_t CurrentSimNanos)
{
    // Declare local copies of message buffers.
    SCStatesMsgPayload scStateInMsgBuffer;      //!< local copy of message buffer
    GyroMsgPayload gyroOutMsgBuffer;            //!< local copy of message buffer

    // Read the input message and zero output message.
    scStateInMsgBuffer = this->scStateInMsg();
    gyroOutMsgBuffer = this->gyroOutMsg.zeroMsgPayload;

    /*
     * Apply the noise to the gyro rate and gyro bias using Eq. 4.54a and 5.54b
     * from Fundamentals of Spacecraft Attitude Determination and Control,
     * ISBN 978-1-4939-0801-1. Note that on the first iteration we cannot
     * implement Eq. 4.54a directly. Instead, we assume the average bias is
     * the given initial bias and do not update the bias using 4.54b.
     */
    dtModule = (CurrentSimNanos - PreviousSimNanos) * NANO2SEC;
    this->noiseForRate.fill(0.0);
    this->noiseForBias.fill(0.0);
    this->randNumHolder1.fill(0.0);
    this->randNumHolder2.fill(0.0);
    
    if (!this->firstCycle && dtModule > DB0_EPS)
    {
        /*
         * Generate random numbers using a normal distribution
         * These numbers will be scaled by the gyro noise properties
         * in the next section of the code.
         */
        this->randNumHolder1[0] = this->normalDist(this->randNumGen);
        this->randNumHolder1[1] = this->normalDist(this->randNumGen);
        this->randNumHolder1[2] = this->normalDist(this->randNumGen);
        
        this->randNumHolder2[0] = this->normalDist(this->randNumGen);
        this->randNumHolder2[1] = this->normalDist(this->randNumGen);
        this->randNumHolder2[2] = this->normalDist(this->randNumGen);
        
        this->noiseForRate = this->randNumHolder1 * sqrt((rateNoiseStd * rateNoiseStd) / dtModule + 1.0 / 12.0 * biasNoiseStd * biasNoiseStd * dtModule);
        this->noiseForBias = this->randNumHolder2 * sqrt(dtModule) * biasNoiseStd;
    }
    
    // Update bias.
    this->wBias_GN_G = this->wBiasPrevious_GN_G + this->noiseForBias;
    
    // Rotate angular velocity vector from the Body frame into the Gyro frame.
    double truthOmega_GN_G[3];
    rotateVectorWithMrp(this->sigma_GB, scStateInMsgBuffer.omega_BN_B, truthOmega_GN_G);
    
    // Add bias and noise to each gyro axis.
    Eigen::Vector3d avgBias = 0.5 * (this->wBias_GN_G + this->wBiasPrevious_GN_G);
    this->wPlusBiasPlusNoise = cArray2EigenVector3d(truthOmega_GN_G) + avgBias + this->noiseForRate;
    
    // Copy the noisy gyro data into the state output message c array.
    eigenVector3d2CArray(this->wPlusBiasPlusNoise, gyroOutMsgBuffer.omega_GN_G);
    
    // Validate and time stamp data.
    gyroOutMsgBuffer.valid = 1;
    gyroOutMsgBuffer.timeTag = this->scStateInMsg.timeWritten();
    
    // Write to the output message.
    this->gyroOutMsg.write(&gyroOutMsgBuffer, this->moduleID, CurrentSimNanos);

    // Set firstCycle flag to false now that the first execution cycle is over.
    this->firstCycle = false;
    
    // Hold onto the previous gyro bias and call time.
    this->wBiasPrevious_GN_G = this->wBias_GN_G;
    this->PreviousSimNanos = CurrentSimNanos;
}
