
#ifndef GYROSENSOR_H
#define GYROSENSOR_H

#include "architecture/_GeneralModuleFiles/sys_model.h"
#include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
#include "architecture/utilities/bskLogging.h"
#include "architecture/messaging/messaging.h"
#include "msgPayloadDefC/GyroMsgPayload.h"
#include <stdint.h>
#include <random>
#include <Eigen/Dense>

/*! @brief Discrete time model of mems gyroscope.
 */
class GyroSensor: public SysModel {
public:
    GyroSensor();
    ~GyroSensor();
    void Reset(uint64_t CurrentSimNanos);
    void UpdateState(uint64_t CurrentSimNanos);
    void setRngSeed(uint64_t newSeed){this->randNumGen.seed(newSeed); this->rngSeed = newSeed;}

public:
    ReadFunctor<SCStatesMsgPayload> scStateInMsg;   //!< [-] Input message for spacecraft states
    Message<GyroMsgPayload> gyroOutMsg;             //!< [-] Output message for Gyro
    BSKLogger bskLogger;                            //!< BSK Logging
    double rateNoiseStd;                            //!< [rad / s^(1/2)] Standard deviation of gyro rate noise
    double biasNoiseStd;                            //!< [rad / s^(3/2)] Standard deviation of gyro bias noise
    Eigen::Vector3d wBiasInit;                      //!< [rad / s] Initial gyro bias
    double sigma_GB[3];                             //!< MRP rotation from the Body to the Gyro frame
    bool firstCycle;                                //!< Flag that is high when the first cycle has not finished running
    Eigen::Vector3d wBias_GN_G;                     //!< [r/s] Rotational Sensor bias value
    Eigen::Vector3d wBiasPrevious_GN_G;             //!< [r/s] Rotational Sensor bias value
    Eigen::Vector3d wPlusBiasPlusNoise;             //!< [r/s] Rotational Sensor bias value
    uint64_t PreviousSimNanos;                      //!< [ns] The previous call time
    double dtModule;                                //!< [ns] Difference between current and previous sim call time
    Eigen::Vector3d noiseForRate;                   //!< [rad / s)] Vector of noise applied to angular rate
    Eigen::Vector3d noiseForBias;                   //!< [rad / s)] Vector of noise applied to angular rate bias
    Eigen::Vector3d randNumHolder1;                 //!< Container for random number
    Eigen::Vector3d randNumHolder2;                 //!< Container for random number

private:
    uint64_t rngSeed;                               //!< -- Seed for random number generator
    std::minstd_rand randNumGen;                    //!< -- Random number generator for model
    std::normal_distribution<double> normalDist;    //!< -- Random number distribution for model
};


#endif
