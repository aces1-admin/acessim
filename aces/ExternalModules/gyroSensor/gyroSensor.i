%module gyroSensor
%{
    #include "gyroSensor.h"
%}

%pythoncode %{
    from Basilisk.architecture.swig_common_model import *
%}
%include "std_string.i"
%include "swig_conly_data.i"
%include "swig_eigen.i"
%include "sys_model.h"
%include "gyroSensor.h"

%include "msgPayloadDefC/GyroMsgPayload.h"
struct GyroMsg_C;

%include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
struct SCStatesMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

