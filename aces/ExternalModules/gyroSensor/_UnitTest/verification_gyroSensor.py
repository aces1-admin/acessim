#
# Gyro Sensor Verification.
#
# Author:   Henry Macanas
# Creation Date:  Mar 26 2022
#

import pytest

from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.ExternalModules import gyroSensor
from Basilisk.utilities import RigidBodyKinematics
from Basilisk.simulation import spacecraft
from Basilisk.ExternalModules import gyroSensor
import matplotlib.pyplot as plt
import numpy.matlib
import numpy as np

def allanDeviation(w, fs, pts):
    """
    Calculates the Allan deviation over various time bins, T.
    """
    N = np.shape(w)[0]
    M = 1
    end = int(np.floor(np.log2(N / 2.)))
    n = 2.**(np.linspace(0, end, end + 1))
    maxN = n[-1]
    endLogInc = np.log10(maxN)
    m = np.unique(np.ceil(np.logspace(0, endLogInc, pts))).astype(int)
    
    t0 = 1. / fs
    T = t0 * m
    theta = np.cumsum(w) / fs
    sigma2 = np.zeros(len(T), );
    
    for ii in range(0, len(m)):
        for kk in range(0, N - 2 * m[ii]):
            sigma2[ii] = sigma2[ii] + (theta[kk+2*m[ii]] - 2*theta[kk+m[ii]] + theta[kk])**2
    
    sigma2 = sigma2 / np.matlib.repmat((2*T**2 * (N-2*m)), 1 , M)[0]
    sigma = np.sqrt(sigma2)
    return T, sigma

def plot_imu_angular_rate(timeData, rate):
    """Plot the body angular velocity rate tracking errors."""
    plt.figure()
    for idx in range(3):
        plt.plot(timeData, rate[:, idx] / macros.D2R,
                 color=unitTestSupport.getLineColor(idx, 3),
                 label=r'$\omega_{GN,' + str(idx) + '}$')
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Gyro Angular Rate [deg/s] ')
    plt.grid(True)
    
def plot_angular_rate(timeData, rate):
    """Plot the body angular velocity rate tracking errors."""
    plt.figure()
    for idx in range(3):
        plt.plot(timeData, rate[:, idx] / macros.D2R,
                 color=unitTestSupport.getLineColor(idx, 3),
                 label=r'$\omega_{BN,' + str(idx) + '}$')
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('True Angular Rate [deg/s] ')
    plt.grid(True)
    
def test_gyroSensor(showPlots, accuracy):
    r"""
    **Validation Test Description**

    Verifies that angle and rate random walk values computed from gyro output
    align with those configured in the gyro sensor.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
        showPlots (bool): flag that is high when plots will be shown
    """
    [testResults, testMessage] = gyroSensorTestFunction(showPlots, accuracy)
    assert testResults < 1, testMessage

def gyroSensorTestFunction(showPlots, accuracy):
    testFailCount = 0
    testMessages = []
    
    #
    # Setup simulation.
    simTaskName = "simTask"
    simProcessName = "simProcess"
    scSim = SimulationBaseClass.SimBaseClass()
    dynProcess = scSim.CreateNewProcess(simProcessName)
    simulationTimeStep = macros.sec2nano(1 / 1.)
    dynProcess.addTask(scSim.CreateNewTask(simTaskName, simulationTimeStep))
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = "spacecraftBody"
    
    I = [900., 0., 0.,
         0., 800., 0.,
         0., 0., 600.]
    scObject.hub.mHub = 750.0
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]]
    scObject.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(I)
    scObject.hub.sigma_BNInit = [[0.], [0.], [0.]]
    scObject.hub.omega_BN_BInit = [[0.0], [0.0], [0.0]]
    scSim.AddModelToTask(simTaskName, scObject)

    #
    # Setup gyro.
    gyro = gyroSensor.GyroSensor()
    gyro.ModelTag = "gyroSensor"
    gyro.rateNoiseStd = 0.007 * macros.D2R
    gyro.biasNoiseStd = 1.0 * macros.D2R / 3600. / np.sqrt(1.0 / 1.0)
    gyro.wBiasInit = np.array([0, 0., 0.]) * macros.D2R
    gyro.sigma_GB = [0.0, 0.0, 0.0]
    scSim.AddModelToTask(simTaskName, gyro)
    gyro.scStateInMsg.subscribeTo(scObject.scStateOutMsg)
    
    #
    # Setup recorders.
    gyroOutMsgRec = gyro.gyroOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, gyroOutMsgRec)
    
    scStateRecorder = scObject.scStateOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, scStateRecorder)
    
    #
    # Execute simulation.
    simulationTime = macros.min2nano(60.)
    scSim.InitializeSimulation()
    scSim.ConfigureStopTime(simulationTime)
    scSim.ExecuteSimulation()
    
    #
    # Compute Allan deviation
    w = np.array(gyroOutMsgRec.omega_GN_G[:, 0])
    pts = len(w)
    fs = 1.                                 # needs to be in line with sim freq
    tau, adev = allanDeviation(w, fs, pts)

    #
    # Compute angle random walk. This is the first idx of adev since we are.
    # running the sim at 1Hz.
    arw = adev[0]
    
    #
    # Compute the rate random walk using eqs from:
    # https://www.mathworks.com/help/fusion/ug/inertial-sensor-noise-analysis-using-allan-variance.html
    
    # Note that the idx of 70 was chosen such the an adev value was selected
    # from the correct region of the curve (just after the minimum).
    rrw = np.sqrt(3. * adev[70]**2 / tau[70])
        

    # Verify that arw is close to specified rate noise. Note that some error
    # is expected as the model is running at a high-ish time step of 1Hz. As
    # such an accuracy of 1E-5 is selected.
    testFailCount, testMessages = unitTestSupport.compareDouble(arw, 
                                        gyro.rateNoiseStd, accuracy, "Test 1", 
                                        testFailCount, testMessages)
    
    # Verify that rrw is close to specified bias noise. Note that some error
    # is expected as the model is running at a high-ish time step of 1Hz. As
    # such an accuracy of 1E-5 is selected.
    testFailCount, testMessages = unitTestSupport.compareDouble(rrw, 
                                        gyro.biasNoiseStd, accuracy, "Test 2", 
                                        testFailCount, testMessages)
    if testFailCount == 0:
        print("PASSED: " + "verification_gyroSensor.py")
    else:
        print(testMessages)

    if showPlots:

        simTimes = scStateRecorder.times() * macros.NANO2SEC
        
        # imu angular rate
        plot_imu_angular_rate(simTimes, gyroOutMsgRec.omega_GN_G)
        
        # true angular rate
        plot_angular_rate(simTimes, scStateRecorder.omega_BN_B)
        
        # Allan deviations
        plt.figure()
        plt.loglog(tau, adev, color=unitTestSupport.getLineColor(0, 1))
        plt.ylabel("Allan Devaition")
        plt.xlabel("Tau")
        plt.grid(True)
        
        plt.show()
        
    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_gyroSensor(False, 1e-5)


