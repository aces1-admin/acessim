#
# Gyro Sensor Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 26 2022
#

import pytest

from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.ExternalModules import gyroSensor
from Basilisk.utilities import RigidBodyKinematics
from Basilisk.simulation import spacecraft
from Basilisk.ExternalModules import gyroSensor
import numpy as np

def test_gyroSensor(accuracy):
    r"""
    **Validation Test Description**

    All execution paths are tested and verified to run without failure.
    Anticipated updates to variables are tested. 

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
        
    **Description of Variables Being Tested**

    The tests in this file check the internal variables of the
    GyroSensor module as well as the output message.
    """
    [testResults, testMessage] = gyroSensorTestFunction(accuracy)
    assert testResults < 1, testMessage

wTruth_BN_B = [1., 2., 3.]
def setupStateInMsg(stateInMsgData, stateInMsg, w = wTruth_BN_B):
    stateInMsgData.omega_BN_B = w
    stateInMsg.write(stateInMsgData)

def gyroSensorTestFunction(accuracy):
    """Test method"""
    testFailCount = 0
    testMessages = []
    unitTaskName = "unitTask"
    unitProcessName = "TestProcess"

    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.1)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # Setup module to be tested.
    module = gyroSensor.GyroSensor()
    module.ModelTag = "gyroSensorTag"
    unitTestSim.AddModelToTask(unitTaskName, module)

    # Configure blank module input messages.
    stateInMsgData = messaging.SCStatesMsgPayload()
    stateInMsg = messaging.SCStatesMsg().write(stateInMsgData)

    # Subscribe input messages to module.
    module.scStateInMsg.subscribeTo(stateInMsg)

    # Setup output message recorder objects.
    gyroOutMsgRec = module.gyroOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, gyroOutMsgRec)
    
    '''
    Test:   Test GyroSensor::Reset().
    '''
    module.firstCycle = False
    wBiasInit = np.array([1., 2., 1.])
    module.wBiasInit = wBiasInit
    module.wBias_GN_G = np.array([1., 2., 3.])
    module.wBiasPrevious_GN_G = np.array([4., 5., 6.])
    module.PreviousSimNanos  = 1
    module.dtModule = 1
    module.rateNoiseStd = 1.
    module.biasNoiseStd = 1.
    sigma_GB = [0.1, 0.5, 0.3]
    module.sigma_GB = sigma_GB
    wPlusBiasPlusNoise = np.array([1., 2., 3.])
    module.wPlusBiasPlusNoise = wPlusBiasPlusNoise
    noiseForRate = np.array([1., 2., 3.])
    module.noiseForRate = noiseForRate
    noiseForBias = np.array([1., 2., 3.])
    module.noiseForBias = noiseForBias
    randNumHolder1 = np.array([1., 2., 3.])
    module.randNumHolder1 = randNumHolder1
    randNumHolder2 = np.array([1., 2., 3.])
    module.randNumHolder2 = randNumHolder2
    module.Reset(0)
    
    # Verify public module variables are reset as expected.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(module.wBiasInit),
                                        module.wBias_GN_G,
                                        accuracy,
                                        "Test 1",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(module.wBiasInit),
                                        module.wBiasPrevious_GN_G,
                                        accuracy,
                                        "Test 2",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(1,
                                        int(module.firstCycle),
                                        "Test 3",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        module.PreviousSimNanos,
                                        "Test 4",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0., 
                                        module.dtModule, 
                                        accuracy, 
                                        "Test 5", 
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(1., 
                                        module.rateNoiseStd, 
                                        accuracy, 
                                        "Test 6", 
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(1., 
                                        module.biasNoiseStd, 
                                        accuracy, 
                                        "Test 7", 
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(wBiasInit,
                                        np.reshape(module.wBiasInit, (3, )),
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(sigma_GB),
                                        module.sigma_GB,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(wPlusBiasPlusNoise,
                                        np.reshape(module.wPlusBiasPlusNoise, (3, )),
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(noiseForRate,
                                        np.reshape(module.noiseForRate, (3, )),
                                        accuracy,
                                        "Test 11",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(noiseForBias,
                                        np.reshape(module.noiseForBias, (3, )),
                                        accuracy,
                                        "Test 12",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(randNumHolder1,
                                        np.reshape(module.randNumHolder1, (3, )),
                                        accuracy,
                                        "Test 13",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(randNumHolder2,
                                        np.reshape(module.randNumHolder2, (3, )),
                                        accuracy,
                                        "Test 14",
                                        testFailCount, testMessages)
    
    
    '''
    Test:   Test GyroSensor::UpdateState() with the firstCyle flag high. Use
            UpdateState() so we can manually input a call time to test 
            PreviousSimNanos easily.
    '''
    
    setupStateInMsg(stateInMsgData, stateInMsg)
    unitTestSim.InitializeSimulation()
    callTime = 1
    module.wBias_GN_G = [1., 2., 3.]
    module.UpdateState(callTime)
    
    # Verify that firstCycle is low.
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        int(module.firstCycle),
                                        "Test 15",
                                        testFailCount, testMessages)
    
    # Verify that PreviousSimNanos is set to callTime.
    testFailCount, testMessages = unitTestSupport.compareInt(callTime,
                                        module.PreviousSimNanos,
                                        "Test 16",
                                        testFailCount, testMessages)
    
    # Verify that wBiasPrevious_GN_G is set to wBias_GN_G.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(module.wBias_GN_G),
                                        np.array(module.wBiasPrevious_GN_G),
                                        accuracy,
                                        "Test 17",
                                        testFailCount, testMessages)
    
    # Verify no noise is added.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForRate,
                                        accuracy,
                                        "Test 18",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForBias,
                                        accuracy,
                                        "Test 19",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder1,
                                        accuracy,
                                        "Test 20",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder2,
                                        accuracy,
                                        "Test 21",
                                        testFailCount, testMessages)
    
    # Verify dtModule is computed correctly.
    testFailCount, testMessages = unitTestSupport.compareDouble(callTime*1E-9, 
                                        module.dtModule, 
                                        accuracy, 
                                        "Test 22", 
                                        testFailCount, testMessages)
    
    
    '''
    Test:   Test GyroSensor::UpdateState() with the firstCyle flag high. Use
            unitTestSim.TotalSim.SingleStepProcesses() to get logged 
            message.
    '''
    setupStateInMsg(stateInMsgData, stateInMsg)
    module.rateNoiseStd = 1.
    module.biasNoiseStd = 2.
    module.wBiasInit = np.array([1., 1., 1.])
    module.sigma_GB = [0.1, 0.5, 0.3]
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()
    
    
    # Verify that the gyro measurement is only the truth plus initial bias
    # for the first iteration.
    GB = RigidBodyKinematics.MRP2C(np.array(module.sigma_GB))
    truthOmega_GN_G =  GB @ np.array(wTruth_BN_B)
    expected = truthOmega_GN_G + np.reshape(module.wBiasInit, (3, ))
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        gyroOutMsgRec.omega_GN_G[0, :],
                                        accuracy,
                                        "Test 23",
                                        testFailCount, testMessages)
    
    # Verify that the gyroOutMsg is marked as valid.
    testFailCount, testMessages = unitTestSupport.compareInt(1,
                                        gyroOutMsgRec.valid,
                                        "Test 24",
                                        testFailCount, testMessages)
    
    # Verify that wPlusBiasPlusNoise is equal to the expected value.
    testFailCount, testMessages = unitTestSupport.compareVector(expected,
                                        np.reshape(np.array(module.wPlusBiasPlusNoise), (3, )),
                                        accuracy,
                                        "Test 25",
                                        testFailCount, testMessages)
    
    
    '''
    Test:   Test GyroSensor::UpdateState() with the firstCyle flag low and a
            non-zero dtModule. 
    '''
    setupStateInMsg(stateInMsgData, stateInMsg)
    module.rateNoiseStd = 1.
    module.biasNoiseStd = 2.
    module.wBiasInit = np.array([1., 1., 1.])
    module.sigma_GB = [0.1, 0.5, 0.3]
    unitTestSim.InitializeSimulation()
    
    module.firstCycle = False
    callTime = 1
    wPlusBiasPlusNoise = np.array([1., 2., 3.])
    module.wPlusBiasPlusNoise = wPlusBiasPlusNoise
    module.UpdateState(callTime)
    
    # Verify noise is added.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForRate,
                                        accuracy,
                                        "Test 26",
                                        testFailCount, testMessages, ExpectedResult = 0)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForBias,
                                        accuracy,
                                        "Test 27",
                                        testFailCount, testMessages, ExpectedResult = 0)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder1,
                                        accuracy,
                                        "Test 28",
                                        testFailCount, testMessages, ExpectedResult = 0)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder2,
                                        accuracy,
                                        "Test 29",
                                        testFailCount, testMessages, ExpectedResult = 0)
    
    # Verify that wPlusBiasPlusNoise has been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(wPlusBiasPlusNoise,
                                        module.wPlusBiasPlusNoise,
                                        accuracy,
                                        "Test 30",
                                        testFailCount, testMessages, ExpectedResult = 0)
    
    '''
    Test:   Test GyroSensor::UpdateState() with the firstCyle flag low and a
            zero dtModule. 
    '''
    setupStateInMsg(stateInMsgData, stateInMsg)
    module.rateNoiseStd = 1.
    module.biasNoiseStd = 2.
    module.wBiasInit = np.array([1., 1., 1.])
    module.sigma_GB = [0.1, 0.5, 0.3]
    unitTestSim.InitializeSimulation()
    
    module.firstCycle = False
    callTime = 0
    wPlusBiasPlusNoise = np.array([1., 2., 3.])
    module.wPlusBiasPlusNoise = wPlusBiasPlusNoise
    module.UpdateState(callTime)
    
    # Verify dtModule is computed correctly.
    testFailCount, testMessages = unitTestSupport.compareDouble(0., 
                                        module.dtModule, 
                                        accuracy, 
                                        "Test 31", 
                                        testFailCount, testMessages)
    
    # Verify no noise is added.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForRate,
                                        accuracy,
                                        "Test 32",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.noiseForBias,
                                        accuracy,
                                        "Test 33",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder1,
                                        accuracy,
                                        "Test 34",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        module.randNumHolder2,
                                        accuracy,
                                        "Test 35",
                                        testFailCount, testMessages)

    if testFailCount == 0:
        print("PASSED: " + "test_gyroSensor.py")
    else:
        print(testMessages)

    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_gyroSensor(1e-12)


