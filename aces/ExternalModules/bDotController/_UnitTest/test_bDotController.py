# 
# ACES, University of Colorado Boulder, 2022
#
# Unit Test Script
# Module Name: bDotController
# Creation Date: 08 Nov 2021
# Last Edit: 08 Feb 2022
# 


#
# Include necessary modules for performing test.
#

import pytest
import numpy as np
from Basilisk.utilities import unitTestSupport
from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import macros
from Basilisk.architecture import messaging
from Basilisk.ExternalModules import bDotController


#
# This method runs and asserts the test cases.
#

def test_bDotController():
    r"""
    **Validation Test Description**

    The unit test for this module tests the output dipoles against the expected results
    for various TAM measurements, the implementation of the max dipole condition, and
    the implementation of the max deltat condition. Additionally, the correct assignment
    of the configuration parameters is tested.
    """
    [testResults, testMessage] = bDotControllerTestFunction()
    assert testResults < 1, testMessage


#
# This method defines the unit tests evaluated.
#

def bDotControllerTestFunction():
    
    #
    # Simulation setup.
    #

    # setup unit test parameters
    testFailCount = 0       # counter for failures
    testMessages = []       # holder for failure messages
    accuracy = 1E-8         # unit test compare accuracy
    dt = 0.5                # process frequency

    # set up simulation for unit test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(dt)
    unitTaskName = "unitTask"
    unitProcessName = "TestProcess"
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # setup module to be tested
    moduleConfig = bDotController.bDotControllerConfig()
    moduleWrap = unitTestSim.setModelDataWrap(moduleConfig)
    moduleWrap.ModelTag = "bDotController"
    unitTestSim.AddModelToTask(unitTaskName, moduleWrap, moduleConfig)

    # setup module config parameters
    moduleConfig.Kp = 0.15              # gain
    moduleConfig.dipoleLimit = 0.1;     # max dipole components
    moduleConfig.dtMax = 1;             # max deltat 

    # configure blank module input messages
    tamSensorBodyInMsgData = messaging.TAMSensorBodyMsgPayload()
    tamSensorBodyInMsg = messaging.TAMSensorBodyMsg().write(tamSensorBodyInMsgData)

    # subscribe input messages to module
    moduleConfig.tamSensorBodyInMsg.subscribeTo(tamSensorBodyInMsg)

    # setup input/output message recorder objects
    dipoleRequestBodyOutMsgRec = moduleConfig.dipoleRequestBodyOutMsg.recorder()
    tamSensorBodyInMsgRec = tamSensorBodyInMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, dipoleRequestBodyOutMsgRec)
    unitTestSim.AddModelToTask(unitTaskName, tamSensorBodyInMsgRec)
    

    #
    # Tests 1-5: Expected dipoles after various TAM measurements.
    #

    # setup and initialize sim
    tam0 = [0., 0., 0.]
    tam1 = [1., 2., 3.]
    tam2 = [-1., 3., 0.]
    unitTestSim.InitializeSimulation()

    # first call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()
    
    # second call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(dt))
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # third call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(2*dt))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(2*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # fourth call: non-zero but invalid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(3*dt))
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(3*dt)
    tamSensorBodyInMsgData.valid = 0
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # fifth call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(4*dt))
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(4*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # expected dipoles: 1st -> 2nd = dipoleTrue1, 2nd -> 3rd = dipoleTrue2, 3rd -> 5th = dipoleTrue1/2
    bHatDot = (1/dt)*(tam2/np.linalg.norm(tam2) - tam1/np.linalg.norm(tam1))
    dipoleTrue1 = -(moduleConfig.Kp/np.linalg.norm(tam2))*bHatDot
    dipoleTrue2 = (moduleConfig.Kp/np.linalg.norm(tam1))*bHatDot

    # Test 1: Zero dipole request on first call.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[0,:],
                                                            accuracy,
                                                            "bDotController Test 1",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 2: Non-zero expected dipole request w/ valid TAM measurement after first call.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue1,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[1,:],
                                                            accuracy,
                                                            "bDotController Test 2",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 3: Non-zero expected dipole request w/ valid TAM measurement after first call. Implicitly checks
    #         tamPrevNorm_B correctly updated with valid TAM measurement on second call.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue2,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[2,:],
                                                            accuracy,
                                                            "bDotController Test 3",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 4: Zero dipole request with invalid TAM measurements.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[3,:],
                                                            accuracy,
                                                            "bDotController Test 4",
                                                            testFailCount, testMessages, ExpectedResult=1)
    
    # Test 5: Non-zero expected dipole request from valid TAM measurements with an invalid non-zero measurement in between.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue1/2,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[4,:],
                                                            accuracy,
                                                            "bDotController Test 5",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Test 6: Expected dipole when exceeding max dipole condition (both positive and negative components tested).
    #

    # setup and initialize sim
    tam3 = [-0.6, 0.1, 0.2]
    tam4 = [0.8, 0., -1.7]
    unitTestSim.InitializeSimulation()

    # first call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0))
    tamSensorBodyInMsgData.tam_B = tam3
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()
    
    # second call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(dt))
    tamSensorBodyInMsgData.tam_B = tam4
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # expected dipole: dipoleTrue
    bHatDot = (1/dt)*(tam4/np.linalg.norm(tam4) - tam3/np.linalg.norm(tam3))
    dipoleTrue = -(moduleConfig.Kp/np.linalg.norm(tam4))*bHatDot
    for ind in range(3):
        if (abs(dipoleTrue[ind]) > moduleConfig.dipoleLimit):
            dipoleTrue[ind] = moduleConfig.dipoleLimit*dipoleTrue[ind]/abs(dipoleTrue[ind])

    # Test 6: Max dipole condition keeps each component of dipole request below limit.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[1,:],
                                                            accuracy,
                                                            "bDotController Test 6",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Tests 7-10: Expected dipole when exceeding max deltat condition.
    # 

    # setup and initialize sim
    unitTestSim.InitializeSimulation()

    # 1st call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 2nd -> 3rd call: zero & invalid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(dt))
    tamSensorBodyInMsgData.tam_B = tam0
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(dt)
    tamSensorBodyInMsgData.valid = 0
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    unitTestSim.ConfigureStopTime(macros.sec2nano(2*dt))
    tamSensorBodyInMsgData.tam_B = tam0
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(2*dt)
    tamSensorBodyInMsgData.valid = 0
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 4th call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(3*dt))    # 3dt = 1.5 > dtMax = 1
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(3*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 5th call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(4*dt))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(4*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()
    
    # 6th -> 7th call: zero & invalid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(5*dt))
    tamSensorBodyInMsgData.tam_B = tam0
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(5*dt)
    tamSensorBodyInMsgData.valid = 0
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    unitTestSim.ConfigureStopTime(macros.sec2nano(6*dt))
    tamSensorBodyInMsgData.tam_B = tam0
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(6*dt)
    tamSensorBodyInMsgData.valid = 0
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 8th call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(7*dt))    # 7dt-4dt = 3dt = 1.5 > dtMax = 1
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(7*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 9th call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(8*dt))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(8*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # expected dipoles: 4th -> 5th = dipoleTrue2, 8th -> 9th = dipoleTrue2, all others = zero    

    # Test 7: Max deltat condition ensures zero dipole request when exceeding limit from initial t of zero.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[3,:],
                                                            accuracy,
                                                            "bDotController Test 7",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 8: Non-zero expected dipole after exceeding deltat limit from initial t of zero.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue2,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[4,:],
                                                            accuracy,
                                                            "bDotController Test 8",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 9: Max deltat condition ensures zero dipole request when exceeding limit from non-zero initial t.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[7,:],
                                                            accuracy,
                                                            "bDotController Test 9",
                                                            testFailCount, testMessages, ExpectedResult=1)
    
    # Test 10: Non-zero expected dipole after exceeding deltat limit from non-zero initial t.
    testFailCount, testMessages = unitTestSupport.compareVector(dipoleTrue2,
                                                            dipoleRequestBodyOutMsgRec.dipole_B[8,:],
                                                            accuracy,
                                                            "bDotController Test 10",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Tests 11-12: Expected dipole when failing positive deltat condition.
    # 

    # setup and initialize sim
    unitTestSim.InitializeSimulation()

    # 1st call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 2nd call: non-zero & valid repeated TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(dt))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 3rd call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(2*dt))
    tamSensorBodyInMsgData.tam_B = tam2
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(2*dt)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 4th call: non-zero & valid "old" TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(3*dt))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # expected dipoles: 1st -> 2nd = zero, 2nd -> 3rd = dipoleTrue1/2, 3rd -> 4th = zero

    # Test 11: Stale/repeated measurements (i.e. deltat = 0) give zero dipole request.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[1,:],
                                                            accuracy,
                                                            "bDotController Test 11",
                                                            testFailCount, testMessages, ExpectedResult=1)

    # Test 12: Past measurements (i.e. deltat < 0) give zero dipole request.
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[3,:],
                                                            accuracy,
                                                            "bDotController Test 12",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Tests 13: Logic for avoiding divide by zero (with tamMag = 0) is followed. Note: dt = 0 checked in Test 11.
    # 

    # setup and initialize sim
    unitTestSim.InitializeSimulation()

    # 1st call: non-zero & valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0))
    tamSensorBodyInMsgData.tam_B = tam1
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # 2nd call: zero but valid TAM
    unitTestSim.ConfigureStopTime(macros.sec2nano(dt))
    tamSensorBodyInMsgData.tam_B = tam0
    tamSensorBodyInMsgData.timeTag = macros.sec2nano(0.0)
    tamSensorBodyInMsgData.valid = 1
    tamSensorBodyInMsg.write(tamSensorBodyInMsgData)
    unitTestSim.ExecuteSimulation()

    # Test 13: Zero dipole request with zero-norm TAM measurement (i.e. no divide by zero).
    testFailCount, testMessages = unitTestSupport.compareVector([0., 0., 0.],
                                                            dipoleRequestBodyOutMsgRec.dipole_B[1,:],
                                                            accuracy,
                                                            "bDotController Test 13",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Tests 14: Configuration parameters are correctly set after sim initialization.
    # 

    # setup and initialize sim
    moduleConfig.Kp = 3.1;                          # gain
    moduleConfig.dipoleLimit = 0.2;                 # max dipole components
    moduleConfig.dtMax = 1.5;                       # max deltat
    unitTestSim.InitializeSimulation()

    # get config parameters from module
    KpCheck = moduleConfig.Kp;                      # gain
    dipoleLimitCheck = moduleConfig.dipoleLimit;    # max dipole components
    dtMaxCheck = moduleConfig.dtMax;                # max deltat

    # Test 13: Configuration parameters are set to the expected values.
    testFailCount, testMessages = unitTestSupport.compareVector([3.1, 0.2, 1.5],
                                                            np.array([KpCheck, dipoleLimitCheck, dtMaxCheck]),
                                                            accuracy,
                                                            "bDotController Test 14",
                                                            testFailCount, testMessages, ExpectedResult=1)


    #
    # Check and report unit test results.
    #

    if testFailCount == 0:
        print("PASSED: " + moduleWrap.ModelTag)
    else:
        print(testMessages)

    return [testFailCount, "".join(testMessages)]


#
# Ensure the test_bDotController() method is executed when the file is run as a stand-alone python script.
#
   
if __name__ == "__main__":
    test_bDotController()

