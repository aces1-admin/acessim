/*
 ACES, University of Colorado Boulder, 2022

 bDotController Module

 Author: Jesse Gutknecht
 Created: 08 Nov 2021
 Last Edit: 08 Feb 2022

 */


#ifndef BDOTCONTROLLER_H
#define BDOTCONTROLLER_H

#include <stdint.h>
#include "cMsgCInterface/TAMSensorBodyMsg_C.h"
#include "cMsgCInterface/DipoleRequestBodyMsg_C.h"
#include "architecture/utilities/bskLogging.h"


/*! @brief This module generates a requested body frame dipole given body frame magnetic field measurements.
 */
typedef struct {

    /*
     * Declare module I/O interfaces and BSK Logger
     */
    TAMSensorBodyMsg_C tamSensorBodyInMsg;              //!< [Tesla] input message of three-axis magnetometer body frame magnetic field data
    DipoleRequestBodyMsg_C dipoleRequestBodyOutMsg;     //!< [A-m2] output message of requested body frame dipole
    BSKLogger* bskLogger;                               //!< BSK Logging

    /* 
     * Declare configuration data
     */
    double Kp;                                          //!< [A-m2-s-Tesla] proportional control gain
    double dipoleLimit;                                 //!< [A-m2] max dipole request along each body frame axis
    double dtMax;                                       //!< [s] max time between measurements to allow numerical differencing to occur

    /*
     * Declare internal variables
     */
    double tamPrevNorm_B[3];                            //!< [Tesla] previous TAM measurement
    uint64_t timePrev;                                  //!< [ns] time of previous TAM measurement
    int validPrev;                                      //!< [~] previous TAM measurement validity flag (high = valid)

}bDotControllerConfig;


/* 
 * Ensure module will build if compiled as C++
 */
#ifdef __cplusplus
extern "C" {
#endif
    void SelfInit_bDotController(bDotControllerConfig *configData, int64_t moduleID);
    void Update_bDotController(bDotControllerConfig *configData, uint64_t callTime, int64_t moduleID);
    void Reset_bDotController(bDotControllerConfig *configData, uint64_t callTime, int64_t moduleID);

#ifdef __cplusplus
}
#endif

#endif
