Executive Summary
-----------------
This module generates a requested body frame dipole given body frame magnetic field measurements.

Message Connection Descriptions
-------------------------------
The following table lists all the module input and output messages.  
The module msg connection is set by the user from python.  
The msg type contains a link to the message structure definition, while the description 
provides information on what this message is used for.

.. list-table:: Module I/O Messages
    :widths: 25 25 50
    :header-rows: 1

    * - Msg Variable Name
      - Msg Type
      - Description
    * - tamSensorBodyInMsg
      - :ref:`TAMSensorBodyMsgPayload`
      - [Tesla] input msg of three-axis magnetometer body frame magnetic field data
    * - dipoleRequestOutMsg
      - :ref:`DipoleRequestBodyMsgPayload`
      - [A-m^2] output message for requested body frame dipole

