/*

 ACES, University of Colorado Boulder, 2022

*/

%module bDotController
%{
    #include "bDotController.h"
%}

%pythoncode %{
    from Basilisk.architecture.swig_common_model import *
%}
%include "swig_conly_data.i"
%constant void Update_bDotController(void*, uint64_t, uint64_t);
%ignore Update_bDotController;
%constant void SelfInit_bDotController(void*, uint64_t);
%ignore SelfInit_bDotController;
%constant void Reset_bDotController(void*, uint64_t, uint64_t);
%ignore Reset_bDotController;

%include "bDotController.h"

%include "architecture/msgPayloadDefC/TAMSensorBodyMsgPayload.h"
struct TAMSensorBodyMsg_C;
%include "architecture/msgPayloadDefC/DipoleRequestBodyMsgPayload.h"
struct DipoleRequestBodyMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

