/* 
 ACES, University of Colorado Boulder, 2022
 
 bDotController Module
  
 Author: Jesse Gutknecht
 Created: 08 Nov 2021
 Last Edit: 08 Feb 2022
 
 */


#include "ExternalModules/bDotController/bDotController.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/macroDefinitions.h"
#include "string.h"
#include <math.h>

/*! This method sets up the module output message of type :ref:`dipoleRequestBodyOutMsg`
 @return    void
 @param     configData: The configuration data associated with this module
 @param     moduleID: The module identifier
 */
void SelfInit_bDotController(bDotControllerConfig *configData, int64_t moduleID)
{
    DipoleRequestBodyMsg_C_init(&configData->dipoleRequestBodyOutMsg);
}


/*! This method performs a complete reset of the module. Local module variables that retain
    time varying states between function calls are reset to their default values.
 @return    void
 @param     configData: The configuration data associated with the module
 @param     callTime: The clock time at which the function was called [ns]
 @param     moduleID: The module identifier
*/
void Reset_bDotController(bDotControllerConfig *configData, uint64_t callTime, int64_t moduleID)
{
    /*
     * Check input message connections
     */

     // TAM sensor measurement
    if (!TAMSensorBodyMsg_C_isLinked(&configData->tamSensorBodyInMsg)) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: bDotController.tamSensorBodyInMsg was not connected.");
    }


    /*
     * Check configuration data
     */

     // require a non-negative gain
    if (configData->Kp < 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: bDotController.Kp is negative.");
    }
    // require a positive max dipole
    if (configData->dipoleLimit <= 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: bDotController.dipoleLimit is negative or zero.");
    }
    // require a positive max deltat
    if (configData->dtMax <= 0.0) {
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: bDotController.dtMax is negative or zero.");
    }


    /*
     * Initialize internal variables
     */
    v3SetZero(configData->tamPrevNorm_B);
    configData->timePrev = 0;
    configData->validPrev = 0;
}


/*! This method takes the TAM readings and computes the requested control dipole.
 @return    void
 @param     configData: The configuration data associated with the module
 @param     callTime: The clock time at which the function was called [ns]
 @param     moduleID: The module identifier
*/
void Update_bDotController(bDotControllerConfig *configData, uint64_t callTime, int64_t moduleID)
{
    /*
     * Prepare module I/O
     */
    TAMSensorBodyMsgPayload tamSensorBodyInMsgBuffer;                                       //!< local copy of message buffer
    DipoleRequestBodyMsgPayload dipoleRequestBodyOutMsgBuffer;                              //!< local copy of message buffer
    tamSensorBodyInMsgBuffer = TAMSensorBodyMsg_C_read(&configData->tamSensorBodyInMsg);    //!< read input message to buffer
    dipoleRequestBodyOutMsgBuffer = DipoleRequestBodyMsg_C_zeroMsgPayload();                //!< zero output message buffer


    /*
     * Compute requested dipole
     */

     // define useful local variables
    double dt = (tamSensorBodyInMsgBuffer.timeTag - configData->timePrev) * NANO2SEC;       //!< [s] difference in measurement times
    double tamMag = v3Norm(tamSensorBodyInMsgBuffer.tam_B);                                 //!< [Tesla] magnitude of current measurement
    double tamNorm_B[3];                                                                    //!< [~] current measurement direction
    double deltaTamNorm_B[3];                                                               //!< [~] difference btwn current and previous measurement directions
    int validCurr = tamSensorBodyInMsgBuffer.valid;                                         //!< [~] current TAM measurement validity flag (high = valid)

    // perform finite differencing & find dipole_B if valid measurements, allowable detlat, and non-zero current TAM measurement
    if (configData->validPrev && validCurr &&  dt > DB0_EPS && dt <= configData->dtMax && tamMag > DB0_EPS) {
        v3Normalize(tamSensorBodyInMsgBuffer.tam_B, tamNorm_B);
        v3Subtract(tamNorm_B, configData->tamPrevNorm_B, deltaTamNorm_B);
        v3Scale(-(configData->Kp / dt / tamMag), deltaTamNorm_B, dipoleRequestBodyOutMsgBuffer.dipole_B);
    }

    // ensure requested dipole components are below dipoleLimit
    int i;
    double dipoleCheck;
    for (i = 0; i < 3; i++) {                                                                       //!< step through each element of dipole_B
        dipoleCheck = fabs(dipoleRequestBodyOutMsgBuffer.dipole_B[i]);
        if (dipoleCheck > configData->dipoleLimit) {                                                //!< if |dipole_B(i)| > dipoleLimit
            dipoleRequestBodyOutMsgBuffer.dipole_B[i] *= configData->dipoleLimit / dipoleCheck;     //!< dipole_B(i) = dipoleLimit * (dipole_B(i) / |dipole_B(i)|) = dipoleLimit * sign(dipole_B(i))
        }
    }


    /*
     * Update internal variables and write to output message
     */

     // update variables for finite differencing if valid TAM measurement
    if (validCurr) {
        v3Normalize(tamSensorBodyInMsgBuffer.tam_B, configData->tamPrevNorm_B);             //!< [~] update stored previous measurement direction
        configData->timePrev = tamSensorBodyInMsgBuffer.timeTag;                            //!< [ns] update stored time for previous measurement
        configData->validPrev = validCurr;                                                  //!< [~] update stored validity flag for previous measurement 
    }

    // write dipole request message
    DipoleRequestBodyMsg_C_write(&dipoleRequestBodyOutMsgBuffer, &configData->dipoleRequestBodyOutMsg, moduleID, callTime);
}

