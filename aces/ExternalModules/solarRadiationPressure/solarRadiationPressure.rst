Executive Summary
-----------------
This module calculates the force and torque acting on the spacecraft from solar radiation pressure using a faceted model of the spacecraft.

Message Connection Descriptions
-------------------------------
The following table lists all the module input and output messages.  
The module msg connection is set by the user from python.  
The msg type contains a link to the message structure definition, while the description 
provides information on what this message is used for.

.. list-table:: Module I/O Messages
    :widths: 25 25 50
    :header-rows: 1

    * - Msg Variable Name
      - Msg Type
      - Description
    * - sunEphmInMsg
      - :ref:`SpicePlanetStateMsgPayload`
      - [m] input msg of current sun position vector
    * - sunEclipseInMsg
      - :ref:`EclipseMsgPayload`
      - (optional) input msg of current shadow factor
    * - solarRadiationPressureOutMsg
      - :ref:`SolarRadiationPressureMsgPayload`
      - [N] output message for force vector in body frame
      - [N-m] output message for torque vector in body frame

