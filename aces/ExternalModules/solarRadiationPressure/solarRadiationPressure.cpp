/*
 ACES, University of Colorado Boulder, 2022

 solarRadiationPressure Module

 Author: Chase Heinen
 Created: 24 Jan, 2022
 Last Edit: 23 Feb, 2022

*/

#include "ExternalModules/solarRadiationPressure/solarRadiationPressure.h"
#include "architecture/utilities/astroConstants.h"
#include "architecture/utilities/avsEigenSupport.h"
#include "architecture/utilities/avsEigenMRP.h"
#include <cstring>
#include <inttypes.h>
#include <math.h>

/*! This is the constructor for the module class.  It sets default variable
    values and initializes the various parts of the model.
*/
SolarRadiationPressure::SolarRadiationPressure()
    :sunMessageRead(false)
{
    this->sunVisibilityFactor.shadowFactor = 1.0;
    this->forceExternal_B.fill(0.0);
    this->torqueExternalPntB_B.fill(0.0);
    this->numFacets = 0;
    return;
}

/*! Module Destructor */
SolarRadiationPressure::~SolarRadiationPressure()
{
    return;
}

/*! This method is used to reset the module and check that the required input
 message is connected.
 @return    void
 @param     CurrentSimNanos: The current simulation time in nanoseconds
*/
void SolarRadiationPressure::Reset(uint64_t CurrentSimNanos)
{
    // check that the required input message is connected
    if (!this->sunEphmInMsg.isLinked()) 
    {
        bskLogger.bskLog(BSK_ERROR, "SolarRadiationPressure.sunEphmInMsg was not linked.");
    }
}

/*! This method retrieves pointers to spacecraft properties stored
 in the dynamic parameter manager.
 @return    void
 @param     states: The current state data from the dynamic parameter manager
*/
void SolarRadiationPressure::linkInStates(DynParamManager& states)
{
    // read in the spacecraft position and attitude
    this->hubSigma_NB = states.getStateObject("hubSigma");
    this->hubR_N = states.getStateObject("hubPosition");
}

/*! This method is used to read incoming ephemeris and eclipse
 messages. This data is stored in the associated buffer structure.
 @return    void
*/
void SolarRadiationPressure::readInputMessage()
{
    // read in the current sun state message
    this->sunEphmInBuffer = this->sunEphmInMsg();
    this->sunMessageRead = this->sunEphmInMsg.isWritten();

    // read in the current sun eclipse message (optional)
    if (this->sunEclipseInMsg.isLinked() && this->sunEclipseInMsg.isWritten())
    {
        this->sunVisibilityFactor = this->sunEclipseInMsg();
    }
}

/*! This method is used to construct and write an
 outgoing message.
 @return    void
 @param     currentTime: The current simulation integration time
*/
void SolarRadiationPressure::writeOutputMessage(uint64_t currentTime)
{
    // write the output message
    SolarRadiationPressureMsgPayload outMsg;
    eigenVector3d2CArray(this->forceExternal_B, outMsg.solarRadiationPressureForce_B);
    eigenVector3d2CArray(this->torqueExternalPntB_B, outMsg.solarRadiationPressureTorque_B);
    this->solarRadiationPressureOutMsg.write(&outMsg, this->moduleID, currentTime);
    return;
}


/*! This method adds facets to model the spacecraft geometry. It is 
 modeled from the facetDynamicEffector class in Basilisk and is designed
 to be called by the simulation dynamics engine.
 @return void
 @param facetArea: The area of the facet
 @param facetAbsorption: The absorption coefficient of the facet material
 @param facetSpecularReflection: The specular reflection coefficient of the facet material
 @param facetDiffusiveReflection: The diffusive reflection coefficient of the facet material
 @param facetNormalHat_B: The unit normal vector giving the orientation of the facet
                          relative to the spacecraft body frame
 @pararm facetLocation_B The location of the center of the facet relative to the center of
                         gravity in the spacecraft body frame
 */
void SolarRadiationPressure::addFacet(double facetArea, double facetAbsorption, double facetSpecularReflection, double facetDiffusiveReflection, Eigen::Vector3d facetNormalHat_B, Eigen::Vector3d facetLocation_B)
{
    // check for valid facet coefficients, area, or normal vector
    if (abs(1 - facetSpecularReflection - facetDiffusiveReflection - facetAbsorption) > 1e-15)
    {
        bskLogger.bskLog(BSK_ERROR, "SolarRadiationPressure facet absorption and reflection coefficients are invalid.");
    }
    else if (facetArea <= 0)
    {
        bskLogger.bskLog(BSK_ERROR, "SolarRadiationPressure facet area is zero or negative.");
    }
    else if (facetNormalHat_B.norm() != 1)
    {
        bskLogger.bskLog(BSK_ERROR, "SolarRadiationPressure facet normal vector is not a unit vector.");
    }
    else
    {
        // add a facet with the specified properties to the spacecraft model
        this->scGeometry.facetAreas.push_back(facetArea);
        this->scGeometry.facetAbsorpCoeffs.push_back(facetAbsorption);
        this->scGeometry.facetSpecCoeffs.push_back(facetSpecularReflection);
        this->scGeometry.facetDiffCoeffs.push_back(facetDiffusiveReflection);
        this->scGeometry.facetNormals_B.push_back(facetNormalHat_B);
        this->scGeometry.facetLocations_B.push_back(facetLocation_B);
        this->numFacets += 1;
    }
}

/*! This method computes the force and torque acting on the spacecraft
 in body frame components from solar radiation pressure. It is an inherited
 method from the Dynamic Effector class and is designed to be called by the
 simulation dynamics engine.
 @return    void
 @param     integTime: The current simulation integration time
 @param     timeStep: The current integration time step
*/
void SolarRadiationPressure::computeForceTorque(double integTime, double timeStep)
{
    // reset the srp force and torque
    this->forceExternal_B.setZero();
    this->torqueExternalPntB_B.setZero();

    // get the current sun position and spacecraft position in the inertial frame
    this->readInputMessage();
    Eigen::Vector3d rSC_N = (Eigen::Vector3d)this->hubR_N->getState();
    Eigen::Vector3d rSun_N(this->sunEphmInBuffer.PositionVector);

    // calculate the vector to the sun with respect to the spacecraft origin in the body frame
    Eigen::MRPd sigmaLocal_NB;
    sigmaLocal_NB = (Eigen::Vector3d)this->hubSigma_NB->getState();
    Eigen::Matrix3d dcmLocal_BN = sigmaLocal_NB.toRotationMatrix().transpose();
    Eigen::Vector3d rSC2Sun_B = dcmLocal_BN * (rSun_N - rSC_N);
    Eigen::Vector3d rSC2SunHat_B = rSC2Sun_B / rSC2Sun_B.norm();

    // check for a divide by zero and a successful sun message read
    if (rSC2Sun_B.norm() != 0 && sunMessageRead)
    {
        // initialize variables
        double fractionOfArea;
        double projectedArea;
        double specReflection;
        double diffReflection;
        Eigen::Vector3d srpForce_B;
        Eigen::Vector3d srpTorque_B;

        // calculate the current solar radiation pressure
        double srp = SOLAR_LUMINOSITY / (4 * M_PI * pow(rSC2Sun_B.norm(), 2) * SPEED_LIGHT);

        // loop through each facet in the spacecraft model
        for (size_t i = 0; i < this->numFacets; i++)
        {
            // calculate the projected facet area
            fractionOfArea = (this->scGeometry.facetNormals_B[i].dot(rSC2SunHat_B) > 0) ? \
                             this->scGeometry.facetNormals_B[i].dot(rSC2SunHat_B) : 0;
            projectedArea = this->scGeometry.facetAreas[i] * fractionOfArea;

            // extract the facet coefficients needed for the srp force calculation
            specReflection = this->scGeometry.facetSpecCoeffs[i];
            diffReflection = this->scGeometry.facetDiffCoeffs[i];

            // calculate the srp force and torque acting on the facet
            srpForce_B = this->sunVisibilityFactor.shadowFactor * srp * projectedArea * \
                         ((specReflection - 1) * rSC2SunHat_B + ((2/3) * diffReflection - \
                         2 * specReflection * fractionOfArea) * this->scGeometry.facetNormals_B[i]);
            srpTorque_B = this->scGeometry.facetLocations_B[i].cross(srpForce_B);

            // sum the force and torque for each facet
            this->forceExternal_B += srpForce_B;
            this->torqueExternalPntB_B += srpTorque_B;
        }
    }
    // write the output message
    this->writeOutputMessage(integTime);
}
