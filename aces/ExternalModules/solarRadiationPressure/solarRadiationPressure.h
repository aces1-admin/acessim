/*
 ACES, University of Colorado Boulder, 2022

 solarRadiationPressure Module

 Author: Chase Heinen
 Created: 24 Jan, 2022
 Last Edit: 22 Feb, 2022

*/


#ifndef SOLAR_RADIATION_PRESSURE_H
#define SOLAR_RADIATION_PRESSURE_H

#include <vector>
#include "simulation/dynamics/_GeneralModuleFiles/dynamicEffector.h"
#include "simulation/dynamics/_GeneralModuleFiles/stateData.h"
#include "simulation/dynamics/_GeneralModuleFiles/dynParamManager.h"
#include "architecture/_GeneralModuleFiles/sys_model.h"
#include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"
#include "architecture/msgPayloadDefC/EclipseMsgPayload.h"
#include "architecture/utilities/bskLogging.h"
#include "architecture/messaging/messaging.h"
#include "msgPayloadDefC/SolarRadiationPressureMsgPayload.h"

/*! @brief spacecraft geometry data */
typedef struct {
    std::vector<double> facetAreas;                                            //!< [m^2] vector of facet areas
    std::vector<double> facetAbsorpCoeffs;                                     //!< [~] vector of facet absorption coefficients
    std::vector<double> facetSpecCoeffs;                                       //!< [~] vector of facet specular reflection coefficients
    std::vector<double> facetDiffCoeffs;                                       //!< [~] vector of facet diffusive reflection coefficients
    std::vector<Eigen::Vector3d> facetNormals_B;                               //!< [~] vector of facet normal vectors
    std::vector<Eigen::Vector3d> facetLocations_B;                             //!< [m] vector of facet location vectors
}SpacecraftGeometryData;

/*! define solar luminosity */
#define SOLAR_LUMINOSITY    3.828e26                                           //!< [W] solar luminosity

/*! @brief solar radiation pressure dynamic effector
 */
class SolarRadiationPressure: public SysModel, public DynamicEffector {
public:
    SolarRadiationPressure();
    ~SolarRadiationPressure();

    void Reset(uint64_t CurrentSimNanos);
    void linkInStates(DynParamManager& states);
    void readInputMessage();
    void writeOutputMessage(uint64_t currentTime);
    void addFacet(double area, double absorption, double specularReflection, double diffusiveReflection, Eigen::Vector3d B_normal_hat, Eigen::Vector3d B_location);
    void computeForceTorque(double integTime, double timeStep);

public:
    uint64_t numFacets;                                                        //!< [~] number of facets
    ReadFunctor<SpicePlanetStateMsgPayload> sunEphmInMsg;                      //!< [~] sun state input message
    ReadFunctor<EclipseMsgPayload> sunEclipseInMsg;                            //!< [~] sun eclipse input message
    Message<SolarRadiationPressureMsgPayload> solarRadiationPressureOutMsg;    //!< [~] output message containing torque/force
    BSKLogger bskLogger;                                                       //!< [~] BSK logging

private:
    SpacecraftGeometryData scGeometry;                                         //!< [~] struct containing spacecraft facet data
    SpicePlanetStateMsgPayload sunEphmInBuffer;                                //!< [~] buffer for incoming ephemeris message data
    bool sunMessageRead;                                                       //!< [bool] indicates successful read of incoming sun message
    EclipseMsgPayload sunVisibilityFactor;                                     //!< [~] eclispse parameter from 0 (fully obscured) to 1 (fully visible)
    StateData *hubR_N;                                                         //!< [m] state data of inertial position of hub
    StateData *hubSigma_NB;                                                    //!< [~] state data of hub attitude (orientation of the body frame relative to the inertial frame)
};

#endif
