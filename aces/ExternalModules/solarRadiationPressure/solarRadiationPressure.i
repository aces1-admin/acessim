/*
 ACES, University of Colorado Boulder, 2022

 solarRadiationPressure Module

 Author: Chase Heinen
 Created: 24 Jan, 2022
 Last Edit: 17 Feb, 2022

*/

%module solarRadiationPressure
%{
    #include "solarRadiationPressure.h"
%}

%pythoncode %{
    from Basilisk.architecture.swig_common_model import *
%}
%include "std_string.i"
%include "swig_conly_data.i"
%include "swig_eigen.i"
%include "simulation/dynamics/_GeneralModuleFiles/dynamicEffector.h"
%include "simulation/dynamics/_GeneralModuleFiles/stateData.h"
%include "sys_model.h"
%include "solarRadiationPressure.h"

%include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"
struct SpicePlanetStateMsg_C;
%include "architecture/msgPayloadDefC/EclipseMsgPayload.h"
struct EclipseMsg_C;
%include "msgPayloadDefC/SolarRadiationPressureMsgPayload.h"
struct SolarRadiationPressureMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}
