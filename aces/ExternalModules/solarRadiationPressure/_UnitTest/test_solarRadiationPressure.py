# 
# ACES, University of Colorado Boulder, 2022
#
# Unit Test Script
# Module: solarRadiationPressure
#
# Author: Chase Heinen
# Created: 24 Jan, 2022
# Last Edit: 25 Feb, 2022 
# 

import pytest
from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.simulation import spacecraft
from Basilisk.ExternalModules import solarRadiationPressure


def test_solarRadiationPressure(accuracy):
    r"""
    **Validation Test Description**

    The unit test for this module tests the expected force and torque for a full eclipse,
    the expected force and torque for an example spacecraft position and orientation,
    and the expected force and torque when the sun ephemeris message is not written. In
    addition, tests are performed to ensure that facets with invalid properties are not
    successfully added.
    """

    [testResults, testMessage] = solarRadiationPressureTestFunction(accuracy)
    assert testResults < 1, testMessage


def solarRadiationPressureTestFunction(accuracy):

    # setup unit test parameters
    testFailCount = 0
    testMessages = []
    unitTaskName = "unitTask"
    unitProcessName = "TestProcess"
    
    #
    # Test 1: check that a full eclipse causes zero force/torque
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)
    
    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 0
    eclipseMsg.write(eclipseMsgData)
    
    # add a spacecraft facet
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [-1.0, 0.0, 0.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # define the force and torque truth vectors (both are zero during a full eclipse)
    trueForce = [0.0, 0.0, 0.0]  # [N]
    trueTorque = [0.0, 0.0, 0.0]  # [N-m]

    # Test: compare the calculated force with the true force
    testFailCount, testMessages = unitTestSupport.compareVector(trueForce,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureForce_B[-1],
                                                                accuracy,
                                                                "SRP Test 1: Force for a full eclipse.",
                                                                testFailCount, testMessages, ExpectedResult=1)
    # Test: compare the calculated torque with the true torque
    testFailCount, testMessages = unitTestSupport.compareVector(trueTorque,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureTorque_B[-1],
                                                                accuracy,
                                                                "SRP Test 1: Torque for a full eclipse.",
                                                                testFailCount, testMessages, ExpectedResult=1)

    #
    # Test 2: check the force/torque calculations for a single facet displaced from sun with no eclipse
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in a randomly selected direction
    scObject.hub.r_CN_NInit = [1.0e11, 0.7e11, 0.86484e11]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # add a spacecraft facet
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [-1.0, 0.0, 0.0]
    bLocation = [0.0, 0.05, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # define the force and torque truth vectors calculated in matlab
    trueForce = [2.02880960e-8, 1.42016672e-8, 1.75459570e-8]  # [N]
    trueTorque = [8.77297849e-10, 0.0, -1.01440480e-09]  # [N-m]

    # Test: compare the force calculation with the true force
    testFailCount, testMessages = unitTestSupport.compareVector(trueForce,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureForce_B[-1],
                                                                accuracy,
                                                                "SRP Test 2: Force calculation",
                                                                testFailCount, testMessages, ExpectedResult=1)

    # Test: compare the torque calculation with the true torque
    testFailCount, testMessages = unitTestSupport.compareVector(trueTorque,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureTorque_B[-1],
                                                                accuracy,
                                                                "SRP Test 2: Torque calculation",
                                                                testFailCount, testMessages, ExpectedResult=1)
    
    #
    # Test 3: check for zero force/torque for a single facet that is facing away from the sun
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # add a spacecraft facet
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [1.0, 0.0, 0.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # define the force and torque truth vectors (both are zero for a facet that is not receiving radiation)
    trueForce = [0.0, 0.0, 0.0]  # [N]
    trueTorque = [0.0, 0.0, 0.0] # [N-m]

    # Test: compare the force calculation with the true force
    testFailCount, testMessages = unitTestSupport.compareVector(trueForce,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureForce_B[-1],
                                                                accuracy,
                                                                "SRP Test 3: Force for a facet facing away from the sun",
                                                                testFailCount, testMessages, ExpectedResult=1)

    # Test: compare the torque calculation with the true torque
    testFailCount, testMessages = unitTestSupport.compareVector(trueTorque,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureTorque_B[-1],
                                                                accuracy,
                                                                "SRP Test 3: Torque for a facet facing away from the sun",
                                                                testFailCount, testMessages, ExpectedResult=1)

    #
    # Test 4: check for zero force/torque if a sun message is not written
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # add a spacecraft facet
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [1.0, 0.0, 0.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # define the force and torque truth vectors (both are zero when no sun message is written)
    trueForce = [0.0, 0.0, 0.0]  # [N]
    trueTorque = [0.0, 0.0, 0.0]  # [N-m]

    # Test: compare the calculated force with the true force
    testFailCount, testMessages = unitTestSupport.compareVector(trueForce,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureForce_B[-1],
                                                                accuracy,
                                                                "SRP Test 4: Force when no sun message is written",
                                                                testFailCount, testMessages, ExpectedResult=1)

    # Test: compare the calculated torque with the true torque
    testFailCount, testMessages = unitTestSupport.compareVector(trueTorque,
                                                                solarRadiationPressureOutMsgRec.solarRadiationPressureTorque_B[-1],
                                                                accuracy,
                                                                "SRP Test 4: Torque when no sun message is written",
                                                                testFailCount, testMessages, ExpectedResult=1)

    #
    # Test 5: check that no facets are added when attempting to add a facet with invalid coefficients
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # setup logging for the numFacets variable
    unitTestSim.AddVariableForLogging(module.ModelTag + ".numFacets",
                                      0, 0, 0, 'double')

    # attempt to add a facet with invalid coefficients
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.7
    diffRefCoeff = 0.3
    bNormal = [1.0, 0.0, 0.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # record and extract the logged variable
    unitTestSim.RecordLogVars()
    numFacets = unitTestSim.GetLogVariableData(module.ModelTag + ".numFacets")

    # Test: check for zero facets if adding a facet with invalid coefficients is attempted
    testPass = unitTestSupport.isDoubleEqual(numFacets[-1][-1], 0.0, accuracy)
    if testPass != 1:
        testFailCount += 1
        testMessages.append("Failed: SRP Test 5: Facet with invalid coefficients was successfully added.")

    #
    # Test 6: check that no facets are added when attempting to add a facet with an invalid area
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # setup logging for the numFacets variable
    unitTestSim.AddVariableForLogging(module.ModelTag + ".numFacets",
                                      0, 0, 0, 'double')

    # attempt to add a facet with an invalid area
    area = -0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [1.0, 0.0, 0.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # record and extract the logged variable
    unitTestSim.RecordLogVars()
    numFacets = unitTestSim.GetLogVariableData(module.ModelTag + ".numFacets")

    # Test: check for zero facets if adding a facet with an invalid area is attempted
    testPass = unitTestSupport.isDoubleEqual(numFacets[-1][-1], 0.0, accuracy)
    if testPass != 1:
        testFailCount += 1
        testMessages.append("Failed: SRP Test 6: Facet with invalid area was successfully added.")

    #
    # Test 7: check that no facets are added when attempting to add a facet with an invalid unit normal vector
    #

    # setup the simulation for the current test
    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # create the spacecraft
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = 'spacecraft'
    unitTestSim.AddModelToTask(unitTaskName, scObject)

    # setup the module to be tested
    module = solarRadiationPressure.SolarRadiationPressure()
    module.ModelTag = "solarRadiationPressureTag"
    scObject.addDynamicEffector(module)
    unitTestSim.AddModelToTask(unitTaskName, module)

    # setup the module input messages
    sunPlanetStateMsgData = messaging.SpicePlanetStateMsgPayload()
    # place the sun at the origin of the inertial frame
    sunPlanetStateMsgData.PositionVector = [0.0, 0.0, 0.0]  # [m]
    sunPlanetStateMsg = messaging.SpicePlanetStateMsg()
    sunPlanetStateMsg.write(sunPlanetStateMsgData)
    sunEphemerisMsgData = messaging.EphemerisMsgPayload()
    sunEphemerisMsg = messaging.EphemerisMsg()
    sunEphemerisMsg.write(sunEphemerisMsgData)
    eclipseMsgData = messaging.EclipseMsgPayload()
    eclipseMsg = messaging.EclipseMsg().write(eclipseMsgData)

    # subscribe to the input messages
    module.sunEclipseInMsg.subscribeTo(eclipseMsg)
    module.sunEphmInMsg.subscribeTo(sunPlanetStateMsg)

    # setup the module output message recorder
    solarRadiationPressureOutMsgRec = module.solarRadiationPressureOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, solarRadiationPressureOutMsgRec)

    # set the spacecraft properties
    # displace the spacecraft by 1 AU in the first axis of the inertial frame
    scObject.hub.r_CN_NInit = [1.4959787e11, 0.0, 0.0]  # [m]
    # set the spacecraft attitude so the body frame is aligned with the inertial frame
    scObject.hub.sigma_BNInit = [0.0, 0.0, 0.0]
    # set the location of the center of mass in the body frame
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]] # [m]

    # set the eclipse shadow factor and write the eclipse message
    eclipseMsgData.shadowFactor = 1
    eclipseMsg.write(eclipseMsgData)

    # setup logging for the numFacets variable
    unitTestSim.AddVariableForLogging(module.ModelTag + ".numFacets",
                                      0, 0, 0, 'double')

    # attempt to add a facet with invalid coefficients
    area = 0.01  # [m^2]
    absorptionCoeff = 1.0
    specRefCoeff = 0.0
    diffRefCoeff = 0.0
    bNormal = [1.0, 1.0, 1.0]
    bLocation = [0.0, 0.0, 0.0]  # [m]
    module.addFacet(area, absorptionCoeff, specRefCoeff, diffRefCoeff, bNormal, bLocation)

    # initialize the simulation and execute for a single time step
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()

    # record and extract the logged variable
    unitTestSim.RecordLogVars()
    numFacets = unitTestSim.GetLogVariableData(module.ModelTag + ".numFacets")

    # Test: check for zero facets if adding a facet with an invalid area is attempted
    testPass = unitTestSupport.isDoubleEqual(numFacets[-1][-1], 0.0, accuracy)
    if testPass != 1:
        testFailCount += 1
        testMessages.append("Failed: SRP Test 7: Facet with invalid normal vector was successfully added.")

    if testFailCount == 0:
        print("PASSED: " + module.ModelTag)
    else:
        print(testMessages)

    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_solarRadiationPressure(1e-8)


