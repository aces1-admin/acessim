/*
 * Author: Henry Macanas
 * Version #: 1
 */

#include "ExternalModules/attitudeEKF/attitudeEKF.h"
#include "architecture/utilities/macroDefinitions.h"
#include "architecture/utilities/linearAlgebra.h"
#include "architecture/utilities/rigidBodyKinematics.h"
#include "../_GeneralModuleFiles/ekfUtilities.h"
#include "../_GeneralModuleFiles/lowPassFilter.h"
#include "../_GeneralModuleFiles/attitudeEkfMath.h"
#include "../_GeneralModuleFiles/mathUtilities.h"
#include "string.h"
#include <math.h>
#include <stdint.h>

/*! This function initializes the output message for this module.
 @return void
 @param configData The configuration data associated with this module
 @param moduleID The module identifier
 */
void SelfInit_attitudeEKF(attitudeEKFConfig *configData, int64_t moduleID)
{
    NavAttMsg_C_init(&configData->navAttMsgEstimated);
}


/*! This function performs a complete reset of the module.  Local module variables that retain
    time varying states between function calls are reset to their default values.
    Check if required input messages are connected.
 @return void
 @param configData The configuration data associated with the module
 @param callTime [ns] time the function is called
 @param moduleID The module identifier
*/
void Reset_attitudeEKF(attitudeEKFConfig *configData, uint64_t callTime, int64_t moduleID)
{
    // Check if the required messages have not been connected.
    if (!GyroMsg_C_isLinked(&configData->gyroInMsg))
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: attitudeEKF.gyroInMsg was not connected.");
    
    if (!StarTrackerMsg_C_isLinked(&configData->starTrackerInMsg))
        _bskLog(configData->bskLogger, BSK_ERROR, "Error: attitudeEKF.starTrackerInMsg was not connected.");
    
    // Explicitly pass through all internal module variables and initialize them.
    v3SetZero(configData->state);
    vCopy(configData->biasInit, 3, &configData->state[3]);
    v3SetZero(configData->wBiasCompensatedFilteredBN_B);
    configData->prevCallTime = 0;
    configData->starTrackerInMsgPrev = StarTrackerMsg_C_zeroMsgPayload();
    configData->gyroInMsgPrev = GyroMsg_C_zeroMsgPayload();
    v3SetZero(configData->mrpMeasurementResidual);
    mCopy(configData->P0, 6, 6, configData->P);
    mSetZero(configData->A, 6, 6);
    mSetZero(configData->F, 6, 6);
    mSetZero(configData->G, 6, 6);
    mSetZero(configData->Qgyro, 6, 6);
    mSetZero(configData->Qtotal, 6, 6);
    mSetZero(configData->K, 6, 3);
    mSetZero(configData->H, 3, 6);
    configData->H[MXINDEX(6, 0, 0)] = 1.0;
    configData->H[MXINDEX(6, 1, 1)] = 1.0;
    configData->H[MXINDEX(6, 2, 2)] = 1.0;
    filterReset(&configData->lpfGyroAxis1);
    filterReset(&configData->lpfGyroAxis2);
    filterReset(&configData->lpfGyroAxis3);
    filterSetCoeffs(&configData->lpfGyroAxis1, configData->lpfNumCoeffs, configData->lpfDenCoeffs);
    filterSetCoeffs(&configData->lpfGyroAxis2, configData->lpfNumCoeffs, configData->lpfDenCoeffs);
    filterSetCoeffs(&configData->lpfGyroAxis3, configData->lpfNumCoeffs, configData->lpfDenCoeffs);
    configData->filterStatus = INITIALIZING;
    mSetIdentity(configData->I6X6, 6, 6);
    configData->timeTag = callTime;
    configData->timeTagPrev = callTime;
}


/*! This function updates the current state estimate given gyro and star tracker measurements using an Extended Kalman Filter.
 @return void
 @param configData The configuration data associated with the module
 @param callTime [ns]  The clock time at which the function was called (nanoseconds)
 @param moduleID The module identifier
*/
void Update_attitudeEKF(attitudeEKFConfig *configData, uint64_t callTime, int64_t moduleID)
{
    // Declare local variables.
    NavAttMsgPayload navAttMsgEstimatedBuffer;      //!< local copy of message buffer
    GyroMsgPayload gyroInMsgBuffer;                 //!< local copy of message buffer
    StarTrackerMsgPayload starTrackerInMsgBuffer;   //!< local copy of message buffer
    
    // Read input messages and initialize output message buffer.
    navAttMsgEstimatedBuffer = NavAttMsg_C_zeroMsgPayload();
    gyroInMsgBuffer = GyroMsg_C_read(&configData->gyroInMsg);
    starTrackerInMsgBuffer = StarTrackerMsg_C_read(&configData->starTrackerInMsg);
    
    // Convert star tracker reading into Body relative to Inertial MRP.
    double sigma_BC[3];
    double sigma_CN[3];
    v3Scale(-1.0, configData->sigma_CB, sigma_BC);
    EP2MRP(starTrackerInMsgBuffer.q_CN, sigma_CN);
    addMRP(sigma_CN, sigma_BC, configData->sigma_BN);
    
    // Rotate angular velocity into the Body frame.
    double omega_BN_B[3];
    double sigma_BG[3];
    v3Scale(-1.0, configData->sigma_GB, sigma_BG);
    rotateVectorWithMrp(sigma_BG, gyroInMsgBuffer.omega_GN_G, omega_BN_B);
    
    /*
     * Compute the propagation time delta. If the star tracker measurement is valid
     * and the current measurement is not the same as the previous, propagate to
     * the measurement time. Otherwise, use the time delta of the algorithm.
     */
    uint64_t dtAlgorithm = (callTime - configData->prevCallTime);
    uint64_t dt2Measurement = (starTrackerInMsgBuffer.timeTag - configData->prevCallTime);
    int newStarTrackerMeasurement = (starTrackerInMsgBuffer.timeTag - configData->starTrackerInMsgPrev.timeTag) > 0;
    uint64_t timeDiffInNanoSecs = (starTrackerInMsgBuffer.valid && newStarTrackerMeasurement) ? dt2Measurement : dtAlgorithm;
    double dtPropagation = timeDiffInNanoSecs * NANO2SEC;
    
    /*
     * Propagate step of the filter. This step uses the previous state
     * estimate to get a new state estimate and propagates the covariance
     * matrix forward in time.
     *
     * Do not predict while filter is initializing. This forces the filter to
     * wait till a star tracker measurement comes in so the filter can be
     * seeded with it.
     */
    if (configData->filterStatus != INITIALIZING)
    {
        computeDynamicsMatrix(configData->state, configData->wBiasCompensatedBN_B, configData->A);
        ekfComputeStateTransitionMatrix(configData->A, configData->I6X6, NUM_ATT_EKF_STATES, dtPropagation, configData->F);
        computeProcessNoiseMatrix(configData->gyroRateNoiseStd, configData->gyroBiasNoiseStd, configData->state, configData->G, configData->Q, configData->Qgyro, dtPropagation, configData->Qtotal);
        ekfPropagateCovarianceMatrix(configData->F, configData->Qtotal, configData->P, NUM_ATT_EKF_STATES, configData->P);
        attitudePropagateSingleStep(configData->state, configData->wBiasCompensatedBN_B, dtPropagation);
        
        /*
         * Since there is a singularity at 360 degree rotations for mrp's, we
         * elect to contain the mrp to a magnitude of 1 (a 180 degree rotation)
         * by moving to the shadow set of the current mrp, avoiding the
         * singularity. This also requires updating the covariance using the
         * shadow set.
         */
        if (v3Norm(configData->state) > 1.0)
            switchMrpAndCovarianceToShadowSet(configData->P, configData->state);
        
        // Update timeTag.
        configData->timeTag = configData->timeTagPrev + timeDiffInNanoSecs;
    }
    
    /*
     * Update step of the filter. This step uses a star tracker measurement
     * to update the propagated state estimate and compute an updated
     * covariance matrix.
     */
    if (starTrackerInMsgBuffer.valid && newStarTrackerMeasurement)
    {
        /*
         * If the filter is in its INITIALIZING state, seed the filter's
         * attitude estimate with the star tracker measurement. Set filter
         * to status to unknown.
         */
        if (configData->filterStatus == INITIALIZING)
            v3Copy(configData->sigma_BN, configData->state);
        configData->filterStatus = UNKNOWN;
        
        ekfComputeKalmanGain(configData->P, configData->H, configData->R, NUM_ATT_EKF_STATES, NUM_ATT_EKF_OUTPUTS, configData->K);
        computeMeasurementResdiual(configData->state, configData->sigma_BN, configData->mrpMeasurementResidual);
        ekfUpdateStateEstimate(configData->state, configData->K, configData->mrpMeasurementResidual, NUM_ATT_EKF_STATES, NUM_ATT_EKF_OUTPUTS, configData->state);
        ekfUpdateCovariance(configData->P, configData->H, configData->K, configData->I6X6, configData->R, NUM_ATT_EKF_STATES, NUM_ATT_EKF_OUTPUTS, configData->P);
        
        // Update timeTag.
        configData->timeTag = starTrackerInMsgBuffer.timeTag;
    }
    
    /*
     * Compute bias compensated and filtered angular rate if there is a new and
     * valid gyro measurement.
     */
    int newGyroMeasurement = (gyroInMsgBuffer.timeTag - configData->gyroInMsgPrev.timeTag) > 0;
    if(gyroInMsgBuffer.valid && newGyroMeasurement)
    {
        // Subtract bias off of rotated measurement.
        v3Subtract(omega_BN_B, &configData->state[3], configData->wBiasCompensatedBN_B);
        
        // Filter bias compensated raw measured rates.
        configData->wBiasCompensatedFilteredBN_B[0] = filter(&configData->lpfGyroAxis1, configData->wBiasCompensatedBN_B[0]);
        configData->wBiasCompensatedFilteredBN_B[1] = filter(&configData->lpfGyroAxis2, configData->wBiasCompensatedBN_B[1]);
        configData->wBiasCompensatedFilteredBN_B[2] = filter(&configData->lpfGyroAxis3, configData->wBiasCompensatedBN_B[2]);
    }
    
    // Write estimate to the navigation buffer.
    v3Copy(configData->state, navAttMsgEstimatedBuffer.sigma_BN);
    v3Copy(configData->wBiasCompensatedFilteredBN_B, navAttMsgEstimatedBuffer.omega_BN_B);
    navAttMsgEstimatedBuffer.timeTag = configData->timeTag;
    NavAttMsg_C_write(&navAttMsgEstimatedBuffer, &configData->navAttMsgEstimated, moduleID, callTime);
    
    // Hold onto previous values.
    configData->prevCallTime = callTime;
    configData->starTrackerInMsgPrev = StarTrackerMsg_C_read(&configData->starTrackerInMsg);
    configData->gyroInMsgPrev = GyroMsg_C_read(&configData->gyroInMsg);
    configData->timeTagPrev = configData->timeTag;
}
