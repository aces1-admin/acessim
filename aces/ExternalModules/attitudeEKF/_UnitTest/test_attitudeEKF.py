#
# Attitude EKF Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 24 2022
#

import pytest

from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.ExternalModules import attitudeEKF
from Basilisk.utilities import RigidBodyKinematics

import numpy as np

def test_attitudeEKF(accuracy):
    r"""
    **Validation Test Description**

    All execution paths are tested and verified to run without failure.
    Anticipated updates to variables are tested.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
        
    **Description of Variables Being Tested**

    The tests in this file check the internal variables of the
    attitudeEKFConfig structure as well as the output of the filter NavMessageEstimated.
    """
    [testResults, testMessage] = attitudeEKFTestFunction(accuracy)
    assert testResults < 1, testMessage

validTimeTag = macros.sec2nano(0.1)
validQ_CN = list(RigidBodyKinematics.MRP2EP([0.1, 0.2, 0.3]))
validOmega_GN_G = list(macros.D2R * np.array([1., 2., 3.]))

def setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg, timeTag = validTimeTag, valid = 1, q_CN = validQ_CN):
    starTrackerInMsgData.timeTag = timeTag
    starTrackerInMsgData.valid = valid
    starTrackerInMsgData.q_CN = q_CN
    starTrackerInMsg.write(starTrackerInMsgData)
    
def setupGyroMsg(gyroInMsgData, gyroInMsg, timeTag = validTimeTag, valid = 1, omega_GN_G = validOmega_GN_G):
    gyroInMsgData.timeTag = timeTag
    gyroInMsgData.valid = valid
    gyroInMsgData.omega_GN_G = omega_GN_G
    gyroInMsg.write(gyroInMsgData)
    
def setupValidEkfConfig(attitudeEkfConfig, unitTestSim):
    unitTestSim.InitializeSimulation()
    ARCSEC2DEG = 1 / 3600.  # conversion from arcseconds to degrees
    aroundBoresightStd = 30 * ARCSEC2DEG * macros.D2R
    crossBoresightStd = 10 * ARCSEC2DEG * macros.D2R

    attitudeEkfConfig.P0 = [(3 * aroundBoresightStd)**2 / 16.0, 0, 0, 0, 0, 0,
                            0, (3 * crossBoresightStd**2) / 16.0, 0, 0, 0, 0,
                            0, 0, (3 * crossBoresightStd**2) / 16.0, 0, 0, 0,
                            0, 0, 0, (3 * 0.07 * macros.D2R * 3)**2, 0, 0,
                            0, 0, 0, 0, (3 * 0.07 * macros.D2R * 3)**2, 0,
                            0, 0, 0, 0, 0, (3 * 0.07 * macros.D2R * 3)**2]
    attitudeEkfConfig.Q = [0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0]
    
    attitudeEkfConfig.R = [aroundBoresightStd**2 / 16.0, 0, 0,
                           0, crossBoresightStd**2 / 16.0, 0,
                           0, 0, crossBoresightStd**2 / 16.0]
    
    rateNoiseStd = 0.007 * macros.D2R
    biasNoiseStd = 10.0 * macros.D2R / 3600. / np.sqrt(0.1)
    attitudeEkfConfig.gyroRateNoiseStd = rateNoiseStd
    attitudeEkfConfig.gyroBiasNoiseStd = biasNoiseStd
    
    attitudeEkfConfig.sigma_GB = [0.1, 0.2, 0.3]
    attitudeEkfConfig.sigma_CB = [-0.1, -0.2, -0.3]
    
    # low pass coeffs (current set to a cutoff freq of 10 deg/s)
    attitudeEkfConfig.lpfNumCoeffs = [0.0087 , 0.0087 ]
    attitudeEkfConfig.lpfDenCoeffs = [1., -0.9827]
    
def truthComputeDynamicsMatrix(sigmaEst_BN, wBiasCompensatedBN_B):
    s = sigmaEst_BN
    w = wBiasCompensatedBN_B
    B = RigidBodyKinematics.BmatMRP(s)
    wTilde = np.array(RigidBodyKinematics.v3Tilde(w))
    O3x3 = np.zeros((3,3))
    I3x3 = np.eye(3)
    upperLeftBlock = 1./2. * (np.outer(s, w) - np.outer(w, s) - wTilde + np.dot(w, s) * I3x3)
    A = np.block([[upperLeftBlock, -1./4. * B], [O3x3, O3x3]])
    return np.reshape(A, (36, ))

def truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt):
    a = gyroRateNoiseStd
    b = gyroBiasNoiseStd
    Qgyro = np.block([[gyroRateNoiseStd**2 * np.eye(3), np.zeros((3, 3))], [np.zeros((3, 3)), gyroBiasNoiseStd**2 * np.eye(3)]])
    return np.reshape(Qgyro, (36, ))

def truthComputeProcessNoiseTransitionMatrix(sigmaEst_BN):
    s = sigmaEst_BN
    B = RigidBodyKinematics.BmatMRP(s)
    O3x3 = np.zeros((3,3))
    I3x3 = np.eye(3)
    G = np.block([[-1./4. * B, O3x3], [O3x3, I3x3]])
    return np.reshape(G, (36, ))

def truthComputeProcessNoiseMatrix(gyroRateNoiseStd, gyroBiasNoiseStd, sigmaEst_BN, Q, dt):
    Qgyro = np.reshape(truthComputeDiscreteTimeProcessNoiseForGyro(gyroRateNoiseStd, gyroBiasNoiseStd, dt), (6,6))
    G = np.reshape(truthComputeProcessNoiseTransitionMatrix(sigmaEst_BN), (6,6))
    Qtotal = dt * (G @ Qgyro @ G.T) + Q
    return np.reshape(Qtotal, (36, ))

def truthAttitudePropagateSingleStep(sigmaEst_BN, wBiasCompensatedBN_B, dt):
    ds = RigidBodyKinematics.dMRP(sigmaEst_BN, wBiasCompensatedBN_B)
    delta = ds * dt
    return sigmaEst_BN + delta

def truthSwitchMrpAndCovarianceToShadowSet(P, sigmaEst_BN):
    s = sigmaEst_BN
    if np.linalg.norm(s) > 1E-30:
        sMag = np.linalg.norm(s)
        I3x3 = np.eye(3)
        O3x3 = np.zeros((3,3))
        alpha = 2 * sMag**(-4) * np.outer(s, s) - sMag**(-2) * I3x3
        L = np.block([[alpha, O3x3], [O3x3, I3x3]])
        s = - s / np.dot(s, s)
        P = np.reshape(L @ P @ L.T, (36, ))
    else:
        s = s
        P = np.reshape(P, (36, ))
    return s, P
    

def truthComputeMeasurementResdiual(sigmaEst_BN, sigmaMeasured_BN):
    s = sigmaEst_BN
    sMeas = sigmaMeasured_BN
    residual = sMeas - s
    if np.linalg.norm(sMeas) > 1./3.:
        sMeasShadow = - sMeas / np.dot(sMeas, sMeas)
        residualShadow = sMeasShadow - s
        if np.linalg.norm(residualShadow) < np.linalg.norm(residual):
            residual = residualShadow
    return residual

def truthComputeStateTransitionMatrix(A, I, numStates, dt):
    F = I + A * dt + A @ A * dt**2 / 2.0
    return np.reshape(F, (numStates**2, ))

def truthPropagateCovarianceMatrix(F, Q, P, numStates):
    P = F @ P @ F.T + Q
    return np.reshape(P, (numStates**2,))

def truthEkfComputeKalmanGain(P, H, R, numStates, numMeasurements):
    K = P @ H.T @ np.linalg.inv(H @ P @ H.T + R)
    return np.reshape(K, (numStates * numMeasurements, ))

def truthEkfUpdateCovariance(P, H, K, I, R, numStates):
    IKH = I - K @ H
    P = IKH @ P @ IKH.T + K @ R @ K.T
    return np.reshape(P, (numStates**2,))

def truthEkfUpdateStateEstimate(x, K, measurementResidual, numStates, numMeasurements):
    x +=  K @ (measurementResidual)
    return x

def attitudeEKFTestFunction(accuracy):
    """Test method"""
    testFailCount = 0
    testMessages = []
    unitTaskName = "unitTask"
    unitProcessName = "TestProcess"

    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.1)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # Setup module to be tested.
    moduleConfig = attitudeEKF.attitudeEKFConfig()
    moduleWrap = unitTestSim.setModelDataWrap(moduleConfig)
    moduleWrap.ModelTag = "attitudeEKF"
    unitTestSim.AddModelToTask(unitTaskName, moduleWrap, moduleConfig)

    # Configure blank module input messages.
    gyroInMsgData = messaging.GyroMsgPayload()
    gyroInMsg = messaging.GyroMsg().write(gyroInMsgData)
    starTrackerInMsgData = messaging.StarTrackerMsgPayload()
    starTrackerInMsg = messaging.StarTrackerMsg().write(starTrackerInMsgData)

    # Subscribe input messages to module.
    moduleConfig.gyroInMsg.subscribeTo(gyroInMsg)
    moduleConfig.starTrackerInMsg.subscribeTo(starTrackerInMsg)
    
    # Setup input/output message recorder objects.
    navAttMsgEstimatedRec = moduleConfig.navAttMsgEstimated.recorder()
    unitTestSim.AddModelToTask(unitTaskName, navAttMsgEstimatedRec)
    
    
    '''
    Test:   Test Reset_attitudeEKF() by calling InitializeSimulation().
    '''
    unitTestSim.InitializeSimulation()
    
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.P0,
                                        accuracy,
                                        "Test 1",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.Q,
                                        accuracy,
                                        "Test 2",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((9, )),
                                        moduleConfig.R,
                                        accuracy,
                                        "Test 3",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.biasInit,
                                        accuracy,
                                        "Test 4",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.gyroRateNoiseStd,
                                        accuracy,
                                        "Test 5",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.gyroBiasNoiseStd,
                                        accuracy,
                                        "Test 6",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.sigma_GB,
                                        accuracy,
                                        "Test 7",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.sigma_CB,
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfNumCoeffs,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfDenCoeffs,
                                        accuracy,
                                        "Test 10",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((6, )),
                                        moduleConfig.state,
                                        accuracy,
                                        "Test 11",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.biasInit,
                                        accuracy,
                                        "Test 12",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.wBiasCompensatedFilteredBN_B,
                                        accuracy,
                                        "Test 13",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.prevCallTime,
                                        accuracy,
                                        "Test 14",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        moduleConfig.starTrackerInMsgPrev.timeTag,
                                        "Test 15",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((4, )),
                                        moduleConfig.starTrackerInMsgPrev.q_CN,
                                        accuracy,
                                        "Test 16",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        moduleConfig.starTrackerInMsgPrev.valid,
                                        "Test 17",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        moduleConfig.gyroInMsgPrev.timeTag,
                                        "Test 18",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.gyroInMsgPrev.omega_GN_G,
                                        accuracy,
                                        "Test 19",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(0,
                                        moduleConfig.gyroInMsgPrev.valid,
                                        "Test 20",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.sigma_BN,
                                        accuracy,
                                        "Test 21",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.mrpMeasurementResidual,
                                        accuracy,
                                        "Test 22",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.P0,
                                        accuracy,
                                        "Test 23",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.A,
                                        accuracy,
                                        "Test 24",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.F,
                                        accuracy,
                                        "Test 25",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.G,
                                        accuracy,
                                        "Test 26",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.Qgyro,
                                        accuracy,
                                        "Test 27",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((36, )),
                                        moduleConfig.Qtotal,
                                        accuracy,
                                        "Test 28",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((18, )),
                                        moduleConfig.K,
                                        accuracy,
                                        "Test 29",
                                        testFailCount, testMessages)
    Htruth = np.reshape(np.block([[np.eye(3), np.zeros((3, 3))]]), (18, ))
    testFailCount, testMessages = unitTestSupport.compareVector(Htruth,
                                        moduleConfig.H,
                                        accuracy,
                                        "Test 30",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis1.num,
                                        accuracy,
                                        "Test 31",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis1.den,
                                        accuracy,
                                        "Test 32",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis1.y,
                                        accuracy,
                                        "Test 33",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis1.yPrev,
                                        accuracy,
                                        "Test 34",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis1.u,
                                        accuracy,
                                        "Test 35",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis1.uPrev,
                                        accuracy,
                                        "Test 36",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis2.num,
                                        accuracy,
                                        "Test 37",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis2.den,
                                        accuracy,
                                        "Test 38",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis2.y,
                                        accuracy,
                                        "Test 39",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis2.yPrev,
                                        accuracy,
                                        "Test 40",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis2.u,
                                        accuracy,
                                        "Test 41",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis2.uPrev,
                                        accuracy,
                                        "Test 42",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis3.num,
                                        accuracy,
                                        "Test 43",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((2, )),
                                        moduleConfig.lpfGyroAxis3.den,
                                        accuracy,
                                        "Test 44",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis3.y,
                                        accuracy,
                                        "Test 45",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis3.yPrev,
                                        accuracy,
                                        "Test 46",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis3.u,
                                        accuracy,
                                        "Test 47",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.lpfGyroAxis3.uPrev,
                                        accuracy,
                                        "Test 48",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareInt(attitudeEKF.INITIALIZING,
                                        moduleConfig.filterStatus,
                                        "Test 49",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.timeTag,
                                        accuracy,
                                        "Test 50",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.,
                                        moduleConfig.timeTagPrev,
                                        accuracy,
                                        "Test 51",
                                        testFailCount, testMessages)
    
    
    '''
    Test:   Test Update_attitudeEKF() by calling ExecuteSimulation() with no
            valid star tracker measurement, no valid gyro measurement, and
            with the filter in the INITIALIZATION state.
    '''
    # Setup case.
    setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg, valid = 0)
    setupGyroMsg(gyroInMsgData, gyroInMsg, valid = 0)
    setupValidEkfConfig(moduleConfig, unitTestSim)
    
    expectedTimeTag = macros.sec2nano(100.)
    moduleConfig.timeTag = expectedTimeTag
    moduleConfig.prevCallTime = expectedTimeTag
    
    expectedValidity = 0
    moduleConfig.gyroInMsgPrev.valid = 1
    moduleConfig.starTrackerInMsgPrev.valid = 1
    
    expectedState = [1., 2., 3., 4., 5., 6.]
    moduleConfig.state = expectedState
    
    expectedWBiasCompensatedFilteredBN_B = [1., 2., 3.]
    moduleConfig.wBiasCompensatedFilteredBN_B = expectedWBiasCompensatedFilteredBN_B
    
    
    
    # Call Update_attitudeEKF() by executing sim.
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0)) # forces only one iteration to run
    unitTestSim.ExecuteSimulation()
    
    # Verify filter state is still INITIALIZING.
    testFailCount, testMessages = unitTestSupport.compareInt(attitudeEKF.INITIALIZING,
                                        moduleConfig.filterStatus,
                                        "Test 52",
                                        testFailCount, testMessages)
    
    # Verify timeTag has not been updated.
    testFailCount, testMessages = unitTestSupport.compareInt(expectedTimeTag,
                                        moduleConfig.timeTag,
                                        "Test 53",
                                        testFailCount, testMessages)
    
    # Verify timeTagPrev was updated.
    testFailCount, testMessages = unitTestSupport.compareInt(expectedTimeTag,
                                        moduleConfig.timeTagPrev,
                                        "Test 54",
                                        testFailCount, testMessages)
    
    # Verify prevCallTime was updated.
    expectedCallTime = 0
    testFailCount, testMessages = unitTestSupport.compareInt(expectedCallTime,
                                        moduleConfig.prevCallTime,
                                        "Test 55",
                                        testFailCount, testMessages)
    
    # Verify starTrackerInMsgPrev was updated.
    testFailCount, testMessages = unitTestSupport.compareInt(validTimeTag,
                                        moduleConfig.starTrackerInMsgPrev.timeTag,
                                        "Test 56",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareInt(expectedValidity,
                                        moduleConfig.starTrackerInMsgPrev.valid,
                                        "Test 57",
                                        testFailCount, testMessages)
        
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(validQ_CN),
                                        moduleConfig.starTrackerInMsgPrev.q_CN,
                                        accuracy,
                                        "Test 58",
                                        testFailCount, testMessages)
    
    # Verify gyroInMsgPrev was updated.
    testFailCount, testMessages = unitTestSupport.compareInt(validTimeTag,
                                        moduleConfig.gyroInMsgPrev.timeTag,
                                        "Test 59",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareInt(expectedValidity,
                                        moduleConfig.gyroInMsgPrev.valid,
                                        "Test 60",
                                        testFailCount, testMessages)
        
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(validOmega_GN_G),
                                        moduleConfig.gyroInMsgPrev.omega_GN_G,
                                        accuracy,
                                        "Test 61",
                                        testFailCount, testMessages)
    
    # Verify that the filter state was not updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(expectedState),
                                        moduleConfig.state,
                                        accuracy,
                                        "Test 62",
                                        testFailCount, testMessages)
    
    # Verify wBiasCompensatedFilteredBN_B has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(expectedWBiasCompensatedFilteredBN_B),
                                        moduleConfig.wBiasCompensatedFilteredBN_B,
                                        accuracy,
                                        "Test 63",
                                        testFailCount, testMessages)
    
    # Verify that navAttMsgEstimated has been updated correctly.
    testFailCount, testMessages = unitTestSupport.compareDouble(expectedTimeTag,
                                        navAttMsgEstimatedRec.timeTag,
                                        accuracy,
                                        "Test 64",
                                        testFailCount, testMessages)
        
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(expectedState[0:3]),
                                        navAttMsgEstimatedRec.sigma_BN[0, :],
                                        accuracy,
                                        "Test 65",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(expectedWBiasCompensatedFilteredBN_B),
                                        navAttMsgEstimatedRec.omega_BN_B[0, :],
                                        accuracy,
                                        "Test 66",
                                        testFailCount, testMessages)
    
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        navAttMsgEstimatedRec.vehSunPntBdy[0, :],
                                        accuracy,
                                        "Test 67",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test Update_attitudeEKF() by calling ExecuteSimulation() with a
            valid star tracker measurement, no valid gyro measurement, and
            with the filter in the INITIALIZATION state.
    '''
    # Setup case.
    setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg)
    setupGyroMsg(gyroInMsgData, gyroInMsg, valid = 0)
    setupValidEkfConfig(moduleConfig, unitTestSim)
    state = [1., 2., 3., 4., 5., 6.]
    moduleConfig.state = state
    moduleConfig.mrpMeasurementResidual = [1., 2., 3.]

    # Call Update_attitudeEKF() by executing sim.
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0)) # forces only one iteration to run
    unitTestSim.ExecuteSimulation()
    
    # Verify filter state is UNKNOWN.
    testFailCount, testMessages = unitTestSupport.compareInt(attitudeEKF.UNKNOWN,
                                        moduleConfig.filterStatus,
                                        "Test 68",
                                        testFailCount, testMessages)
    
    # Verify that estimate time tag matches star tracker time tag.
    testFailCount, testMessages = unitTestSupport.compareDouble(validTimeTag,
                                        navAttMsgEstimatedRec.timeTag,
                                        accuracy,
                                        "Test 69",
                                        testFailCount, testMessages)
    
    # Verify that the filter state has been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(state),
                                        moduleConfig.state,
                                        accuracy,
                                        "Test 70",
                                        testFailCount, testMessages, ExpectedResult=0)
    
    # Verify that that filter is seeded with the star tracker measurement.
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        moduleConfig.mrpMeasurementResidual,
                                        accuracy,
                                        "Test 71",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test Update_attitudeEKF() by calling ExecuteSimulation() with a
            valid star tracker measurement, no valid gyro measurement, and
            with the filter not in the INITIALIZATION state.
    '''
    # Setup case.
    setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg)
    setupGyroMsg(gyroInMsgData, gyroInMsg, valid = 0)
    setupValidEkfConfig(moduleConfig, unitTestSim)
    state = [1., 2., 3., 4., 5., 6.]
    moduleConfig.state = state
    moduleConfig.filterStatus = attitudeEKF.UNKNOWN
    wBiasCompensatedBN_B = [1., 2., 3.]
    moduleConfig.wBiasCompensatedBN_B = wBiasCompensatedBN_B
    
    q_CN = np.array(validQ_CN)
    sigma_CN = RigidBodyKinematics.EP2MRP(q_CN)
    sigma_BC = -np.array(moduleConfig.sigma_CB)
    sigmaMeas_BN = RigidBodyKinematics.addMRP(sigma_CN, sigma_BC)
    
    numStates = 6
    numMeasurements = 3
    P = np.reshape(np.array(moduleConfig.P0), (numStates, numStates))
    H = np.reshape(np.array(moduleConfig.H), (numMeasurements, numStates))
    R = np.reshape(np.array(moduleConfig.R), (numMeasurements, numMeasurements))
    
    # Call Update_attitudeEKF() by executing sim.
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.0)) # forces only one iteration to run
    unitTestSim.ExecuteSimulation()
    
    # Verify that the state is computed correctly.
    A = truthComputeDynamicsMatrix(np.array(state[0:3]), np.array(wBiasCompensatedBN_B))
    A = np.reshape(A, (numStates, numStates))
    
    I6x6 = np.eye(numStates)
    dt = 0.1
    F = truthComputeStateTransitionMatrix(A, I6x6, numStates, dt)
    
    Q = np.zeros((numStates, numStates))
    Qtotal = truthComputeProcessNoiseMatrix(moduleConfig.gyroRateNoiseStd, moduleConfig.gyroBiasNoiseStd, np.array(state[0:3]), Q, dt)
    Qtotal = np.reshape(Qtotal, (numStates, numStates))
    
    F = np.reshape(F, (numStates, numStates))
    P = truthPropagateCovarianceMatrix(F, Qtotal, P, numStates)
    
    P = np.reshape(P, (numStates, numStates))
    sigmaEst_BN = truthAttitudePropagateSingleStep(np.array(state[0:3]), np.array(wBiasCompensatedBN_B), dt)
    
    sigmaEst_BN, P = truthSwitchMrpAndCovarianceToShadowSet(P, sigmaEst_BN)    
    
    P = np.reshape(P, (numStates, numStates))
    K = truthEkfComputeKalmanGain(P, H, R, numStates, numMeasurements)
    
    K = np.reshape(K, (numStates, numMeasurements))
    residual = truthComputeMeasurementResdiual(sigmaEst_BN, sigmaMeas_BN)
    
    newStateEst = np.array([sigmaEst_BN[0], sigmaEst_BN[1], sigmaEst_BN[2], 4., 5., 6.])
    xEst = truthEkfUpdateStateEstimate(newStateEst, K, residual, numStates, numMeasurements)
    
    testFailCount, testMessages = unitTestSupport.compareVector(xEst,
                                        moduleConfig.state,
                                        accuracy,
                                        "Test 72",
                                        testFailCount, testMessages)
    
    # Verify that the covariance is computed correctly.
    P = truthEkfUpdateCovariance(P, H, K, I6x6, R, numStates)
    P = np.reshape(P, (numStates**2, ))
    testFailCount, testMessages = unitTestSupport.compareVector(P,
                                        moduleConfig.P,
                                        accuracy,
                                        "Test 73",
                                        testFailCount, testMessages)
    
        
    '''
    Test:   Test Update_attitudeEKF() by calling ExecuteSimulation() with no
            valid star tracker measurement, a valid gyro measurement, with the
            filter in the not INITIALIZATION state, no new gyro
            measurement, and a mrp estimate that has a norm greater than 1
            after propagation.
    '''
    # Setup case.
    setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg, valid = 0)
    setupGyroMsg(gyroInMsgData, gyroInMsg)
    setupValidEkfConfig(moduleConfig, unitTestSim)
    state = [1000., 2., 3., 4., 5., 6.]
    moduleConfig.state = state
    moduleConfig.filterStatus = attitudeEKF.UNKNOWN
    wBiasCompensatedFilteredBN_B = [1., 2., 3.]
    wBiasCompensatedBN_B = [1., 2., 3.]
    moduleConfig.wBiasCompensatedFilteredBN_B = wBiasCompensatedFilteredBN_B
    moduleConfig.wBiasCompensatedBN_B = wBiasCompensatedFilteredBN_B
    moduleConfig.gyroInMsgPrev.timeTag = validTimeTag
    moduleConfig.timeTagPrev = 1
       
    # Call Update_attitudeEKF() by executing sim.
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.1)) # forces only one iteration to run
    unitTestSim.ExecuteSimulation()
    
    # Verify filter state is UNKNOWN.
    testFailCount, testMessages = unitTestSupport.compareInt(attitudeEKF.UNKNOWN,
                                        moduleConfig.filterStatus,
                                        "Test 74",
                                        testFailCount, testMessages)
    
    # Verify that the filter bias estimate has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(state[3:]),
                                        moduleConfig.state[3:],
                                        accuracy,
                                        "Test 75",
                                        testFailCount, testMessages)
    
    # Verify that the filter mrp estimate has been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(state[0:3]),
                                        moduleConfig.state[0:3],
                                        accuracy,
                                        "Test 76",
                                        testFailCount, testMessages, ExpectedResult=0)
    
    # Verify that the filtered and bias compensated rate has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(wBiasCompensatedFilteredBN_B),
                                        moduleConfig.wBiasCompensatedFilteredBN_B,
                                        accuracy,
                                        "Test 77",
                                        testFailCount, testMessages)
    
    # Verify that the bias compensated rate has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(wBiasCompensatedBN_B),
                                        moduleConfig.wBiasCompensatedBN_B,
                                        accuracy,
                                        "Test 78",
                                        testFailCount, testMessages)
    
    # Verify that timeTag has been updated properly.
    testFailCount, testMessages = unitTestSupport.compareDouble(1 +  macros.sec2nano(0.1),
                                        moduleConfig.timeTag,
                                        accuracy,
                                        "Test 79",
                                        testFailCount, testMessages)
    
    
    '''
    Test:   Test Update_attitudeEKF() by calling ExecuteSimulation() with no
            valid star tracker measurement, a valid gyro measurement, with the
            filter in the INITIALIZATION state, and a new gyro measurement is 
            available.
    '''
    # Setup case.
    setupStarTrackerMsg(starTrackerInMsgData, starTrackerInMsg, valid = 0)
    setupGyroMsg(gyroInMsgData, gyroInMsg)
    setupValidEkfConfig(moduleConfig, unitTestSim)
    state = [1., 2., 3., 4., 5., 6.]
    moduleConfig.state = state
    moduleConfig.filterStatus = attitudeEKF.INITIALIZING
    moduleConfig.timeTagPrev = 1
    
    # Call Update_attitudeEKF() by executing sim.
    unitTestSim.ConfigureStopTime(macros.sec2nano(0.)) # forces only one iteration to run
    unitTestSim.ExecuteSimulation()
    
    # Verify filter state is INITIALIZING.
    testFailCount, testMessages = unitTestSupport.compareInt(attitudeEKF.INITIALIZING,
                                        moduleConfig.filterStatus,
                                        "Test 80",
                                        testFailCount, testMessages)
    
    # Verify that the filter bias estimate has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(state[3:]),
                                        moduleConfig.state[3:],
                                        accuracy,
                                        "Test 81",
                                        testFailCount, testMessages)
    
    # Verify that the filter mrp estimate has not been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(state[0:3]),
                                        moduleConfig.state[0:3],
                                        accuracy,
                                        "Test 82",
                                        testFailCount, testMessages)
    
    # Verify that the filtered and bias compensated rate has been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        moduleConfig.wBiasCompensatedFilteredBN_B,
                                        accuracy,
                                        "Test 83",
                                        testFailCount, testMessages, ExpectedResult=0)
    
    # Verify that the bias compensated rate has been updated.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array([0., 0., 0.]),
                                        moduleConfig.wBiasCompensatedBN_B,
                                        accuracy,
                                        "Test 84",
                                        testFailCount, testMessages, ExpectedResult=0)
    
    # Verify that that wBiasCompensated is computed correctly.
    sigma_BG = -np.array(moduleConfig.sigma_GB)
    BG = RigidBodyKinematics.MRP2C(sigma_BG)
    omega_BN_B = BG @ np.array(validOmega_GN_G)
    wBiasCompensatedBN_B = omega_BN_B - np.array(moduleConfig.state[3:])
    
    testFailCount, testMessages = unitTestSupport.compareVector(wBiasCompensatedBN_B,
                                        moduleConfig.wBiasCompensatedBN_B,
                                        accuracy,
                                        "Test 85",
                                        testFailCount, testMessages)
    
    # Verify that star tracker rotation is computed correctly.
    q_CN = np.array(validQ_CN)
    sigma_CN = RigidBodyKinematics.EP2MRP(q_CN)
    sigma_BC = -np.array(moduleConfig.sigma_CB)
    sigma_BN = RigidBodyKinematics.addMRP(sigma_CN, sigma_BC)
    
    testFailCount, testMessages = unitTestSupport.compareVector(sigma_BN,
                                        moduleConfig.sigma_BN,
                                        accuracy,
                                        "Test 86",
                                        testFailCount, testMessages)
    
    
    if testFailCount == 0:
        print("PASSED: " + "test_attitudeEKF.py")
    else:
        print(testMessages)

    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_attitudeEKF(1e-12)
