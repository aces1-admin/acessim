#ifndef ATTITUDEEKF_H
#define ATTITUDEEKF_H

#include <stdint.h> // needed for swigging filterStatusEnum
#include "cMsgCInterface/NavAttMsg_C.h"
#include "cMsgCInterface/GyroMsg_C.h"
#include "cMsgCInterface/StarTrackerMsg_C.h"
#include "architecture/utilities/bskLogging.h"
#include "../_GeneralModuleFiles/lowPassFilter.h"

//! Number of filter states. 3 for attitude MRP and 3 for gyro bias.
#define NUM_ATT_EKF_STATES 6

//! Number of filter measurements. 3 for each element of the measured MRP.
#define NUM_ATT_EKF_OUTPUTS 3

//! Enum defining filter status.
typedef enum
{
    INITIALIZING = 0,
    PROPAGATING = 1,
    CONVERGED = 2,
    UNCONVERGED = 3,
    UNKNOWN = 4
} filterStatusEnum;

/*! @brief Extended Kalman Filter that estimates the spacecraft attitude and gyro bias.
 */
typedef struct {
    // Declare the input and output messages.
    NavAttMsg_C navAttMsgEstimated;                         //!< Estimated attitude output message
    GyroMsg_C gyroInMsg;                                    //!< Input message from Gyro
    StarTrackerMsg_C starTrackerInMsg;                      //!< Input message from Star Tracker
    
    // Declare the configuration parameters.
    double P0[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];     //!< Initial covariance matrix
    double Q[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];      //!< Process noise matrix
    double R[NUM_ATT_EKF_OUTPUTS * NUM_ATT_EKF_OUTPUTS];    //!< Measurement noise matrix
    double biasInit[3];                                     //!< [rad/s] Initial gyro bias of the filter
    double gyroRateNoiseStd;                                //!< [rad / s^(1/2)] Standard deviation of gyro rate noise
    double gyroBiasNoiseStd;                                //!< [rad / s^(3/2)] Standard deviation of gyro bias noise
    double sigma_GB[3];                                     //!< MRP rotation from the Body to the Gyro frame
    double sigma_CB[3];                                     //!< MRP rotation from the Body to the star tracker Case frame
    double lpfNumCoeffs[2];                                 //!< Numerator coefficients for gyro low pass filter
    double lpfDenCoeffs[2];                                 //!< Denominator coefficients for gyro low pass filter

    // Declare internal module variables.
    double state[NUM_ATT_EKF_STATES];                       //!< State of the filter (mrp and bias estimate)
    double wBiasCompensatedBN_B[3];                         //!< [rad / s] Bias compensated spacecraft angular velocity
    double wBiasCompensatedFilteredBN_B[3];                 //!< [rad / s] Low pass filtered bias compensated spacecraft angular velocity
    double prevCallTime;                                    //!< Previous call time of the algorithm
    StarTrackerMsgPayload starTrackerInMsgPrev;             //!< Previous input message from Star Tracker
    GyroMsgPayload gyroInMsgPrev;                           //!< Previous input message from Gyro
    double sigma_BN[3];                                     //!< MRP measurement rotated to be Body relative to Inertial
    double mrpMeasurementResidual[3];                       //!< MRP measurement residual
    double P[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];      //!< Covariance matrix
    double A[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];      //!< Dynamics matrix
    double F[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];      //!< State transition matrix
    double G[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];      //!< Gyro process noise transition matrix
    double Qgyro[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES];  //!< Gyro process noise matrix
    double Qtotal[NUM_ATT_EKF_STATES * NUM_ATT_EKF_STATES]; //!< Total process noise matrix
    double K[NUM_ATT_EKF_STATES * NUM_ATT_EKF_OUTPUTS];     //!< Kalman gain matrix
    double H[NUM_ATT_EKF_OUTPUTS * NUM_ATT_EKF_STATES];     //!< Measurement sensitivity matrix
    double I6X6[6 * 6];                                     //!< Helpful definition of 6x6 identity matrix
    LPF lpfGyroAxis1;                                       //!< Low pass filter config for the first axis of the bias compensated angular rate
    LPF lpfGyroAxis2;                                       //!< Low pass filter config for the second axis of the bias compensated angular rate
    LPF lpfGyroAxis3;                                       //!< Low pass filter config for the third axis of the bias compensated angular rate
    filterStatusEnum filterStatus;                          ///!< Status of the EKF
    uint64_t timeTag;                                       //!< [ns] Time associated with current attitude estimate
    uint64_t timeTagPrev;                                   //!< [ns] Time associated with previous attitude estimate
    BSKLogger *bskLogger;                                   //!< BSK Logging
}attitudeEKFConfig;

#ifdef __cplusplus
extern "C" {
#endif
    void SelfInit_attitudeEKF(attitudeEKFConfig *configData, int64_t moduleID);
    void Update_attitudeEKF(attitudeEKFConfig *configData, uint64_t callTime, int64_t moduleID);
    void Reset_attitudeEKF(attitudeEKFConfig *configData, uint64_t callTime, int64_t moduleID);

#ifdef __cplusplus
}
#endif

#endif
