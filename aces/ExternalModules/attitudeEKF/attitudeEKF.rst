Executive Summary
-----------------
Extended Kalman Filter that estimates the spacecraft attitude and angular rates.

Message Connection Descriptions
-------------------------------
The following table lists all the module input and output messages.  
The module msg connection is set by the user from python.  
The msg type contains a link to the message structure definition, while the description 
provides information on what this message is used for.

.. list-table:: Module I/O Messages
    :widths: 25 25 50
    :header-rows: 1

    * - Msg Variable Name
      - Msg Type
      - Description
    * - navAttMsgEstimated
      - :ref:`NavAttMsgPayload`
      - Estimated attitude output message.
    * - gyroInMsg
      - :ref:`GyroMsgPayload`
      -  Input message from GyroSensor.
    * - starTrackerInMsg
      - :ref:`StarTrackerMsgPayload`
      -  Input message from StarTrackerSensor.
