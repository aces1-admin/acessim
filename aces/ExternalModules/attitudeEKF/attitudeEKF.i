%module attitudeEKF
%{
    #include "attitudeEKF.h"
%}


%pythoncode %{
    from Basilisk.architecture.swig_common_model import *
%}
%include "swig_conly_data.i"
%constant void Update_attitudeEKF(void*, uint64_t, uint64_t);
%ignore Update_attitudeEKF;
%constant void SelfInit_attitudeEKF(void*, uint64_t);
%ignore SelfInit_attitudeEKF;
%constant void Reset_attitudeEKF(void*, uint64_t, uint64_t);
%ignore Reset_attitudeEKF;

%include "attitudeEKF.h"

%include "architecture/msgPayloadDefC/NavAttMsgPayload.h"
struct NavAttMsg_C;

%include "msgPayloadDefC/GyroMsgPayload.h"
struct GyroMsg_C;

%include "msgPayloadDefC/StarTrackerMsgPayload.h"
struct StarTrackerMsg_C;

%include "../_GeneralModuleFiles/lowPassFilter.h"
struct LPF;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}

