#
# Attitude EKF Performance Test
#
# Purpose:  Component level testing for the attitude
#           Extended Kalman Filter.
# Author:   Chase Heinen
# Creation Date: September 27, 2022
# Last Edit Date: November 1, 2022
#
# Description:
#
#   This script evaluates the performance of the attitude Extended Kalman
#   Filter used in the ACES FSW.
#

#
# Import required modules.
from dataclasses import dataclass
from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.utilities import (macros,
                                RigidBodyKinematics)
from Basilisk.ExternalModules import (attitudeEKF,
                                      gyroSensor,
                                      starTrackerSensor)
from Basilisk.simulation import spacecraft
from Basilisk import __path__
bskPath = __path__[0]

#
# Add other folders to path.
import sys, os
sys.path.append(os.path.join(bskPath, '../../aces/sim/analysis'))

#
# Import plotting methods.
from scenarioBaselinePlotting import *

#
# Creating plotting directory if it does not exist.
plotPath = os.path.join(bskPath, '../../aces/ExternalModules/attitudeEKF/_ComponentTest/plots')
if not os.path.exists(plotPath):
    os.makedirs(plotPath)

#
# Define simulation constants.
TASK_NAME = "testTask"
DYNAMICS_PROCESS_NAME = 'DynamicsProcess'
FSW_PROCESS_NAME = 'FSWProcess'

#
# Define test constants.
AROUND_BORESIGHT_STD = 20. * macros.ARCSEC2RAD
CROSS_BORESIGHT_STD = 5/3. * macros.ARCSEC2RAD

#
# Configure matplotlib properties.
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=MEDIUM_SIZE) # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE) # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE) # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE) # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE) # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE) # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE) # fontsize of the figure title
plt.rcParams['figure.figsize'] = [16.0, 9.0]
plt.rcParams['lines.linewidth'] = 2

#
# Define custom helper methods.
def MRP2RAD(mrp):
    """
    A method that converts an MRP to 321 Euler angles
    with units of radians.
    """

    #
    # Convert to Euler 321.
    return RigidBodyKinematics.MRP2Euler123(mrp)

def RAD2MRP(rad):
    """
    A method that converts 321 Euler angles with units
    of radians into MRP.
    """

    #
    # Convert to MRP.
    mrp = RigidBodyKinematics.euler3212MRP(rad)
    return RigidBodyKinematics.MRPswitch(mrp, 1)

#
# Define custom plotting methods.
def plotAttitude(times, estAttRad, truthAttRad, figNum = -1, runNum = -1, total = -1):
    """
    A method that plots the estimated and truth spacecraft
    attitude.
    """

    #
    # Create figure.
    fig, axes = plt.subplots(3, sharex=True, sharey=True, dpi=150)

    #
    # Iterate through axes.
    for axis in range(0, 3):
        color,label = getColorLabel(0, 2, r'$\hat{\sigma}_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, estAttRad[:, axis], color=color, label=label)
        color,label = getColorLabel(1, 2, r'$\sigma_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, truthAttRad[:, axis], color=color, label=label)
        axes[axis].grid(True)
        axes[axis].legend(loc="center left", bbox_to_anchor=(1.01, 0.5))
    axes[0].set_title("Truth and Estimated Attitue vs. Time")
    fig.text(0.5, -0.02, 'Time [min]', ha='center')
    fig.text(-0.02, 0.5, 'Attitude [rad]', va='center', rotation='vertical')

def plotAttitudeError(times, diffAttRad, sigma, figNum = -1, runNum = -1, total = -1):
    """
    A method that plots the error between the estimated and 
    truth spacecraft attitude.
    """

    #
    # Create figure.
    fig, axes = plt.subplots(3, sharex=True, sharey=False, dpi=150)

    #
    # Convert radians to arcseconds.
    diffAttArcSec = []
    for attRad in diffAttRad:
        diffAttArcSec.append(np.array((attRad * macros.RAD2ARCSEC)))
    diffAttArcSec = np.array(diffAttArcSec)

    #
    # Construct plot.
    for idx in range(0, 3):
            color,label = getColorLabel(idx, 3, r'$\sigma_{BN,' + str(idx) + '} - \hat{\sigma}_{BN,' + str(idx) + '}$', runNum, total)
            axes[idx].plot(times, diffAttArcSec[:, idx], color=color, label=label)
            axes[idx].plot(times, 2 * sigma[:, idx], color='black', label='', linestyle='--')
            axes[idx].plot(times, - 2 * sigma[:, idx], color='black', label='', linestyle='--')
            axes[idx].set_xlim(times[0], times[-1])
            axes[idx].grid(True)
    axes[0].set_title("Estimated Attitude Error vs. Time")
    fig.legend(loc="center left", bbox_to_anchor=(1.01, 0.5))
    fig.text(0.5, -0.02, 'Time [min]', ha='center')
    fig.text(-0.02, 0.5, 'Attitude Error [arcseconds]', va='center', rotation='vertical')

def plotAngularRate(times, estAngRate, truthAngRate, figNum = -1, runNum = -1, total = -1):
    """
    A method that plots the estimated and truth spacecraft
    angular rate.
    """

    #
    # Create figure.
    fig, axes = plt.subplots(3, sharex=True, sharey=True, dpi=150)

    #
    # Construct numpy arrays.
    estAngRate = np.array(estAngRate)
    truthAngRate = np.array(truthAngRate)

    #
    # Iterate through axes.
    for axis in range(0, 3):
        color,label = getColorLabel(0, 2, r'$\hat{\omega}_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, estAngRate[:, axis], color=color, label=label)
        color,label = getColorLabel(1, 2, r'$\omega_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, truthAngRate[:, axis], color=color, label=label)
        axes[axis].grid(True)
        axes[axis].legend(loc="center left", bbox_to_anchor=(1.01, 0.5))
    axes[0].set_title("Truth and Estimated Angular Rate vs. Time")
    fig.text(0.5, -0.02, 'Time [min]', ha='center')
    fig.text(-0.02, 0.5, 'Angular Rate [rad/s]', va='center', rotation='vertical')

def plotAngularRateError(times, estAngRate, truthAngRate, sigma, figNum = -1, runNum = -1, total = -1):
    """
    A method that plots the error between the estimated 
    and truth spacecraft angular rate.
    """

    #
    # Create figure.
    fig, axes = plt.subplots(3, sharex=True, sharey=True, dpi=150)

    #
    # Construct numpy arrays.
    angRateErr = (np.array(estAngRate) - np.array(truthAngRate)) * macros.RAD2ARCSEC

    #
    # Iterate through axes.
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\omega_{BN,' + str(axis) + '} - \hat{\omega}_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, angRateErr[:, axis], color=color, label=label)
        axes[axis].plot(times, 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='', linestyle='--')
        if axis == 2:
            axes[axis].plot(times, - 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='2 ' + r'$\sigma$' + ' bounds', linestyle='--')
        else:
            axes[axis].plot(times, - 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='', linestyle='--')
        axes[axis].set_xlim(times[0], times[-1])
        axes[axis].set_ylim(-25, 25)
        axes[axis].grid(True)
    axes[0].set_title("Estimated Angular Rate Error vs. Time")    
    fig.legend(loc="center left", bbox_to_anchor=(1.01, 0.5))
    fig.text(0.5, -0.02, 'Time [min]', ha='center')
    fig.text(-0.02, 0.5, 'Angular Rate Error [arcseconds/s]', va='center', rotation='vertical')

def plotRateBias(times, truthAngRate, biasCompAngRate, sigma, figNum = -1, runNum = -1, total = -1):
    """
    A method that compares the true, estimates, bias
    compensated, and bias compensated filtered angular
    rate.
    """

    #
    # Create figure.
    fig, axes = plt.subplots(3, sharex=True, sharey=True, dpi=150)

    #
    # Construct numpy arrays.
    angRateErr = (np.array(biasCompAngRate) - np.array(truthAngRate)) * macros.RAD2ARCSEC

    #
    # Iterate through axes.
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\omega_{BN,' + str(axis) + '} - \hat{\omega}_{BN,' + str(axis) + '}$', runNum, total)
        axes[axis].plot(times, angRateErr[:, axis], color=color, label=label)
        axes[axis].plot(times, 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='', linestyle='--')
        if axis == 2:
            axes[axis].plot(times, - 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='2 ' + r'$\sigma$' + ' bounds', linestyle='--')
        else:
            axes[axis].plot(times, - 2 * sigma[:, axis] * macros.RAD2ARCSEC, color='black', label='', linestyle='--')
        axes[axis].set_xlim(times[0], times[-1])
        axes[axis].set_ylim(-25, 25)
        axes[axis].grid(True)
    axes[0].set_title("Estimated Bias Compensated Angular Rate Error vs. Time")
    fig.legend(loc="center left", bbox_to_anchor=(1.01, 0.5))
    fig.text(0.5, -0.02, 'Time [min]', ha='center')
    fig.text(-0.02, 0.5, 'Angular Rate Error [arcseconds/s]', va='center', rotation='vertical')

#
# Define component configuration dataclass.
@dataclass
class configData:
    """
    Component configuration data class.
    """

    #
    # Define class attributes.
    gyro_rateNoiseStd: float
    gyro_biasNoiseStd: float
    gyro_wBiasInit: list
    gyro_sigma_GB: list
    starTracker_R: list
    starTracker_sigma_CB: list
    ekf_wBiasInit: list
    ekf_P0: list
    ekf_Q: list
    ekf_R: list
    ekf_rateNoiseStd: float
    ekf_biasNoiseStd: float
    ekf_sigma_GB: list
    ekf_sigma_CB: list
    ekf_lpfNum: list
    ekf_lpfDen: list
    sc_sigma_BN: list
    sc_omega_BN: list

#
# Define component level testing class.
class ComponentTest(SimulationBaseClass.SimBaseClass):
    """
    Component level testing simulation class.
    """

    def __init__(self, dynRate, fswRate, simLength, testNum, configData):
        """
        Initializes the component level simulation
        base class.
        """

        #
        # Set the simulation properties.
        self.dynRate = dynRate
        self.fswRate = fswRate
        self.simTime = macros.min2nano(simLength)

        #
        # Set the test properties.
        self.testNum = testNum

        #
        # Set the configuration data.
        self.configData = configData

        #
        # Create the simulation module.
        SimulationBaseClass.SimBaseClass.__init__(self)

        #
        # Set dynamics and fsw process variables.
        self.dynProc = None
        self.fswProc = None

        #
        # Declare message recording variables.
        self.msgRecList = {}
        self.scStateMsgName = "scStateMsg"
        self.starTrackerMsgName = "starTrackerMsg"
        self.gyroMsgName = "gyroMsg"
        self.attitudeEKFMsgName = "attitudeEKFMsg"

        #
        # Set dynamics and FSW.
        self.set_dyn_model()
        self.set_fsw_model()

        #
        # Link message.
        self.link_messages()

        #
        # Call functions to configure the initial simulation conditions and log outputs.
        self.configure_initial_conditions()
        self.log_outputs()

    def set_dyn_model(self):
        """
        Sets the simulation dynamics model.
        """

        #
        # Create process.
        self.dynProc = self.CreateNewProcess(DYNAMICS_PROCESS_NAME)

        #
        # Create dynamics side task.
        dynTaskRate = macros.sec2nano(self.dynRate)
        self.dynProc.addTask(self.CreateNewTask(TASK_NAME, dynTaskRate))

        #
        # Instantiate dynamics modules.
        self.scObject = spacecraft.Spacecraft()
        self.gyro = gyroSensor.GyroSensor()
        self.starTracker = starTrackerSensor.StarTrackerSensor()

        #
        # Configure dynamics modules.
        self.set_gyro_sensor()
        self.set_star_tracker_sensor()

        #
        # Add modules to task list.
        self.AddModelToTask(TASK_NAME, self.scObject)
        self.AddModelToTask(TASK_NAME, self.gyro)
        self.AddModelToTask(TASK_NAME, self.starTracker)

    def set_fsw_model(self):
        """
        Sets the simulation fsw model.
        """

        #
        # Create process.
        self.fswProc = self.CreateNewProcess(FSW_PROCESS_NAME)

        #
        # Create fsw side task.
        fswTaskRate = macros.sec2nano(self.fswRate)
        self.fswProc.addTask(self.CreateNewTask(TASK_NAME, fswTaskRate))

        #
        # Instantiate fsw modules.
        self.attitudeEkfConfig = attitudeEKF.attitudeEKFConfig()
        self.attitudeEkfWrap = self.setModelDataWrap(self.attitudeEkfConfig)
        self.attitudeEkfWrap.ModelTag = "attitudeEKF"

        #
        # Configure fsw modules.
        self.set_attitude_ekf()

        #
        # Add modules to task list.
        self.AddModelToTask(TASK_NAME, self.attitudeEkfWrap, self.attitudeEkfConfig)

    def set_gyro_sensor(self):
        """
        Sets the GyroSensor module.
        """

        #
        # Configure module.
        self.gyro.ModelTag = "gyroSensor"
        self.gyro.rateNoiseStd = self.configData.gyro_rateNoiseStd
        self.gyro.biasNoiseStd = self.configData.gyro_biasNoiseStd
        self.gyro.wBiasInit = self.configData.gyro_wBiasInit
        self.gyro.sigma_GB = self.configData.gyro_sigma_GB

    def set_star_tracker_sensor(self):
        """
        Sets the StarTrackerSensor module.
        """

        #
        # Configure module.
        self.starTracker.ModelTag = "starTrackerSensor"
        self.starTracker.R = self.configData.starTracker_R
        self.starTracker.sigma_CB = self.configData.starTracker_sigma_CB

    def set_attitude_ekf(self):
        """
        Sets the attitutdeEKFConfig module.
        """

        #
        # Configure module.
        self.attitudeEkfConfig.biasInit = self.configData.ekf_wBiasInit
        self.attitudeEkfConfig.P0 = self.configData.ekf_P0
        self.attitudeEkfConfig.Q = self.configData.ekf_Q
        self.attitudeEkfConfig.R = self.configData.ekf_R
        self.attitudeEkfConfig.gyroRateNoiseStd = self.configData.ekf_rateNoiseStd
        self.attitudeEkfConfig.gyroBiasNoiseStd = self.configData.ekf_biasNoiseStd
        self.attitudeEkfConfig.sigma_GB = self.configData.ekf_sigma_GB
        self.attitudeEkfConfig.sigma_CB = self.configData.ekf_sigma_CB
        self.attitudeEkfConfig.lpfNumCoeffs = self.configData.ekf_lpfNum
        self.attitudeEkfConfig.lpfDenCoeffs = self.configData.ekf_lpfDen

    def link_messages(self):
        """
        Link module messages.
        """

        #
        # Link messages for GyroSensor module
        self.gyro.scStateInMsg.subscribeTo(self.scObject.scStateOutMsg)
        
        #
        # Link messages for StarTracker module
        self.starTracker.scStateInMsg.subscribeTo(self.scObject.scStateOutMsg)

        #
        # Link messages for the attitudeEKF module.
        self.attitudeEkfConfig.gyroInMsg.subscribeTo(self.gyro.gyroOutMsg)
        self.attitudeEkfConfig.starTrackerInMsg.subscribeTo(self.starTracker.starTrackerOutMsg)

    def configure_initial_conditions(self):
        """
        Configure the initial simulation conditions.
        """

        #
        # Set initial simulation conditions.
        self.scObject.hub.sigma_BNInit = self.configData.sc_sigma_BN
        self.scObject.hub.omega_BN_BInit = self.configData.sc_omega_BN

    def log_outputs(self):
        """
        Log output variables for plotting.
        """

        #
        # Calculate recording variables.
        sampleRate = min([self.dynRate, self.fswRate])
        numDataPoints = int(self.simTime / sampleRate)
        samplingTime = unitTestSupport.samplingTime(self.simTime, sampleRate, numDataPoints)

        #
        # Record scStateOutMsg.
        self.msgRecList[self.scStateMsgName] = self.scObject.scStateOutMsg.recorder(samplingTime)
        self.AddModelToTask(TASK_NAME, self.msgRecList[self.scStateMsgName])

        #
        # Record starTrackerOutMsg. 
        self.msgRecList[self.starTrackerMsgName] = self.starTracker.starTrackerOutMsg.recorder(samplingTime)
        self.AddModelToTask(TASK_NAME, self.msgRecList[self.starTrackerMsgName])

        #
        # Record gyroOutMsg. 
        self.msgRecList[self.gyroMsgName] = self.gyro.gyroOutMsg.recorder(samplingTime)
        self.AddModelToTask(TASK_NAME, self.msgRecList[self.gyroMsgName])
        
        #
        # Record navAttMsgEstimated. 
        self.msgRecList[self.attitudeEKFMsgName] = self.attitudeEkfConfig.navAttMsgEstimated.recorder(samplingTime)
        self.AddModelToTask(TASK_NAME, self.msgRecList[self.attitudeEKFMsgName])

        #
        # Log attitude EKF covariance.
        self.AddVariableForLogging(self.attitudeEkfWrap.ModelTag + "." + "P", sampleRate, 0, 35, 'double')

        #
        # Log attitude EKF gyro bias estimates.
        self.AddVariableForLogging(self.attitudeEkfWrap.ModelTag + "." + "wBiasCompensatedBN_B", sampleRate, 0, 2, 'double')

        #
        # Log attitude EKF gyro bias estimates.
        self.AddVariableForLogging(self.attitudeEkfWrap.ModelTag + "." + "wBiasCompensatedFilteredBN_B", sampleRate, 0, 2, 'double')

    def pull_outputs(self):
        """
        Pull output variables and plot them.
        """

        #
        # Retrieve variables from recorders.
        truthSigmaBN = self.msgRecList[self.scStateMsgName].sigma_BN
        truthOmegaBN_B = self.msgRecList[self.scStateMsgName].omega_BN_B
        estSigma_BN = self.msgRecList[self.attitudeEKFMsgName].sigma_BN
        estOmega_BN = self.msgRecList[self.attitudeEKFMsgName].omega_BN_B
        simTimes = self.msgRecList[self.gyroMsgName].times() * macros.NANO2MIN

        #
        # Retrieve variables from log.
        ekf_P = self.GetLogVariableData(self.attitudeEkfWrap.ModelTag + "." + "P")
        ekf_P = ekf_P[:, 1:] # Remove time tag.
        ekf_wBiasCompensatedBN_B = self.GetLogVariableData(self.attitudeEkfWrap.ModelTag + "." + "wBiasCompensatedBN_B")
        ekf_wBiasCompensatedBN_B = ekf_wBiasCompensatedBN_B[:, 1:] # Remove time tag.
        ekf_wBiasCompensatedFilteredBN_B = self.GetLogVariableData(self.attitudeEkfWrap.ModelTag + "." + "wBiasCompensatedFilteredBN_B")
        ekf_wBiasCompensatedFilteredBN_B = ekf_wBiasCompensatedFilteredBN_B[:, 1:] # Remove time tag.

        #
        # Convert attitude MRPs to radians.
        truthSigmaBN_Rad = []
        estSigmaBN_Rad = []
        for mrp in truthSigmaBN:
            truthSigmaBN_Rad.append(MRP2RAD(mrp))
        for mrp in estSigma_BN:
            estSigmaBN_Rad.append(MRP2RAD(mrp))

        #
        # Convert to numpy arrays.
        truthSigmaBN_Rad = np.array(truthSigmaBN_Rad)
        estSigmaBN_Rad = np.array(estSigmaBN_Rad)

        #
        # Calculate attitude error.
        diffSigmaBN_Rad = []
        diffSigmaBN_Rad =  truthSigmaBN_Rad - estSigmaBN_Rad

        #
        # Calculate 1-sigma bounds.
        sigma = np.zeros((len(ekf_P), 6))
        for idx, P in enumerate(ekf_P):
            attSigmaMRP = [np.sqrt(P[0]), np.sqrt(P[7]), np.sqrt(P[14])]
            attSigmaRad = MRP2RAD(attSigmaMRP) * macros.RAD2ARCSEC
            sigma[idx, :] = [attSigmaRad[0], attSigmaRad[1], attSigmaRad[2], np.sqrt(P[21]), np.sqrt(P[28]), np.sqrt(P[35])]

        #
        # Construct plots.
        plotAttitude(simTimes[1:-1], estSigmaBN_Rad[1:-1], truthSigmaBN_Rad[1:-1])
        plt.savefig(os.path.join(plotPath, f"test_{self.testNum}_att.png"), bbox_inches='tight')
        plotAttitudeError(simTimes[1:-1], diffSigmaBN_Rad[1:-1], sigma[2:, :3])
        plt.savefig(os.path.join(plotPath, f"test_{self.testNum}_att_error.png"), bbox_inches='tight')
        plotAngularRate(simTimes[1:-1], estOmega_BN[1:-1], truthOmegaBN_B[1:-1])
        plt.savefig(os.path.join(plotPath, f"test_{self.testNum}_ang_rate.png"), bbox_inches='tight')
        plotAngularRateError(simTimes[1:-1], estOmega_BN[2:], truthOmegaBN_B[1:-1], sigma[2:, 3:])
        plt.savefig(os.path.join(plotPath, f"test_{self.testNum}_ang_rate_err.png"), bbox_inches='tight')
        plotRateBias(simTimes[1:-1], ekf_wBiasCompensatedBN_B[1:-1], truthOmegaBN_B[1:-1], sigma[2:, 3:])
        plt.savefig(os.path.join(plotPath, f"test_{self.testNum}_bias_comp_ang_rate.png"), bbox_inches='tight')

def run_test(dynRate, fswRate, simLength, testNum, configData):
    """
    Initializes and executes the scenario.
    """

    #
    # Create the test scenario.
    testScenario = ComponentTest(dynRate, fswRate, simLength, testNum, configData)

    #
    # Initialize and execute the simulation.
    testScenario.SetProgressBar(True)
    testScenario.InitializeSimulation()
    testScenario.ConfigureStopTime(testScenario.simTime)
    testScenario.ExecuteSimulation()

    #
    # Pull outputs.
    testScenario.pull_outputs()

def test_cases():
    """
    The main attitude EKF performance analysis script.
    """

    #
    # Test 1:
    #   Attitude EKF zeroed divergence test.
    #
    # Description:  
    #   Give the attitude EKF zeroed gyro and star
    #   tracker measurements with no noise or bias.
    #   Ensure that the EKF does not diverge from the
    #   constant zeroed truth attitudes and angular rates.
    #

    #
    # Display start of test.
    print("Starting Test 1: Attitude EKF zeroed divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 1
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Configure test data.
    gyro_rateNoiseStd = 0.
    gyro_biasNoiseStd = 0.
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.], [0.], [0.]]
    sc_omega_BN = [[0. * macros.D2R], [0. * macros.D2R], [0. * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 1.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print("Test 1 finished.\n")

    #
    # Test 2:
    #   Attitude EKF simple attitude, zero angular rate 
    #   divergence test.
    #
    # Description:  
    #   Give the attitude EKF simple star tracker measurements
    #   with no noise. The gyro measurements will be held constant
    #   at zero with no noise or bias. Ensure that the EKf does not
    #   diverge from the constant truth attitudes and angular rates.
    #

    #
    # Display start of test.
    print("Starting Test 2: Attitude EKF simple attitude, zero angular " + \
        "rate divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 2
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Configure test data.
    gyro_rateNoiseStd = 0.
    gyro_biasNoiseStd = 0.
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.1], [0.2], [-0.3]]
    sc_omega_BN = [[0. * macros.D2R], [0. * macros.D2R], [0. * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 2.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print(f"Test 2 finished.\n")

    #
    # Test 3:
    #   Attitude EKF simple attitude and angular rate 
    #   divergence test.
    #
    # Description:  
    #   Give the attitude EKF simple gyro and star tracker
    #   measurements with no noise or bias. Ensure that the
    #   EKF does not diverge from the constant truth attitude
    #   and angular rates.
    #

    #
    # Display start of test.
    print("Starting Test 3: Attitude EKF simple attitude and angular rate divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 3
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Define test parameters.
    gyro_rateNoiseStd = 0.
    gyro_biasNoiseStd = 0.
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.1], [0.2], [-0.3]]
    sc_omega_BN = [[0.25 * macros.D2R], [-0.5 * macros.D2R], [1 * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 3.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print("Test 3 finished.\n")

    #
    # Test 4:
    #   Attitude EKF noisy zeroed divergence test.
    #
    # Description:  
    #   Give the attitude EKF zeroed gyro and star tracker
    #   measurements with noise and bias. Ensure that the
    #   EKF does not diverge from the truth and responds
    #   to noise and bias in an expected manor.
    #

    #
    # Display start of test.
    print("Starting Test 4: Attitude EKF noisy zeroed divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 4
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Define test parameters.
    gyro_rateNoiseStd = 0.007 * macros.D2R
    gyro_biasNoiseStd = 0.001 * macros.D2R
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.], [0.], [0.]]
    sc_omega_BN = [[0. * macros.D2R], [0. * macros.D2R], [0. * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 4.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print("Test 4 finished.\n")
    
    #
    # Test 5:
    #   Attitude EKF noisy simple attitude, zero angular rate 
    #   divergence test.
    #
    # Description:  
    #   Give the attitude EKF simple star tracker measurements
    #   with noise. The gyro measurements will be held constant
    #   at zero, with noise and bias applied. Ensure that the
    #   EKF does not diverge from the truth and responds to noise
    #   and bias in an expected manor.
    #

    #
    # Display start of test.
    print("Starting Test 5: Attitude EKF noisy simple attitude, zero " + \
        "angular rate divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 5
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Define test parameters.
    gyro_rateNoiseStd = 0.007 * macros.D2R
    gyro_biasNoiseStd = 0.001 * macros.D2R
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.1], [0.2], [-0.3]]
    sc_omega_BN = [[0. * macros.D2R], [0. * macros.D2R], [0. * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 5.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print("Test 5 finished.\n")

    #
    # Test 6:
    #   Attitude EKF noisy simple attitude and angular rate divergence test.
    #
    # Description:  
    #   Give the attitude EKF simple gyro and star tracker
    #   measurements with noise and bias. Ensure that the 
    #   EKF does not diverge from the consant truth data
    #   and responds to noise and bias in an expected manor.
    #

    #
    # Display start of test.
    print("Starting Test 6: Attitude EKF noisy simple attitude and " + \
        "angular rate divergence test.")

    #
    # Define test and simulation parameters.
    testNum = 6
    dynRate = 0.1 # seconds
    fswRate = 0.1 # seconds
    simLength = 60 # minutes

    #
    # Define test parameters.
    gyro_rateNoiseStd = 0.007 * macros.D2R
    gyro_biasNoiseStd = 0.001 * macros.D2R
    gyro_wBiasInit = [0., 0., 0.]
    gyro_sigma_GB = [0., 0., 0.]
    starTracker_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    starTracker_sigma_CB = [0., 0., 0.]
    ekf_wBiasInit = [0., 0., 0.]
    ekf_P0 = [
        (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
        0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
        0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
        0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
        0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
        0, 0, 0, 0, 0, (0.1 * macros.D2R)**2
    ]
    ekf_Q = [
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0  
    ]
    ekf_R = [
        AROUND_BORESIGHT_STD**2 / 16.0, 0, 0,
        0, CROSS_BORESIGHT_STD**2 / 16.0, 0,
        0, 0, CROSS_BORESIGHT_STD**2 / 16.0
    ]
    ekf_rateNoiseStd = 0.007 * macros.D2R
    ekf_biasNoiseStd = 0.001 * macros.D2R
    ekf_sigma_GB = gyro_sigma_GB
    ekf_sigma_CB = starTracker_sigma_CB
    ekf_lpfNum = [0.0087 , 0.0087]
    ekf_lpfDen = [1., -0.9827]
    sc_sigma_BN = [[0.1], [0.2], [-0.3]]
    sc_omega_BN = [[0.25 * macros.D2R], [-0.5 * macros.D2R], [1 * macros.D2R]]
    config = configData(
        gyro_rateNoiseStd,
        gyro_biasNoiseStd,
        gyro_wBiasInit,
        gyro_sigma_GB,
        starTracker_R,
        starTracker_sigma_CB,
        ekf_wBiasInit,
        ekf_P0,
        ekf_Q,
        ekf_R,
        ekf_rateNoiseStd,
        ekf_biasNoiseStd,
        ekf_sigma_GB,
        ekf_sigma_CB,
        ekf_lpfNum,
        ekf_lpfDen,
        sc_sigma_BN,
        sc_omega_BN
    )

    #
    # Run Test 6.
    run_test(dynRate, fswRate, simLength, testNum, config)

    #
    # Display end of test.
    print("Test 6 finished.\n")

if __name__ == "__main__":
    
    #
    # Run component test script.
    test_cases()