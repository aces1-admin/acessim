#ifndef STARTRACKERSENSOR_H
#define STARTRACKERSENSOR_H

#include "architecture/_GeneralModuleFiles/sys_model.h"
#include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
#include "msgPayloadDefC/StarTrackerMsgPayload.h"
#include "architecture/utilities/bskLogging.h"
#include "architecture/messaging/messaging.h"
#include <stdint.h>
#include <random>
#include <Eigen/Dense>

/*! @brief Star Tracker model that includes cross and around boresight noise.
 */
class StarTrackerSensor: public SysModel {
public:
    StarTrackerSensor();
    ~StarTrackerSensor();
    void Reset(uint64_t CurrentSimNanos);
    void UpdateState(uint64_t CurrentSimNanos);
    void setRngSeed(uint64_t newSeed){this->randNumGen.seed(newSeed); this->rngSeed = newSeed;}
    
public:
    ReadFunctor<SCStatesMsgPayload> scStateInMsg;           //!< [-] Input message for spacecraft states
    Message<StarTrackerMsgPayload> starTrackerOutMsg;       //!< [-] Output message for Star Tracker
    BSKLogger bskLogger;                                    //!< BSK Logging
    double sigma_CB[3];                                     //!< [-] MRP orientation of star tracker Case relative to the Body frame
    double sigma_CN[3];                                     //!< [-] Noisy MRP star tracker measurement
    double R[3*3];                                          //!< Measurement noise matrix
    double noise[3];                                        //!< Array of noise that is applied to truth MRP
    
private:
    uint64_t rngSeed;                                       //!< -- Seed for random number generator
    std::minstd_rand randNumGen;                            //!< -- Random number generator for model
    std::normal_distribution<double> normalDist;            //!< -- Random number distribution for model
};


#endif
