#
# Star Tracker Sensor Unit Test
#
# Author:   Henry Macanas
# Creation Date:  Mar 26 2022
#

import pytest

from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.ExternalModules import starTrackerSensor
import numpy as np
from Basilisk.utilities import RigidBodyKinematics

def test_starTrackerSensor(accuracy):
    r"""
    **Validation Test Description**

    All execution paths are tested and verified to run without failure.
    Anticipated updates to variables are tested. 

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
        
    **Description of Variables Being Tested**

    The tests in this file check the internal variables of the
    StarTrackerSensor module as well as the output message.
    """
    [testResults, testMessage] = starTrackerSensorTestFunction(accuracy)
    assert testResults < 1, testMessage


sigmaTruth_BN = [0.1, 0.2, 0.3]

def setupStateInMsg(scStateInMsgData, scStateInMsg, s = sigmaTruth_BN):
    scStateInMsgData.sigma_BN = s
    scStateInMsg.write(scStateInMsgData)
    
def starTrackerSensorTestFunction(accuracy):
    """Test method"""
    testFailCount = 0
    testMessages = []
    unitTaskName = "unitTask"
    unitProcessName = "TestProcess"

    unitTestSim = SimulationBaseClass.SimBaseClass()
    testProcessRate = macros.sec2nano(0.5)
    testProc = unitTestSim.CreateNewProcess(unitProcessName)
    testProc.addTask(unitTestSim.CreateNewTask(unitTaskName, testProcessRate))

    # Setup module to be tested.
    module = starTrackerSensor.StarTrackerSensor()
    module.ModelTag = "starTrackerSensorTag"
    unitTestSim.AddModelToTask(unitTaskName, module)

    # Configure blank module input messages.
    scStateInMsgData = messaging.SCStatesMsgPayload()
    scStateInMsg = messaging.SCStatesMsg().write(scStateInMsgData)

    # Subscribe input messages to module.
    module.scStateInMsg.subscribeTo(scStateInMsg)

    # Setup output message recorder objects.
    starTrackerOutMsgMsgRec = module.starTrackerOutMsg.recorder()
    unitTestSim.AddModelToTask(unitTaskName, starTrackerOutMsgMsgRec)


    '''
    Test:   Test StarTrackerSensor::Reset().
    '''
    sigma_CB = [1., 2., 3.]
    module.sigma_CB = sigma_CB
    module.sigma_CN = [1., 2., 3.]
    R = [1., 2., 3., 4., 5., 6., 7., 8., 9.]
    module.R = R
    module.noise = np.array([1., 2., 3.])
    module.Reset(0)
    
    # Verify public module variables are reset as expected.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(sigma_CB),
                                        module.sigma_CB,
                                        accuracy,
                                        "Test 5",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        module.sigma_CN,
                                        accuracy,
                                        "Test 6",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(R),
                                        module.R,
                                        accuracy,
                                        "Test 7",
                                        testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        module.noise,
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)

    '''
    Test:   Test StarTrackerSensor::UpdateState() with no noise.
    '''
    module.Reset(0)
    R = [0., 0., 0., 0., 0., 0., 0., 0., 0.]
    module.R = R
    module.UpdateState(0)
    
    # Verify that noise is set to zero.
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        module.noise,
                                        accuracy,
                                        "Test 6",
                                        testFailCount, testMessages)
    
    '''
    Test:   Test StarTrackerSensor::UpdateState() with noise.
    '''
    setupStateInMsg(scStateInMsgData, scStateInMsg)
    sigma_CB = [1., 2., 3.]
    module.sigma_CB = sigma_CB
    module.sigma_CN = [1., 2., 3.]
    R = [1., 2., 3., 4., 5., 6., 7., 8., 9.]
    module.R = R
    unitTestSim.InitializeSimulation()
    unitTestSim.TotalSim.SingleStepProcesses()
    
    # Verify that noise is non-zero.
    testFailCount, testMessages = unitTestSupport.compareVector(np.zeros((3, )),
                                        module.noise,
                                        accuracy,
                                        "Test 7",
                                        testFailCount, testMessages, ExpectedResult = 0)
    
    # Verify that sigma_CB is did not change.
    testFailCount, testMessages = unitTestSupport.compareVector(np.array(sigma_CB),
                                        module.sigma_CB,
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    
    # Verify that sigma_CN is computed as expected.
    sigma_CN = RigidBodyKinematics.addMRP(np.array(sigmaTruth_BN), np.array(sigma_CB))
    sigma_CN += np.array(module.noise)
    testFailCount, testMessages = unitTestSupport.compareVector(sigma_CN,
                                        module.sigma_CN,
                                        accuracy,
                                        "Test 8",
                                        testFailCount, testMessages)
    
    # Verify that q_CN is computed as expected.
    q_CN = RigidBodyKinematics.MRP2EP(sigma_CN)
    testFailCount, testMessages = unitTestSupport.compareVector(q_CN,
                                        starTrackerOutMsgMsgRec.q_CN[0, :],
                                        accuracy,
                                        "Test 9",
                                        testFailCount, testMessages)
    # Verify that the starTrackerOutMsgMsg is marked as valid.
    testFailCount, testMessages = unitTestSupport.compareInt(1,
                                        starTrackerOutMsgMsgRec.valid,
                                        "Test 10",
                                        testFailCount, testMessages)
    
    
    if testFailCount == 0:
        print("PASSED: " + module.ModelTag)
    else:
        print(testMessages)

    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_starTrackerSensor(1e-12)


