#
# Star Tracker Sensor Verification.
#
# Author:   Henry Macanas
# Creation Date:  Mar 28 2022
#

import pytest

from Basilisk.utilities import SimulationBaseClass
from Basilisk.utilities import unitTestSupport
from Basilisk.architecture import messaging
from Basilisk.utilities import macros
from Basilisk.ExternalModules import starTrackerSensor
import numpy as np
from Basilisk.utilities import RigidBodyKinematics
from Basilisk.simulation import spacecraft
from Basilisk.ExternalModules import starTrackerSensor
import matplotlib.pyplot as plt
import numpy.matlib

def plot_attitude_quaternion_star_tracker(timeData, dataQ):
    """Plot the attitude errors."""
    plt.figure()
    for idx in range(4):
        plt.plot(timeData, dataQ[:, idx],
                 color=unitTestSupport.getLineColor(idx, 4),
                 label=r'$q_' + str(idx) + '$')
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'Attitude $q_{C/N}$')
    plt.grid(True)

def plot_attitude_truth(timeData, dataSigmaBN):
    """Plot the attitude errors."""
    plt.figure()
    for idx in range(3):
        plt.plot(timeData, dataSigmaBN[:, idx],
                 color=unitTestSupport.getLineColor(idx, 3),
                 label=r'$\sigma_' + str(idx) + '$')
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'True Attitude $\sigma_{B/N}$')
    plt.grid(True)
    
def plot_attitude_measured(timeData, dataSigmaBN):
    """Plot the attitude errors."""
    plt.figure()
    for idx in range(3):
        plt.plot(timeData, dataSigmaBN[:, idx],
                 color=unitTestSupport.getLineColor(idx, 3),
                 label=r'$\sigma_' + str(idx) + '$')
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'Measured Attitude $\sigma_{B/N}$')
    plt.grid(True)
    
def test_starTrackerSensor(showPlots, accuracy):
    r"""
    **Validation Test Description**

    Verifies that amount of noise added to each mrp element does not excede
    expected amount and is in line with the true attitude.

    **Test Parameters**

    N/A.

    Args:
        accuracy (float): absolute accuracy value used in the validation tests
        showPlots (bool): flag that is high when plots will be shown
    """
    [testResults, testMessage] = starTrackerSensorTestFunction(showPlots, accuracy)
    assert testResults < 1, testMessage


def starTrackerSensorTestFunction(showPlots, accuracy):
    testFailCount = 0
    testMessages = []
    
    #
    # Setup simulation.
    simTaskName = "simTask"
    simProcessName = "simProcess"
    scSim = SimulationBaseClass.SimBaseClass()
    dynProcess = scSim.CreateNewProcess(simProcessName)
    simulationTimeStep = macros.sec2nano(1)
    dynProcess.addTask(scSim.CreateNewTask(simTaskName, simulationTimeStep))
    scObject = spacecraft.Spacecraft()
    scObject.ModelTag = "spacecraftBody"
    
    I = [900., 0., 0.,
         0., 800., 0.,
         0., 0., 600.]
    scObject.hub.mHub = 750.0
    scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]]
    scObject.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(I)
    scObject.hub.sigma_BNInit = [[0.], [0.], [0.]]
    scObject.hub.omega_BN_BInit = [[0.01], [0.02], [0.03]]
    scSim.AddModelToTask(simTaskName, scObject)

    #
    # Setup the Star Tacker.
    starTracker = starTrackerSensor.StarTrackerSensor()
    starTracker.ModelTag = "starTrackerSensor"
    ARCSEC2DEG = 1 / 3600.  # conversion from arcseconds to degrees
    aroundBoresightStd = 30 * ARCSEC2DEG * macros.D2R
    crossBoresightStd = 10 * ARCSEC2DEG * macros.D2R
    starTracker.R = [aroundBoresightStd**2 / 16.0, 0, 0,
                           0, crossBoresightStd**2 / 16.0, 0,
                           0, 0, crossBoresightStd**2 / 16.0]
    starTracker.sigma_CB = [0.0, 0.0, 0.0]
    scSim.AddModelToTask(simTaskName, starTracker)
    starTracker.scStateInMsg.subscribeTo(scObject.scStateOutMsg)
    
    #
    # Setup recorders.
    starTrackerOutMsgRec = starTracker.starTrackerOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, starTrackerOutMsgRec)
    
    scStateRecorder = scObject.scStateOutMsg.recorder()
    scSim.AddModelToTask(simTaskName, scStateRecorder)
    
    scSim.AddVariableForLogging(starTracker.ModelTag + ".sigma_CN", simulationTimeStep, 0, 2)
    
    #
    # Execute simulation.
    simulationTime = macros.min2nano(1000.)
    scSim.InitializeSimulation()
    scSim.ConfigureStopTime(simulationTime)
    scSim.ExecuteSimulation()
    
    #
    # Pull data.
    sigma_CN_And_Time = np.array(scSim.GetLogVariableData(starTracker.ModelTag + ".sigma_CN"))
    sigma_CN = sigma_CN_And_Time[:, 1:]
        
    
    #
    # Analyze data
    
    # Get difference between true and noisy mrp.
    diff = scStateRecorder.sigma_BN - sigma_CN
    
    # For each axis, find elements where difference is greater than 3
    # standard deviation away from 0.
    axis1NumValuesOutsideRange = len(np.where(np.absolute(diff[:, 0]) > 3. * aroundBoresightStd / 4.0)[0])
    axis2NumValuesOutsideRange = len(np.where(np.absolute(diff[:, 1]) > 3. * crossBoresightStd / 4.0)[0])
    axis3NumValuesOutsideRange = len(np.where(np.absolute(diff[:, 2]) > 3. * crossBoresightStd / 4.0)[0])
    
    # Find percentage of values outside range
    numOfDataPts = len(diff[:, 0])
    axis1PercentOutside = axis1NumValuesOutsideRange / numOfDataPts * 100.
    axis2PercentOutside = axis2NumValuesOutsideRange / numOfDataPts * 100.
    axis3PercentOutside = axis3NumValuesOutsideRange / numOfDataPts * 100.
    
    # Verify that about 97.7% of values are within 3 sigma bounds
    testFailCount, testMessages = unitTestSupport.compareDouble(0.3, 
                                        axis1PercentOutside, accuracy,
                                        "Test 1", testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.3, 
                                        axis2PercentOutside, accuracy,
                                        "Test 2", testFailCount, testMessages)
    testFailCount, testMessages = unitTestSupport.compareDouble(0.3, 
                                        axis3PercentOutside, accuracy,
                                        "Test 3", testFailCount, testMessages)
        
    
    if testFailCount == 0:
        print("PASSED: " + "verification_starTrackerSensor.py")
    else:
        print(testMessages)

    if showPlots:
        simTimes = scStateRecorder.times() * macros.NANO2SEC
        plot_attitude_quaternion_star_tracker(simTimes, starTrackerOutMsgRec.q_CN)
        plot_attitude_measured(simTimes, sigma_CN)
        plot_attitude_truth(simTimes, scStateRecorder.sigma_BN)
        
        plt.figure()
        plt.hist(diff[:, 0], bins=20)
        plt.title("Attitude Error Axis 1")
        plt.grid(True)
        
        plt.figure()
        plt.hist(diff[:, 1], bins = 20)
        plt.title("Attitude Error Axis 2")
        plt.grid(True)
        
        plt.figure()
        plt.hist(diff[:, 2], bins = 20)
        plt.title("Attitude Error Axis 3")
        plt.grid(True)
        
        
        plt.show()
        
    return [testFailCount, "".join(testMessages)]


if __name__ == "__main__":
    test_starTrackerSensor(False, 2E-2)

