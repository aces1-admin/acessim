#include "ExternalModules/starTrackerSensor/starTrackerSensor.h"
#include "architecture/utilities/avsEigenSupport.h"
#include "architecture/utilities/rigidBodyKinematics.h"
#include "architecture/utilities/linearAlgebra.h"
#include <cstring>
#include <random> // used for gaussian noise generation

/*! This is the constructor for the module class.  It sets default variable
    values and initializes the various parts of the model */
StarTrackerSensor::StarTrackerSensor()
{
    this->rngSeed = 0x1badcad1;
    std::normal_distribution<double>::param_type meanPlusStddev(0.0, 1.0);
    this->normalDist.param(meanPlusStddev);
}

/*! Module Destructor */
StarTrackerSensor::~StarTrackerSensor()
{
}

/*! This method is used to reset the module and checks that required input messages are connected.
    @return void
*/
void StarTrackerSensor::Reset(uint64_t CurrentSimNanos)
{
    // Check that required input message is connected.
    if (!this->scStateInMsg.isLinked()) {
        bskLogger.bskLog(BSK_ERROR, "StarTrackerSensor.scStateInMsg was not linked.");
    }
    v3SetZero(this->sigma_CN);
    v3SetZero(this->noise);
}


/*! This is the main method that gets called every time the module is updated.  This method outputs the noisy attitude in the form of a quaternion..
    @return void
*/
void StarTrackerSensor::UpdateState(uint64_t CurrentSimNanos)
{
    // Declare local copies of message buffer.
    SCStatesMsgPayload scStateInMsgBuffer;          //!< local copy of message buffer
    StarTrackerMsgPayload starTrackerOutMsgBuffer;  //!< local copy of message buffer

    // Read in the input messages and zero the output message buffer.
    scStateInMsgBuffer = this->scStateInMsg();
    starTrackerOutMsgBuffer = this->starTrackerOutMsg.zeroMsgPayload;

    /*
     * Generate random noise using a normal distribution and scale
     * this random number by the corresponding measurement noise
     * standard deviations.
     */
    v3SetZero(this->noise);
    this->noise[0] = this->normalDist(this->randNumGen) * sqrt(this->R[MXINDEX(3, 0, 0)]);;
    this->noise[1] = this->normalDist(this->randNumGen) * sqrt(this->R[MXINDEX(3, 1, 1)]);;
    this->noise[2] = this->normalDist(this->randNumGen) * sqrt(this->R[MXINDEX(3, 2, 2)]);;
    
    // Rotate true attitude into star tracker Case frame.
    addMRP(scStateInMsgBuffer.sigma_BN, this->sigma_CB, this->sigma_CN);
    
    // Add noise to the truth mrp.
    v3Add(this->sigma_CN, this->noise, this->sigma_CN);
    
    // Convert mrp to quaternion.
    MRP2EP(this->sigma_CN, starTrackerOutMsgBuffer.q_CN);
    
    // Validate and time stamp star tracker data.
    starTrackerOutMsgBuffer.valid = 1;
    starTrackerOutMsgBuffer.timeTag = this->scStateInMsg.timeWritten();
    
    // Write to the output message.
    this->starTrackerOutMsg.write(&starTrackerOutMsgBuffer, this->moduleID, CurrentSimNanos);
}
