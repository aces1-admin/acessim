#
# ACES Dynamics Module
#
# Purpose:  Dynamics Module for ACES ADCS simulations.
# Author:   Brennan Gray
# Creation Date:  January 31, 2022
#
# Description:
#
#   Module containing core ACES ADCS dynamics.
#
#   More details here:
#   https://aces.atlassian.net/l/c/zsMjHqRt

import numpy as np

from Basilisk import __path__
from Basilisk.architecture import messaging
from Basilisk.simulation import (reactionWheelStateEffector,
                                 simpleNav,
                                 magneticFieldWMM,
                                 magnetometer,
                                 MtbEffector,
                                 spacecraft,
                                 groundLocation,
                                 GravityGradientEffector,
                                 facetDragDynamicEffector,
                                 exponentialAtmosphere,
                                 spiceInterface,
                                 eclipse)
from Basilisk.ExternalModules import (solarRadiationPressure,
                                      gyroSensor,
                                      starTrackerSensor)
from Basilisk.utilities import (macros,
                                simIncludeGravBody,
                                simIncludeRW,
                                simSetPlanetEnvironment,
                                astroFunctions,
                                unitTestSupport)

bskPath = __path__[0]


class ACESDynamicModels:
    """
    ACES dynamics model for simulation.
    """

    def __init__(self, SimBase, dynRate, mass, length, width, height):
        #
        # Define empty class variables.
        self.I_sc = None
        self.Gs = None
        self.gravBodies = None
        self.rwList = None
        self.numRW = None
        self.epochMsg = None
        self.additionalModules = {}  # Storage for additional modules added by scenarios.

        #
        # Define spacecraft mass properties and geometry.
        self.mass = mass  # kg, spacecraft mass
        self.length = length  # m, length
        self.width = width  # m, width
        self.height = height  # m, height

        #
        # Create the truth side task.
        self.processName = SimBase.DynamicsProcessName
        self.taskName = "simTask"
        self.truthTaskTimeStep = macros.sec2nano(dynRate)
        SimBase.dynProc.addTask(SimBase.CreateNewTask(self.taskName, self.truthTaskTimeStep))

        #
        # Create star tracker task to run @ 10Hz
        self.starTrackerTaskName = "starTrackerTask"
        self.starTrackerTaskTimeStep = macros.sec2nano(1. / 10.)
        SimBase.dynProc.addTask(SimBase.CreateNewTask(self.starTrackerTaskName, self.starTrackerTaskTimeStep))

        #
        # Define the initial simulation time.
        # Note: The updated magnetic field data "WMM.COF"
        # is only valid from 2020-2025.
        self.simTimeInit = '2022 April 7, 00:00:0.0 UTC'

        #
        # Instantiate dynamics modules.
        self.scObject = spacecraft.Spacecraft()
        self.gravFactory = simIncludeGravBody.gravBodyFactory()
        self.sNavObject = simpleNav.SimpleNav()
        self.groundStation = groundLocation.GroundLocation()
        self.rwFactory = simIncludeRW.rwFactory()
        self.rwStateEffector = reactionWheelStateEffector.ReactionWheelStateEffector()
        self.mtbEff = MtbEffector.MtbEffector()
        self.magModule = magneticFieldWMM.MagneticFieldWMM()
        self.atmosphere = exponentialAtmosphere.ExponentialAtmosphere()
        self.ggEff = GravityGradientEffector.GravityGradientEffector()
        self.facetDragEff = facetDragDynamicEffector.FacetDragDynamicEffector()
        self.TAM = magnetometer.Magnetometer()
        self.spiceObject = spiceInterface.SpiceInterface()
        self.eclipseObject = eclipse.Eclipse()
        self.srpEff = solarRadiationPressure.SolarRadiationPressure()

        #
        # Instantiate sensor modules.
        self.gyro = gyroSensor.GyroSensor()
        self.starTracker = starTrackerSensor.StarTrackerSensor()

        #
        # Initialize all modules and write init one-time messages.
        self.InitAllDynObjects()

        #
        # Assign initialized modules to task.
        SimBase.AddModelToTask(self.taskName, self.scObject)
        SimBase.AddModelToTask(self.taskName, self.sNavObject)
        SimBase.AddModelToTask(self.taskName, self.groundStation)
        SimBase.AddModelToTask(self.taskName, self.rwStateEffector)
        SimBase.AddModelToTask(self.taskName, self.mtbEff)
        SimBase.AddModelToTask(self.taskName, self.magModule)
        SimBase.AddModelToTask(self.taskName, self.atmosphere)
        SimBase.AddModelToTask(self.taskName, self.ggEff)
        SimBase.AddModelToTask(self.taskName, self.facetDragEff)
        SimBase.AddModelToTask(self.taskName, self.TAM)
        SimBase.AddModelToTask(self.taskName, self.spiceObject)
        SimBase.AddModelToTask(self.taskName, self.eclipseObject)
        SimBase.AddModelToTask(self.taskName, self.srpEff)
        SimBase.AddModelToTask(self.taskName, self.gyro)
        SimBase.AddModelToTask(self.starTrackerTaskName, self.starTracker)

        #
        # Link messages.
        # Note that this only links messages which are completely internal to the dynamics module. The vast majority of
        # messages are linked in the ACES_FSW module since the FSW must be instantiated before most messages are linked.
        self.LinkMessages()

    def AddAdditionalModule(self, SimBase, moduleName, module):
        """
        Adds the provided module to the dynamics task and stores it. Note that the module should be fully configured and
        linked before it is added to the dynamics model.

        Once added, the new module can be accessed through the additionalModules dictionary as follows:
            DynModel.additionalModules[moduleName]
        where DynModel is a reference to this class.
        """

        self.additionalModules[moduleName] = module
        SimBase.AddModelToTask(self.taskName, module)

    # ------------------------------------------------------------------------------------------- #
    # These are module initialization methods.
    def SetSpacecraftHub(self):
        """
        Specify the spacecraft hub properties.
        """

        self.scObject.ModelTag = "acesSat"

        #
        # Define spacecraft inertia assuming a 6U CubeSat of uniform mass
        # distribution. Note that this inertia includes the effects of offset
        # reactions wheels from the center of mass. For more detail see:
        #
        # https://aces.atlassian.net/wiki/spaces/~551756489/pages/491521/Baseline+Scenario
        l = self.length
        w = self.width
        h = self.height
        Ix = self.mass * (h ** 2 + w ** 2) / 12.
        Iy = self.mass * (l ** 2 + h ** 2) / 12.
        Iz = self.mass * (l ** 2 + w ** 2) / 12.
        self.I_sc = [Ix, 0., 0.,
                     0., Iy, 0.,
                     0., 0., Iz]

        self.scObject.hub.mHub = self.mass
        self.scObject.hub.r_BcB_B = [[0.0], [0.0], [0.0]]  # m - position vector of body-fixed point B relative to CM
        self.scObject.hub.IHubPntBc_B = unitTestSupport.np2EigenMatrix3d(self.I_sc)

    def SetSpacecraftFacets(self):
        """
        Sets the properties of the spacecraft's facets.
        """

        #
        # Extract geometric properties for ease of use.
        l = self.length
        w = self.width
        h = self.height

        #
        # Define area of spacecraft facets. These are just the areas of the
        # surfaces of a 6U.
        areaPlusX = w * h
        areaMinusX = areaPlusX
        areaPlusY = l * h
        areaMinusY = areaPlusY
        areaPlusZ = l * w
        areaMinusZ = areaPlusZ
        self.scFacetAreas = [areaPlusX, areaMinusX, areaPlusY, areaMinusY, areaPlusZ, areaMinusZ]

        #
        # Define drag coefficients for spacecraft facets. These are default values
        # and should be tuned down the line once real spacecraft surface
        # properties are more well known.
        self.scFacetDragCoeffs = np.array([2.0, 2.0, 2.0, 2.0, 2.0, 2.0])

        #
        # Define the absorption/reflection coefficients for the spacecraft facets.
        # These values are based on the properties of aluminum alloy and should
        # be tuned once the real spacecraft surface properties are more well known.
        self.scFacetAbsorpCoeffs = [0.2, 0.2, 0.2, 0.2, 0.2, 0.2]
        self.scFacetSpecCoeffs = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
        self.scFacetDiffCoeffs = [0.7, 0.7, 0.7, 0.7, 0.7, 0.7]

        #
        # Define vectors normal to the facet surfaces of the spacecraft in the
        # body frame. These are used to calculate the apparent area relative
        # to the velocity of the spacecraft.
        bNormalPlusX = np.array([1, 0, 0])
        bNormalMinusX = np.array([-1, 0, 0])
        bNormalPlusY = np.array([0, 1, 0])
        bNormalMinusY = np.array([0, -1, 0])
        bNormalPlusZ = np.array([0, 0, 1])
        bNormalMinusZ = np.array([0, 0, -1])
        self.scFacetNormals_B = [bNormalPlusX, bNormalMinusX, bNormalPlusY, bNormalMinusY, bNormalPlusZ, bNormalMinusZ]

        #
        # Define the geometric center of the spacecraft facets relative
        # to the center of mass in the body frame. This is used to
        # calculate the moment arm used in the torque calculation.
        bLocPlusX = np.array([l / 2., 0, 0])
        bLocMinusX = np.array([-l / 2., 0, 0])
        bLocPlusY = np.array([0, w / 2., 0])
        bLocMinusY = np.array([0, -w / 2., 0])
        bLocPlusZ = np.array([0, 0, h / 2.])
        bLocMinusZ = np.array([0, 0, -h / 2.])
        self.scFacetLocations_B = [bLocPlusX, bLocMinusX, bLocPlusY, bLocMinusY, bLocPlusZ, bLocMinusZ]

    def SetGravityBodies(self):
        """
        Sets the gravitational bodies to include in the simulation.
        """

        self.gravBodyNames = ['earth', 'moon', 'sun']
        self.gravBodies = self.gravFactory.createBodies(self.gravBodyNames)
        self.gravBodies['earth'].isCentralBody = True  # ensure this is the central gravitational body
        self.gravBodies['earth'].useSphericalHarmParams = True
        simIncludeGravBody.loadGravFromFile(bskPath + '/supportData/LocalGravData/GGM05S.txt',
                                            self.gravBodies['earth'].spherHarm, 25)
        self.scObject.gravField.gravBodies = spacecraft.GravBodyVector(list(self.gravFactory.gravBodies.values()))

    def SetSpiceInterface(self):
        """
        Sets the required ephemerides to all gravitational bodies in the simulation.
        """
        self.spiceObject.ModelTag = "spiceInterface"
        self.spiceObject.SPICEDataPath = bskPath + '/supportData/EphemerisData/'
        self.spiceObject.addPlanetNames(spiceInterface.StringVector(self.gravBodyNames))
        self.spiceObject.UTCCalInit = self.simTimeInit
        self.spiceObject.zeroBase = "earth"

    def SetSimpleNavObject(self):
        """
        Sets the simple navigation object.
        """

        self.sNavObject.ModelTag = "SimpleNavigation"

    def SetGroundStation(self):
        """
        Sets the ground station object.
        """

        self.groundStation.ModelTag = "BoulderGroundStation"
        self.groundStation.planetRadius = astroFunctions.E_radius*1e3
        self.groundStation.specifyLocation(np.radians(40.009971), np.radians(-105.243895), 1624)
        self.groundStation.minimumElevation = np.radians(10.)
        self.groundStation.maximumRange = 1e9

    def SetReactionWheelDynEffector(self):
        """
        Sets the reaction wheels.
        """

        varRWModel = messaging.JitterSimple
        massRW = 90e-3  # mass [kg]
        maxRPM = 6000.  # max wheel speed [RPM]
        maxTorque = 2.3e-3  # max torque [Nm]
        minTorque = 1e-7  # min torque [Nm]
        maxMom = 3.6e-3  # max momentum [Nms]
        Us = 4e-8  # static imbalance [kg*m]
        Ud = 1.4e-9  # dynamic imbalance [kg*m*m]
        self.Gs = np.array([  # wheel alignments in body frame
            [1., 0., 0.],
            [0., 1., 0.],
            [0., 0., 1.]
        ])

        RW1 = self.rwFactory.create('custom',  # wheel type
                                    self.Gs[:, 0],  # wheel spin axis
                                    rWB_B=[0.125, 0.075, 0.025],  # wheel location in body frame [m]
                                    Omega=0.,  # initial wheel speed [RPM]
                                    Omega_max=maxRPM,  # max wheel speed [RPM]
                                    u_max=maxTorque,  # max wheel torque [Nm]
                                    maxMomentum=maxMom,  # max wheel momentum [Nms]
                                    RWModel=varRWModel,  # rw model (BalancedWheels, JitterSimple or JitterFullyCoupled)
                                    useRWfriction=False,  # use friction in model (False, True)
                                    useMaxTorque=True,  # use max torque (False, True)
                                    useMinTorque=False)  # use min torque (False, True)

        RW2 = self.rwFactory.create('custom',  # wheel type
                                    self.Gs[:, 1],  # wheel spin axis
                                    rWB_B=[0.125, 0.075, 0.025],  # wheel location in body frame [m]
                                    Omega=0.,  # initial wheel speed [RPM]
                                    Omega_max=maxRPM,  # max wheel speed [RPM]
                                    u_max=maxTorque,  # max wheel torque [Nm]
                                    maxMomentum=maxMom,  # max wheel momentum [Nms]
                                    RWModel=varRWModel,  # rw model (BalancedWheels, JitterSimple or JitterFullyCoupled)
                                    useRWfriction=False,  # use friction in model (False, True)
                                    useMaxTorque=True,  # use max torque (False, True)
                                    useMinTorque=False)  # use min torque (False, True)

        RW3 = self.rwFactory.create('custom',  # wheel type
                                    self.Gs[:, 2],  # wheel spin axis
                                    rWB_B=[0.125, 0.075, 0.025],  # wheel location in body frame [m]
                                    Omega=0.,  # initial wheel speed [RPM]
                                    Omega_max=maxRPM,  # max wheel speed [RPM]
                                    u_max=maxTorque,  # max wheel torque [Nm]
                                    maxMomentum=maxMom,  # max wheel momentum [Nms]
                                    RWModel=varRWModel,  # rw model (BalancedWheels, JitterSimple or JitterFullyCoupled)
                                    useRWfriction=False,  # use friction in model (False, True)
                                    useMaxTorque=True,  # use max torque (False, True)
                                    useMinTorque=False)  # use min torque (False, True)

        # masses not used unless using fully coupled jitter model for wheel dynamics
        RW1.mass = massRW
        RW2.mass = massRW
        RW3.mass = massRW

        # define min torques (get forced even if useMinTorque=True so comment out to not use)
        # RW1.u_min = minTorque
        # RW2.u_min = minTorque
        # RW3.u_min = minTorque

        # define wheel imbalance coefficients (used only if using a jitter model)
        RW1.U_s = Us
        RW2.U_s = Us
        RW3.U_s = Us
        RW1.U_d = Ud
        RW2.U_d = Ud
        RW3.U_d = Ud

        self.rwList = [RW1, RW2, RW3]
        self.numRW = self.rwFactory.getNumOfDevices()
        self.rwStateEffector.ModelTag = "ReactionWheelEffectors"
        self.rwFactory.addToSpacecraft(self.rwStateEffector.ModelTag, self.rwStateEffector, self.scObject)

    def SetMtbEffector(self):
        """
        Sets the magnetic torque bar effector module.
        """

        self.mtbEff.ModelTag = "MtbEffectors"
        self.scObject.addDynamicEffector(self.mtbEff)

    def SetMagModule(self):
        """
        Sets the magnetic field module. Note that this module is set to use data from the World Magnetic Model (WMM).
        """

        self.magModule.ModelTag = "WMM"
        self.magModule.dataPath = bskPath + '/supportData/MagneticField/'
        self.epochMsg = unitTestSupport.timeStringToGregorianUTCMsg(self.simTimeInit)
        self.magModule.epochInMsg.subscribeTo(self.epochMsg)
        self.magModule.addSpacecraftToModel(self.scObject.scStateOutMsg)  # this command can be repeated if multiple

    def SetAtmosphere(self):
        """
        Sets the atmosphere model. Note that this will likely be replaced in the future with data from the MSISE model.
        """

        self.atmosphere.ModelTag = "ExponentialAtmosphere"
        simSetPlanetEnvironment.exponentialAtmosphere(self.atmosphere, "earth")
        self.atmosphere.addSpacecraftToModel(self.scObject.scStateOutMsg)

    def SetGravityGradientEffector(self):
        """
        Sets the gravity gradient effector.
        """

        self.ggEff.ModelTag = "GravityGradientEffector"
        self.ggEff.addPlanetName(self.gravBodies['earth'].planetName)
        self.scObject.addDynamicEffector(self.ggEff)

    def SetFacetDragEffector(self):
        """
        Sets the facet drag effector. For geometry used to model the facets defined below see:
        https://aces.atlassian.net/wiki/spaces/~551756489/pages/491521/Baseline+Scenario
        """

        self.facetDragEff.ModelTag = "FacetDragEffector"
        for ind in range(0, len(self.scFacetAreas)):
            self.facetDragEff.addFacet(self.scFacetAreas[ind],
                                       self.scFacetDragCoeffs[ind],
                                       self.scFacetNormals_B[ind],
                                       self.scFacetLocations_B[ind])
        self.scObject.addDynamicEffector(self.facetDragEff)

    def SetTAM(self):
        """
        Sets the three axis magnetometer module. No noise is added here since noise parameters of the three axis
        magnetometer that will be used is unknown.
        """

        self.TAM.ModelTag = "tamSensor"
        self.TAM.scaleFactor = 1.0
        self.TAM.senNoiseStd = [0.0, 0.0, 0.0]

    def SetEclipse(self):
        """
        Sets the eclipse module.
        """

        self.eclipseObject.ModelTag = "eclipse"
        self.eclipseObject.addSpacecraftToModel(self.scObject.scStateOutMsg)
        self.eclipseObject.addPlanetToModel(self.spiceObject.planetStateOutMsgs[0])  # 0 = Earth state msg
        self.eclipseObject.addPlanetToModel(self.spiceObject.planetStateOutMsgs[1])  # 1 = Moon state msg

    def SetSrpEffector(self):
        """
        Sets the solar radiation pressure module.
        """

        self.srpEff.ModelTag = "SrpEffector"
        for ind in range(0, len(self.scFacetAreas)):
            self.srpEff.addFacet(self.scFacetAreas[ind],
                                 self.scFacetAbsorpCoeffs[ind],
                                 self.scFacetSpecCoeffs[ind],
                                 self.scFacetDiffCoeffs[ind],
                                 self.scFacetNormals_B[ind],
                                 self.scFacetLocations_B[ind])
        self.scObject.addDynamicEffector(self.srpEff)

    def SetGyroSensor(self):
        """
        Sets the GyroSensor module.
        """

        #
        # Gyroscope noise properties modelled off of Bosch BMG250. See details
        # here: 
        #    https://aces.atlassian.net/wiki/spaces/~551756489/pages/28606687/Gyro+Trade
        #

        self.gyro.ModelTag = "gyroSensor"
        self.gyro.rateNoiseStd = 0.007 * macros.D2R
        self.gyro.biasNoiseStd = 0.001 * macros.D2R
        self.gyro.wBiasInit = np.array([1, -1, 0.5]) * macros.D2R
        self.gyro.sigma_GB = [0., 0., 0.]

    def SetStarTrackerSensor(self):
        """
        Sets the StarTrackerSensor module.
        """

        #
        # Star tracker noise configs based on KAIROSPACE Star Tracker. See
        # details here: 
        #    https://aces.atlassian.net/wiki/spaces/~551756489/pages/28672028/Star+Tracker+Trade
        #
        self.starTracker.ModelTag = "starTrackerSensor"
        aroundBoresightStd = 20. * macros.ARCSEC2RAD
        crossBoresightStd = 5 / 3. * macros.ARCSEC2RAD
        self.starTracker.R = [aroundBoresightStd ** 2 / 16.0, 0, 0,
                              0, crossBoresightStd ** 2 / 16.0, 0,
                              0, 0, crossBoresightStd ** 2 / 16.0]
        self.starTracker.sigma_CB = [0.0, 0.0, 0.0]

    def InitAllDynObjects(self):
        """
        Initialize all the dynamics objects.
        """

        self.SetSpacecraftHub()
        self.SetSpacecraftFacets()
        self.SetGravityBodies()
        self.SetSpiceInterface()
        self.SetSimpleNavObject()
        self.SetGroundStation()
        self.SetReactionWheelDynEffector()
        self.SetMtbEffector()
        self.SetMagModule()
        self.SetAtmosphere()
        self.SetGravityGradientEffector()
        self.SetFacetDragEffector()
        self.SetTAM()
        self.SetEclipse()
        self.SetSrpEffector()
        self.SetGyroSensor()
        self.SetStarTrackerSensor()

    def LinkMessages(self):
        """
        Link messages that are fully internal to the dynamics modules. Note that the vast majority of messages link to
        FSW algorithms, and all other linking occurs in the ACES_FSW module.
        """

        #
        # Link messages for navigation module.
        self.sNavObject.scStateInMsg.subscribeTo(self.scObject.scStateOutMsg)
        self.sNavObject.sunStateInMsg.subscribeTo(self.spiceObject.planetStateOutMsgs[2])  # 2 = Sun state msg

        #
        # Link messages for ground station module.
        # It's not a subscribe, but it looks like message linking to me. BSK works in mysterious ways...
        self.groundStation.addSpacecraftToModel(self.scObject.scStateOutMsg)

        #
        # Link messages for three axis magnetometer sensor module.
        self.TAM.stateInMsg.subscribeTo(self.scObject.scStateOutMsg)
        self.TAM.magInMsg.subscribeTo(self.magModule.envOutMsgs[0])

        #
        # Link messages for the magnetic torque bar effector.
        self.mtbEff.magInMsg.subscribeTo(self.magModule.envOutMsgs[0])

        #
        # Link messages for facet drag effector.
        self.facetDragEff.atmoDensInMsg.subscribeTo(self.atmosphere.envOutMsgs[0])

        #
        # Link messages for eclipse module.
        self.eclipseObject.sunInMsg.subscribeTo(self.spiceObject.planetStateOutMsgs[2])  # 2 = Sun state msg

        #
        # Link messages for the solar radiation pressure module.
        self.srpEff.sunEphmInMsg.subscribeTo(self.spiceObject.planetStateOutMsgs[2])  # 2 = Sun state msg
        self.srpEff.sunEclipseInMsg.subscribeTo(self.eclipseObject.eclipseOutMsgs[0])

        #
        # Link messages for the gravity model.
        for ind, gravBody in enumerate(self.gravBodyNames):
            self.gravBodies[gravBody].planetBodyInMsg.subscribeTo(self.spiceObject.planetStateOutMsgs[ind])

        #
        # Link messages for GyroSensor module
        self.gyro.scStateInMsg.subscribeTo(self.scObject.scStateOutMsg)

        #
        # Link messages for StarTracker module
        self.starTracker.scStateInMsg.subscribeTo(self.scObject.scStateOutMsg)
