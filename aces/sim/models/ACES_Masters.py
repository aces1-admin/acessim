#
# ACES Masters
#
# Purpose:  Master modules for ACES simulations.
# Author:   Brennan Gray
# Creation Date:  January 31, 2022
#
# Description:
#
#   Module containing master modules for ACES scenarios and simulations.
#
#   More details here:
#   https://aces.atlassian.net/l/c/zsMjHqRt

# Import architectural modules
from Basilisk.utilities import SimulationBaseClass

# Get current file path
import sys, os, inspect
filename = inspect.getframeinfo(inspect.currentframe()).filename
path = os.path.dirname(os.path.abspath(filename))

class ACESSim(SimulationBaseClass.SimBaseClass):
    """
    Main ACES simulation class.
    """

    def __init__(self, fswRate=0.1, dynRate=0.01, mass=10, length=0.3, width=0.2, height=0.1):
        self.dynRate = dynRate
        self.fswRate = fswRate

        # Spacecraft mass and geometry properties
        self.mass = mass  # kg, spacecraft mass
        self.length = length  # m, length
        self.width = width  # m, width
        self.height = height  # m, height

        # Create a sim module as an empty container
        SimulationBaseClass.SimBaseClass.__init__(self)

        self.DynModels = []
        self.FSWModels = []
        self.DynamicsProcessName = None
        self.FSWProcessName = None
        self.dynProc = None
        self.fswProc = None

        self.dynamics_added = False
        self.fsw_added = False

    def get_DynModel(self):
        """
        Gets the dynamics model of this simulation.
        """

        assert (self.dynamics_added is True), "It is mandatory to use a dynamics model as an argument"
        return self.DynModels

    def set_DynModel(self, dynModel):
        """
        Sets the dynamics model of this simulation.
        """

        self.dynamics_added = True
        self.DynamicsProcessName = 'DynamicsProcess'  # Create simulation process name
        self.dynProc = self.CreateNewProcess(self.DynamicsProcessName)  # Create process
        self.DynModels = dynModel.ACESDynamicModels(self, self.dynRate, self.mass, self.length, self.width,
                                                    self.height)  # Create Dynamics

    def get_FswModel(self):
        """
        Gets the FSW model of this simulation.
        """

        assert (self.fsw_added is True), "A flight software model has not been added yet"
        return self.FSWModels

    def set_FswModel(self, fswModel):
        """
        Sets the FSW model of this simulation.
        """

        self.fsw_added = True
        self.FSWProcessName = "FSWProcess"  # Create simulation process name
        self.fswProc = self.CreateNewProcess(self.FSWProcessName)  # Create process
        self.FSWModels = fswModel.ACESFSWModels(self, self.fswRate)  # Create FSW


class ACESScenario(object):
    """
    Main ACES scenario class.
    """

    def __init__(self):
        self.name = "scenario"

    def configure_initial_conditions(self):
        """
        Developer must override this method in their ACESScenario derived subclass.
        """

        pass

    def log_outputs(self):
        """
        Developer must override this method in their ACESScenario derived subclass.
        """

        pass

    def pull_outputs(self, showPlots=False):
        """
        Developer must override this method in their ACESScenario derived subclass.
        """

        pass
