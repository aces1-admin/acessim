#
# ACES FSW Module
#
# Purpose:  FSW Module for ACES ADCS simulations.
# Author:   Brennan Gray
# Creation Date:  January 31, 2022
#
# Description:
#
#   Module containing core ACES ADCS flight software modeling.
#
#   More details here:
#   https://aces.atlassian.net/l/c/zsMjHqRt

from Basilisk import __path__
from Basilisk.architecture import messaging
from Basilisk.fswAlgorithms import (attTrackingError,
                                    inertial3D,
                                    locationPointing,
                                    rwMotorTorque,
                                    tamComm,
                                    mtbMomentumManagementSimple,
                                    torque2Dipole,
                                    dipoleMapping,
                                    rwNullSpace)
from Basilisk.ExternalModules import (attitudeEKF,
                                      bDotController,
                                      modeController,
                                      mrpFeedbackModes,
                                      mtbFeedforwardModes,
                                      sunSafePointWithRef)
from Basilisk.utilities import (macros)
bskPath = __path__[0]


class ACESFSWModels:
    """
    ACES FSW model for simulation.
    """

    def __init__(self, SimBase, fswRate):
        #
        # Define empty class variables.
        self.vcMsg = None
        self.fswRwParamMsg = None
        self.mtbParamsInMsg = None
        self.rwConstellationConfigInMsg = None
        self.rwNullSpaceWheelSpeedBiasInMsg = None
        self.additionalModules = {}  # Storage for additional modules added by scenarios.

        #
        # Create the flight software side task.
        self.vehicleConfigOut = messaging.VehicleConfigMsgPayload()

        self.processName = SimBase.FSWProcessName
        self.taskName = "fswTask"
        self.fswTimeStep = macros.sec2nano(fswRate)
        SimBase.fswProc.addTask(SimBase.CreateNewTask(self.taskName, self.fswTimeStep))

        #
        # Instantiate FSW modules and module wraps.
        self.modeControllerConfig = modeController.modeControllerConfig()
        self.modeControllerWrap = SimBase.setModelDataWrap(self.modeControllerConfig)
        self.modeControllerWrap.ModelTag = "modeController"

        self.attitudeEkfConfig = attitudeEKF.attitudeEKFConfig()
        self.attitudeEkfWrap = SimBase.setModelDataWrap(self.attitudeEkfConfig)
        self.attitudeEkfWrap.ModelTag = "attitudeEKF"
        
        self.tamCommConfig = tamComm.tamConfigData()
        self.tamCommWrap = SimBase.setModelDataWrap(self.tamCommConfig)
        self.tamCommWrap.ModelTag = "tamComm"

        self.bDotControllerConfig = bDotController.bDotControllerConfig()
        self.bDotControllerWrap = SimBase.setModelDataWrap(self.bDotControllerConfig)
        self.bDotControllerWrap.ModelTag = "bDotController"

        self.inertial3DConfig = inertial3D.inertial3DConfig()
        self.inertial3DWrap = SimBase.setModelDataWrap(self.inertial3DConfig)
        self.inertial3DWrap.ModelTag = "inertial3D"

        self.inertialScienceConfig = inertial3D.inertial3DConfig()
        self.inertialScienceWrap = SimBase.setModelDataWrap(self.inertialScienceConfig)
        self.inertialScienceWrap.ModelTag = "inertialScience"

        self.sunSafePointConfig = sunSafePointWithRef.sunSafePointWithRefConfig()
        self.sunSafePointWrap = SimBase.setModelDataWrap(self.sunSafePointConfig)
        self.sunSafePointWrap.ModelTag = "sunSafePoint"

        self.locationPointConfig = locationPointing.locationPointingConfig()
        self.locationPointWrap = SimBase.setModelDataWrap(self.locationPointConfig)
        self.locationPointWrap.ModelTag = "locationPoint"

        self.attErrorConfig = attTrackingError.attTrackingErrorConfig()
        self.attErrorWrap = SimBase.setModelDataWrap(self.attErrorConfig)
        self.attErrorWrap.ModelTag = "attErrorInertial3D"

        self.attErrorScienceConfig = attTrackingError.attTrackingErrorConfig()
        self.attErrorScienceWrap = SimBase.setModelDataWrap(self.attErrorScienceConfig)
        self.attErrorScienceWrap.ModelTag = "attErrorInertialScience"

        self.mrpControlConfig = mrpFeedbackModes.mrpFeedbackModesConfig()
        self.mrpControlWrap = SimBase.setModelDataWrap(self.mrpControlConfig)
        self.mrpControlWrap.ModelTag = "mrpFeedbackModes"

        self.mtbMomentumManagementSimpleConfig = \
            mtbMomentumManagementSimple.mtbMomentumManagementSimpleConfig()
        self.mtbMomentumManagementSimpleWrap = \
            SimBase.setModelDataWrap(self.mtbMomentumManagementSimpleConfig)
        self.mtbMomentumManagementSimpleWrap.ModelTag = "mtbMomentumManagementSimple"

        self.torque2DipoleConfig = torque2Dipole.torque2DipoleConfig()
        self.torque2DipoleWrap = SimBase.setModelDataWrap(self.torque2DipoleConfig)
        self.torque2DipoleWrap.ModelTag = "torque2Dipole"

        self.mtbConfigParams = messaging.MTBArrayConfigMsgPayload()
        self.dipoleMappingConfig = dipoleMapping.dipoleMappingConfig()
        self.dipoleMappingWrap = SimBase.setModelDataWrap(self.dipoleMappingConfig)
        self.dipoleMappingWrap.ModelTag = "dipoleMapping"

        self.mtbFeedforwardConfig = mtbFeedforwardModes.mtbFeedforwardModesConfig()
        self.mtbFeedforwardWrap = SimBase.setModelDataWrap(self.mtbFeedforwardConfig)
        self.mtbFeedforwardWrap.ModelTag = "mtbFeedforwardModes"

        self.rwMotorTorqueConfig = rwMotorTorque.rwMotorTorqueConfig()
        self.rwMotorTorqueWrap = SimBase.setModelDataWrap(self.rwMotorTorqueConfig)
        self.rwMotorTorqueWrap.ModelTag = "rwMotorTorque"

        self.rwNullSpaceConfig = rwNullSpace.rwNullSpaceConfig()
        self.rwConstellationConfig = messaging.RWConstellationMsgPayload()
        self.rwNullSpaceWheelSpeedBias = messaging.RWSpeedMsgPayload()
        self.rwNullSpaceWrap = SimBase.setModelDataWrap(self.rwNullSpaceConfig)
        self.rwNullSpaceWrap.ModelTag = "rwNullSpace"

        #
        # Initialize all modules.
        self.InitializeAllFSWObjects(SimBase)

        #
        # Add models to task.
        SimBase.AddModelToTask(self.taskName, self.modeControllerWrap, self.modeControllerConfig)
        SimBase.AddModelToTask(self.taskName, self.attitudeEkfWrap, self.attitudeEkfConfig)
        SimBase.AddModelToTask(self.taskName, self.tamCommWrap, self.tamCommConfig)
        SimBase.AddModelToTask(self.taskName, self.bDotControllerWrap, self.bDotControllerConfig)
        SimBase.AddModelToTask(self.taskName, self.inertial3DWrap, self.inertial3DConfig)
        SimBase.AddModelToTask(self.taskName, self.inertialScienceWrap, self.inertialScienceConfig)
        SimBase.AddModelToTask(self.taskName, self.sunSafePointWrap, self.sunSafePointConfig)
        SimBase.AddModelToTask(self.taskName, self.locationPointWrap, self.locationPointConfig)
        SimBase.AddModelToTask(self.taskName, self.attErrorWrap, self.attErrorConfig)
        SimBase.AddModelToTask(self.taskName, self.attErrorScienceWrap, self.attErrorScienceConfig)
        SimBase.AddModelToTask(self.taskName, self.mrpControlWrap, self.mrpControlConfig)
        SimBase.AddModelToTask(self.taskName, self.mtbMomentumManagementSimpleWrap,
                               self.mtbMomentumManagementSimpleConfig)
        SimBase.AddModelToTask(self.taskName, self.torque2DipoleWrap, self.torque2DipoleConfig)
        SimBase.AddModelToTask(self.taskName, self.dipoleMappingWrap, self.dipoleMappingConfig)
        SimBase.AddModelToTask(self.taskName, self.mtbFeedforwardWrap, self.mtbFeedforwardConfig)
        SimBase.AddModelToTask(self.taskName, self.rwMotorTorqueWrap, self.rwMotorTorqueConfig)
        SimBase.AddModelToTask(self.taskName, self.rwNullSpaceWrap, self.rwNullSpaceConfig)

        #
        # Link messages.
        # Note that this also links almost all messages for dynamics modules, since FSW needs to be initiated before
        # that can be done in most cases.
        self.LinkMessages(SimBase)

    def AddAdditionalModule(self, SimBase, moduleName, module, moduleWrap):
        """
        Adds the provided module to the FSW task with the provided module data wrap and stores it. Note that the module
        should be fully configured and linked before it is added to the FSW model.

        Once added, the new module can be accessed through the additionalModules dictionary as follows:
            FswModel.additionalModules[moduleName]
        where FswModel is a reference to this class. Module wraps can be accessed similarly:
            FswModel.additionalModules[moduleName + "Wrap"]
        if they are needed after initialization.
        """

        self.additionalModules[moduleName] = module
        self.additionalModules[moduleName + "Wrap"] = moduleWrap
        SimBase.AddModelToTask(self.taskName, moduleWrap, module)

    # ------------------------------------------------------------------------------------------- #
    # These are module initialization methods.
    def SetVehicleOutConfig(self, SimBase):
        """
        Set the FSW vehicle configuration. Note that this assumes the FSW has perfect knowledge of the vehicle.
        """

        self.vehicleConfigOut.ISCPntB_B = SimBase.DynModels.I_sc
        self.vcMsg = messaging.VehicleConfigMsg().write(self.vehicleConfigOut)
        self.fswRwParamMsg = SimBase.DynModels.rwFactory.getConfigMessage()

    def SetModeController(self):
        """
        Sets the ModeController module.
        """

        self.modeControllerConfig.detumbleTimeout = 10000  # [s] Timeout before mode controller moves to safe mode from
                                                           # detumble
        self.modeControllerConfig.detumbleExitRateError = 0.5 * macros.D2R  # [rad/s] Rate error at which the mode
                                                                            # controller exits detumble
        self.modeControllerConfig.settledPointingError = 0.005  # [] Max MRP error considered settled
        self.modeControllerConfig.settledRateError = 100 * macros.ARCSEC2RAD  # [rad/s] Max rate error considered settled
        self.modeControllerConfig.settledCycles = 150  # [] Number of cycles in the box to be considered settled
                                                       # (each cycle is 0.1s currently)

    def SetAttitudeEKF(self):
        """
        Sets the attitudeEKF module.
        """
        
        #
        # Star tracker noise configs based on KAIROSPACE Star Tracker. See
        # details here: 
        #    https://aces.atlassian.net/wiki/spaces/~551756489/pages/28672028/Star+Tracker+Trade
        #
        # Gyroscope noise properties modelled off of Bosch BMG250. See details
        # here: 
        #    https://aces.atlassian.net/wiki/spaces/~551756489/pages/28606687/Gyro+Trade
        #
        self.attitudeEkfConfig.biasInit = [0 * macros.D2R, 0 * macros.D2R, 0 * macros.D2R]
        aroundBoresightStd        = 20. * macros.ARCSEC2RAD
        crossBoresightStd         = 5/3. * macros.ARCSEC2RAD
        self.attitudeEkfConfig.P0 = [(3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0, 0,
                                0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0, 0,
                                0, 0, (3 * macros.D2R)**2 / 16.0, 0, 0, 0,
                                0, 0, 0, (0.1 * macros.D2R)**2, 0, 0,
                                0, 0, 0, 0, (0.1 * macros.D2R)**2, 0,
                                0, 0, 0, 0, 0, (0.1 * macros.D2R)**2]
        self.attitudeEkfConfig.Q = [0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0]
        self.attitudeEkfConfig.R = [aroundBoresightStd**2 / 16.0, 0, 0,
                               0, crossBoresightStd**2 / 16.0, 0,
                               0, 0, crossBoresightStd**2 / 16.0]
        rateNoiseStd = 0.007 * macros.D2R
        biasNoiseStd = 0.001 * macros.D2R
        sigma_GB     = [0., 0., 0]
        sigma_CB = [0.0, 0.0, 0.0]
        self.attitudeEkfConfig.gyroRateNoiseStd = rateNoiseStd
        self.attitudeEkfConfig.gyroBiasNoiseStd = biasNoiseStd
        self.attitudeEkfConfig.sigma_GB = sigma_GB
        self.attitudeEkfConfig.sigma_CB = sigma_CB
        
        # low pass coeffs (current set to a cutoff freq of 10 deg/s)
        self.attitudeEkfConfig.lpfNumCoeffs = [0.0087 , 0.0087 ]
        self.attitudeEkfConfig.lpfDenCoeffs = [1., -0.9827]
        
    def SetTamComm(self):
        """
        Set the TAM communications module. This interfaces with the TAM module and produces the magnetic field in the
        Body frame.
        """

        self.tamCommConfig.dcm_BS = [1., 0., 0., 0., 1., 0., 0., 0., 1.]

    def SetBdotController(self):
        """
        Set the Bdot controller module.
        """

        self.bDotControllerConfig.Kp = 0.0005  # [A m^2 s Tesla] proportional gain
        self.bDotControllerConfig.dipoleLimit = 0.39  # [A m^2] max dipole along each body frame axis
        self.bDotControllerConfig.dtMax = 1  # [s] max delta-t to perform finite differencing

    def SetInertial3DConfig(self):
        """
        Set the Inertial 3D guidance module.
        """

        self.inertial3DConfig.sigma_R0N = [0., 0., 0.]  # Reference attitude

    def SetInertialScienceConfig(self):
        """
        Set the Inertial science guidance module. Note sigma_R0N is chosen
        arbitrarily here and does not represent pointing at the sun, a star, etc.
        """

        self.inertialScienceConfig.sigma_R0N = [-0.7, 0.2, 0.1]  # set the desired inertial orientation

    def SetSunSafeConfig(self):
        """
        Set the sun safe guidance module.
        """

        self.sunSafePointConfig.sHatBdyCmd = [0., 0., 1.]  # Set the body axis to point towards the sun

    def SetLocationPointConfig(self):
        """
        Set the location pointing guidance module.
        """

        self.locationPointConfig.pHat_B = [0., 0., 1.]  # Axis to point at the ground target
        self.locationPointConfig.useBoresightRateDamping = 1

    def SetMRPControl(self):
        """
        Set the MRP feedback control module. Note that
        the integral gain is turned off and the K and P gains were chosen
        arbitrarily and should be tuned down the line. They work ok for now.
        """

        self.mrpControlConfig.Ki = -1  # make value negative to turn off integral feedback
        self.mrpControlConfig.K = 0.0003
        self.mrpControlConfig.P = 0.003
        self.mrpControlConfig.integralLimit = 2. / self.mrpControlConfig.Ki * 0.1

    def SetMtbMomentumManagement(self):
        """
        Set the MTB momentum management module. Note the gain Kp was chosen arbitrarily and needs to be tuned,
        but works ok for now.
        """

        self.mtbMomentumManagementSimpleConfig.Kp = 0.003

    def SetDipoleMapping(self):
        """
        Set the dipole mapping module. This module maps body frame dipole commands into torque rod specific dipole
        commands. Note that the alignment and steering matrices are in row major order.
        """

        self.mtbConfigParams.numMTB = 2
        self.mtbConfigParams.GtMatrix_B = [1., 0., 0., 1., 0., 0.]
        maxDipole = 0.39
        self.mtbConfigParams.maxMtbDipoles = [maxDipole] * self.mtbConfigParams.numMTB
        self.mtbParamsInMsg = messaging.MTBArrayConfigMsg().write(self.mtbConfigParams)
        self.dipoleMappingConfig.steeringMatrix = [1, 0., 0., 0., 1., 0.]

    def SetRWMotorTorque(self):
        """
        Set the RW motor torque.
        """

        controlAxes_B = [1, 0, 0, 0, 1, 0, 0, 0, 1]
        self.rwMotorTorqueConfig.controlAxes_B = controlAxes_B

    def SetRWNullSpace(self, SimBase):
        """
        Set the RW null space module. Note that the feedback gain OmegaGain is chosen arbitrarily as well as the desired
        wheel speeds. These should be refined at some point.
        """

        numRW = SimBase.DynModels.numRW
        rwList = SimBase.DynModels.rwList

        self.rwNullSpaceConfig.OmegaGain =  3E-8
        self.rwConstellationConfig.numRW = numRW
        rwConfigElementList = list()
        for j in range(0, numRW):
            rwConfigElementMsg = messaging.RWConfigElementMsgPayload()
            rwConfigElementMsg.gsHat_B = SimBase.DynModels.Gs[:, j]
            rwConfigElementMsg.Js = rwList[j].Js
            rwConfigElementMsg.uMax = rwList[j].u_max
            rwConfigElementList.append(rwConfigElementMsg)
        self.rwConstellationConfig.reactionWheels = rwConfigElementList
        self.rwConstellationConfigInMsg = \
            messaging.RWConstellationMsg().write(self.rwConstellationConfig)
        desiredOmega = [100 * macros.rpm2radsec] * numRW
        self.rwNullSpaceWheelSpeedBias = messaging.RWSpeedMsgPayload()
        self.rwNullSpaceWheelSpeedBias.wheelSpeeds = desiredOmega
        self.rwNullSpaceWheelSpeedBiasInMsg = \
            messaging.RWSpeedMsg().write(self.rwNullSpaceWheelSpeedBias)

    def InitializeAllFSWObjects(self, SimBase):
        """
        Initialize all FSW objects.
        """

        self.SetModeController()
        self.SetAttitudeEKF()
        self.SetVehicleOutConfig(SimBase)
        self.SetTamComm()
        self.SetBdotController()
        self.SetInertial3DConfig()
        self.SetInertialScienceConfig()
        self.SetSunSafeConfig()
        self.SetLocationPointConfig()
        self.SetMRPControl()
        self.SetMtbMomentumManagement()
        self.SetDipoleMapping()
        self.SetRWMotorTorque()
        self.SetRWNullSpace(SimBase)

    def LinkMessages(self, SimBase):
        """
        Link all messages.
        """

        #
        # Link messages for the ModeController module.
        self.modeControllerConfig.offAttGuidInMsg.subscribeTo(self.attErrorConfig.attGuidOutMsg)
        self.modeControllerConfig.offAttRefInMsg.subscribeTo(self.inertial3DConfig.attRefOutMsg)

        self.modeControllerConfig.detumbleAttGuidInMsg.subscribeTo(self.attErrorConfig.attGuidOutMsg)
        self.modeControllerConfig.detumbleAttRefInMsg.subscribeTo(self.inertial3DConfig.attRefOutMsg)
        self.modeControllerConfig.detumbleDipoleRequestBodyInMsg.\
            subscribeTo(self.bDotControllerConfig.dipoleRequestBodyOutMsg)

        self.modeControllerConfig.safeAttGuidInMsg.subscribeTo(self.sunSafePointConfig.attGuidanceOutMsg)
        self.modeControllerConfig.safeAttRefInMsg.subscribeTo(self.sunSafePointConfig.attRefOutMsg)
        self.modeControllerConfig.safeDipoleRequestBodyInMsg.\
            subscribeTo(self.torque2DipoleConfig.dipoleRequestOutMsg)

        self.modeControllerConfig.sunPointingAttGuidInMsg.subscribeTo(self.sunSafePointConfig.attGuidanceOutMsg)
        self.modeControllerConfig.sunPointingAttRefInMsg.subscribeTo(self.sunSafePointConfig.attRefOutMsg)
        self.modeControllerConfig.sunPointingDipoleRequestBodyInMsg.\
            subscribeTo(self.torque2DipoleConfig.dipoleRequestOutMsg)

        self.modeControllerConfig.firstCommandAttGuidInMsg.subscribeTo(self.locationPointConfig.attGuidOutMsg)
        self.modeControllerConfig.firstCommandAttRefInMsg.subscribeTo(self.locationPointConfig.attRefOutMsg)
        self.modeControllerConfig.firstCommandDipoleRequestBodyInMsg. \
            subscribeTo(self.torque2DipoleConfig.dipoleRequestOutMsg)

        self.modeControllerConfig.secondCommandAttGuidInMsg.subscribeTo(self.attErrorScienceConfig.attGuidOutMsg)
        self.modeControllerConfig.secondCommandAttRefInMsg.subscribeTo(self.inertialScienceConfig.attRefOutMsg)
        self.modeControllerConfig.secondCommandDipoleRequestBodyInMsg. \
            subscribeTo(self.torque2DipoleConfig.dipoleRequestOutMsg)

        #
        # Link messages for the attitudeEKF module.
        self.attitudeEkfConfig.gyroInMsg.subscribeTo(SimBase.DynModels.gyro.gyroOutMsg)
        self.attitudeEkfConfig.starTrackerInMsg.subscribeTo(SimBase.DynModels.starTracker.starTrackerOutMsg)

        #
        # Link messages for the sun safe pointing module.
        # NOTE: This is using the true attitude states right now. In the future, this should take information from a sun
        # vector computation FSW module.
        self.sunSafePointConfig.imuInMsg.subscribeTo(self.attitudeEkfConfig.navAttMsgEstimated)
        self.sunSafePointConfig.sunDirectionInMsg.subscribeTo(SimBase.DynModels.sNavObject.attOutMsg)

        #
        # Link messages for the location pointing module.
        # NOTE: This is using the true ground station position right now. In the future, we may want to change this to
        # more accurately reflect how the spacecraft will acquire the ground station.
        self.locationPointConfig.scAttInMsg.subscribeTo(self.attitudeEkfConfig.navAttMsgEstimated)
        self.locationPointConfig.scTransInMsg.subscribeTo(SimBase.DynModels.sNavObject.transOutMsg)
        self.locationPointConfig.locationInMsg.subscribeTo(SimBase.DynModels.groundStation.currentGroundStateOutMsg)

        #
        # Link messages for attitude tracking error module.
        self.attErrorConfig.attNavInMsg.subscribeTo(self.attitudeEkfConfig.navAttMsgEstimated)
        self.attErrorConfig.attRefInMsg.subscribeTo(self.inertial3DConfig.attRefOutMsg)

        # Link messages for attitude science tracking error module.
        self.attErrorScienceConfig.attNavInMsg.subscribeTo(self.attitudeEkfConfig.navAttMsgEstimated)
        self.attErrorScienceConfig.attRefInMsg.subscribeTo(self.inertialScienceConfig.attRefOutMsg)

        #
        # Link messages for attitude control module.
        self.mrpControlConfig.guidInMsg.subscribeTo(self.modeControllerConfig.attErrorOutMsg)
        self.mrpControlConfig.vehConfigInMsg.subscribeTo(self.vcMsg)
        self.mrpControlConfig.rwParamsInMsg.subscribeTo(self.fswRwParamMsg)
        self.mrpControlConfig.modeInMsg.subscribeTo(self.modeControllerConfig.modeOutMsg)
        self.mrpControlConfig.rwSpeedsInMsg.subscribeTo(SimBase.DynModels.rwStateEffector.rwSpeedOutMsg)

        #
        # Link messages for three axis magnetometer sensor module.
        self.tamCommConfig.tamInMsg.subscribeTo(SimBase.DynModels.TAM.tamDataOutMsg)

        #
        # Link messages for Bdot controller module.
        self.bDotControllerConfig.tamSensorBodyInMsg.subscribeTo(self.tamCommConfig.tamOutMsg)

        #
        # Link messages for momentum management module.
        self.mtbMomentumManagementSimpleConfig.rwParamsInMsg.subscribeTo(self.fswRwParamMsg)
        self.mtbMomentumManagementSimpleConfig.rwSpeedsInMsg\
            .subscribeTo(SimBase.DynModels.rwStateEffector.rwSpeedOutMsg)

        #
        # Link messages for torque to dipole module.
        self.torque2DipoleConfig.tauRequestInMsg.subscribeTo(self.mtbMomentumManagementSimpleConfig.tauMtbRequestOutMsg)
        self.torque2DipoleConfig.tamSensorBodyInMsg.subscribeTo(self.tamCommConfig.tamOutMsg)

        #
        # Link messages for the dipole mapping module.
        self.dipoleMappingConfig.dipoleRequestBodyInMsg.subscribeTo(self.modeControllerConfig.dipoleRequestBodyOutMsg)
        self.dipoleMappingConfig.mtbArrayConfigParamsInMsg.subscribeTo(self.mtbParamsInMsg)

        #
        # Link messages for magnetic torque bar feedforward module. This module
        # appends the anticipated MTB torque to the requested Body frame torque
        # output by the attitude controller.
        self.mtbFeedforwardConfig.vehControlInMsg.subscribeTo(self.mrpControlConfig.cmdTorqueOutMsg)
        self.mtbFeedforwardConfig.dipoleRequestMtbInMsg.subscribeTo(self.dipoleMappingConfig.dipoleRequestMtbOutMsg)
        self.mtbFeedforwardConfig.tamSensorBodyInMsg.subscribeTo(self.tamCommConfig.tamOutMsg)
        self.mtbFeedforwardConfig.modeInMsg.subscribeTo(self.modeControllerConfig.modeOutMsg)
        self.mtbFeedforwardConfig.mtbArrayConfigParamsInMsg.subscribeTo(self.mtbParamsInMsg)

        #
        # Link messages for the reaction wheel motor torque module.
        self.rwMotorTorqueConfig.rwParamsInMsg.subscribeTo(self.fswRwParamMsg)
        self.rwMotorTorqueConfig.vehControlInMsg.subscribeTo(self.mtbFeedforwardConfig.vehControlOutMsg)

        #
        # Link messages for the reaction wheel null space controller module.
        self.rwNullSpaceConfig.rwMotorTorqueInMsg.subscribeTo(self.rwMotorTorqueConfig.rwMotorTorqueOutMsg)
        self.rwNullSpaceConfig.rwSpeedsInMsg.subscribeTo(SimBase.DynModels.rwStateEffector.rwSpeedOutMsg)
        self.rwNullSpaceConfig.rwConfigInMsg.subscribeTo(self.rwConstellationConfigInMsg)
        self.rwNullSpaceConfig.rwDesiredSpeedsInMsg.subscribeTo(self.rwNullSpaceWheelSpeedBiasInMsg)

        #
        # Link messages for the reaction wheel effectors.
        SimBase.DynModels.rwStateEffector.rwMotorCmdInMsg.subscribeTo(self.rwNullSpaceConfig.rwMotorTorqueOutMsg)

        #
        # Link messages for the magnetic torque bar effector.
        SimBase.DynModels.mtbEff.mtbCmdInMsg.subscribeTo(self.dipoleMappingConfig.dipoleRequestMtbOutMsg)
        SimBase.DynModels.mtbEff.mtbParamsInMsg.subscribeTo(self.mtbParamsInMsg)
