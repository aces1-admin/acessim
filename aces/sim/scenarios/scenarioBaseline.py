#
# ACES Baseline Scenario
#
# Purpose:  ACES Baseline Scenario.
# Author:   Brennan Gray
# Creation Date:  January 31, 2022
#
# Description:
#
#   Simulation of inertial pointing spacecraft in a LEO orbit equipped with
#   reaction wheels, torque rods, and a three axis magnetometer.
#
#   More details here:
#   https://aces.atlassian.net/wiki/spaces/~551756489/pages/491521/Baseline+Scenario
#   https://aces.atlassian.net/l/c/zsMjHqRt
#
#   Scenario creation manual available at:
#   https://aces.atlassian.net/l/c/oWqGwVwh

#
# Import argparse and define command line arguments.
import argparse
parser = argparse.ArgumentParser(description="ACES Baseline Scenario.")
parser.add_argument('--no-plot', dest='plot', action='store_false', help='Disables plotting.')
parser.set_defaults(plot=True)
parser.add_argument('-l', '--length', type=float, nargs='?', const=60, default=60,
                    help='Sets the desired simulation length in minutes.')
parser.add_argument('--do-analysis', dest='analysis', action='store_true',
                    help='Enables requirements analysis. Note that requirements analysis cannot be run for time '
                         'periods less than 16 minutes and will be force disabled if length is less than 16 minutes.')
parser.set_defaults(analysis=False)

from Basilisk.utilities import orbitalMotion, macros, vizSupport, RigidBodyKinematics

#
# Add other folders to path to import files from other folders without creating a package.
import sys, os
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'models'))
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'analysis'))

#
# Import Basilisk path.
from Basilisk import __path__

bskPath = __path__[0]
saveFileName = os.path.basename(os.path.splitext(__file__)[0])

#
# Import master classes: simulation base class and scenario base class.
from ACES_Masters import ACESSim, ACESScenario
import ACES_Dynamics, ACES_FSW
from scenarioBaselinePlotting import *
from inlineAnalysis import *


class scenarioBaseline(ACESSim, ACESScenario):
    def __init__(self, length=60):
        """
        Initialize the scenario.
        """

        #
        # Simulation timing variables.
        self.simulationTime = macros.min2nano(length)
        self.dynRate = 0.01
        self.fswRate = 0.1

        #
        # Call parent constructor message.
        super(scenarioBaseline, self).__init__(self.fswRate, self.dynRate)
        self.name = 'scenarioBaseline'

        #
        # Declare additional class variables.
        self.msgRecList = {}
        self.rwMotorMsgName = "rwMotorMsg"
        self.attErrorMsgName = "attErrorMsg"
        self.attRefMsgName = "attRefMsg"
        self.mrpFeedbackMsgName = "mrpFeedbackModesMsg"
        self.magMsgName = "magMsg"
        self.tamMsgName = "tamMsg"
        self.tamCommMsgName = "tamCommMsg"
        self.dipoleRequestMsgName = "dipoleRequestMsg"
        self.atmosphereMsgName = "atmosphereMsg"
        self.scStateMsgName = "scStateMsg"
        self.rwStateMsgName = "rwStateMsg"
        self.ggMsgName = "ggMsg"
        self.rwModuleMsgNames = ["rw1Msg", "rw2Msg", "rw3Msg"]
        self.spiceMsgNames = ["earthMsg", "moonMsg", "sunMsg"]
        self.eclipseMsgName = "eclipseMsg"
        self.srpMsgName = "srpMsg"
        self.starTrackerMsgName = "starTrackerMsg"
        self.gyroMsgName = "gyroMsg"
        self.attitudeEkfMsgName = "attitudeEkfMsg"
        self.modeControllerMsgName = "modeControllerMsg"

        self.analysisList = {}
        self.detumbleName = "detumble"
        self.pointingDeterminationName = "pointingDetermination"
        self.pointingErrorName = "pointingError"
        self.jitterName = "jitter"

        self.hubref = None  # For Monte Carlo dispersions

        #
        # Set dynamics and FSW.
        self.set_DynModel(ACES_Dynamics)
        self.set_FswModel(ACES_FSW)

        #
        # Call functions to configure the initial simulation conditions and log simulation outputs.
        self.configure_initial_conditions()
        self.log_outputs()

        #
        # Configure simulation so that the data from this simulation can be used for a visualization in Vizard.
        # Uncomment these lines to create data for Vizard.

        #viz = vizSupport.enableUnityVisualization(self, self.DynModels.taskName, self.DynModels.scObject
        #                                         , saveFile=saveFileName
        #                                         , rwEffectorList=self.DynModels.rwStateEffector
        #                                         )

    def configure_initial_conditions(self):
        """
        Configure the initial conditions for the scenario.
        """

        #
        # Define translation state initial conditions using Keplerian elements.
        # Note that the ClassicElements class is used here for convenience and the
        # orbital elements defined are osculating elements rather than mean
        # elements.
        oe = orbitalMotion.ClassicElements()
        oe.a = self.DynModels.gravBodies['earth'].radEquator + 350. * 1000.
        oe.e = 0.0001
        oe.i = 53.2 * macros.D2R
        oe.Omega = 0. * macros.D2R
        oe.omega = 0. * macros.D2R
        oe.f = 0. * macros.D2R
        rN, vN = orbitalMotion.elem2rv(self.DynModels.gravBodies['earth'].mu, oe)

        #
        # Configure spacecraft initial conditions. For more
        # detail see: https://aces.atlassian.net/wiki/spaces/~551756489/pages/491521/Baseline+Scenario
        self.DynModels.scObject.hub.r_CN_NInit = rN  # m   - r_CN_N
        self.DynModels.scObject.hub.v_CN_NInit = vN  # m/s - v_CN_N
        self.DynModels.scObject.hub.sigma_BNInit = [[0.1], [0.2], [-0.3]]  # sigma_CN_B
        self.DynModels.scObject.hub.omega_BN_BInit = [[0.001], [-0.01], [0.03]]  # rad/s - omega_CN_B
        self.hubref = self.DynModels.scObject.hub

        #
        # Configure mode transitions.
        self.FSWModels.modeControllerConfig.commandedModes = [1, 4, 5]
        self.FSWModels.modeControllerConfig.commandedTimes = [300, 1800, 3600]
        self.FSWModels.modeControllerConfig.commandedTimeTypes = [0, 1, 1]
        self.FSWModels.modeControllerConfig.totalNumCommands = 3

    def log_outputs(self):
        """
        Log output variables for later processing or plotting.
        """

        FswModel = self.get_FswModel()
        DynModel = self.get_DynModel()
        numDataPoints = int(self.simulationTime / self.dynRate)
        samplingTime = unitTestSupport.samplingTime(self.simulationTime, self.dynRate, numDataPoints)

        #
        # Record rwMotorTorqueOutMsg.
        self.msgRecList[self.rwMotorMsgName] = FswModel.rwMotorTorqueConfig.rwMotorTorqueOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.rwMotorMsgName])

        #
        # Record attGuidOutMsg.
        self.msgRecList[self.attErrorMsgName] = FswModel.modeControllerConfig.attErrorOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.attErrorMsgName])

        #
        # Record attRefOutMsg.
        self.msgRecList[self.attRefMsgName] = FswModel.modeControllerConfig.attRefOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.attRefMsgName])

        #
        # Record mrpFeedbackModesMsg.
        self.msgRecList[self.mrpFeedbackMsgName] = FswModel.mrpControlConfig.cmdTorqueOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.mrpFeedbackMsgName])

        #
        # Record magModule.envOutMsgs[0].
        self.msgRecList[self.magMsgName] = DynModel.magModule.envOutMsgs[0].recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.magMsgName])

        #
        # Record tamDataOutMsg.
        self.msgRecList[self.tamMsgName] = DynModel.TAM.tamDataOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.tamMsgName])

        #
        # Record tamOutMsg.
        self.msgRecList[self.tamCommMsgName] = FswModel.tamCommConfig.tamOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.tamCommMsgName])

        #
        # Record dipoleRequestMtbOutMsg.
        self.msgRecList[self.dipoleRequestMsgName] = \
            FswModel.dipoleMappingConfig.dipoleRequestMtbOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.dipoleRequestMsgName])

        #
        # Record atmosphere.envOutMsgs[0].
        self.msgRecList[self.atmosphereMsgName] = DynModel.atmosphere.envOutMsgs[0].recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.atmosphereMsgName])

        #
        # Record scStateOutMsg.
        self.msgRecList[self.scStateMsgName] = DynModel.scObject.scStateOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.scStateMsgName])

        #
        # Record rwSpeedOutMsg.
        self.msgRecList[self.rwStateMsgName] = DynModel.rwStateEffector.rwSpeedOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.rwStateMsgName])

        #
        # Record gravityGradientOutMsg.
        self.msgRecList[self.ggMsgName] = DynModel.ggEff.gravityGradientOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.ggMsgName])

        #
        # Record rwOutMsgs.
        for ind, name in enumerate(self.rwModuleMsgNames):
            self.msgRecList[name] = DynModel.rwStateEffector.rwOutMsgs[ind].recorder(samplingTime)
            self.AddModelToTask(DynModel.taskName, self.msgRecList[name])

        #
        # Record planetStateOutMsgs.
        for ind, name in enumerate(self.spiceMsgNames):
            self.msgRecList[name] = DynModel.spiceObject.planetStateOutMsgs[ind].recorder(samplingTime)
            self.AddModelToTask(DynModel.taskName, self.msgRecList[name])

        #
        # Record eclipseOutMsgs.
        self.msgRecList[self.eclipseMsgName] = DynModel.eclipseObject.eclipseOutMsgs[0].recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.eclipseMsgName])

        #
        # Record solarRadiationPressureOutMsg.
        self.msgRecList[self.srpMsgName] = DynModel.srpEff.solarRadiationPressureOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.srpMsgName])

        #
        # Record starTrackerOutMsg. 
        self.msgRecList[self.starTrackerMsgName] = DynModel.starTracker.starTrackerOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.starTrackerMsgName])

        #
        # Record gyroOutMsg. 
        self.msgRecList[self.gyroMsgName] = DynModel.gyro.gyroOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.gyroMsgName])
        
        #
        # Record navAttMsgEstimated. 
        self.msgRecList[self.attitudeEkfMsgName] = FswModel.attitudeEkfConfig.navAttMsgEstimated.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.attitudeEkfMsgName])

        #
        # Record ModeControllerMsg
        self.msgRecList[self.modeControllerMsgName] = FswModel.modeControllerConfig.modeOutMsg.recorder(samplingTime)
        self.AddModelToTask(DynModel.taskName, self.msgRecList[self.modeControllerMsgName])
        
    def pull_outputs(self, showPlots=False, doAnalysis=True):
        """
        Pulls output variables and plots them if requested.
        """

        #
        # Retrieve specific variables from recorders to be used in plotting or
        # general analysis in the future.
        dataUsReq = self.msgRecList[self.rwMotorMsgName].motorTorque
        dataSigmaBN = self.msgRecList[self.scStateMsgName].sigma_BN
        dataSigmaRN = self.msgRecList[self.attRefMsgName].sigma_RN
        dataOmegaBR = self.msgRecList[self.attErrorMsgName].omega_BR_B
        dataOmegaBN_B = self.msgRecList[self.scStateMsgName].omega_BN_B
        dataOmegaRW = self.msgRecList[self.rwStateMsgName].wheelSpeeds
        dataTauCmd = self.msgRecList[self.mrpFeedbackMsgName].torqueRequestBody
        dataRW = []
        for name in self.rwModuleMsgNames:
            dataRW.append(self.msgRecList[name].u_current)
        dataMagFieldTruth = self.msgRecList[self.magMsgName].magField_N
        dataTamMagField_S = self.msgRecList[self.tamMsgName].tam_S
        dataTamMagFieldComm_B = self.msgRecList[self.tamCommMsgName].tam_B
        dataMtbDipoleCmds_T = self.msgRecList[self.dipoleRequestMsgName].mtbDipoleCmds
        dataGgTorque_B = self.msgRecList[self.ggMsgName].gravityGradientTorque_B
        dataAtmospherericDensity = self.msgRecList[self.atmosphereMsgName].neutralDensity
        dataR_BN_N = self.msgRecList[self.scStateMsgName].r_BN_N
        dataV_BN_N = self.msgRecList[self.scStateMsgName].v_BN_N
        dataSpice = []
        for name in self.spiceMsgNames:
            dataSpice.append(self.msgRecList[name].PositionVector)
        dataEclipse = self.msgRecList[self.eclipseMsgName].shadowFactor
        dataSrpForce_B = self.msgRecList[self.srpMsgName].solarRadiationPressureForce_B
        dataSrpTorque_B = self.msgRecList[self.srpMsgName].solarRadiationPressureTorque_B
        dataQ_CN = self.msgRecList[self.starTrackerMsgName].q_CN
        dataOmegaGN = self.msgRecList[self.gyroMsgName].omega_GN_G
        dataSigma_BN_Est = self.msgRecList[self.attitudeEkfMsgName].sigma_BN
        dataOmega_BN_Est = self.msgRecList[self.attitudeEkfMsgName].omega_BN_B
        dataModeId = self.msgRecList[self.modeControllerMsgName].modeId
        dataModeSettled = self.msgRecList[self.modeControllerMsgName].isSettled

        #
        # Create simTimes vector.
        simTimes = self.msgRecList[self.rwMotorMsgName].times() * macros.NANO2MIN

        # Compute true sigmaBR, estimation error
        dataSigmaBR = np.zeros((len(simTimes), 3))
        dataSigmaEstError = np.zeros((len(simTimes), 3))
        for i in range(len(simTimes)):
            dataSigmaBR[i, :] = RigidBodyKinematics.subMRP(dataSigmaBN[i, :], dataSigmaRN[i, :])
            dataSigmaEstError[i, :] = RigidBodyKinematics.subMRP(dataSigma_BN_Est[i, :], dataSigmaBN[i, :])

        if doAnalysis:
            #
            # Check requirements compliance and store values.

            # Check detumble compliance.
            compliant, H_init_exceeded, time_detumble, H_init = checkDetumbleCompliance(simTimes, dataOmegaBN_B,
                                                                                        dataModeId,
                                                                                        self.get_DynModel().I_sc)
            detumble = [compliant, H_init_exceeded, time_detumble, H_init]  # Create detumble data list
            self.analysisList[self.detumbleName] = detumble  # Save detumble data

            # Check pointing determination compliance.
            compliant, axis_std = checkPointingDeterminationCompliance(simTimes, dataSigmaEstError, dataModeId)
            pointing_determination = [compliant[0], compliant[1], axis_std[0], axis_std[1]]  # Create data list
            self.analysisList[self.pointingDeterminationName] = pointing_determination  # Save pointing determination

            # Check pointing error compliance.
            modes, compliant, axis_std = checkPointingErrorCompliance(simTimes, dataSigmaBR, dataModeId,
                                                                      dataModeSettled)
            pointing_error = [modes, compliant, axis_std]
            self.analysisList[self.pointingErrorName] = pointing_error  # Save pointing error data

            # Check jitter compliance.
            modes, compliant, jitter_max, time_max, jitter, times = checkPointingJitterCompliance(simTimes, dataSigmaBR,
                                                                                                  dataModeId,
                                                                                                  dataModeSettled)
            jitter_full = [jitter, times]  # Create list of all jitter values and times
            jitter = [modes, compliant, jitter_max, jitter_full]  # Create jitter data list
            self.analysisList[self.jitterName] = jitter  # Save jitter data

        #
        # Plot the results.
        if showPlots:
            numRW = len(self.rwModuleMsgNames)
            plotAttitudeError(simTimes, dataSigmaBR)
            plotMrps(simTimes, dataSigmaBN, r'Attitude $\sigma_{B/N}$')
            plotMrps(simTimes, dataSigmaRN, r'Attitude $\sigma_{R/N}$')
            plotTorqueCommand(simTimes, dataTauCmd)
            plotRwMotorTorque(simTimes, dataUsReq, dataRW, numRW)
            plotRateError(simTimes, dataOmegaBR)
            plotRwSpeeds(simTimes, dataOmegaRW, numRW)
            plotDataTamComm(simTimes, dataTamMagFieldComm_B)
            plotDataMtbMomentumManagement(simTimes, dataMtbDipoleCmds_T, self.FSWModels.mtbConfigParams.numMTB)
            plotDataGgTorque(simTimes, dataGgTorque_B)
            plotDataAtmosphericDensity(simTimes, dataAtmospherericDensity)
            for ind, gravBody in enumerate(self.DynModels.gravBodyNames):
                plotDataSpice(simTimes, dataSpice[ind], gravBody)
            plotDataEclipse(simTimes, dataEclipse)
            plotDataSrpForce(simTimes, dataSrpForce_B)
            plotDataSrpTorque(simTimes, dataSrpTorque_B)
            plotStarTrackerQuaternion(simTimes, dataQ_CN)
            plotGyroAngularRate(simTimes, dataOmegaGN)
            plotAttitudeEstimate(simTimes, dataSigma_BN_Est)
            plotAngularEstimate(simTimes, dataOmega_BN_Est)
            plotAttitudeKnowledgeError(simTimes, dataSigma_BN_Est, dataSigmaBN)
            plotAngularEstimateError(simTimes, dataOmega_BN_Est, dataOmegaBN_B)
            plotAngularEstimateError(simTimes, dataOmega_BN_Est, dataOmegaGN)
            plotAngularEstimateError(simTimes, dataOmegaGN, dataOmegaBN_B)
            plotModeControllerState(simTimes, dataModeId, dataModeSettled)
            plt.show()

        #
        # Close the plots being saved off to avoid over-writing old and new figures.
        plt.close("all")


def runScenario(scenario):
    """
    Initializes and executes scenario.
    """

    #
    # Show task execution order and simulation progress bar. Uncomment line
    # below to see figure of execution order.
    scenario.ShowExecutionOrder()
    scenario.SetProgressBar(True)
    # scenario.ShowExecutionFigure(True)

    #
    # Initialize, configure, and execute simulation.
    scenario.InitializeSimulation()
    scenario.ConfigureStopTime(scenario.simulationTime)
    scenario.ExecuteSimulation()


def run(showPlots, doAnalysis, length):
    """
    Runs the scenario.
    """

    scenario = scenarioBaseline(length)
    runScenario(scenario)
    scenario.pull_outputs(showPlots, doAnalysis)


if __name__ == "__main__":
    args = parser.parse_args()

    # Check length and force analysis off if length is less than 16 minutes.
    if args.length <= 16 and args.analysis:
        args.analysis = False
        print('Simulation time set to less than 16 minutes, but analysis was enabled. Analysis cannot be run for '
              'simulations less than 16 minutes, and has been disabled.')

    run(args.plot, args.analysis, args.length)
