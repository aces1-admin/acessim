#
# ACES Baseline Scenario Monte Carlo
#
# Purpose:  Monte Carlo simulation of ACES Baseline Scenario.
# Author:   Brennan Gray
# Creation Date:  January 24, 2022
#
# Description:
#
#   Monte Carlo simulation of ACES Baseline Scenario allowing for
#   dispersion of parameters.
#
#   More details here:
#   https://aces.atlassian.net/l/c/zsMjHqRt
#
#   Monte Carlo creation user manual available at:
#   https://aces.atlassian.net/l/c/oWqGwVwh

from functools import partial

import argparse
parser = argparse.ArgumentParser(description="Monte Carlo simulation of ACES Baseline Scenario.")
parser.add_argument('-o', '--output', type=str, nargs='?', const='scenarioBaselineMC',
                    default='scenarioBaselineMC',
                    help='Sets the output subfolder within the MonteCarloData folder.')
parser.add_argument('-r', '--runs', type=int, nargs='?', const=4, default=4,
                    help='Sets the number of simulation runs.')
parser.add_argument('-t', '--threads', type=int, nargs='?', const=4, default=4,
                    help='Sets the number of threads used to run the Monte Carlo simulations.')
parser.add_argument('-l', '--length', type=float, nargs='?', const=60, default=60,
                    help='Sets the time length of the simulations in minutes.')

import inspect
import os
import numpy as np

filename = inspect.getframeinfo(inspect.currentframe()).filename
fileNameString = os.path.basename(os.path.splitext(__file__)[0])
path = os.path.dirname(os.path.abspath(filename))

from Basilisk import __path__
bskPath = __path__[0]

# Import general simulation support files.
import sys
from Basilisk.utilities import macros
from Basilisk.utilities.MonteCarlo.Controller import Controller
from Basilisk.utilities.MonteCarlo.RetentionPolicy import RetentionPolicy
from Basilisk.utilities.MonteCarlo.Dispersions import (UniformEulerAngleMRPDispersion, UniformDispersion,
                                                       NormalVectorCartDispersion, InertiaTensorDispersion)

import scenarioBaseline
import monteCarloUtilities

analysisListName = "analysisList"


def run(show_plots):
    """
    This function executes the Monte Carlo simulation.

    Args:
        show_plots (bool): Determines if the script should display plots
    """

    #
    # Retrieve command-line arguments.
    args = parser.parse_args()

    #
    # Create the Monte Carlo simulation controller.
    monteCarlo = Controller()
    creationModule = partial(scenarioBaseline.scenarioBaseline, args.length)  # Apply length to scenario
    monteCarlo.setSimulationFunction(creationModule)  # Function that creates the scenario
    # Wrap the execution function to also execute pull_outputs for performance metric logging.
    executionModule = partial(monteCarloUtilities.runScenarioAndPullOutputs, scenarioBaseline.runScenario)
    monteCarlo.setExecutionFunction(executionModule)  # Function that runs the scenario
    monteCarlo.setExecutionCount(args.runs)  # Number of MCs to run

    #
    # Construct output path and ensure it exists.
    outputPath = path + "/MonteCarloData/" + args.output
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)

    monteCarlo.setArchiveDir(outputPath)  # Path for saved data
    monteCarlo.setShouldDisperseSeeds(True)  # Randomize the seed for each module
    # Notes on this parameter: this is NOT whether to use different random seeds for each run in the cohort. Each run in
    # the cohort receives a seed, set on lines 765-766 of Controller.py, which is its index multiplied by 10. This
    # seeding occurs independently of the value of this parameter. This parameter sets individual random seeds for each 
    # dynamics module if set to true, which may be used within those dynamics modules depending on their implementation.

    monteCarlo.setThreadCount(args.threads)  # Number of processes to spawn MCs on
    monteCarlo.setVerbose(True)  # Produce supplemental text output in console describing status
    monteCarlo.setVarCast('float')  # Downcast the retained numbers to float32 to save on storage space
    monteCarlo.setDispMagnitudeFile(True)  # Produce a .txt file that shows dispersion in std dev units

    #
    # Statistical dispersions can be applied to initial parameters using the Monte Carlo module
    # In the future better, more useful, and more accurate dispersions should be selected and applied.
    # Disperse initial conditions and dynamics parameters.
    dispMRPInit = 'TaskList[0].TaskModels[0].hub.sigma_BNInit'
    dispOmegaInit = 'TaskList[0].TaskModels[0].hub.omega_BN_BInit'
    dispMass = 'TaskList[0].TaskModels[0].hub.mHub'
    dispCoMOff = 'TaskList[0].TaskModels[0].hub.r_BcB_B'
    dispInertia = 'hubref.IHubPntBc_B'

    #
    # Add dispersions with their dispersion type.
    monteCarlo.addDispersion(UniformEulerAngleMRPDispersion(dispMRPInit))
    monteCarlo.addDispersion(NormalVectorCartDispersion(dispOmegaInit, 5.0 * np.pi / 180, 5.0 * np.pi / 180))
    monteCarlo.addDispersion(UniformDispersion(dispMass, ([8, 12])))
    monteCarlo.addDispersion(NormalVectorCartDispersion(dispCoMOff, [0.0, 0.0, 0.0], [0.05/0.3, 0.05/0.2, 0.05/0.1]))
    monteCarlo.addDispersion(InertiaTensorDispersion(dispInertia, stdDiag=0.005))

    #
    # A `RetentionPolicy` is used to define what data from the simulation should be retained. A `RetentionPolicy`
    # is a list of messages and variables to log from each simulation run. It also can have a callback,
    # used for plotting/processing the retained data.
    retentionPolicy = RetentionPolicy()
    retainAnalysis = partial(monteCarloUtilities.retainVariableOneShot, analysisListName, analysisListName)
    retentionPolicy.addRetentionFunction(retainAnalysis)
    monteCarlo.addRetentionPolicy(retentionPolicy)

    failures = monteCarlo.executeSimulations()

    return


if __name__ == "__main__":
    run(True)
