#
# ACES Monte Carlo Utilities Module
#
# Purpose:  Containing file for Monte Carlo utility modules.
# Author:   Brennan Gray
# Creation Date:  March 11, 2022
#
# Description:
#
#   Contains various modules to assist in the creation and
#   usage of Monte Carlo simulations. See specific functions
#   for documentation of their purpose and usage.


def retainVariableOneShot(storageName, varName, simInstance):
    """
    Retains the value of a variable(s) within a simulation after the simulation is completed.
    Can be used to retain analysis variables generated by the simulation after execution is concluded. To retain
    variables generated in pull_outputs (such as analysis variables), this function should be combined with
    runScenarioAndPullOutputs to ensure that pull_outputs is executed by the Monte Carlo after the simulation is
    concluded.
    
    Args:
        storageName: String or list of strings. The key(s) to store the data in the data dictionary.
        varName: String or list of strings. The name(s) of the variable to be retained.
        simInstance: The simulation instance to retain data from.
        
    Returns:
        A single-entry dictionary of format (storageName, varValue). This complies with the expected return format
        of the Basilisk RetentionPolicy file.
        
    Example usage:
        from functools import partial - Need to utilize partial functions.
        ...
        func = partial(monteCarloUtilities.retainVariableOneshot, "myData", "myVariable") - Create function handle.
        retentionPolicy.addRetentionFunction(func) - Add the function to the retention functions.
        
        This will result in the requested variable being stored within the "custom" top-level key of the retained
        data dictionary. Access as follows from the retention policy:
        
        myData = data["custom"]["myData"]
    """

    #
    # Input validation.
    if type(storageName) != type(varName):
        raise Exception("storageName and varName must be the same datatype.")

    if type(storageName) == list and len(storageName) != len(varName):
        raise Exception("storageName and varName must have the same length when providing lists.")

    if type(storageName) == str:  # Single variable, wrap with list to process below
        storageName = [storageName]
        varName = [varName]

    if type(storageName) == list:  # Valid input provided, process
        data = {}  # Create return data dictionary
        for key, value in zip(storageName, varName):  # Iterate over each provided key/value pair
            varData = getattr(simInstance, value)  # Retrieve data from the instance
            data[key] = varData  # Store key/data pair

        return data
    
    else:  # Unsupported datatype
        raise Exception("storageName and varName must be strings or lists.")


def runScenarioAndPullOutputs(executionFunction, simInstance):
    """
    Scenario execution wrapper that also executes pull_outputs, to generate analysis variables if needed.
    """

    executionFunction(simInstance)  # Execute the instance with the provided execution function
    simInstance.pull_outputs()  # Pull the outputs after the simulation is complete
