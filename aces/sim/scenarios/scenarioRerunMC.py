#
# ACES Baseline Scenario Monte Carlo Rerun Script
#
# Purpose:  Monte Carlo simulation of ACES Baseline Scenario.
# Author:   Brennan Gray
# Creation Date:  March 9, 2022
#
# Description:
#
#   Script to rerun specific simulations within a Monte
#   Carlo cohort for further analysis.

from functools import partial
import argparse
import glob
import importlib

parser = argparse.ArgumentParser(description="Monte Carlo Simulation Rerunner.")
parser.add_argument('scenario', type=str, help='Name of the scenario to be rerun.')
parser.add_argument('data', type=str,
                    help='Name of the folder within the MonteCarloData folder containing the run data.')
parser.add_argument('runs', type=str, help='Comma-separated string of run numbers to rerun.')
parser.add_argument('-o', '--output', type=str, nargs='?', const='rerun',
                    default='rerun',
                    help='Sets the output subfolder within the provided data folder.')
parser.add_argument('-t', '--threads', type=int, nargs='?', const=4, default=4,
                    help='Sets the number of threads used to run the Monte Carlo simulations.')
parser.add_argument('-l', '--length', type=float, nargs='?', const=60, default=60,
                    help='Sets the time length of the simulations in minutes.')

import inspect
import os
import sys
import numpy as np

filename = inspect.getframeinfo(inspect.currentframe()).filename
fileNameString = os.path.basename(os.path.splitext(__file__)[0])
path = os.path.dirname(os.path.abspath(filename))

from Basilisk import __path__
bskPath = __path__[0]

# Import general simulation support files.
from Basilisk.utilities import macros
from Basilisk.utilities.MonteCarlo.Controller import Controller
from Basilisk.utilities.MonteCarlo.RetentionPolicy import RetentionPolicy
from Basilisk.utilities.MonteCarlo.Dispersions import (UniformEulerAngleMRPDispersion, UniformDispersion,
                                                       NormalVectorCartDispersion, InertiaTensorDispersion)

import monteCarloUtilities

analysisListName = "analysisList"
rwMotorMsgName = "rwMotorMsg"
attRefMsgName = "attRefMsg"
attErrorMsgName = "attErrorMsg"
magMsgName = "magMsg"
tamMsgName = "tamMsg"
tamCommMsgName = "tamCommMsg"
dipoleRequestMsgName = "dipoleRequestMsg"
atmosphereMsgName = "atmosphereMsg"
scStateMsgName = "scStateMsg"
rwStateMsgName = "rwStateMsg"
ggMsgName = "ggMsg"
rwModuleMsgNames = ["rw1Msg", "rw2Msg", "rw3Msg"]
srpMsgName = "srpMsg"
modeControllerMsgName = "modeControllerMsg"

def run(show_plots):
    """
    This function executes the Monte Carlo simulation.

    Args:
        show_plots (bool): Determines if the script should display plots
    """

    #
    # Retrieve command-line arguments.
    args = parser.parse_args()

    #
    # Import the scenario to rerun.
    try:
        scenario = importlib.import_module(args.scenario)
    except:
        print("Unable to import scenario named " + args.scenario)
        return

    #
    # Parse run list
    runStrs = args.runs.split(',')
    runs = [int(num) for num in runStrs]

    #
    # Create the Monte Carlo simulation controller.
    monteCarlo = Controller()
    simulationModule = getattr(scenario, args.scenario)
    simPartial = partial(simulationModule, args.length)
    monteCarlo.setSimulationFunction(simPartial)  # Function that creates the scenario
    # Wrap the execution function to also execute pull_outputs for performance metric logging.
    executionModule = partial(monteCarloUtilities.runScenarioAndPullOutputs, getattr(scenario, 'runScenario'))
    monteCarlo.setExecutionFunction(executionModule)  # Function that runs the scenario
    monteCarlo.setExecutionCount(len(runs))  # Number of MCs to run

    #
    # Set the IC data, which is the data from previous runs we're rerunning.
    icDir = path + "/MonteCarloData/" + args.data
    monteCarlo.setICDir(icDir)
    monteCarlo.setICRunFlag(True)

    #
    # Construct output path and ensure it exists.
    outputPath = path + "/MonteCarloData/" + args.data + "/" + args.output
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)

    monteCarlo.setThreadCount(args.threads)  # Number of processes to spawn MCs on
    monteCarlo.setArchiveDir(outputPath)  # Path for saved data
    monteCarlo.setShouldDisperseSeeds(False)  # Randomize the seed for each module
    monteCarlo.setVerbose(True)  # Produce supplemental text output in console describing status

    #
    # A `RetentionPolicy` is used to define what data from the simulation should be retained. A `RetentionPolicy`
    # is a list of messages and variables to log from each simulation run. It also can have a callback,
    # used for plotting/processing the retained data.

    # Note that when using retention policies for reruns, the data is saved over the existing data, i.e. it replaces the
    # existing run#.data file within the base data directory, which means that previous data is lost. For this reason,
    # we must rerun the analyses (by calling runScenarioAndPullOutputs above) and save them again.
    # Additionally, we save all data needed to generate the plots contained within postProcess.py/PlotAllData.
    retentionPolicy = RetentionPolicy()
    retainAnalysis = partial(monteCarloUtilities.retainVariableOneShot, analysisListName, analysisListName)
    retentionPolicy.addRetentionFunction(retainAnalysis)
    retentionPolicy.addMessageLog(rwMotorMsgName, ["motorTorque"])
    retentionPolicy.addMessageLog(attRefMsgName, ["sigma_RN"])
    retentionPolicy.addMessageLog(attErrorMsgName, ["omega_BR_B"])
    retentionPolicy.addMessageLog(rwStateMsgName, ["wheelSpeeds"])
    for name in rwModuleMsgNames:
        retentionPolicy.addMessageLog(name, ["u_current"])
    retentionPolicy.addMessageLog(magMsgName, ["magField_N"])
    retentionPolicy.addMessageLog(tamMsgName, ["tam_S"])
    retentionPolicy.addMessageLog(tamCommMsgName, ["tam_B"])
    retentionPolicy.addMessageLog(dipoleRequestMsgName, ["mtbDipoleCmds"])
    retentionPolicy.addMessageLog(ggMsgName, ["gravityGradientTorque_B"])
    retentionPolicy.addMessageLog(atmosphereMsgName, ["neutralDensity"])
    retentionPolicy.addMessageLog(scStateMsgName, ["sigma_BN", "r_BN_N", "v_BN_N"])
    retentionPolicy.addMessageLog(srpMsgName, ["solarRadiationPressureForce_B", "solarRadiationPressureTorque_B"])
    retentionPolicy.addMessageLog(modeControllerMsgName, ["modeId", "isSettled"])
    monteCarlo.addRetentionPolicy(retentionPolicy)

    failures = monteCarlo.runInitialConditions(runs)  # Execute runs with the initial conditions of the selected runs

    return


if __name__ == "__main__":
    run(True)
