#
# ACES Inline Analysis Module
#
# Purpose:  Containing file for inline analysis functions.
# Author:   Brennan Gray
# Creation Date:  March 11, 2022
#
# Description:
#
#   Contains various functions to perform inline (i.e.
#   directly post-simulation execution, within
#   pull_outputs) analysis of data produced by ACES
#   scenarios. See individual functions for descriptions
#   of their purpose and usage.
#
#   Inline analysis creation manual available at:
#   https://aces.atlassian.net/l/c/oWqGwVwh

import math
import numpy as np
from Basilisk.utilities import (SimulationBaseClass,
                                fswSetupRW,
                                macros,
                                orbitalMotion,
                                simIncludeGravBody,
                                RigidBodyKinematics,
                                simIncludeRW,
                                unitTestSupport,
                                vizSupport,
                                simSetPlanetEnvironment)

# Requirement definitions

# L3-ATD02 - Pointing knowledge requirement.
PointingKnowledgeLimit2Axis = 10.8  # Two-axis pointing accuracy standard deviation requirement, arcseconds
PointingKnowledgeLimit1Axis = 10.8  # Third-axis pointing accuracy standard deviation requirement, arcseconds

# L3-ATC01 - Pointing error requirement.
PointingErrorLimit2Axis = 10.8  # Two-axis pointing accuracy standard deviation requirement, arcseconds
PointingErrorLimit1Axis = 25.2  # Third-axis pointing accuracy standard deviation requirement, arcseconds

# L3-ATC02 - Pointing jitter requirement.
PointingJitterLimit = 18  # Pointing jitter requirement, 18 arcseconds over 600 seconds
PointingJitterTimeWindow = 600 / 60  # Jitter time limit, minutes

# L3-ATC05 - Detumbling requirement.
DetumbleInitialAngularMomentum = 0.018914602  # Maximum angular momentum that should be detumbled from, kg*m^2/s
DetumbleTimeLimit = 10000  # Maximum time for detumbling to be accomplished, seconds

def checkPointingDeterminationCompliance(simTimes, dataSigmaEstError, dataModeId, endTime=np.Inf):
    """
    Compliance with L3-ATD02.
    Computes the pointing errors at each time in the simulation and compares their standard deviation to the set
    pointing determination requirement. Only data from modes > 1 (not off, not detumble) is considered.

    Args:
        simTimes: The time data of the simulation, minutes.
        dataSigmaEstError: The estimation error MRP, which can be obtained by subtracting the true sigmaBN from the
                           estimated sigmaBN produced by the EKF.
        dataModId: Mode ID message log of the spacecraft.
        endTime: Optional time at which to stop considering data (only data up to this time is considered), minutes.

    Returns:
        compliant: Boolean list of whether the pointing knowledge requirement was met for cross and around boresight.
        axis_std: The standard deviations (1-sigma) of the cross and around boresight pointing knowledge within the time
                  period, radians.
    """

    # Note to whoever ends up working on this in the future: this is probably not the correct way of determining
    # pointing knowledge for L3-ATD01. However, the requirements as currently written don't really make much sense to me
    # so this is what I implemented. In the future, this function should be reassessed/rewritten to compute whatever
    # pointing knowledge really means.


    dataErrorAngles = np.zeros((len(dataSigmaEstError), 2))
    for i in range(len(dataSigmaEstError)):
        # The cross and around boresight error angle computation converts three angular error values into two. To do
        # this, the cross boresight errors are combined by considering the full angle between the reference first axis
        # [1, 0, 0], and the body first axis. To compute the around boresight error, we project the reference second
        # and third axes (r2, r3) into the plane defined by the body second and third axes (b2, b3). We then compute the
        # angles between the projected reference axes and their equivalent body frame axes. Note that in general these
        # angles are not equivalent, and either could be considered the around boresight error, so we average them to
        # arrive at what we consider the around boresight error. Also note that since the attitude errors are coupled,
        # cross boresight error adds a project into the around boresight error. Depending on the magnitudes of the
        # errors involved, this projection can be significant, but it is not for small errors, such as those allowed by
        # our requirements, so we ignore that additional projection error.

        # If this is confusing, try plotting it out, and it'll (hopefully) make more sense.
        BR = RigidBodyKinematics.MRP2C(dataSigmaEstError[i][:])  # Convert to DCM, to pick off basis vectors later

        # Compute full angle between reference and body first axis. Note selecting DCM components directly is equivalent
        # to a dot product with the [1, 0, 0] vector.
        theta_cb = math.acos(BR[0, 0])

        # Compute around boresight error. Note that projection into the b2-b3 plane is accomplished by
        # not including the x components of the reference axes in the norm computation.
        theta_ab_y = math.acos(BR[1, 1]/np.linalg.norm(BR[1:, 1]))
        theta_ab_z = math.acos(BR[2, 2]/np.linalg.norm(BR[1:, 2]))
        theta_ab = (theta_ab_y + theta_ab_z)/2

        # Build error array and store in errors array.
        thetas = np.array([theta_cb, theta_ab])
        dataErrorAngles[i, :] = thetas

        # If we've passed the end time, stop considering data.
        if simTimes[i] > endTime:
            break

    # Compute standard deviation along each axis.
    axis_std = np.std(dataErrorAngles[dataModeId > 1, :], axis=0)

    # Convert requirement values to radians.
    Lim2 = PointingKnowledgeLimit2Axis * macros.ARCSEC2RAD  # Pointing knowledge limit for 2 axes (cross boresight)
    Lim1 = PointingKnowledgeLimit1Axis * macros.ARCSEC2RAD  # Pointing knowledge limit for 1 axis (around boresight)

    # Check compliance with requirements.
    compliant = [axis_std[0] <= Lim2, axis_std[1] <= Lim1]

    # Return the computed quantities.
    return compliant, axis_std


def checkPointingErrorCompliance(simTimes, dataSigmaBR, dataModeId, dataModeSettled, endTime=np.Inf):
    """
    Compliance with L3-ATC01.
    Computes the pointing errors at each time in the simulation and compares their standard deviation to the set
    pointing error requirement. Outputs data for each mode, and only considers data when that mode was considered
    settled if settling occurred. If settling did not occur, then data for the entire mode time is considered - this
    makes modes which did not settle appear much worse than they should! Ensure settling occurs for the best data to be
    produced.

    Args:
        simTimes: The time data of the simulation, minutes.
        dataSigmaBR: The true MRP between the body and reference frame data for the entire time span. Note
                     that the two frames should be constructed such that the first axis is the view direction of the
                     star tracker. The other two axes are arbitrary so long as they construct a proper frame.
        dataModeId: Mode ID message log of the spacecraft.
        dataModeSettled: Settled flag message log of the spacecraft.
        endTime: Optional time at which to stop considering data (only data up to this time is considered), minutes.

    Returns:
        modes: List of the mode IDs for each entry on the compliant and axis_std lists.
        compliant: Boolean list of whether the pointing error requirement was met for cross and around boresight
                   (first dimension), for each mode which was ever considered settled (second dimension).
        axis_std: The standard deviations (1-sigma) of the cross and around boresight pointing error within the time
                  period (first dimension), for each mode which was ever considered settled (second dimension), radians.
    """

    dataErrorAngles = np.zeros((len(dataSigmaBR), 2))

    # Count number of mode transitions and allocate mode data arrays
    numTransitions = np.count_nonzero(dataModeId[:-1] != dataModeId[1:]) + 1
    modes = np.zeros((numTransitions, 1))
    modeIndices = np.zeros((numTransitions, 2))
    settledIndices = -np.ones((numTransitions, 1))

    # Store initial mode
    modes[0] = dataModeId[0]
    modeIndices[0, 0] = 0
    currentModeIndex = 0

    for i in range(len(dataSigmaBR)):
        # The cross and around boresight error angle computation converts three angular error values into two. To do
        # this, the cross boresight errors are combined by considering the full angle between the reference first axis
        # [1, 0, 0], and the body first axis. To compute the around boresight error, we project the reference second
        # and third axes (r2, r3) into the plane defined by the body second and third axes (b2, b3). We then compute the
        # angles between the projected reference axes and their equivalent body frame axes. Note that in general these
        # angles are not equivalent, and either could be considered the around boresight error, so we average them to
        # arrive at what we consider the around boresight error. Also note that since the attitude errors are coupled,
        # cross boresight error adds a project into the around boresight error. Depending on the magnitudes of the
        # errors involved, this projection can be significant, but it is not for small errors, such as those allowed by
        # our requirements, so we ignore that additional projection error.

        # If this is confusing, try plotting it out, and it'll (hopefully) make more sense.
        BR = RigidBodyKinematics.MRP2C(dataSigmaBR[i][:])  # Convert to DCM, to pick off basis vectors later

        # Compute full angle between reference and body first axis. Note selecting DCM components directly is equivalent
        # to a dot product with the [1, 0, 0] vector.
        theta_cb = math.acos(BR[0, 0])

        # Compute around boresight error. Note that projection into the b2-b3 plane is accomplished by
        # not including the x components of the reference axes in the norm computation.
        theta_ab_y = math.acos(BR[1, 1]/np.linalg.norm(BR[1:, 1]))
        theta_ab_z = math.acos(BR[2, 2]/np.linalg.norm(BR[1:, 2]))
        theta_ab = (theta_ab_y + theta_ab_z)/2

        # Build error array and store in errors array.
        thetas = np.array([theta_cb, theta_ab])
        dataErrorAngles[i, :] = thetas

        # Check if we've transitioned into a different mode
        if modes[currentModeIndex] != dataModeId[i]:
            modeIndices[currentModeIndex, 1] = i - 1

            currentModeIndex += 1
            modes[currentModeIndex] = dataModeId[i]
            modeIndices[currentModeIndex, 0] = i

        # Check if we've become settled and still need to store settling for the current mode
        if settledIndices[currentModeIndex] < 0 and dataModeSettled[i] == 1:
            settledIndices[currentModeIndex] = i

        # If we've passed the end time, stop considering data.
        if simTimes[i] > endTime:
            break

    # Store the final time as the end time of the final mode
    modeIndices[currentModeIndex, 1] = len(dataSigmaBR)

    # Convert requirement values to radians.
    Lim2 = PointingErrorLimit2Axis * macros.ARCSEC2RAD  # Pointing error limit for 2 axes (cross boresight)
    Lim1 = PointingErrorLimit1Axis * macros.ARCSEC2RAD  # Pointing error limit for 1 axis (around boresight)

    # Preallocate compliant and axis_std arrays
    compliant = np.zeros((2, numTransitions))
    axis_std = np.zeros((2, numTransitions))

    for i in range(numTransitions):

        # Get beginning time
        if settledIndices[i] < 0:  # Use start index
            dataIndex = modeIndices[i, 0]
        else:
            dataIndex = settledIndices[i]

        # Compute standard deviation along each axis.
        axis_std[:, i] = np.std(dataErrorAngles[int(dataIndex):int(modeIndices[i, 1]), :], axis=0)

        # Check compliance with requirements.
        compliant[:, i] = [axis_std[0, i] <= Lim2, axis_std[1, i] <= Lim1]

    # Return the computed quantities.
    return modes, compliant, axis_std


def checkPointingJitterCompliance(simTimes, dataSigmaBR, dataModeId, dataModeSettled, endTime=np.Inf):
    """
    Compliance with L3-ATC02.
    Computes the jitter utilizing a moving window and compares it with the set pointing jitter requirement. Only data
    after the provided settle time is considered.

    Args:
        simTimes: The time data of the simulation, minutes.
        dataSigmaBR: The true MRP between the body and reference frame data for the entire time span.
        dataModeId: Mode ID message log of the spacecraft.
        dataModeSettled: Settled flag message log of the spacecraft.
        endTime: Optional time at which to stop considering data (only data up to this time is considered), minutes.

    Returns:
        modes: List of the mode IDs for each entry of the other returned lists.
        compliant: Boolean list of whether the jitter requirement was met for each mode.
        jitter_max: List of maximum values of jitter for each mode, radians.
        time_max: List of times where the maximum values of jitter occurred in each mode, seconds.
        jitter: List of lists of jitter values computed for each time window for each mode, radians.
        jitterTime: The list of end times of the windows used to compute the jitter values, minutes.
    """

    # Compute Euler angle at each time step.
    dataEulerAngle = np.zeros(len(dataSigmaBR))
    for i in range(len(dataSigmaBR)):
        # Note that converting an MRP to a quaternion (EP) gives back a vector of 4
        epBR = RigidBodyKinematics.MRP2EP(dataSigmaBR[i][:])
        EulerAngle = 2 * math.acos(epBR[0])  # Compute the Euler angle. MRPs are unitless and acos returns radians
        # The above equation comes from beta_0 = cos(phi/2), we are solving for phi, phi = 2*acos(beta_0)
        dataEulerAngle[i] = EulerAngle  # Store Euler angle in radians

    # Compute timing variables and sample time.
    simDur = simTimes[-1]  # Simulation duration in minutes
    timePerDataPointMin = simDur/len(dataEulerAngle)  # Simulation duration time divided by number of data points
                                                      # (minutes/datapoint)
    samplingRate = math.ceil((1/60)/timePerDataPointMin)  # Compute how many data points from dataEulerAngle exist per
                                                          # second (seconds/datapoint) also known as the sampling rate
    dataElementsPerTimeWindow = math.ceil(PointingJitterTimeWindow/timePerDataPointMin)  # Amount of data points per
                                                                                         # pointing jitter time window

    # Count number of mode transitions and allocate mode data arrays
    numTransitions = np.count_nonzero(dataModeId[:-1] != dataModeId[1:]) + 1
    modes = np.zeros((numTransitions, 1))
    modeIndices = np.zeros((numTransitions, 2))
    settledIndices = -np.ones((numTransitions, 1))

    # Store initial mode
    modes[0] = dataModeId[0]
    modeIndices[0, 0] = 0
    currentModeIndex = 0

    # Find mode transitions and timings
    for i in range(len(simTimes)):
        # Check if we've transitioned into a different mode
        if modes[currentModeIndex] != dataModeId[i]:
            modeIndices[currentModeIndex, 1] = i - 1

            currentModeIndex += 1
            modes[currentModeIndex] = dataModeId[i]
            modeIndices[currentModeIndex, 0] = i

        # Check if we've become settled and still need to store settling for the current mode
        if settledIndices[currentModeIndex] < 0 and dataModeSettled[i] == 1:
            settledIndices[currentModeIndex] = i

        # If we've passed the end time, stop considering data.
        if simTimes[i] > endTime:
            break

    # Store the final time as the end time of the final mode
    modeIndices[currentModeIndex, 1] = len(dataSigmaBR)

    # Preallocate outputs
    compliant = []
    jitter_max = []
    time_max = []
    jitter = [[] for _ in range(len(modes))]
    jitterTime = [[] for _ in range(len(modes))]

    # Compute jitter limit
    limJitter = PointingJitterLimit * macros.ARCSEC2RAD  # Convert jitter limit to radians

    # Loop through modes and compute jitter
    for i in range(len(modes)):
        # Get beginning time
        if settledIndices[i] < 0:  # Use start index
            dataIndex = modeIndices[i, 0]
        else:
            dataIndex = settledIndices[i]

        # Length of the mode minus the elements of ONE time window
        simUC = modeIndices[i, 1] - dataElementsPerTimeWindow

        # Skip this mode if we don't have enough data
        # Note to future team members: This solution to the integration window issue might not be the best. If enough
        # data isn't present for the full, 600 second integration window, an alternative might be to fall back to
        # computing jitter for the entire mode, however long it was, or perhaps just for the time it was settled. The
        # 600 second window is kind of long anyway.
        if simUC < dataIndex:
            print("Warning: Not enough data for mode ", str(modes[i]), " jitter analysis. Skipping.")
            compliant.append(0)
            jitter_max.append(-1)
            time_max.append(-1)
            continue

        # Preallocate space for Jitter and JitterTime
        jitterMode = []
        jitterModeTime = []

        # At every second compute jitter
        for j in range(int(dataIndex), int(simUC), samplingRate):

            # If we've passed the end time, stop considering data.
            if simTimes[j] > endTime:
                break

            T_minIdx = j  # Scalar index of time
            T_maxIdx = T_minIdx + dataElementsPerTimeWindow  # Add Timewindow
            DataWindow = dataEulerAngle[T_minIdx:T_maxIdx]

            PointingAccuracy = math.sqrt((1 / len(DataWindow)) * sum(DataWindow ** 2))  # Equation defined in requirements
            Displacement = np.mean(DataWindow)
            jitterMode.append(math.sqrt(PointingAccuracy ** 2 - Displacement ** 2))
            jitterModeTime.append(simTimes[T_maxIdx])

        # Check compliance and compute output values.
        jitterModeMax = np.amax(jitterMode)  # Find maximum jitter
        index = jitterMode.index(jitterModeMax)  # Find the index of the max
        jitter_max.append(jitterModeMax)  # Store the max value
        time_max.append(jitterModeTime[index])  # Time of the maximum jitter
        compliant.append(jitterModeMax <= limJitter)  # Check compliance
        jitter[i] = jitterMode
        jitterTime[i] = jitterModeTime

    # Return computed values.
    return modes, compliant, jitter_max, time_max, jitter, jitterTime


def checkDetumbleCompliance(simTimes, dataOmegaBN_B, dataModeId, I_sc):
    """
    Compliance with L3-ATC05.
    Checks whether the detumbling requirement is met, and whether the initial spacecraft angular momentum was above or
    below the specified requirement maximum. Note that for some simulations the requirement value will not be met, but
    this does not constitute noncompliance because the initial spacecraft angular momentum could be more than the
    specified requirement max.

    Also note that this analysis excludes reaction wheel momentum intentionally. Any momentum contained within the
    reaction wheels at the beginning of detumbling is not part of the angular momentum considered by the requirement.
    The spacecraft will have been powered off entirely shortly before detumbling begins per all CubeSat deployer ICDs,
    so the reaction wheels cannot hold any momentum during this time anyway.

    Args:
        simTimes: The time data of the simulation, minutes.
        dataOmegaBN_B: The true angular rates of the spacecraft relative to the inertial frame, expressed in the body
                       frame, rad/s.
        dataModeId: Mode ID message log of the spacecraft.
        I_sc: The inertia tensor of the spacecraft, kg*m^2.

    Returns:
        compliant: Boolean of whether the detumbling requirement was met.
        H_sc_init_exceeded: Boolean of whether the initial spacecraft angular momentum magnitude exceeded the specified
                            maximum.
        time_detumble: The amount of time the spacecraft spent in detumble mode
        H_sc_init: The initial spacecraft angular momentum vector, kg*m^2/s.
    """

    simTimesSeconds = simTimes * 60  # Convert sim times to seconds

    I_sc = np.reshape(I_sc, [3, 3])  # Reshape inertia into 3x3 array for matrix computations

    H_sc_init = I_sc@np.reshape(dataOmegaBN_B[0][:], [3, 1])  # Compute the initial spacecraft angular momentum
    H_sc_mag = np.linalg.norm(H_sc_init)  # Compute initial spacecraft angular momentum magnitude
    H_sc_init_exceeded = H_sc_mag > DetumbleInitialAngularMomentum  # Check whether this spacecraft angular momentum
                                                                    # exceeds the limit

    # Find detumble time from mode message
    detumbleTimes = simTimesSeconds[dataModeId == 1]
    time_detumble = detumbleTimes[-1] - detumbleTimes[0]

    # Check compliance
    compliant = time_detumble <= DetumbleTimeLimit

    # If we weren't compliant, check whether the data ran for the full detumble time period.
    if not compliant and simTimesSeconds[-1] < DetumbleTimeLimit:
        print("Warning: Detumble analysis determined noncompliance, but was not provided data for the entire "
              "requirement detumble time. Rerun the analysis with a longer time period to accurately determine "
              "compliance.")

    # Return the computed quantities.
    return compliant, H_sc_init_exceeded, time_detumble, H_sc_init
