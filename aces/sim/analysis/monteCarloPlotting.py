#
# ACES Monte Carlo Plotting Module
#
# Purpose:  Containing file for Monte Carlo plotting modules.
# Author:   Brennan Gray
# Creation Date:  March 11, 2022
#
# Description:
#
#   Contains various modules for plotting Monte Carlo data.
#   See specific functions for documentation of their usage.

import matplotlib.pyplot as plt
import scenarioBaselinePlotting
from Basilisk.utilities import macros
import numpy as np

from inlineAnalysis import *

SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
plt.rcParams['figure.figsize'] = [16.0, 9.0]
plt.rcParams['lines.linewidth'] = 2

modeNames = ['0/Off', '1/Detumble', '2/Safe', '3/Sun-pointing', '4/Communications', '5/Science']

def plotCrossAroundBoresightErrorStdScatter(cross, around, figNum, plotLimits=False):
    """
    Plots a scatter plot of the cross and around boresight errors for all runs.
    """

    plt.figure(figNum)

    maxCross = 0
    maxAround = 0
    for i in range(len(cross)):  # Iterate over modes
        if len(cross[i]) <= 0:  # Skip empty modes
            continue

        # Store max cross and around if this mode is higher than current
        if max(cross[i]) > maxCross:
            maxCross = max(cross[i])
        if max(around[i]) > maxAround:
            maxAround = max(around[i])

        color = scenarioBaselinePlotting.equispacedColorInHue(i - 2, 4)
        # Plot around on x-axis since it's larger (better scale at 16:9)
        plt.scatter(around[i], cross[i], color=color, label=modeNames[i])

    plt.xlabel('Around Boresight Control Error std [arcsec]')
    plt.ylabel('Cross Boresight Control Error std [arcsec]')
    plt.xlim([0, max(maxAround*1.1, PointingErrorLimit1Axis * 1.5)])
    plt.ylim([0, max(maxCross*1.1, PointingErrorLimit2Axis * 1.5)])
    plt.grid(True)

    if plotLimits:
        # Plot time limit
        plt.plot([PointingErrorLimit1Axis, PointingErrorLimit1Axis], [0, PointingErrorLimit2Axis], color="red",
                 label='Requirement Bounds')

        # Plot angular momentum limit
        plt.plot([0, PointingErrorLimit1Axis], [PointingErrorLimit2Axis, PointingErrorLimit2Axis], color="red")

    plt.legend(loc='lower right')


def plotCrossAroundBoresightKnowledgeStdScatter(cross, around, figNum, plotLimits=False):
    """
    Plots a scatter plot of the cross and around boresight knowledge errors for all runs.
    """

    plt.figure(figNum)
    # Plot around on x-axis since it's larger (better scale at 16:9)
    plt.scatter(around, cross, color='black', label='Cases')
    plt.xlabel('Around Boresight Knowledge Error std [arcsec]')
    plt.ylabel('Cross Boresight Knowledge Error std [arcsec]')
    plt.xlim([0, max(max(around)*1.1, PointingKnowledgeLimit1Axis * 1.5)])
    plt.ylim([0, max(max(cross)*1.1, PointingKnowledgeLimit2Axis * 1.5)])
    plt.grid(True)

    if plotLimits:
        # Plot time limit
        plt.plot([PointingKnowledgeLimit1Axis, PointingKnowledgeLimit1Axis], [0, PointingKnowledgeLimit2Axis],
                 color="red", label='Requirement Bounds')

        # Plot angular momentum limit
        plt.plot([0, PointingKnowledgeLimit1Axis], [PointingKnowledgeLimit2Axis, PointingKnowledgeLimit2Axis],
                 color="red")

    plt.legend(loc='lower right')

def plotJitterHist(jitterValues, figNum):
    """
    Plots a histogram of the given jitter values.
    """

    plt.figure(figNum)
    bins = np.arange(0, 500, 10)

    numModes = 0
    for i in range(len(jitterValues)):  # Count number of non-empty modes
        if len(jitterValues[i]) > 0:
            numModes += 1

    names = []
    colors = []
    trimmed = [[] for _ in range(numModes)]
    weights = [[] for _ in range(numModes)]
    curMode = 0
    for i in range(len(jitterValues)):  # Iterate over modes
        if len(jitterValues[i]) <= 0:  # Skip empty modes
            continue

        color = scenarioBaselinePlotting.equispacedColorInHue(i - 2, 4)
        colors.append(color)
        names.append(modeNames[i])
        trimmed[curMode] = jitterValues[i]
        weights[curMode] = [1/len(trimmed[curMode])] * len(trimmed[curMode])
        curMode += 1

    plt.hist(trimmed, bins, weights=weights, histtype='bar', stacked=True, color=colors, label=names)
    plt.xlabel("Jitter [arcsec]")
    plt.ylabel("Normalized Number of Occurrences")
    plt.grid(True)
    plt.legend(loc='upper right')


def plotDetumbleScatter(time, angularMomentumMag, figNum, plotLimits=False):
    """
    Plots a scatter plot of initial angular momentum magnitude vs. detumbling time for all runs.
    """

    plt.figure(figNum)
    plt.scatter(time, angularMomentumMag, color="black", label='Cases')
    plt.xlabel('Detumbling Time [s]')
    plt.ylabel('Initial Angular Momentum Magnitude [kg*m^2/s]')
    plt.ylim([0, DetumbleInitialAngularMomentum*1.5])
    plt.xlim([0, DetumbleTimeLimit*1.5])
    plt.grid(True)

    if plotLimits:
        # Plot time limit
        plt.plot([DetumbleTimeLimit, DetumbleTimeLimit], [0, DetumbleInitialAngularMomentum * 1.5], color="red",
                 label='Requirement Time Limit')

        # Plot angular momentum limit
        plt.plot([0, DetumbleTimeLimit * 1.5], [DetumbleInitialAngularMomentum, DetumbleInitialAngularMomentum],
                 color="gray",
                 linestyle='--',
                 label='Requirement Max Momentum')

    plt.legend(loc='lower right')
