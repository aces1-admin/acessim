#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 21:33:23 2021

@author: henrymacanas
"""
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

from Basilisk.utilities import (unitTestSupport, macros, orbitalMotion, RigidBodyKinematics)

SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
plt.rcParams['figure.figsize'] = [16.0, 9.0]
plt.rcParams['lines.linewidth'] = 2


#
# Utility functions
#
def equispacedColorInHue(number, total):
    """
    Chooses equispaced colors in hue-space for plotting multiple simulation runs on the same plot.
    """

    h = number/total  # Generate an equispaced hue value

    # Construct HSV array - saturation and value are arbitrary to be "not too bright".
    hsv = [h, 0.7, 0.7]

    # Convert to RGB then add alpha channel of 1 (solid).
    rgb = colors.hsv_to_rgb(hsv)
    rgba = np.append(rgb, 1)

    return rgba

def createFigure(figNum):
    """
    Processes provided figure numbers to create new figures or write to existing ones.
    """

    if figNum == -1:  # Figure number is not specified, so use the next available number.
        plt.figure()
    else:  # Figure number is specified, so use that number.
        plt.figure(figNum)

def getColorLabel(idx, numColors, labelString, runNum, total):
    """
    Determines which color and label to use depending on the provided arguments.
    """

    if runNum == -1 or total == -1:  # This is not part of a Monte Carlo cohort, so use default colors and labels.
        color = unitTestSupport.getLineColor(idx, numColors)
        label = labelString
    else:  # This is part of a Monte Carlo cohort, so use equispaced hue per run and run number labels.
        color = equispacedColorInHue(runNum, total)
        label = "Run " + str(runNum) if idx == 0 else ""

    return color,label

#
# Plotting functions
#
def plotAttitudeError(timeData, dataSigmaBR, figNum=-1, runNum=-1, total=-1):
    """Plot the attitude errors."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\sigma_' + str(idx) + '$', runNum, total)

        plt.plot(timeData, dataSigmaBR[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'Attitude Error $\sigma_{B/R}$')
    plt.grid(True)

def plotMrps(timeData, dataSigmaBN, yLabStr, figNum=-1, runNum=-1, total=-1):
    """Plot the MRPs."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\sigma_' + str(idx) + '$', runNum, total)

        plt.plot(timeData, dataSigmaBN[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(yLabStr)
    plt.grid(True)

def plotRwMotorTorque(timeData, dataUsReq, dataRW, numRW, figNum=-1, runNum=-1, total=-1):
    """Plot the RW actual motor torques."""
    createFigure(figNum)

    for idx in range(numRW):
        color,label1 = getColorLabel(idx, numRW, r'$\hat u_{s,' + str(idx) + '}$', runNum, total)
        color,label2 = getColorLabel(idx, numRW, '$u_{s,' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataUsReq[:, idx],
                 '--',
                 color=color,
                 label=label1)
        plt.plot(timeData, dataRW[idx],
                 color=color,
                 label=label2)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('RW Motor Torque [Nm]')
    plt.grid(True)

def plotRateError(timeData, dataOmegaBR, figNum=-1, runNum=-1, total=-1):
    """Plot the body angular velocity rate tracking errors."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\omega_{BR,' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataOmegaBR[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Rate Tracking Error [rad/s] ')
    plt.grid(True)

def plotRwSpeeds(timeData, dataOmegaRW, numRW, figNum=-1, runNum=-1, total=-1):
    """Plot the RW spin rates."""
    createFigure(figNum)

    for idx in range(numRW):
        color,label = getColorLabel(idx, 4, r'$\Omega_{' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataOmegaRW[:, idx] / macros.RPM,
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('RW Speed [RPM] ')
    plt.grid(True)

def plotMagneticField(timeData, dataMagField, figNum=-1, runNum=-1, total=-1):
    """Plot the magnetic field."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$B\_N_{' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataMagField[:, idx] * 1e9,
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Magnetic Field [nT]')
    plt.grid(True)

def plotDataTam(timeData, dataTam, figNum=-1, runNum=-1, total=-1):
    """Plot the magnetic field."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$TAM\_S_{' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataTam[:, idx] * 1e9,
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Magnetic Field [nT]')
    plt.grid(True)

def plotDataTamComm(timeData, dataTamComm, figNum=-1, runNum=-1, total=-1):
    """Plot the magnetic field."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$TAM\_B_{' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataTamComm[:, idx] * 1e9,
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Magnetic Field [nT]')
    plt.grid(True)

def plotDataMtbMomentumManagement(timeData, dataMtbMomentumManegement, numMTB, figNum=-1, runNum=-1, total=-1):
    """Plot the magnetic field."""
    createFigure(figNum)

    for idx in range(numMTB):
        color,label = getColorLabel(idx, 3, r'$MTB\_T_{' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataMtbMomentumManegement[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Torque Rod Dipoles [A-m2]')
    plt.grid(True)

def plotDataGgTorque(timeData, ggData, figNum=-1, runNum=-1, total=-1):
    """Plot the gravity gradient torques."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\tau_' + str(idx) + '$', runNum, total)

        plt.plot(timeData, ggData[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'GG Torque [Nm]')
    plt.grid(True)

def plotDataAtmosphericDensity(timeData, dataAtmosphere, figNum=-1, runNum=-1, total=-1):
    """Plot the atmospheric density."""
    createFigure(figNum)

    color,label = getColorLabel(0, 3, "", runNum, total)

    plt.plot(timeData, dataAtmosphere,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'Density [kg-m3]')
    plt.grid(True)

def plotDataOscOe(timeData, r, v, mu, figNums=-1 * np.ones(6, dtype=int), runNum=-1, total=-1):
    """Plot the osculating orbital elements."""
    a = []
    e = []
    inc = []
    AP = []
    AN = []
    f = []

    for idx in range(1, len(r)):
        osculatingElements = orbitalMotion.rv2elem(mu, r[idx], v[idx])
        a.append(osculatingElements.a)
        e.append(osculatingElements.e)
        inc.append(osculatingElements.i * macros.R2D)
        AP.append(osculatingElements.omega * macros.R2D)
        AN.append(osculatingElements.Omega * macros.R2D)
        if osculatingElements.f > np.pi:
            f.append(osculatingElements.f * macros.R2D - 360.0)
        else:
            f.append(osculatingElements.f * macros.R2D)

    createFigure(figNums[0])
    color, label = getColorLabel(0, 3, "", runNum, total)
    plt.plot(timeData[1:], a,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'SMA [m]')
    plt.grid(True)

    createFigure(figNums[1])
    plt.plot(timeData[1:], e,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'Eccentricity [n/a]')
    plt.grid(True)

    createFigure(figNums[2])
    plt.plot(timeData[1:], inc,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'Inclination [deg]')
    plt.grid(True)

    createFigure(figNums[3])
    plt.plot(timeData[1:], AP,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'Argument of Perigee [deg]')
    plt.grid(True)

    createFigure(figNums[4])
    plt.plot(timeData[1:], AN,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'Ascending Node [deg]')
    plt.grid(True)

    createFigure(figNums[5])
    plt.plot(timeData[1:], f,
             color=color,
             label=label)

    if runNum != -1 and total != -1:
        plt.legend(loc='lower right')

    plt.xlabel('Time [min]')
    plt.ylabel(r'True Anomaly [deg]')
    plt.grid(True)

def plotDataEclipse(timeData, dataEclipse, figNum=-1, runNum=-1, total=-1):
    """
    Plot the eclipse shadow factor.
    """

    createFigure(figNum)
    color,label = getColorLabel(0, 3, r'$s_f$', runNum, total)
    plt.plot(timeData, dataEclipse, color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('Shadow Factor')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotDataSrpForce(timeData, dataForce, figNum=-1, runNum=-1, total=-1):
    """
    Plot the force and torque from solar radiation pressure.
    """

    createFigure(figNum)
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$F\_B_{' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataForce[:, axis], color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('SRP Force [N]')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotDataSrpTorque(timeData, dataTorque, figNum=-1, runNum=-1, total=-1):
    """
    Plot the solar radiation pressure torque.
    """

    createFigure(figNum)
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\tau\_B_{' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataTorque[:, axis], color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('SRP Torque [N-m]')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotDataSpice(timeData, dataSpice, bodyName, figNum=-1, runNum=-1, total=-1):
    """
    Plot ephemeris positions.
    """

    createFigure(figNum)
    for axis in range(0, 3):
        color, label = getColorLabel(axis, 3, r'$R_{' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataSpice[:, axis], color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel(str(bodyName) + ' Position [m]')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotStarTrackerQuaternion(timeData, dataQ, figNum=-1, runNum=-1, total=-1):
    """Plot the star tracker quaternion."""
    
    createFigure(figNum)
    for idx in range(0, 4):
        color,label = getColorLabel(idx, 4, r'$q_' + str(idx) + '$', runNum, total)
        plt.plot(timeData, dataQ[:, idx], color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel(r'Attitude $q_{C/N}$')
    plt.legend(loc='lower right')
    plt.grid(True)
    
def plotGyroAngularRate(timeData, dataOmegaGN, figNum=-1, runNum=-1, total=-1):
    """Plot the raw angular rate of the gyro."""
    
    createFigure(figNum)
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\omega_{GN,' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataOmegaGN[:, axis] * macros.R2D, color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('Gyro Angular Rate [deg/s] ')
    plt.legend(loc='lower right')
    plt.grid(True)
    
def plotAttitudeEstimate(timeData, dataSigma_BN_Est, figNum=-1, runNum=-1, total=-1):
    """Plot the attitude estimate."""
    createFigure(figNum)
    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\sigma_' + str(idx) + '$', runNum, total)
        plt.plot(timeData, dataSigma_BN_Est[:, idx], color=color,label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'Attitude Estimate $\sigma_{B/R}$')
    plt.grid(True)

def plotAngularEstimate(timeData, dataOmega_BN_Est, figNum=-1, runNum=-1, total=-1):
    """Plot the raw angular rate of the gyro."""
    
    createFigure(figNum)
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\omega_{GN,' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataOmega_BN_Est[:, axis] * macros.R2D, color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('Body Angular Rate  Estimate [deg/s] ')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotAttitudeKnowledgeError(timeData, dataSigma_BN_Est, dataSigmaBN, figNum=-1, runNum=-1, total=-1):
    """Plot the attitude estimate."""
    plt.rcParams.update({'font.size': 36})
    
    dataSigma_BN_Est = np.array(dataSigma_BN_Est)
    dataSigmaBN = np.array(dataSigmaBN)
    
    dataSigma_BN_Est = dataSigma_BN_Est[1:-1, :]
    dataSigmaBN = dataSigmaBN[0:-2, :]
    timeData = timeData[0:-2]
    
    dataSigma_BN_Est = dataSigma_BN_Est[0:-1:10, :]
    dataSigmaBN = dataSigmaBN[0:-1:10, :]
    timeData = timeData[0:-1:10]
    
    lengthOfData = len(timeData)
    
    dataSigmaErr = np.zeros((lengthOfData, 3))
    eulerParams = np.zeros([lengthOfData, 3])
    
    
    for jj in range(0, lengthOfData):
        dataSigmaErr[jj, :] = RigidBodyKinematics.subMRP(dataSigmaBN[jj, :], dataSigma_BN_Est[jj, :])
        eulerParams[jj, :] = RigidBodyKinematics.MRP2Euler321(dataSigmaErr[jj, :]) * macros.RAD2ARCSEC
    createFigure(figNum)
    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\sigma_' + str(idx) + '$', runNum, total)
        plt.plot(timeData, dataSigmaErr[:, idx], color=color,label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel(r'Attitude Knowledge Error $\sigma_{Err}$')
    plt.grid(True)
    
    
    plt.figure()
    plt.plot(timeData, eulerParams[:, 0])
    plt.xlabel('Time [min]')
    plt.ylabel('Cross Boresight Knowledge Error Axis 1 [arcseconds]')
    plt.grid(True)
    plt.ylim(-200, 200)
    
    plt.figure()
    plt.plot(timeData, eulerParams[:, 1])
    plt.xlabel('Time [min]')
    plt.ylabel('Cross Boresight Knowledge Error Axis 2 [arcseconds]')
    plt.grid(True)
    plt.ylim(-200, 200)
    
    plt.figure()
    plt.plot(timeData, eulerParams[:, 2])
    plt.xlabel('Time [min]')
    plt.ylabel('Around Boresight Knowledge Error [arcseconds]')
    plt.grid(True)
    plt.ylim(-200, 200)
    
    '''
    Uncomment these to print out 3 sigma knowledge error in arcseconds
    print(3 * np.std(eulerParams[int(lengthOfData/2):, 0]))
    print(3 * np.std(eulerParams[int(lengthOfData/2):, 1]))
    print(3 * np.std(eulerParams[int(lengthOfData/2):, 2]))
    
    print(3 * np.std(dataSigmaErr[int(lengthOfData/2):, 0]) * 4 * macros.RAD2ARCSEC)
    print(3 * np.std(dataSigmaErr[int(lengthOfData/2):, 1]) * 4 * macros.RAD2ARCSEC)
    print(3 * np.std(dataSigmaErr[int(lengthOfData/2):, 2]) * 4 * macros.RAD2ARCSEC)
    '''
    
def plotAngularEstimateError(timeData, dataOmega_BN_Est, dataOmega_BN, figNum=-1, runNum=-1, total=-1):
    """Plot the raw angular rate of the gyro."""
    
    
    dataOmega_BN_Est = np.array(dataOmega_BN_Est)
    dataOmega_BN = np.array(dataOmega_BN)
    
    dataOmega_BN_Est = dataOmega_BN_Est[1:-1, :]
    dataOmega_BN = dataOmega_BN[0:-2, :]
    timeData = timeData[0:-2]
    lengthOfData = len(timeData)
        
    
    dataOmega_BN_Est = dataOmega_BN_Est[0:lengthOfData:10]
    dataOmega_BN = dataOmega_BN[0:lengthOfData:10]
    timeData = timeData[0:lengthOfData:10]
    dataErr = dataOmega_BN - dataOmega_BN_Est
    
    np.set_printoptions(formatter={'float': lambda x: "{0:0.10f}".format(x)})
    
        
    createFigure(figNum)
    for axis in range(0, 3):
        color,label = getColorLabel(axis, 3, r'$\omega_{GN,' + str(axis) + '}$', runNum, total)
        plt.plot(timeData, dataErr[:, axis] * macros.R2D, color=color, label=label)
    plt.xlabel('Time [min]')
    plt.ylabel('Body Angular Rate  Estimate [deg/s] ')
    plt.legend(loc='lower right')
    plt.grid(True)

def plotModeControllerState(timeData, modeId, isSettled, figNum=-1, runNum=-1,
                            total=-1):
    """Plot the mode ID and settled flag."""
    createFigure(figNum)
    color,_ = getColorLabel(0, 2, '', runNum, total)
    plt.plot(timeData, modeId, color=color,label="Mode ID")
    color,_ = getColorLabel(1, 2, '', runNum, total)
    plt.plot(timeData, isSettled, color=color,label="Settled Flag")
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('Mode Controller States')
    plt.yticks([0, 1, 2, 3, 4, 5], ['0/Off', '1/Detumble', '2/Safe', '3/Sun-pointing', '4/Communications', '5/Science'])
    plt.grid(True)

def plotTorqueCommand(timeData, dataTauCmd, figNum=-1, runNum=-1, total=-1):
    """Plot the MRP feedback torque command."""
    createFigure(figNum)

    for idx in range(3):
        color,label = getColorLabel(idx, 3, r'$\tau_{,' + str(idx) + '}$', runNum, total)

        plt.plot(timeData, dataTauCmd[:, idx],
                 color=color,
                 label=label)
    plt.legend(loc='lower right')
    plt.xlabel('Time [min]')
    plt.ylabel('MRP Feedback Torque Command [Nm] ')
    plt.grid(True)