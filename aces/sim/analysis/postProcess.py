#
# ACES Post-Processing Module
#
# Purpose:  Containing file for post-processing functions.
# Author:   Brennan Gray
# Creation Date:  March 28, 2022
#
# Description:
#   Contains data post-processing (i.e. plotting,
#   printing, conversion to user-friendly format)
#   functions to analyze data from Monte Carlo
#   cohorts. See individual functions for descriptions
#   of their purpose and usage.
#
#   Post-processing user manual available at:
#   https://aces.atlassian.net/l/c/oWqGwVwh

import argparse
import glob
import os.path
import pickle
import sys
import gzip
import re
import pandas as pd
from Basilisk.utilities import macros

parser = argparse.ArgumentParser(description="Analysis tools module.")
parser.add_argument('analysis', type=str, help='Name of the analysis type to run.')
parser.add_argument('data', type=str,
                    help='Name of the folder within the MonteCarloData folder containing the run data.')
parser.add_argument('-r', '--runs', type=str, nargs='?', const="", default="",
                    help='Comma-separated string of run numbers to process. Will analyze all runs if left unspecified.')

from scenarioBaselinePlotting import *
from monteCarloPlotting import *

analysisListName = "analysisList"
detumbleName = "detumble"
pointingErrorName = "pointingError"
pointingKnowledgeName = "pointingDetermination"
jitterName = "jitter"

rwMotorMsgName = "rwMotorMsg"
attErrorMsgName = "attErrorMsg"
attRefMsgName = "attRefMsg"
magMsgName = "magMsg"
tamMsgName = "tamMsg"
tamCommMsgName = "tamCommMsg"
dipoleRequestMsgName = "dipoleRequestMsg"
atmosphereMsgName = "atmosphereMsg"
scStateMsgName = "scStateMsg"
rwStateMsgName = "rwStateMsg"
ggMsgName = "ggMsg"
rwModuleMsgNames = ["rw1Msg", "rw2Msg", "rw3Msg"]
srpMsgName = "srpMsg"
modeControllerMsgName = "modeControllerMsg"


def PrintRequirementsCompliance(dataDir, cases):
    """
    Prints compliance of Monte Carlo runs with requirements L3-ATC01, L3-ATC02, and L3-ATC05.

    Args:
        dataDir: The directory of the retained data to be processed.
        cases: List of case number integers to be processed.
    """

    #
    # Iterate over cases and process their data.
    for index in cases:

        #
        # Retrieve data for the current case.
        data = GetRetainedData(dataDir, index)

        #
        # Extract data from retained messages.
        detumble = data["custom"][analysisListName][detumbleName]  # Detumble data
        pointing_knowledge = data["custom"][analysisListName][pointingKnowledgeName]  # Pointing knowledge data
        pointing_error = data["custom"][analysisListName][pointingErrorName]  # Pointing error data
        jitter = data["custom"][analysisListName][jitterName]  # Jitter data

        #
        # Check if requirements were met, and display messages if they were not.

        # Detumble compliance.
        if not detumble[0] and not detumble[1]:  # Not detumble compliant, but within detumble angular rate limits
            print("Simulation", str(index), "failed to detumble, initial spacecraft angular momentum magnitude: ",
                  np.linalg.norm(detumble[3]), "kg*m^2/s")

        # Knowledge compliance.
        pointingKnowledgeStr = ""
        shouldPrintPointingKnowledge = False
        if not pointing_knowledge[0]:  # Not compliant for cross boresight
            pointingKnowledgeStr += " cross boresight (std " + str(pointing_knowledge[2] * macros.RAD2ARCSEC) + \
                                " arcsec)"
            shouldPrintPointingKnowledge = True

        if not pointing_knowledge[1]:  # Not compliant for around boresight
            pointingKnowledgeStr += " around boresight (std " + str(pointing_knowledge[3] * macros.RAD2ARCSEC) + \
                                " arcsec)"
            shouldPrintPointingKnowledge = True

        if shouldPrintPointingKnowledge:
            print("Simulation", str(index), "not pointing knowledge compliant for", pointingKnowledgeStr)

        # Pointing error compliance and message building.
        for i in range(len(pointing_error[0])):  # Loop over all modes
            if pointing_error[0][i] <= 1:  # Skip off, detumble modes
                continue

            pointingErrorStr = ""
            shouldPrintPointingError = False
            if not pointing_error[1][0][i]:  # Not compliant for cross boresight
                pointingErrorStr += " cross boresight (std " + str(pointing_error[2][0][i] * macros.RAD2ARCSEC) + \
                                    " arcsec)"
                shouldPrintPointingError = True

            if not pointing_error[1][1][i]:  # Not compliant for around boresight
                pointingErrorStr += " around boresight (std " + str(pointing_error[2][1][i] * macros.RAD2ARCSEC) + \
                                    " arcsec)"
                shouldPrintPointingError = True

            if shouldPrintPointingError:
                print("Simulation", str(index), ", mode", int(pointing_error[0][i][0]),
                      "not pointing error compliant for", pointingErrorStr)

        # Jitter compliance.
        for i in range(len(jitter[0])):  # Loop over all modes
            if pointing_error[0][i] <= 1:  # Skip off, detumble modes
                continue

            if not jitter[1][i] and jitter[2][i] > 0:  # Not compliant with jitter requirement due to actual jitter
                jitter_as = jitter[2][i] * macros.RAD2ARCSEC  # Convert to arcseconds
                print("Simulation", str(index), ", mode", int(jitter[0][i][0]),
                      "not jitter compliant, maximum value", jitter_as, "arcsec")
            elif not jitter[1][i]:  # Not compliant due to not enough data
                print("Simulation", str(index), ", mode", int(jitter[0][i][0]),
                      "not jitter compliant. Not enough data to compute jitter.")


def PlotPointingScatter(dataDir, cases):
    """
    Plots scatter plots of pointing error standard deviations, for analysis of requirement L3-ATC01.

    Args:
        dataDir: The directory of the retained data to be processed.
        cases: List of case number integers to be processed.
    """

    #
    # Iterate over cases and process their data.
    cross_boresight = [[], [], [], [], [], []]  # One list for each mode
    around_boresight = [[], [], [], [], [], []]  # One list for each mode
    for index in cases:

        #
        # Retrieve data for the current case.
        data = GetRetainedData(dataDir, index)

        #
        # Extract data from retained messages.
        pointing_error = data["custom"][analysisListName][pointingErrorName]  # Pointing error data

        for i in range(len(pointing_error[0])):  # Loop over modes
            if pointing_error[0][i] <= 1:  # Don't plot off, detumble modes
                continue

            cross_boresight[int(pointing_error[0][i])].append(pointing_error[2][0][i])
            around_boresight[int(pointing_error[0][i])].append(pointing_error[2][1][i])

    #
    # Convert to arcsec from radians.
    for i in range(len(cross_boresight)):  # Iterate over modes
        cross_boresight[i] = [val * macros.RAD2ARCSEC for val in cross_boresight[i]]
        around_boresight[i] = [val * macros.RAD2ARCSEC for val in around_boresight[i]]

    plotCrossAroundBoresightErrorStdScatter(cross_boresight, around_boresight, 1, plotLimits=True)

    #
    # Show and close plot after it has been created.
    plt.show()
    plt.close("all")

def PlotPointingKnowledgeScatter(dataDir, cases):
        """
        Plots scatter plots of pointing knowledge error standard deviations, for analysis of requirement L3-ATD02.

        Args:
            dataDir: The directory of the retained data to be processed.
            cases: List of case number integers to be processed.
        """

        #
        # Iterate over cases and process their data.
        cross_boresight = []
        around_boresight = []
        for index in cases:

            #
            # Retrieve data for the current case.
            data = GetRetainedData(dataDir, index)

            #
            # Extract data from retained messages.
            pointing_knowledge = data["custom"][analysisListName][pointingKnowledgeName]  # Pointing error data

            #
            # Append data to lists
            cross_boresight.append(pointing_knowledge[2])
            around_boresight.append(pointing_knowledge[3])

        #
        # Convert to arcsec from radians.
        cross_boresight = [val * macros.RAD2ARCSEC for val in cross_boresight]
        around_boresight = [val * macros.RAD2ARCSEC for val in around_boresight]

        plotCrossAroundBoresightKnowledgeStdScatter(cross_boresight, around_boresight, 1, plotLimits=True)

        #
        # Show and close plot after it has been created.
        plt.show()
        plt.close("all")


def PlotDetumbleScatter(dataDir, cases):
    """
    Plots scatter plot of detumble time vs. initial angular momentum, for analysis of requirement L3-ATC05.

    Args:
        dataDir: The directory of the retained data to be processed.
        cases: List of case number integers to be processed.
    """

    #
    # Iterate over cases and process their data.
    ang_momentum = []
    time = []
    for index in cases:

        #
        # Retrieve data for the current case.
        data = GetRetainedData(dataDir, index)

        #
        # Extract data from retained messages.
        detumble = data["custom"][analysisListName][detumbleName]  # Detumble data

        if not detumble[2] == -1:  # Only include points that detumbled properly
            ang_momentum.append(np.linalg.norm(detumble[3]))
            time.append(detumble[2])

    plotDetumbleScatter(time, ang_momentum, 1, plotLimits=True)

    #
    # Show and close plot after it has been created.
    plt.show()
    plt.close("all")


def PlotJitterHistogram(dataDir, cases):
    """
    Plots a histogram of all jitter values from the provided cases (for 60 minutes, this is about 2500 values per case).

    Args:
        dataDir: The directory of the retained data to be processed.
        cases: List of case number integers to be processed.
    """

    #
    # Iterate over cases and process their data.
    jitterValues = [[], [], [], [], [], []]  # One list for each mode
    for index in cases:

        #
        # Retrieve data for the current case.
        data = GetRetainedData(dataDir, index)

        #
        # Extract data from retained messages.
        jitter = data["custom"][analysisListName][jitterName]  # Jitter data

        for i in range(len(jitter[0])):  # Iterate over modes
            if jitter[0][i][0] <= 1:  # Skip off, detumble modes
                continue

            #
            # Append on the jitter values for this case.
            jitterValues[int(jitter[0][i][0])].extend(jitter[3][0][i])

    #
    # Convert to arcsec from radians.
    for i in range(len(jitterValues)):  # Iterate over modes
        jitterValues[i] = [val * macros.RAD2ARCSEC for val in jitterValues[i]]

    #
    # Plot the jitter values.
    plotJitterHist(jitterValues, 1)

    #
    # Show and close plot after it has been created.
    plt.show()
    plt.close("all")


def PlotAllData(dataDir, cases):
    """
    Plots the following data: attitude error, RW motor torques, attitude rate error, RW speeds, magnetic field,
        MTB torque commands, gravity gradient torque, atmospheric density, SRP force, and SRP torque.

        Note that the data required for these plots is only available after a scenario has been rerun, thus, if these
        plots are required, first rerun the desired case(s), then use this analysis.

    Args:
        dataDir: The directory of the retained data to be processed.
        cases: List of case number integers to be processed.
    """

    #
    # The total number of Monte Carlo runs in the cohort for plotting. This ensures colors are consistent.
    numRuns = len(glob.glob(dataDir + "/run*.data"))

    #
    # Iterate over cases and process their data.
    for index in cases:

        #
        # Retrieve data for the current case.
        data = GetRetainedData(dataDir, index)

        #
        # Extract data from retained messages.
        index = data["index"]
        try:
            dataUsReq = data["messages"][rwMotorMsgName + ".motorTorque"]
            dataSigmaRN = data["messages"][attRefMsgName + ".sigma_RN"]
            dataSigmaBN = data["messages"][scStateMsgName + ".sigma_BN"]
            dataOmegaBR = data["messages"][attErrorMsgName + ".omega_BR_B"]
            dataOmegaRW = data["messages"][rwStateMsgName + ".wheelSpeeds"]
            dataRW = []
            for name in rwModuleMsgNames:
                dataRW.append(data["messages"][name + ".u_current"][:, 1:])
            dataMagFieldTruth = data["messages"][magMsgName + ".magField_N"]
            dataTamMagField_S = data["messages"][tamMsgName + ".tam_S"]
            dataTamMagFieldComm_B = data["messages"][tamCommMsgName + ".tam_B"]
            dataMtbDipoleCmds_T = data["messages"][dipoleRequestMsgName + ".mtbDipoleCmds"]
            dataGgTorque_B = data["messages"][ggMsgName + ".gravityGradientTorque_B"]
            dataAtmospherericDensity = data["messages"][atmosphereMsgName + ".neutralDensity"]
            dataR_BN_N = data["messages"][scStateMsgName + ".r_BN_N"]
            dataV_BN_N = data["messages"][scStateMsgName + ".v_BN_N"]
            dataSrpForce_B = data["messages"][srpMsgName + ".solarRadiationPressureForce_B"]
            dataSrpTorque_B = data["messages"][srpMsgName + ".solarRadiationPressureTorque_B"]
            dataModeId = data["messages"][modeControllerMsgName + ".modeId"]
            dataModeSettled = data["messages"][modeControllerMsgName + ".isSettled"]
        except Exception as exception:  # Print message if data is unavailable
            print("Unable to retrieve data for case %i. Was the case rerun before calling this function?" % index)
            print("Exception was:", repr(exception))
            continue

        #
        # Plot the results.
        simTimes = dataSigmaBN[:, 0] * macros.NANO2MIN
        numRW = len(rwModuleMsgNames)
        numMTB = 2

        # Compute true sigmaBR
        dataSigmaBR = np.zeros((len(simTimes), 3))
        for i in range(len(simTimes)):
            dataSigmaBR[i, :] = RigidBodyKinematics.subMRP(dataSigmaBN[i, 1:], dataSigmaRN[i, 1:])

        # Need to remove column 0 from all these output messages because that column is time.
        if len(cases) > 1:
            plotAttitudeError(simTimes, dataSigmaBR, 1, index, numRuns)
            plotMrps(simTimes, dataSigmaBN[:, 1:], r'Attitude $\sigma_{B/N}$', 2, index, numRuns)
            plotMrps(simTimes, dataSigmaRN[:, 1:], r'Attitude $\sigma_{R/N}$', 3, index, numRuns)
            plotRwMotorTorque(simTimes, dataUsReq[:, 1:], dataRW, numRW, 4, index, numRuns)
            plotRateError(simTimes, dataOmegaBR[:, 1:], 5, index, numRuns)
            plotRwSpeeds(simTimes, dataOmegaRW[:, 1:], numRW, 6, index, numRuns)
            plotDataTamComm(simTimes, dataTamMagFieldComm_B[:, 1:], 7, index, numRuns)
            plotDataMtbMomentumManagement(simTimes, dataMtbDipoleCmds_T[:, 1:], numMTB, 8, index, numRuns)
            plotDataGgTorque(simTimes, dataGgTorque_B[:, 1:], 9, index, numRuns)
            plotDataAtmosphericDensity(simTimes, dataAtmospherericDensity[:, 1:], 10, index, numRuns)
            plotDataSrpForce(simTimes, dataSrpForce_B[:, 1:], 11, index, numRuns)
            plotDataSrpTorque(simTimes, dataSrpTorque_B[:, 1:], 12, index, numRuns)
            plotModeControllerState(simTimes, dataModeId[:, 1:], dataModeSettled[:, 1:], 13, index, numRuns)
        else:
            plotAttitudeError(simTimes, dataSigmaBR, 1)
            plotMrps(simTimes, dataSigmaBN[:, 1:], r'Attitude $\sigma_{B/N}$', 2)
            plotMrps(simTimes, dataSigmaRN[:, 1:], r'Attitude $\sigma_{R/N}$', 3)
            plotRwMotorTorque(simTimes, dataUsReq[:, 1:], dataRW, numRW, 4)
            plotRateError(simTimes, dataOmegaBR[:, 1:], 5)
            plotRwSpeeds(simTimes, dataOmegaRW[:, 1:], numRW, 6)
            plotDataTamComm(simTimes, dataTamMagFieldComm_B[:, 1:], 7)
            plotDataMtbMomentumManagement(simTimes, dataMtbDipoleCmds_T[:, 1:], numMTB, 8)
            plotDataGgTorque(simTimes, dataGgTorque_B[:, 1:], 9)
            plotDataAtmosphericDensity(simTimes, dataAtmospherericDensity[:, 1:], 10)
            plotDataSrpForce(simTimes, dataSrpForce_B[:, 1:], 11)
            plotDataSrpTorque(simTimes, dataSrpTorque_B[:, 1:], 12)
            plotModeControllerState(simTimes, dataModeId[:, 1:], dataModeSettled[:, 1:], 13)

    #
    # Show and close plot after it has been created.
    plt.show()
    plt.close("all")


def ParseCases(dataDir, caseStr):
    """
    Parses the given case string into a list of ints.

    Args:
        dataDir: Directory of the data being processed.
        caseStr: Comma separated case string or empty string.

    Returns:
        A list of the cases to be analyzed.
    """

    if len(caseStr) == 0:  # Empty string, analyze all cases in directory
        runs = glob.glob(dataDir + "/run*.data")
        indices = []
        for run in runs:
            capture = re.search('run(\d+).data', run)
            indices.append(capture.group(1))
        return indices
    else:  # Non-empty string, split by commas
        runStrs = caseStr.split(',')
        return [int(num) for num in runStrs]


def GetRetainedData(dataDir, case):
    """
    Get the data that was retained for the given run.

    Args:
        dataDir: Directory of the data to retrieve.
        case: The desired case to retrieve data from, int.

    Returns:
        The retained data dictionary for the given case.
    """

    #
    # Construct data path.
    dataPath = dataDir + "/run" + str(case) + ".data"

    with gzip.open(dataPath) as pickledData:
        data = pickle.load(pickledData)
        return data


def run():

    #
    # Retrieve command-line arguments.
    args = parser.parse_args()

    #
    # Process data directory and cases arguments.
    dataDir = os.path.join(os.path.dirname(sys.path[0]), 'scenarios', 'MonteCarloData', args.data)
    cases = ParseCases(dataDir, args.runs)

    #
    # Parse requested analysis and run it.
    availableMethods = globals().copy()
    method = availableMethods.get(args.analysis)
    if not method:
        print("No available analysis named " + args.analysis)
        return

    method(dataDir, cases)


if __name__ == "__main__":
    run()